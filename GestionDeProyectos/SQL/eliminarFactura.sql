use gestiondeproyectos;

DROP PROCEDURE if exists eliminarFactura;

DELIMITER $$
 CREATE PROCEDURE eliminarFactura(
	_n_factura integer
 )
BEGIN
	
    declare _id_factura bigint(20);
    
    select id_factura into _id_factura
    from factura
    where(    
		numero_factura = _n_factura
    );
    
    delete from factura_hito
    where(
		fk_id_factura = _id_factura
    );
    
    delete from factura
    where(
		id_factura = _id_factura
    );

	DELETE 
FROM factura 
   WHERE id_factura = _id_factura;
END$$
 DELIMITER ;
