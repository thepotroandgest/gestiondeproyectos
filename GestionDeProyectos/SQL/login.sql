use gestiondeproyectos;

drop table login;

CREATE TABLE login(
   seq_login INT NOT NULL AUTO_INCREMENT
  ,user_name VARCHAR(20) NOT NULL
  ,txt_pass VARCHAR(40) NOT NULL
  ,PRIMARY KEY ( seq_login )
  ,UNIQUE INDEX txtUserName_UNIQUE(user_name ASC)
);
