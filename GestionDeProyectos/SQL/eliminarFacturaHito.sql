use gestiondeproyectos;

 DROP PROCEDURE if exists eliminarFacturaHito;
 
DELIMITER $$
CREATE PROCEDURE eliminarFacturaHito(
	
     _numero_factura int
    ,_id_proyecto varchar(100)
    ,_nombre_hito varchar(150)
	 
)
BEGIN

	declare _id_factura bigint;
	declare _sec_hito bigint(20); 
    declare _valor_uf double;

	select 	 sec_hito
			,valor_uf  
	into 	 _sec_hito
			,_valor_uf
	from hito_comercial
	where ( 
				fk_id_proyecto = _id_proyecto
			and nombre_hito = _nombre_hito );
	
    select id_factura into  _id_factura
    from factura
    where (  numero_factura  = _numero_factura );
   
	DELETE 
	FROM factura_hito 
	WHERE (
				fk_sec_hito = _sec_hito
            and fk_id_factura = _id_factura
		);
        
    update factura 
    set monto =( monto - _valor_uf)
    where ( id_factura = _id_factura  );
    
END$$
DELIMITER ;




