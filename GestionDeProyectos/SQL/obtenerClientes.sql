
use gestiondeproyectos;

drop procedure if exists obtenerClientes; 

DELIMITER $$
CREATE PROCEDURE obtenerClientes
( 
	_rut_cliente VARCHAR(20) 
)
BEGIN
	SELECT 
		 rut_cliente 
		,nombre 
		,fono 
		,correo 
		,direccion 
    FROM cliente
    where
    (
			rut_cliente = _rut_cliente
        or	_rut_cliente is null
    );
    
END$$
DELIMITER ;
