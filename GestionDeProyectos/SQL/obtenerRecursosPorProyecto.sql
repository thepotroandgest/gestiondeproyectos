use gestiondeproyectos;



DROP procedure IF exists obtenerRecursosPorProyecto;

DELIMITER $$
CREATE PROCEDURE obtenerRecursosPorProyecto( 

	 _id_proyecto varchar(100) 
    ,_id_recurso varchar( 10 )

)
BEGIN

	select  distinct 
			rec.id_recurso
			,rec.rut_recurso
            ,rec.nombre
			,crg.fk_id_proyecto
			,pry.fecha_inicio
			,pry.fecha_termino
	from recurso as rec
	right join carga_tracking as crg on rec.rut_recurso = crg.fk_rut_recurso
	left join proyecto as pry on crg.fk_id_proyecto = pry.id_proyecto
	where (

			( crg.fk_id_proyecto = _id_proyecto or _id_proyecto is null )
		and ( rec.id_recurso = _id_recurso or _id_recurso is null )
        
	);

END$$
DELIMITER ;

