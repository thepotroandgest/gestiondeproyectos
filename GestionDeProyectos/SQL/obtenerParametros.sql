use gestiondeproyectos;

drop procedure if exists obtenerParametros; 


DELIMITER $$
CREATE PROCEDURE obtenerParametros( 
	_id int
)
BEGIN

	select 	 id 
			,nombre 
			,descripcion 
			,valor
	from parametros
    where ( id = _id or _id is null );

END$$
DELIMITER ;



