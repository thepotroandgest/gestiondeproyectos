use gestiondeproyectos;

DROP procedure IF exists setearEsfuerzoReal;

DELIMITER $$
CREATE  PROCEDURE setearEsfuerzoReal( )
BEGIN

	declare	sumaHoras double;
    declare	max_id int;
    declare _id_proyecto varchar(100);
    declare _filas int;
    declare _contador int;
    declare _recurso varchar( 50 );
    declare _rut_recurso varchar( 10 );
    
    select sec_traspaso into _contador
    from traspasoesfuerzoreal
    limit 1;
        
    select count(*) into _filas
    from traspasoesfuerzoreal; 
    
    select proyecto into _id_proyecto
    from traspasoesfuerzoreal
    limit 1;
    
    set _filas = _filas + _contador;
    
    while _contador <= _filas do
    
		select usuario into _recurso
        from traspasoesfuerzoreal
        where sec_traspaso = _contador;
        
		select rut_recurso into _rut_recurso
        from recurso
        where nombre = _recurso;
        
		insert into carga_tracking
		(
			 fk_id_proyecto
			,fk_rut_recurso)
		values
		(
			 _id_proyecto
			,_rut_recurso );
        
        set _contador = _contador + 1;
        
    end while;
    
	SELECT SUM(horas) INTO sumaHoras 
    FROM traspasoesfuerzoreal;
		
	SELECT MAX(id) INTO max_id 
    FROM avance
	WHERE
	(
		fk_id_proyecto = _id_proyecto
	);
		
	UPDATE avance 
	SET esfuerzo_real = sumaHoras 
	WHERE
    (
		fk_id_proyecto = _id_proyecto
        AND id = max_id
	);
    
    delete from traspasoesfuerzoreal;
    
        
END$$
DELIMITER ;
