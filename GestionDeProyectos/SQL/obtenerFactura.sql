use gestiondeproyectos;

DROP PROCEDURE obtenerFacturas;

DELIMITER $$
CREATE PROCEDURE obtenerFacturas(
	 _fk_id_proyecto VARCHAR(100)
     ,_numero_factura int
)
BEGIN

SELECT * from
(

	SELECT 
         ft.id_factura 
		,ft.numero_factura
        ,hc.fk_id_proyecto
		,ft.fecha_emision
		,ft.monto
		,ft.estado
        ,ft.glosa
	from factura_hito as fh
	inner join hito_comercial as hc
		on fh.fk_sec_hito = hc.sec_hito  
	inner join factura as ft
		on fh.fk_id_factura = ft.id_factura

    UNION
    
	SELECT 
         ft.id_factura 
		,ft.numero_factura
        ,null as fk_id_proyecto
		,ft.fecha_emision
		,ft.monto
		,ft.estado
        ,ft.glosa 
	FROM factura as ft
    LEFT join factura_hito as fh
		on ft.id_factura =  fh.fk_id_factura
	where( fh.fk_id_factura is null   )

) as FACTURAS
    
where 
(
		( FACTURAS.fk_id_proyecto = _fk_id_proyecto or _fk_id_proyecto is null)
	and ( FACTURAS.numero_factura = _numero_factura or _numero_factura is null)
)
group by FACTURAS.numero_factura;
    
END$$
DELIMITER ;
