use gestiondeproyectos;


CREATE TABLE sesion (
   seq_sesion INT NOT NULL AUTO_INCREMENT
  ,token VARCHAR(150) NOT NULL
  ,token_time DATETIME not NULL
  ,login INT NOT NULL
  ,PRIMARY KEY ( seq_sesion )
  ,UNIQUE INDEX txtToken_UNIQUE ( token ASC )
  ,UNIQUE INDEX login_UNIQUE (login ASC)
  ,CONSTRAINT login
    FOREIGN KEY (login)
    REFERENCES login (seq_login)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);
