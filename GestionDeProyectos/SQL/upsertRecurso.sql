use gestiondeproyectos;


DROP procedure IF exists upsertRecurso;

DELIMITER $$
CREATE  PROCEDURE upsertRecurso
(
	rut_recurso VARCHAR(20)
    ,id_recurso VARCHAR(10)
    ,nombre VARCHAR(50)
	,out codError int
	,out mensaje  varchar( 50 )
    
)
BEGIN

	set codError = -1;
	set mensaje = 'Falló ingreso recurso';

	IF EXISTS (
				SELECT 1 
				FROM recurso 
				WHERE recurso.rut_recurso=rut_recurso
            ) 
	THEN
		UPDATE recurso SET 
			recurso.rut_recurso = rut_recurso
            ,recurso.id_recurso = id_recurso
            ,recurso.nombre = nombre
		WHERE recurso.rut_recurso = rut_recurso;
	ELSE
		INSERT INTO recurso
			(
				 rut_recurso
                ,id_recurso
                ,nombre
			)
		VALUES
			(
				rut_recurso
                ,id_recurso
                ,nombre
			);
	END IF;
    
	set codError = 0;
	set mensaje = 'Ingreso recurso exitoso';
    
END$$
DELIMITER ;
