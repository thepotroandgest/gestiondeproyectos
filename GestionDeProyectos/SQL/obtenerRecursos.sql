use gestiondeproyecto;

-- DROP PROCEDURE obtenerRecursos;

DELIMITER $$
CREATE PROCEDURE obtenerRecursos
(
	_rut_recurso varchar ( 20 ) 
)
BEGIN
	SELECT
     rut_recurso
    ,id_recurso
    ,nombre
    FROM recurso
    where
    (
		rut_recurso = _rut_recurso or _rut_recurso is null
    );
    
END$$
DELIMITER ;