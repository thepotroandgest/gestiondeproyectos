
use gestiondeproyectos;
DROP procedure IF exists eliminarTarea;

DELIMITER $$
CREATE PROCEDURE eliminarTarea
(
	in _id_traspaso integer
    ,in _id_proyecto varchar (100)
)
BEGIN
	DELETE FROM tarea  
    where 
    (		
			id_traspaso =_id_traspaso
		AND fk_id_proyecto = _id_proyecto
    );
    
END $$
DELIMITER ;

