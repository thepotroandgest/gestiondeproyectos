use gestiondeproyectos;

-- drop table factura;


CREATE TABLE factura
(
	 id_factura bigint NOT NULL  AUTO_INCREMENT
    ,numero_factura int
    ,fecha_emision date NOT NULL
    ,estado varchar(10) NOT NULL
    ,glosa varchar( 300 )
    ,monto double NOT NULL
    ,PRIMARY KEY ( id_factura )
    
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
