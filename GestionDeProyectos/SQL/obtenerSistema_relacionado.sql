use gestiondeproyectos;

DROP procedure IF exists obtenerSistema_relacionado;

DELIMITER $$
CREATE  PROCEDURE obtenerSistema_relacionado
(
	_id_sistema_relacionado varchar(50)
)
BEGIN
	SELECT 
		 id_sistema_relacionado 
		,nombre
    FROM sistema_relacionado
    where
    (
			id_sistema_relacionado = _id_sistema_relacionado 
        or	_id_sistema_relacionado is null 
    );
END$$
DELIMITER ;
