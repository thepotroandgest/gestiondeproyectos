use gestiondeproyectos;

DROP procedure IF exists upsertProyecto;

DELIMITER $$
CREATE  PROCEDURE upsertProyecto
(
    
     _id_proyecto varchar(100) 
	,_nombre varchar(50) 
	,_tipo varchar(30) 
	,_esfuerzo_vendido DOUBLE 
	,_fecha_inicio date 
	,_fecha_termino date 
    ,_descripcion varchar( 200 )
    ,_valor_vendido double
	,_fk_id_contraparte varchar(50) 
	,_fk_id_sistema_relacionado varchar(50) 
	,_fk_rut_recurso varchar(20) 
    ,out codError int
	,out mensaje  varchar( 50 )
    
)
BEGIN

	set codError = -1;
	set mensaje = 'Falló ingreso cliente';

	IF EXISTS 
	(
		SELECT 1 
		FROM proyecto 
		WHERE  id_proyecto = _id_proyecto
	) 
    THEN
		UPDATE proyecto SET 
			 nombre = _nombre 
			,tipo = _tipo
			,esfuerzo_vendido = _esfuerzo_vendido
			,fecha_inicio = _fecha_inicio
			,fecha_termino = _fecha_termino
			,descripcion = _descripcion
			,valor_vendido = _valor_vendido
			,fk_id_contraparte = _fk_id_contraparte
			,fk_id_sistema_relacionado = _fk_id_sistema_relacionado
			,fk_rut_recurso = _fk_rut_recurso

			
		WHERE id_proyecto = _id_proyecto;
	ELSE
		INSERT INTO proyecto
		(
			 id_proyecto
            ,nombre
			,tipo
			,esfuerzo_vendido
			,fecha_inicio
			,fecha_termino
			,descripcion
			,valor_vendido
			,fk_id_contraparte
			,fk_id_sistema_relacionado
			,fk_rut_recurso
		)
		VALUES
		(
			 _id_proyecto
            ,_nombre
			,_tipo
			,_esfuerzo_vendido
			,_fecha_inicio
			,_fecha_termino
			,_descripcion
			,_valor_vendido
			,_fk_id_contraparte
			,_fk_id_sistema_relacionado
			,_fk_rut_recurso
        
        );
	END IF;
    
    set codError = 0;
	set mensaje = 'Ingreso cliente exitoso';
    
    
END$$
DELIMITER ;
