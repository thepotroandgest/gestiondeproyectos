
use gestiondeproyectos;


-- drop table cliente;

CREATE TABLE cliente
(
   rut_cliente varchar(20) NOT NULL
  ,nombre varchar(50) NOT NULL
  ,fono varchar( 15 )
  ,correo varchar( 40 )
  ,direccion varchar( 50 )
  ,PRIMARY KEY ( rut_cliente )
);
