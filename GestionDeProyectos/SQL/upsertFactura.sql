use gestiondeproyectos;

 DROP procedure IF exists upsertFactura;

DELIMITER $$
CREATE  PROCEDURE upsertFactura
(
	 _n_factura INTEGER
    ,_fecha_emision DATE
    ,_estado VARCHAR(10)
    ,_glosa VARCHAR(300)
    ,_monto_uf DOUBLE
	,out codError int
	,out mensaje  varchar( 50 )
)
BEGIN

	set codError = -1;
	set mensaje = 'Falló ingreso factura';

	IF EXISTS 
    (
		SELECT 1 
        FROM factura 
        WHERE numero_factura = _n_factura) 
    THEN
		UPDATE factura SET 
             numero_factura = _n_factura
            ,fecha_emision = _fecha_emision
            ,estado = _estado
            ,glosa = _glosa
            ,monto = _monto_uf
		WHERE numero_factura = _n_factura;
	ELSE
    
		INSERT INTO factura
		(
			 numero_factura
            ,fecha_emision
            ,estado
            ,glosa
            ,monto)
		VALUES(
			 _n_factura
            ,_fecha_emision
            ,_estado
            ,_glosa
            ,_monto_uf
		);
        
	END IF;
    
	set codError = 0;
	set mensaje = 'Ingreso factura exitosa';
    
END$$
DELIMITER ;
















