use gestiondeproyectos;

drop table if exists definicion_hitos;

CREATE TABLE definicion_hitos(
	 id int NOT NULL 
    ,nombre varchar(30) not null
	,PRIMARY KEY ( id )
);