
use gestiondeproyectos;

-- drop table sistema_relacionado;

CREATE TABLE sistema_relacionado
(
   id_sistema_relacionado varchar(50) NOT NULL
  ,nombre varchar(30) NOT NULL
  ,PRIMARY KEY ( id_sistema_relacionado )
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
