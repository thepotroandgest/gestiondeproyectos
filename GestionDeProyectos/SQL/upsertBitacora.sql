use gestiondeproyectos;


DROP procedure IF exists upsertBitacora;

DELIMITER $$
CREATE  PROCEDURE upsertBitacora(
	 _tipo varchar(20) 
	,_asunto varchar(30) 
	,_descripcion varchar(300) 
	,_rut_responsable varchar(20) 
	,_fecha_ingreso date
	,_fecha_vencimiento date
    ,_link varchar(200)
	,_fk_id_proyecto VARCHAR(100) 
    ,out codError int
	,out mensaje  varchar( 50 )
)
BEGIN

	set codError = -1;
	set mensaje = 'Falló ingreso bitacora';

	IF EXISTS (
				SELECT 1 
				FROM bitacora  
				WHERE( 
						tipo = _tipo
                    and fk_id_proyecto = _fk_id_proyecto 
				)
	) 
	THEN
		UPDATE bitacora SET 
			 tipo = _tipo
			,asunto = _asunto
			,descripcion = _descripcion
			,rut_responsable = _rut_responsable
			,fecha_ingreso = _fecha_ingreso
			,fecha_vencimiento = _fecha_vencimiento
            ,link = _link
			,fk_id_proyecto = _fk_id_proyecto		
		WHERE( 
				tipo = _tipo
			and fk_id_proyecto = _fk_id_proyecto 
		);	
    ELSE
		INSERT INTO bitacora(
				 tipo
				,asunto
				,descripcion
				,rut_responsable
				,fecha_ingreso
				,fecha_vencimiento
                ,link
				,fk_id_proyecto
			)
		VALUES(
				 _tipo
				,_asunto
				,_descripcion
				,_rut_responsable
				,_fecha_ingreso
				,_fecha_vencimiento
                ,_link
				,_fk_id_proyecto
			);
	END IF;
    
	set codError = 0;
	set mensaje = 'Ingreso recurso exitoso';
    
END$$
DELIMITER ;





