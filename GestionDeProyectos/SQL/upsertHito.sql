use gestiondeproyectos;

DROP procedure IF exists upsertHito;

DELIMITER $$
CREATE  PROCEDURE upsertHito
(
	 
   _nombre_hito varchar(150) 
  ,_porcentaje double 
  ,_valor_uf double 
  ,_fk_id_proyecto varchar(100) 
	,out codError int
	,out mensaje  varchar( 50 )
)
BEGIN

	set codError = -1;
	set mensaje = 'Falló ingreso hito';

	if exists
	(
		select 1 
		from hito_comercial 
		where 
		(
				nombre_hito =  _nombre_hito 
			and	fk_id_proyecto = _fk_id_proyecto 
		)

	)then
		update hito_comercial
		set 
			 nombre_hito = _nombre_hito
			,porcentaje = _porcentaje
			,valor_uf = _valor_uf
			,fk_id_proyecto = _fk_id_proyecto
		where 
		(
				nombre_hito =  _nombre_hito 
			and	fk_id_proyecto = _fk_id_proyecto 
		);
	else

		INSERT INTO hito_comercial
		(
			  nombre_hito
			 ,porcentaje
			 ,valor_uf
			 ,fk_id_proyecto
		)
		VALUES
		(
			  _nombre_hito
			 ,_porcentaje
			 ,_valor_uf
			 ,_fk_id_proyecto
		);
	end if;

	set codError = 0;
	set mensaje = 'Ingreso hito exitoso';
    
END$$
DELIMITER ;