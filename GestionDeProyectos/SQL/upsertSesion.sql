use gestiondeproyectos;

DROP procedure IF EXISTS upsertSesion;

DELIMITER $$
CREATE  PROCEDURE upsertSesion(
	 in _token varchar(150)
    ,in _login int
    ,out _result int
    ,out _message varchar(50)
)
BEGIN

    declare toke_life int;
    declare token_temp varchar(150);
    declare now_time datetime;
    declare token_time_temp datetime;
    
    set token_temp = null;
    set token_time_temp = null;
    set now_time = now();
    set toke_life = 1800; -- tiempo de vida del token en segundos (1/2 hora)
    
    select 
		 token
		,token_time 
	into 
		 token_temp
        ,token_time_temp 
    from sesion 
    where( 
		token = _token and login = _login
	);
        
    if token_temp is null and token_time_temp is null  then
        
        insert into session ( 
			 token 
			,token_time
            ,login
        )
        values(
			 _token 
			,now_time
            ,_login 
        );
        
		set _result = 0;
        set _message = 'token nuevo ingresado';
        
    else 
		if timestampdiff( second, token_time_temp, now_time ) <= toke_life then
		update session set 
			token_time = now_time
		where(   token = _token and login = _login );
			
		set _result = 1;
		set _message = 'token actualizado';
	else
		set _result = 2;
		set _message = 'token no valido';
		end if;

    end if;
    
END;$$
DELIMITER ;