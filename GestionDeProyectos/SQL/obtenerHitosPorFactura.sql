use gestiondeproyectos;

DROP PROCEDURE IF exists obtenerHitosPorFactura;

DELIMITER $$
CREATE PROCEDURE obtenerHitosPorFactura(
	   _numero_factura int
      ,_id_proyecto varchar( 100 )
      ,_contraparte varchar(20)
)
BEGIN

	SELECT *
	FROM(

		SELECT
			 hc.sec_hito
			,hc.fk_id_proyecto
			,pr.estado as estado_proyecto
			,hc.nombre_hito
			,hc.porcentaje
			,hc.valor_uf
			,fh.abono
			,round( hc.valor_uf - fh.abono, 1 ) as saldo
			,case  
				when round( hc.valor_uf - fh.abono, 1 ) = 0 then 'facturado'
				when ( 		round( hc.valor_uf - fh.abono, 1 ) > 0 
						and round( hc.valor_uf - fh.abono, 1 ) <  hc.valor_uf ) then 'abonado'
				when round( hc.valor_uf - fh.abono, 1 ) =  hc.valor_uf then 'pendiente'
			 end as estado_hito 
			,ft.fecha_emision
			,ft.numero_factura
			,ft.estado as estado_factura
			,pr.fk_id_contraparte
		from factura_hito as fh
		inner join hito_comercial as hc
			on fh.fk_sec_hito = hc.sec_hito  
		inner join factura as ft
			on fh.fk_id_factura = ft.id_factura
		inner join proyecto as pr
			on hc.fk_id_proyecto = pr.id_proyecto
			
		UNION
		
		SELECT
			 hc.sec_hito
			,hc.fk_id_proyecto
			,pr.estado as estado_proyecto
			,hc.nombre_hito
			,hc.porcentaje
			,hc.valor_uf
			,0 as abono
			,hc.valor_uf  as saldo
			,'pendiente' as estado_hito 
			,null as fecha_emision
			,null as numero_factura
			,null as estado_factura
			,pr.fk_id_contraparte
		from factura_hito as fh
		right join hito_comercial as hc
			on fh.fk_sec_hito = hc.sec_hito  
		inner join proyecto as pr
			on hc.fk_id_proyecto = pr.id_proyecto
		where
		(  
			fh.fk_sec_hito is null
		)

	) as HITOS
	where( 		
				( HITOS.numero_factura = _numero_factura or _numero_factura is null )  
			and ( HITOS.fk_id_proyecto = _id_proyecto or _id_proyecto is null )
			and ( HITOS.fk_id_contraparte = _contraparte or _contraparte is null )
				
		)
	order by HITOS.fk_id_proyecto ; 

END$$
DELIMITER ;





