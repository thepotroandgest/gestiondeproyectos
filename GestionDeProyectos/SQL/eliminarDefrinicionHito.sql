use gestiondeproyectos;

DROP PROCEDURE if exists eliminarDefrinicionHito;
 
DELIMITER $$
CREATE PROCEDURE eliminarDefrinicionHito( _id int )
BEGIN

	DELETE FROM 
    definicion_hitos 
    WHERE  ( id = _id );

END$$
DELIMITER ;
