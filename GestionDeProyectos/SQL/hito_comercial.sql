use gestiondeproyectos;

-- drop table hito_comercial;

CREATE TABLE hito_comercial
(
   sec_hito bigint(20) NOT NULL AUTO_INCREMENT
  ,nombre_hito varchar(150) NOT NULL
  ,porcentaje double NOT NULL
  ,valor_uf double NOT NULL
  ,fk_id_proyecto varchar(100) NOT NULL
  ,PRIMARY KEY ( sec_hito )
  ,KEY fk_id_proyecto ( fk_id_proyecto )
  ,CONSTRAINT hito_comercial_ibfk_2 FOREIGN KEY (fk_id_proyecto) REFERENCES proyecto (id_proyecto)

) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
