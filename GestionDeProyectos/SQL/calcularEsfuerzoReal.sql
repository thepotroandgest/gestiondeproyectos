use gestiondeproyectos;

DROP procedure IF exists calcularEsfuerzoReal;

DELIMITER $$
CREATE PROCEDURE calcularEsfuerzoReal
( 
	 in _fk_id_proyecto varchar(100)
    ,in _id_traspaso int
    ,out _result float
)
BEGIN
    
    select 	  sum( hora )  into _result
	from 		esfuerzo_real e
	left join 	tarea t
	on (
				e.fk_id_traspaso = t.id_traspaso
			and e.fk_fk_id_proyecto = t.fk_id_proyecto
		)
	where (
					t.fk_id_proyecto = _fk_id_proyecto
				and	t.id_traspaso = _id_traspaso
		   )
	group by fk_id_traspaso
			,fk_fk_id_proyecto;
            
	select round( (_result / 24) , 2) into _result;
        
END$$
DELIMITER ;
