use gestiondeproyectos;

 DROP procedure IF exists upsertFacturaHito;

DELIMITER $$
CREATE  PROCEDURE upsertFacturaHito
(
	 _n_factura INTEGER
    ,_id_proyecto varchar(100)
    ,_nombre_hito varchar(150)
    ,_abono double
	,out codError int
	,out mensaje  varchar( 50 )
)
BEGIN

	declare _fk_id_factura bigint;
    declare _fk_sec_hito bigint(20);

	set codError = -1;
	set mensaje = 'Falló ingreso factura_hito';

	select id_factura into _fk_id_factura
	from factura 
	where( numero_factura = _n_factura );
    
	select sec_hito into _fk_sec_hito
	from hito_comercial
	where( 
			fk_id_proyecto = _id_proyecto
		and	nombre_hito = _nombre_hito
	);
    
	if exists(
    
		select 1 
        from factura_hito
        where
        (
			fk_sec_hito  = _fk_sec_hito
			and fk_id_factura = _fk_id_factura )
    
    )then
		update factura_hito
        set abono = _abono
        where 
        (
			fk_sec_hito  = _fk_sec_hito
			and fk_id_factura = _fk_id_factura );
    else
		INSERT INTO factura_hito
		(
			 fk_sec_hito
			,fk_id_factura 
			,abono)
		VALUES
		(
			 _fk_sec_hito
			,_fk_id_factura
			,_abono);
				
		set codError = 0;
		set mensaje = 'Ingreso factura_hito exitosa';
    end if;
END$$
DELIMITER ;




