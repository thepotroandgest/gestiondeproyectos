use gestiondeproyectos;

DROP PROCEDURE eliminarHito;

DELIMITER $$
CREATE PROCEDURE eliminarHito
(
	 _nombre_hito varchar(150) 
    ,_fk_id_proyecto varchar(100)
 )
BEGIN

	DELETE 
	FROM hito_comercial 
	WHERE 
	(
   
			nombre_hito = _nombre_hito
		and fk_id_proyecto = _fk_id_proyecto
	);
    
END$$
DELIMITER ;
