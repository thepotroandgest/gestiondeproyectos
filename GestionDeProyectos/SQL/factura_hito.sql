use gestiondeproyectoS;


-- drop table factura_hito;


CREATE TABLE factura_hito
(
	 fk_sec_hito bigint(20) NOT NULL 
	,fk_id_factura bigint NOT NULL 
    ,abono double NOT NULL
    ,primary key(fk_sec_hito,fk_id_factura  )
    ,FOREIGN KEY ( fk_sec_hito ) REFERENCES hito_comercial( sec_hito )
    ,FOREIGN KEY ( fk_id_factura ) REFERENCES factura( id_factura )

) 

