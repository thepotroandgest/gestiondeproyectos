use gestiondeproyectos;

DROP procedure IF exists listarTareas;

DELIMITER $$
CREATE PROCEDURE listarTareas
( 
	in _fk_id_proyecto varchar(100)
)
BEGIN
    
    select   
			  t.sec_tarea
			, t.id_traspaso
			, t.descripcion
			, t.esfuerzo_estimado
			, t.esfuerzo_real
			, t.fecha_inicio_estimado
			, t.fecha_inicio_real
			, t.fecha_termino_estimado
			, t.fecha_termino_real
			, t.porcentaje_estimado
			, t.porcentaje_real
			, t.fk_id_proyecto
			, t.fk_rut_recurso
            , r.id_recurso
			
    from  tarea t
		 ,recurso r
	where (
				t.fk_rut_recurso = r.rut_recurso
            and 
            (	
					t.fk_id_proyecto = _fk_id_proyecto
				or 	_fk_id_proyecto is null
            )
		);
    
     
   
END$$
DELIMITER ;



