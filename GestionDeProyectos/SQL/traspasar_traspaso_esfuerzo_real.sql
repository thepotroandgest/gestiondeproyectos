use gestiondeproyectos;


DROP procedure IF exists traspasar_traspaso_esfuerzo_real;

DELIMITER $$
CREATE PROCEDURE traspasar_traspaso_esfuerzo_real
(
	  out codError int
	 ,out mensaje  varchar(50)
)
BEGIN
	 
	declare _cliente varchar(100);
	declare _proyecto varchar(50);
	declare _usuario varchar(50);
	declare _tarea varchar(50);
	declare _fechaInicio datetime;
	declare _fechaFin datetime;
	declare _horas float;
	declare _id bigint(20);
    declare _contador int;
    declare _id_traspaso int;
    declare _filasTraspaso int;
    declare _usuario_flag varchar(50);
    
    -- conteo de filas en traspasoesfuerzoreal
    select count( * ) 
    into _filasTraspaso 
	from traspasoesfuerzoreal;

	-- se busca en minimo id, se usara mas adelant 	
	select min(sec_traspaso) into _id
    from traspasoesfuerzoreal
    limit 1;

	-- seleccionamos el primer proyecto de la tabla
    set @proyecto = (
						select proyecto 
                        from traspasoesfuerzoreal
                        limit 1
					);
                    
	-- contamos cuantas veces se mantiene el nombre de proyecto en la tabla para luego compararlo con la cantidad de filas
    set  @proyectos = (	
						SELECT COUNT(*)  
						FROM traspasoesfuerzoreal 
						WHERE proyecto = @proyecto
					);

	if  ( (_filasTraspaso != 0 )  AND (  @proyectos = _filasTraspaso)  ) then
	
		set _contador = 0;     
        
		while_label: while ( _contador <  _filasTraspaso    ) do
        
			select   
					 cliente 
					,proyecto 
					,usuario 
					,fechaInicio 
					,fechaFin 
					,horas 
                    ,tarea
 			into
					 _cliente 
					,_proyecto 
					,_usuario 
					,_fechaInicio 
					,_fechaFin 
					,_horas 
                    ,_tarea
 			from traspasoesfuerzoreal
 			where sec_traspaso = _id; 
            
			set @idTransitorio = _id;
            
            select sec_traspaso into _id  
            from traspasoesfuerzoreal
            where sec_traspaso = ( 	
									select min(sec_traspaso) 
									from traspasoesfuerzoreal
									where sec_traspaso > @idTransitorio
								);
            
            set _id_traspaso = null;
	
            select id_traspaso into _id_traspaso
            from tarea
            where id_traspaso =  cast( SUBSTRING_INDEX( _tarea, ' ', 1) as unsigned );
    
			if( _id_traspaso is not null ) then
            
				insert into esfuerzo_real
				(
					 fecha_inicio
					,fecha_fin
					,usuario
					,cliente
					,hora
					,fk_id_traspaso
					,fk_fk_id_proyecto
				)
				values
				(
					 _fechaInicio
					,_fechaFin
					,_usuario
					,_cliente
					,_horas
					,_id_traspaso
					,_proyecto	
				);    
				
                
                
                call calcularEsfuerzoReal(   _proyecto
											, _id_traspaso
											,@suma
										);
                
                update tarea
				set esfuerzo_real = @suma
                where
                (
						id_traspaso = _id_traspaso
					and fk_id_proyecto = _proyecto
                );
                
                
				set codError = 0;
				set mensaje = 'Grabación exitosa';
                    
			else  	
				-- TODO: tomar el registro que falla
                delete from esfuerzo_real
                where fk_fk_id_proyecto = _proyecto;
                set codError = -1;
				set mensaje = 'Tarea no definida';
                LEAVE while_label;
            end if;
    
			set _contador = _contador + 1;
            
		end while while_label;
         
	else
		set codError = -1;
		set mensaje = 'Proyecto mal definido';

		select '';
	end if;
     
	delete from traspasoesfuerzoreal;
    
END$$
DELIMITER ;

