use gestiondeproyectos;

-- DROP procedure IF exists upsertSistema_relacionado;
drop procedure if exists upsertSistemaRelacionado;


DELIMITER $$
CREATE PROCEDURE upsertSistemaRelacionado
(
	 _id_sistema_relacionado VARCHAR(20)
    ,_nombre VARCHAR(30)
    ,out codError int
	,out mensaje  varchar( 50 )
    
)
BEGIN

	set codError = -1;
	set mensaje = 'Falló ingreso cliente';


	IF EXISTS 
	( 
		SELECT 1 
		FROM sistema_relacionado 
		WHERE id_sistema_relacionado= _id_sistema_relacionado
	) 
	THEN
		UPDATE sistema_relacionado 
		SET 
			 id_sistema_relacionado = _id_sistema_relacionado
			,nombre = _nombre
		WHERE id_sistema_relacionado = _id_sistema_relacionado;
	ELSE
		INSERT INTO sistema_relacionado
		(
			 id_sistema_relacionado
			,nombre
		)
		VALUES
		(
			 _id_sistema_relacionado
			,_nombre
		);
	END IF;
    
	set codError = 0;
	set mensaje = 'Ingreso cliente exitoso';
    
    
END$$
DELIMITER ;
