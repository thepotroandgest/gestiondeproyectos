
use gestiondeproyectos;

 -- drop table contraparte;


CREATE TABLE contraparte
(
	 id_contraparte varchar(50) NOT NULL
	,nombre varchar(50) NOT NULL
	,telefono varchar(15)  NULL
	,correo varchar(60)  NULL
	,fk_rut_cliente varchar(20) NOT NULL
	,PRIMARY KEY ( id_contraparte )
	,FOREIGN KEY (fk_rut_cliente) REFERENCES cliente(rut_cliente) ON DELETE NO ACTION
	,KEY fk_rut_cliente_idx ( fk_rut_cliente )
    
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
