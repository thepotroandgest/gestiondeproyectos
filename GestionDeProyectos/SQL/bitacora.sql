use gestiondeproyectos;

drop table bitacora;

CREATE TABLE bitacora
(
   sec_bitacora bigint(20) NOT NULL AUTO_INCREMENT
  ,tipo varchar(20) DEFAULT NULL
  ,asunto varchar(30) DEFAULT NULL
  ,descripcion varchar(300) DEFAULT NULL
  ,rut_responsable varchar(20) DEFAULT NULL
  ,fecha_ingreso date NOT NULL
  ,fecha_vencimiento date DEFAULT NULL
  ,link varchar(200) DEFAULT NULL
  ,fk_id_proyecto VARCHAR(100) NOT NULL
  ,FOREIGN KEY ( fk_id_proyecto ) REFERENCES proyecto( id_proyecto )
  ,PRIMARY KEY ( sec_bitacora )
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;