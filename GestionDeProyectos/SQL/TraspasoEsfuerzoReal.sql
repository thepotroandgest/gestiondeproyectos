use gestiondeproyectos;

drop table traspasoesfuerzoreal;


CREATE TABLE traspasoesfuerzoreal(
	
     sec_traspaso bigint(20) NOT NULL AUTO_INCREMENT
	,servicio	varchar(50)
	,cliente varchar(100)
	,proyecto varchar(50)
	,usuario varchar(50)
	,tarea varchar(50)
	,fechaEntrega datetime
	,estimatedHours float
	,facturable varchar(10)
	,precioHora float
	,archivado varchar(10)
	,fechaInicio datetime
	,fechaFin datetime
	,zonaHoraria varchar(30)
	,duracion varchar(10)
	,horas float
	,notas varchar(100)
	,total float
	,moneda varchar(10)
    ,KEY  sec_traspaso_idx (sec_traspaso)
  
);



