use gestiondeproyectos;

DROP procedure IF exists insertAvance;

DELIMITER $$
CREATE  PROCEDURE insertAvance
(
     _avance_estimado double
    ,_avance_real double
    ,_fk_id_proyecto varchar(100) 
    ,_observacion varchar(300) 
    ,_fecha  datetime 
    ,_id bigint(20)
    ,_esfuerzo_real DOUBLE
    ,out codError int
	,out mensaje  varchar( 50 )
)
BEGIN

	-- declare _esfuerzo_real double;

	if( _id is not null ) then
    
		if exists (
			select 1 
			from avance
			where id = _id
		)then
				
			select _esfuerzo_real;
        
			update avance 
            set	 avance_estimado = _avance_estimado
				,avance_real = _avance_real
				,fecha = _fecha
				,esfuerzo_real = _esfuerzo_real
				,observacion = _observacion
			WHERE ( id = _id ); 
        end if;
        
        set codError = 0;
		set mensaje = 'Actualizar avance exitoso';
        
        
	else
    
		select max( esfuerzo_real ) into _esfuerzo_real
		from avance
		where (  fk_id_proyecto = _fk_id_proyecto );
		
		set codError = -1;
		set mensaje = 'Falló ingreso de avance';
		
		INSERT INTO avance
		(
			 avance_estimado
			,avance_real
			,esfuerzo_real
			,fecha
			,observacion
			,fk_id_proyecto
		)
		VALUES
		(
			 _avance_estimado
			,_avance_real
			,_esfuerzo_real
			,_fecha   
			,_observacion
			,_fk_id_proyecto
		);

		set codError = 0;
		set mensaje = 'Ingreso avance exitoso';
	
	end if;
	
END$$
DELIMITER ;