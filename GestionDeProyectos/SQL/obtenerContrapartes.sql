
use gestiondeproyectos;

drop procedure if exists obtenerContrapartes; 

DELIMITER $$
CREATE PROCEDURE obtenerContrapartes
(
	_id_contraparte varchar(50) 
)
BEGIN
	SELECT 
	     id_contraparte
		,nombre
		,telefono
		,correo
		,fk_rut_cliente
    FROM contraparte
    where
    (
			id_contraparte = _id_contraparte
        or 	_id_contraparte is null
    )
    
    ;
END$$
DELIMITER ;
