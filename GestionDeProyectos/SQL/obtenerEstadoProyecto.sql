use gestiondeproyectos;

DROP procedure IF exists obtenerEstadoProyecto;

DELIMITER $$
CREATE PROCEDURE obtenerEstadoProyecto( _id_proyecto varchar(100) )
BEGIN

    select 	 
		 av.id
		,av.fk_id_proyecto
		,pr.fecha_inicio
		,pr.fecha_termino
		,now() as fecha_actual
		,av.avance_estimado
		,av.avance_real
		,pr.esfuerzo_vendido
		,round( av.esfuerzo_real, 2 ) as esfuerzo_real
		,round( av.avance_real/av.avance_estimado, 2 ) as indice_avance
		,round( av.esfuerzo_real/pr.esfuerzo_vendido,2  ) as indice_esfuerzo
	from 
	( 
		select fk_id_proyecto, max(id) as maxid
		from avance
		group by fk_id_proyecto
		
	) as x 
	inner join 	avance as av 
		on  av.fk_id_proyecto = x.fk_id_proyecto 
		and	av.id = x.maxid				
	left join proyecto as pr
		on pr.id_proyecto = av.fk_id_proyecto
	where ( x.fk_id_proyecto = _id_proyecto or _id_proyecto is null  );


END$$
DELIMITER ;







