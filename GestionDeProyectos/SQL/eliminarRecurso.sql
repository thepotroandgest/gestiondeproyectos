use gestiondeproyectos;

 DROP PROCEDURE if exists eliminarRecurso;
 
DELIMITER $$
CREATE PROCEDURE eliminarRecurso(
	_id_recurso varchar(10)
)
BEGIN

	declare _rut_recurso varchar(20);

	select rut_recurso into _rut_recurso 
    from recurso 
    where ( id_recurso = _id_recurso );

	DELETE 
	FROM recurso 
	WHERE ( rut_recurso = _rut_recurso );
    
END$$
DELIMITER ;

