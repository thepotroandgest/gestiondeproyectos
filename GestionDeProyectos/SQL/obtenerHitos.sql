use gestiondeproyectos;

DROP PROCEDURE obtenerHitos;

DELIMITER $$
CREATE PROCEDURE obtenerHitos(

	_fk_id_proyecto varchar(100) 
)
BEGIN
	SELECT 
		 hc.sec_hito
		,hc.nombre_hito
		,hc.porcentaje
		,hc.valor_uf
		,hc.fk_id_proyecto
		,sum(fh.abono) as abono
		,round( hc.valor_uf - sum(fh.abono), 1 ) as saldo
	from factura_hito as fh
	inner join hito_comercial as hc
		on fh.fk_sec_hito = hc.sec_hito  
	inner join factura as ft
		on fh.fk_id_factura = ft.id_factura
	where 
	(
		fk_id_proyecto = _fk_id_proyecto or _fk_id_proyecto is null
	)
    group by hc.sec_hito
    union
    select 
		 sec_hito
		-- ,null as id_factura
        ,nombre_hito
        ,porcentaje
        ,valor_uf
        ,fk_id_proyecto
        ,null as abono
		,valor_uf as saldo
	from hito_comercial
	where
    (
			(fk_id_proyecto = _fk_id_proyecto or _fk_id_proyecto is null)
		and	sec_hito not in (  select fk_sec_hito from factura_hito   )

    );
END$$
DELIMITER ;


















