use gestiondeproyectos;

 DROP PROCEDURE if exists eliminarCliente;
 
DELIMITER $$
CREATE PROCEDURE eliminarCliente( _rut_cliente VARCHAR(20) )
BEGIN

	DELETE FROM 
    cliente 
    WHERE  ( cliente.rut_cliente = rut_cliente );
    
END$$
DELIMITER ;

