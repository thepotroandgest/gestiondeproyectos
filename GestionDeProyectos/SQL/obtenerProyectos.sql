use gestiondeproyectos;

DROP procedure IF exists obtenerProyectos;

DELIMITER $$
CREATE  PROCEDURE obtenerProyectos
(
	 _id_proyecto varchar(100) 
)
BEGIN
	SELECT 
		 pr.id_proyecto as id_proyecto
        ,pr.nombre as nombre
		,pr.tipo as tipo
		,pr.esfuerzo_vendido as esfuerzo_vendido
		,pr.fecha_inicio as fecha_inicio
		,pr.fecha_termino as fecha_termino
		,pr.descripcion as descripcion
		,pr.valor_vendido as valor_vendido
		,pr.fk_id_contraparte as fk_id_contraparte
		,pr.fk_id_sistema_relacionado as fk_id_sistema_relacionado
		,pr.fk_rut_recurso as fk_rut_recurso
        ,pr.fase as fase
        ,pr.estado as estado
        ,rc.id_recurso as id_recurso
        ,cn.nombre as nombre_contraparte
        ,sr.nombre as nombre_sistema
    FROM 
    proyecto pr 
    left join contraparte cn on pr.fk_id_contraparte = cn.id_contraparte
    left join sistema_relacionado sr on pr.fk_id_sistema_relacionado = sr.id_sistema_relacionado
    left join recurso rc on pr.fk_rut_recurso = rc.rut_recurso
    where
    (
			id_proyecto = _id_proyecto
        or	_id_proyecto is null
    )
    order by pr.id_proyecto;
END$$
DELIMITER ;
