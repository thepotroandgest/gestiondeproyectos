use gestiondeproyectos;

DROP procedure IF exists upsertContraparte;


DELIMITER $$
CREATE PROCEDURE upsertContraparte
(
	 _id_contraparte VARCHAR(50)
    ,_nombre VARCHAR(50)
    ,_telefono varchar(15)
    ,_correo VARCHAR(100)
    ,_fk_rut_cliente VARCHAR(20)
    ,out codError int
	,out mensaje  varchar( 50 )
)
BEGIN

	set codError = -1;
	set mensaje = 'Falló ingreso cliente';


	IF EXISTS 
    ( 
    
		SELECT 1 
        FROM contraparte
        WHERE id_contraparte = _id_contraparte
    ) 
    THEN
		UPDATE contraparte SET 
			 id_contraparte = _id_contraparte
			,nombre = _nombre
			,telefono = _telefono
			,correo = _correo
			,fk_rut_cliente = _fk_rut_cliente
		WHERE id_contraparte = _id_contraparte;
	ELSE
		INSERT INTO contraparte
			(
				 id_contraparte
                ,nombre
                ,telefono
                ,correo
                ,fk_rut_cliente
			)
		VALUES
			(
				 _id_contraparte
                ,_nombre
                ,_telefono
                ,_correo
                ,_fk_rut_cliente
			);
	END IF;
    
    set codError = 0;
	set mensaje = 'Ingreso cliente exitoso';
    
END$$
DELIMITER ;
