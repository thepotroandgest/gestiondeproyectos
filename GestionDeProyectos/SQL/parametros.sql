use gestiondeproyectos;

drop table if exists parametros;

CREATE TABLE parametros(
	 id int NOT NULL 
    ,nombre varchar(30) not null
    ,descripcion varchar(100)  null
    ,valor double null
	,PRIMARY KEY ( id )
);