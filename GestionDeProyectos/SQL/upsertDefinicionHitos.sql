use gestiondeproyectos;

drop procedure if exists upsertDefinicionHitos; 

DELIMITER $$
CREATE PROCEDURE upsertDefinicionHitos(
	 _id int
    ,_nombre varchar(30)
    ,out codError int
	,out mensaje  varchar( 50 )
)
BEGIN

	set codError = -1;
	set mensaje = 'Falló ingreso recurso';    
    
	IF EXISTS (	
		SELECT 1 
		FROM definicion_hitos 
        WHERE nombre = _nombre
    ) THEN
		UPDATE 	 definicion_hitos SET 
				 nombre = _nombre
		WHERE   nombre = _nombre;
        
        set codError = 0;
		set mensaje = 'Parámetro se actualizó de manera exitosa'; 
	ELSE
		INSERT INTO definicion_hitos(
			 id
            ,nombre 
		)
		VALUES(
			 _id 
			,_nombre 
		);
        
        set codError = 0;
		set mensaje = 'Parámetro se ingresó de manera exitosa'; 
        
	END IF;
END$$
DELIMITER ;