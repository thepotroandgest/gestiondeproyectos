use gestiondeproyectos;

DROP procedure IF EXISTS validarLogin;

DELIMITER $$
CREATE PROCEDURE validarLogin(
	 in _user_name varchar(20)
    ,in _pass varchar(40)
    ,out _result int
    ,out _message varchar(50)
)
BEGIN

	declare _usuario varchar(20);
	declare	_pasword varchar(40);
    
    set _usuario = null;
    set _pasword = null;
    
    select 	 user_name
			,pass 
	into 	 _usuario
			,_pasword 
	from login 
    where user_name = _user_name;
   
    if _usuario is null then
        set _result = 1;
        set _message = 'usuario incorrecto';
    else 
        if _pasword = _pass then
            set _result = 0;
            set _message = 'password correcto';
        else
            set _result = 1;
            set _message = 'password incorrecto';
        end if;
    end if;

END$$
DELIMITER ;





CREATE DEFINER=`root`@`localhost` PROCEDURE `ssl_loginVALIDATE`(
    in _txtUserName varchar(20),
    in _txtPass varchar(40),
    out _result int,
    out _message varchar(50)
)
BEGIN

    
END