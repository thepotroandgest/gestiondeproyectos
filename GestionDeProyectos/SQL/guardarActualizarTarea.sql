use gestiondeproyectos;

DROP procedure IF exists guardarActualizarTarea;

DELIMITER $$
CREATE PROCEDURE guardarActualizarTarea( 
			
			 in _id_traspaso int(11)
			,in _descripcion varchar(50)
			,in _esfuerzo_estimado float
			,in _esfuerzo_real float
			,in _fecha_inicio_estimado datetime
			,in _fecha_inicio_real datetime
			,in _fecha_termino_estimado datetime
			,in _fecha_termino_real datetime
			,in _porcentaje_estimado float
			,in _porcentaje_real float
			,in _fk_id_proyecto varchar(100)
			,in _fk_rut_recurso varchar(20)
            ,out codError int
			,out mensaje  varchar(50)
)
BEGIN


	INSERT INTO tarea
		( 	 
			
			 id_traspaso
			,descripcion
			,esfuerzo_estimado
			,esfuerzo_real
			,fecha_inicio_estimado
			,fecha_inicio_real
			,fecha_termino_estimado
			,fecha_termino_real
			,porcentaje_estimado
			,porcentaje_real
			,fk_id_proyecto
			,fk_rut_recurso
        )
	VALUES
		( 	
			 _id_traspaso
			,_descripcion
			,_esfuerzo_estimado
			,_esfuerzo_real
			,_fecha_inicio_estimado
			,_fecha_inicio_real
			,_fecha_termino_estimado
			,_fecha_termino_real
			,_porcentaje_estimado
			,_porcentaje_real
			,_fk_id_proyecto
			,_fk_rut_recurso
		);
        
        if(tarea != null) then
        
			set codError = -1;
			set mensaje = 'Inserción no exitosa';
        
        else
        
			set codError = 0;
			set mensaje = 'Grabación exitosa';
        
        end if;
        
        
END$$
DELIMITER ;