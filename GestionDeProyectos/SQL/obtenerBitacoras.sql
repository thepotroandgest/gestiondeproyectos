use gestiondeproyectos;


DROP PROCEDURE if exists obtenerBitacora;

DELIMITER $$
CREATE PROCEDURE obtenerBitacora(
	 _sec_bitacora bigint(20)
    ,_fk_id_proyecto varchar( 100 )
    ,_tipo varchar(20)
 )
BEGIN
	
SELECT * FROM(    
    
    SELECT
		 bt.sec_bitacora
		,bt.tipo
		,bt.asunto
		,bt.descripcion
		,rc.id_recurso
        ,bt.rut_responsable
		,bt.fecha_ingreso
		,bt.fecha_vencimiento
        ,bt.link
		,bt.fk_id_proyecto
	FROM bitacora as bt
    left join recurso rc
		on ( bt.rut_responsable = rc.rut_recurso )
        
    UNION     
    
	SELECT
		 bt.sec_bitacora
		,bt.tipo
		,bt.asunto
		,bt.descripcion
		,null as id_recurso
        ,bt.rut_responsable
		,bt.fecha_ingreso
		,bt.fecha_vencimiento
        ,bt.link
		,bt.fk_id_proyecto
	FROM bitacora bt
    where(  bt.rut_responsable is null )
        
 ) AS BITACORAS       
        
where( 
			( BITACORAS.fk_id_proyecto = _fk_id_proyecto or _fk_id_proyecto is null )
        and ( BITACORAS.sec_bitacora = _sec_bitacora or _sec_bitacora is null )
        and ( BITACORAS.tipo = _tipo or _tipo is null )
)
order by fk_id_proyecto;
END$$
DELIMITER ;


