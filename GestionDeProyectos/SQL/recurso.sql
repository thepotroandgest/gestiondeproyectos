use gestiondeproyectos;

-- drop table recurso;

CREATE TABLE recurso 

(
   rut_recurso varchar(20) NOT NULL
  ,id_recurso varchar(10) NOT NULL
  ,nombre varchar(50) NOT NULL
  ,PRIMARY KEY (rut_recurso)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
