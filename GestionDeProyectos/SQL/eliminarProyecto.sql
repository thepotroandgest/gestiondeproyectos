use gestiondeproyectos;

DROP PROCEDURE eliminarProyecto;
 
DELIMITER $$
CREATE PROCEDURE eliminarProyecto(
	_id_proyecto VARCHAR(100)
    ,out codError int
	,out mensaje  varchar( 50 )
)
BEGIN
    
    DECLARE exit handler for sqlexception
	  BEGIN
		
		-- ERROR
	  ROLLBACK;
	END;

	DECLARE exit handler for sqlwarning
	 BEGIN
		
		-- WARNING
	 ROLLBACK;
	END;

	START TRANSACTION;
	 
		set codError = -1;
		set mensaje = 'problemas al borrar tracking de proyecto';
		
		delete from carga_tracking
		where fk_id_proyecto = _id_proyecto;

		set codError = -2;
		set mensaje = 'problemas al borrar avance de proyecto';
        
        delete from avance
		where fk_id_proyecto = _id_proyecto;
        
        set codError = -3;
		set mensaje = 'problemas al borrar hitos de proyecto';
		
		delete from hito_comercial
		where fk_id_proyecto = _id_proyecto;
        
        set codError = -4;
		set mensaje = 'problemas al borrar bitacora de proyecto';
		
		delete from bitacora
		where fk_id_proyecto = _id_proyecto;
        
        set codError = -5;
		set mensaje = 'problemas al borrar proyecto';
        
		DELETE FROM proyecto 
		WHERE proyecto.id_proyecto = _id_proyecto; 
     
		set codError = 0;
		set mensaje = 'exito';
     
	COMMIT;
    
END$$
DELIMITER ;







