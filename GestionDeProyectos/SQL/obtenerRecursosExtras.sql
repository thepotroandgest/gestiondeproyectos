use gestiondeproyectos;

drop procedure if exists obtenerRecursosExtras; 

DELIMITER $$
CREATE PROCEDURE obtenerRecursosExtras
(
	_id_proyecto varchar(100)
)
BEGIN

	select  distinct
		 rc.rut_recurso
		,rc.nombre
		,rc.id_recurso
	from recurso as rc
	left join carga_tracking as ct
		on( rc.rut_recurso = ct.fk_rut_recurso )
	where ( ct.fk_id_proyecto = _id_proyecto or  _id_proyecto is null   );

END$$
DELIMITER ;











