use gestiondeproyectos;

DROP PROCEDURE if exists eliminarParametro;
 
DELIMITER $$
CREATE PROCEDURE eliminarParametro( _id int )
BEGIN

	DELETE FROM 
    parametros 
    WHERE  ( id = _id );

END$$
DELIMITER ;

