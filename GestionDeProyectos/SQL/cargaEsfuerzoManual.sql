use gestiondeproyectos;


drop procedure if exists cargaEsfuerzoManual; 

DELIMITER $$
CREATE PROCEDURE cargaEsfuerzoManual
(
	 _avance_real double
    ,_id_proyecto varchar(100)
)
BEGIN
	
	declare	_max_id int;
    
    SELECT MAX(id) INTO _max_id 
    FROM avance
	WHERE
	(
		fk_id_proyecto = _id_proyecto
	);
		
	UPDATE avance 
	SET esfuerzo_real = _avance_real
	WHERE
    (
			fk_id_proyecto = _id_proyecto
        AND id = _max_id
	);
    
    
END$$
DELIMITER ;







