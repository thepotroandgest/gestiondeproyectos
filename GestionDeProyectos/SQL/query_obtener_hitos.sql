use gestiondeproyectos;

SELECT 
	 hc.sec_hito
	,hc.nombre_hito
	,hc.porcentaje
	,hc.valor_uf
	,hc.fk_id_proyecto
    ,fh.abono
    ,round( hc.valor_uf - fh.abono, 1 ) as saldo
from factura_hito as fh
inner join hito_comercial as hc
	on fh.fk_sec_hito = hc.sec_hito  
inner join factura as ft
	on fh.fk_id_factura = ft.id_factura
where 
(
	fk_id_proyecto = 'PPL-01-TEST3' 
);
    
    
-- FROM hito_comercial as hc
-- left join factura_hito as fh
-- 	on hc.sec_hito = fh.fk_sec_hito
-- where 
-- (
-- 	fk_id_proyecto = 'PPL-01-TEST3' 
-- );