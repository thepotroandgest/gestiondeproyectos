use gestiondeproyectos;

DROP procedure IF exists insertarTraspasoEsfuerzoReal;

DELIMITER $$
CREATE PROCEDURE insertarTraspasoEsfuerzoReal
( 
	 in _servicio	varchar(50)
	,in _cliente varchar(100)
	,in _proyecto varchar(50)
	,in _usuario varchar(50)
	,in _tarea varchar(50)
	,in _fechaEntrega datetime
	,in _estimatedHours float
	,in _facturable varchar(10)
	,in _precioHora float
	,in _archivado varchar(10)
	,in _fechaInicio datetime
	,in _fechaFin datetime
	,in _zonaHoraria varchar(30)
	,in _duracion varchar(10)
	,in _horas float
	,in _notas varchar(100)
	,in _total float
	,in _moneda varchar(10)
)
BEGIN


	INSERT INTO TraspasoEsfuerzoReal
		( 	 
			 servicio
			,cliente
			,proyecto
			,usuario
			,tarea
			,fechaEntrega
			,estimatedHours
			,facturable
			,precioHora
			,archivado
			,fechaInicio
			,fechaFin
			,zonaHoraria
			,duracion
			,horas
			,notas
			,total
			,moneda
        )
	VALUES
		( 	 
			 _servicio
			,_cliente
			,_proyecto
			,_usuario
			,_tarea
			,_fechaEntrega
			,_estimatedHours
			,_facturable
			,_precioHora
			,_archivado
			,_fechaInicio
			,_fechaFin
			,_zonaHoraria
			,_duracion
			,_horas
			,_notas
			,_total
			,_moneda
		);
END$$
DELIMITER ;
