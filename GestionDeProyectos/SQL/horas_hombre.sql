
use gestiondeproyectos;

-- drop table horas_hombre;

CREATE TABLE horas_hombre 
(
	 sec_hh bigint(20) NOT NULL AUTO_INCREMENT
    ,fecha_inicio datetime NOT NULL
	,fecha_fin datetime NOT NULL
	,usuario varchar(50) NOT NULL
	,cliente varchar(50) NOT NULL
    ,tarea varchar(50) NOT NULL
	,horas float NOT NULL
    ,fk_id_proyecto varchar(100) NOT NULL
    ,KEY  sec_hh_idx ( sec_hh )
	,PRIMARY KEY ( sec_hh )
	,CONSTRAINT horas_hombre_ibfk_1 FOREIGN KEY ( fk_id_proyecto ) REFERENCES proyecto ( id_proyecto ) ON DELETE NO ACTION
    
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

