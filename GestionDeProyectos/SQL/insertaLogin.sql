use gestiondeproyectos;

DROP procedure IF EXISTS insertaLogin;

DELIMITER $$
CREATE PROCEDURE insertaLogin(
	 in _user_name varchar(20)
    ,in _txt_pass varchar(40)
)
BEGIN

	insert into login(
		 user_name
		,txt_pass
    )
	values(
		 _user_name
        ,_txt_pass
    );
    
END$$
DELIMITER ;
