use gestiondeproyectos;


DROP PROCEDURE if exists obtenerAvance;

DELIMITER $$
CREATE PROCEDURE obtenerAvance(
	 _id bigint(20)
    ,_fk_id_proyecto varchar( 100 )
 )
BEGIN
	SELECT
		 id 
		,avance_estimado 
		,avance_real 
		,fecha  
		,round(esfuerzo_real, 1) as  esfuerzo_real
		,observacion  
		,fk_id_proyecto 
	FROM avance
    where( 
				( fk_id_proyecto = _fk_id_proyecto or _fk_id_proyecto is null )
            and ( id = _id or _id is null )
	)
    order by fk_id_proyecto;
END$$
DELIMITER ;






