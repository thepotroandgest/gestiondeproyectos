use gestiondeproyectos;

drop procedure if exists upsertParametros; 


DELIMITER $$
CREATE PROCEDURE upsertParametros(
	 _id int
    ,_nombre varchar(30)
    ,_descripcion varchar(100) 
    ,_valor double 
    ,out codError int
	,out mensaje  varchar( 50 )
)
BEGIN

	set codError = -1;
	set mensaje = 'Falló ingreso recurso';    
    
	IF EXISTS (	
		SELECT 1 
		FROM parametros 
        WHERE nombre = _nombre
    ) THEN
		UPDATE 	 parametros SET 
				 nombre = _nombre
				,descripcion = _descripcion
				,valor = _valor 
		WHERE   nombre = _nombre;
        
        set codError = 0;
		set mensaje = 'Parámetro se actualizó de manera exitosa'; 
        
	ELSE
		INSERT INTO parametros(
			 id
            ,nombre 
			,descripcion 
			,valor 
		)
		VALUES(
			 _id 
			,_nombre 
			,_descripcion 
			,_valor
		);
        
        set codError = 0;
		set mensaje = 'Parámetro se ingresó de manera exitosa'; 
        
	END IF;
END$$
DELIMITER ;
