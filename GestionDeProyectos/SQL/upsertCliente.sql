

use gestiondeproyectos;

drop procedure if exists upsertCliente; 


DELIMITER $$
CREATE PROCEDURE upsertCliente
(
	 _rut_cliente varchar( 20 ) 
	,_nombre varchar( 50 ) 
	,_fono varchar( 15 )
	,_correo varchar( 40 )
	,_direccion varchar( 50 )
	,out codError int
	,out mensaje  varchar( 50 )
    
)
BEGIN

	set codError = -1;
	set mensaje = 'Falló ingreso cliente';

	IF EXISTS 
    (	
		SELECT 1 
		FROM cliente 
        WHERE rut_cliente = _rut_cliente
    
    ) THEN
		UPDATE 	 cliente SET 
				 rut_cliente = _rut_cliente
				,nombre = _nombre
                ,fono = _fono
                ,correo = _correo
                ,direccion = _direccion
		WHERE   rut_cliente = _rut_cliente;
	ELSE
		INSERT INTO cliente
		(
			 rut_cliente 
            ,nombre 
            ,fono 
            ,correo 
            ,direccion 
		)
		VALUES
		(
			 _rut_cliente 
            ,_nombre 
            ,_fono 
            ,_correo 
            ,_direccion
            
		);
	END IF;
    
    set codError = 0;
	set mensaje = 'Ingreso cliente exitoso';
    
    
END$$
DELIMITER ;
















