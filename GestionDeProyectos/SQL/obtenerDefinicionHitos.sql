use gestiondeproyectos;

drop procedure if exists obtenerDefinicionHitos; 


DELIMITER $$
CREATE PROCEDURE obtenerDefinicionHitos( 
	_id int
)
BEGIN

	select 	 id 
			,nombre 
	from definicion_hitos
    where ( id = _id or _id is null );

END$$
DELIMITER ;



