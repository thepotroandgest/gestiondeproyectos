USE gestiondeproyectos;

DROP procedure IF EXISTS obtenerLogin;

DELIMITER $$
CREATE PROCEDURE obtenerLogin()
BEGIN

	select   user_name
			,txt_pass 
	from login;

END$$
DELIMITER ;

