use gestiondeproyectos;

DROP procedure IF exists traspasarTraspasoTarea;
-- comentario para git
DELIMITER $$
CREATE PROCEDURE traspasarTraspasoTarea
( 
	 in idProyectoTraspaso varchar(100)
    ,out codError int
    ,out mensaje  varchar(50)
 )
BEGIN
	 
    declare _idProyectoTareaTXT varchar(100);
    declare _id int;
    declare _nombre varchar(100) ;
	declare _duration float;
	declare _scheduled_Work float;
	declare _start_Date datetime;
	declare _finish_Date datetime;
	declare _predecessors int;
	declare _resource_Initials varchar(50); 
    declare _filasTraspaso int;
    declare _contador int;
    declare _descripcionTXT varchar(100);
    declare _id_recurso varchar(10);
    declare _rut_recurso varchar(20);
    
    
	-- buscamos la descripción del proyecto para confirmar si el proyecto esta definido en su tabla
    select   nombre
			,id_proyecto
    into  	 _descripcionTXT
			,_idProyectoTareaTXT
    from proyecto
    where id_proyecto = idProyectoTraspaso;
 
	
    select count( * ) 
    into _filasTraspaso 
	from TraspasoTarea;
    
    
    -- acá se valida que el proyecto esta definido en la tabla proyecto
	if  ( _idProyectoTareaTXT  is not null   ) then
    
		set _contador = 2;     
        
        select min(id) into @idActual 
		from TraspasoTarea
		limit 1;
            
		while ( _contador <=  _filasTraspaso + 1  ) do
        
			select   id
					,nombre
					,duration
 					,start_Date
					,finish_Date
                    ,resource_Initials
 			into
					 _id
					,_nombre
 					,_duration
 					,_start_Date
 					,_finish_Date
                    ,_id_recurso
 			from TraspasoTarea
 			where id = @idActual; 
            
			set @idTransitorio = @idActual;
            
            select id into @idActual  
            from TraspasoTarea
            where id = ( 	
							select min(id) 
							from TraspasoTarea
							where id > @idTransitorio
						);
            
            select rut_recurso 
            into _rut_recurso
            from recurso
            where id_recurso = _id_recurso;
            
            
 			insert into tarea
            (
				 descripcion
				,esfuerzo_estimado
				,fecha_inicio_estimado								
				,fecha_termino_estimado
				,fk_id_proyecto
				,id_traspaso
                ,fk_rut_recurso
			)
			values
            (
				 _nombre
				,_duration
				,_start_Date
				,_finish_Date
				,_idProyectoTareaTXT
				,_id
                ,_rut_recurso
 						
			);         
                    
			set _contador = _contador + 1;
            
         end while;
		
   
		set codError = 0;
        set mensaje = 'Grabación exitosa';
    
	else
		
        set codError = -1;
        set mensaje = 'El proyecto no esta definido';
    
	end if;
	
	delete from TraspasoTarea;

END$$
DELIMITER ;



