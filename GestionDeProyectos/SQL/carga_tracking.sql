use gestiondeproyectos;

 drop table carga_tracking;

CREATE TABLE carga_tracking 
(
	 sec_esfuerzo bigint(20) NOT NULL AUTO_INCREMENT
    ,fecha_inicio datetime 
	,fecha_fin datetime 
	,usuario varchar(50) 
	,cliente varchar(50) 
	,horas double 
    ,fk_id_proyecto varchar(100) NOT NULL
    ,fk_rut_recurso varchar(20) NOT NULL
	,KEY  sec_esfuerzo_idx (sec_esfuerzo)
	,PRIMARY KEY (sec_esfuerzo )
	,FOREIGN KEY ( fk_id_proyecto ) REFERENCES proyecto( id_proyecto ) 
    ,FOREIGN KEY ( fk_rut_recurso ) REFERENCES recurso( rut_recurso )  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
