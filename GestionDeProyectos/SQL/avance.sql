use gestiondeproyectos;

drop table avance;

CREATE TABLE avance
(
	 id bigint(20) NOT NULL AUTO_INCREMENT
    ,avance_estimado double not null
    ,avance_real double not null
    ,fecha datetime 
    ,esfuerzo_real DOUBLE  null
    ,observacion varchar(300) 
    ,fk_id_proyecto varchar(100) not null
	,PRIMARY KEY ( id )
    ,FOREIGN KEY ( fk_id_proyecto ) 
	REFERENCES proyecto( id_proyecto )
                 
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
