use gestiondeproyectos;

 DROP PROCEDURE if exists eliminarContraparte;
 
DELIMITER $$
CREATE PROCEDURE eliminarContraparte( _id_contraparte VARCHAR(50) )
BEGIN

	DELETE FROM 
    contraparte 
    WHERE  ( contraparte.id_contraparte = _id_contraparte );
    
END$$
DELIMITER ;
