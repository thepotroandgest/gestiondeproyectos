
use gestiondeproyectos;

drop procedure if exists obtenerCalendario; 

DELIMITER $$
CREATE PROCEDURE obtenerCalendario( )
BEGIN
    
	SELECT   concat(bt.asunto,' ',pr.nombre  ) AS title
		,bt.fecha_ingreso AS start
		,bt.fecha_vencimiento AS end
	FROM bitacora as bt
	left join proyecto as pr
		on( bt.fk_id_proyecto = pr.id_proyecto )
	WHERE bt.tipo = 'Compromiso';
    
END$$
DELIMITER ;
