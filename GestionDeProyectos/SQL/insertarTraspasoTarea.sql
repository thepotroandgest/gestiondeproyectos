use gestiondeproyectos;

DROP procedure IF exists insertarTraspasoTarea;

DELIMITER $$
CREATE PROCEDURE insertarTraspasoTarea( 
	 in _id int
	,in _nombre varchar( 50 )
	,in _duration float
	,in _scheduled_Work float
	,in _start_Date datetime
	,in _finish_Date datetime
	,in _predecessors int
	,in _resource_Initials varchar( 50 ) 
)
BEGIN


	INSERT INTO TraspasoTarea
		( 	 id
			,nombre
			,duration
			,scheduled_Work
			,start_Date
			,finish_Date
			,predecessors
			,resource_Initials
        )
	VALUES
		( 	 _id
			,_nombre
            ,_duration
            ,_scheduled_Work
            ,_start_Date
            ,_finish_Date
            ,_predecessors
            ,_resource_Initials
		);
END$$
DELIMITER ;
