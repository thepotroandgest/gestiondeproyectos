USE GESTIONDEPROYECTOS;

CREATE TABLE proyecto 
(
	 id_proyecto varchar(100) NOT NULL
	,nombre varchar(50) NOT NULL
	,tipo varchar(30)  NULL
	,esfuerzo_vendido DOUBLE DEFAULT NULL
	,fecha_inicio date 
	,fecha_termino date 
    ,descripcion varchar( 200 )
    ,valor_vendido double
    ,fase varchar(20)
    ,estado varchar( 20 )
	,fk_id_contraparte varchar(50)  NULL
	,fk_id_sistema_relacionado varchar(50)  NULL
	,fk_rut_recurso varchar(20)  NULL
	,PRIMARY KEY ( id_proyecto )
	,FOREIGN KEY ( fk_id_contraparte ) 
	REFERENCES contraparte( id_contraparte )
	,FOREIGN KEY ( fk_id_sistema_relacionado ) 
	REFERENCES sistema_relacionado(id_sistema_relacionado)
	,FOREIGN KEY ( fk_rut_recurso ) 
	REFERENCES recurso( rut_recurso )
    
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




