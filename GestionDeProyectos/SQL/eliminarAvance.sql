use gestiondeproyectos;

DROP PROCEDURE if exists eliminarAvance;
 
DELIMITER $$
CREATE PROCEDURE eliminarAvance(
	_id_avance bigint(20)
)
BEGIN
	DELETE 
	FROM avance 
	WHERE id = _id_avance;
END$$
DELIMITER ;