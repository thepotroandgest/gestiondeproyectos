use gestiondeproyectos;

 drop table tarea;

CREATE TABLE tarea
(
	 sec_tarea bigint(20) NOT NULL AUTO_INCREMENT
    ,id_traspaso int not null
	,descripcion varchar(50) NOT NULL
    ,esfuerzo_estimado float NOT NULL
    ,esfuerzo_real float DEFAULT NULL
	,fecha_inicio_estimado date NOT NULL
	,fecha_inicio_real datetime DEFAULT NULL
	,fecha_termino_estimado date NOT NULL
	,fecha_termino_real datetime DEFAULT NULL
    ,porcentaje_estimado float DEFAULT NULL
    ,porcentaje_real float DEFAULT NULL
	,fk_id_proyecto varchar(100) NOT NULL
    ,fk_rut_recurso varchar(20) NOT NULL
	,KEY  sec_tarea_idx (sec_tarea)
	,PRIMARY KEY (
					 id_traspaso	
					,fk_id_proyecto 
                 )
	,FOREIGN KEY ( fk_id_proyecto ) REFERENCES proyecto( id_proyecto )
                 
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

