use gestiondeproyectos;

DROP procedure IF exists traspasar_traspaso_horas_hombre;

DELIMITER $$
CREATE PROCEDURE traspasar_traspaso_horas_hombre
(
	 out codError int
    ,out mensaje  varchar(50)

)
BEGIN
	 
	declare _cliente varchar(100);
	declare _proyecto varchar(50);
	declare _usuario varchar(50);
	declare _tarea varchar(50);
	declare _fechaInicio datetime;
	declare _fechaFin datetime;
	declare _horas float;
	declare _id bigint(20);
    declare _contador int;
    declare _id_traspaso int;
    declare _filasTraspaso int;
    declare _usuario_flag varchar(50);
    
    -- conteo de filas en traspasoesfuerzoreal
    select count( * ) 
    into _filasTraspaso 
	from traspasoesfuerzoreal;

	-- se busca en minimo id, s usara mas adelante
	select min(sec_traspaso) into _id
    from traspasoesfuerzoreal
    limit 1;

	-- seleccionamos el primer proyecto de la tabla
    set @proyecto = (
						select proyecto 
                        from traspasoesfuerzoreal
                        limit 1
					);
                    
	-- contamos cuantas veces se mantiene el nombre de proyecto en la tabla para luego compararlo con la cantidad de filas
    set  @proyectos = (	
						SELECT COUNT(*)  
						FROM traspasoesfuerzoreal 
						WHERE proyecto = @proyecto
					);

	if  ( (_filasTraspaso != 0 )  AND (  @proyectos = _filasTraspaso)  ) then
	
		set _contador = 0;     
        
		while ( _contador <  _filasTraspaso    ) do
        
			select   
					 cliente 
					,proyecto 
					,usuario 
					,fechaInicio 
					,fechaFin 
					,horas 
                    ,tarea
 			into
					 _cliente 
					,_proyecto 
					,_usuario 
					,_fechaInicio 
					,_fechaFin 
					,_horas 
                    ,_tarea
 			from traspasoesfuerzoreal
 			where sec_traspaso = _id; 
            
			set @idTransitorio = _id;
            
            select sec_traspaso into _id  
            from traspasoesfuerzoreal
            where sec_traspaso = ( 	
									select min(sec_traspaso) 
									from traspasoesfuerzoreal
									where sec_traspaso > @idTransitorio
								);
            
			insert into horas_hombre
			(
				 fecha_inicio
				,fecha_fin
				,usuario
				,cliente
				,horas
				,tarea
				,fk_id_proyecto
			)
			values
			(
				 _fechaInicio
				,_fechaFin
				,_usuario
				,_cliente
				,_horas
				,_tarea
				,_proyecto	
			);      
		
			set _contador = _contador + 1;
            
         end while;
        
        set codError = 0;
        set mensaje = 'Grabación exitosa';
        
     else
     
		set codError = -1;
        set mensaje = 'Proyecto mal definido';
        
     end if;
     
	delete from traspasoesfuerzoreal;

     
END$$
DELIMITER ;