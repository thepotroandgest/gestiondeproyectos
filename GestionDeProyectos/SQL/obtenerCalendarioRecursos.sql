use gestiondeproyectos;

drop procedure if exists obtenerCalendarioRecursos; 

DELIMITER $$
CREATE PROCEDURE obtenerCalendarioRecursos( )
BEGIN
    
    select  distinct 
			 concat (rec.id_recurso, ' ', crg.fk_id_proyecto  )AS title
			,pry.fecha_inicio AS start
			,pry.fecha_termino AS end
	from recurso as rec
	right join carga_tracking as crg on rec.rut_recurso = crg.fk_rut_recurso
	left join proyecto as pry on crg.fk_id_proyecto = pry.id_proyecto;
    
END$$
DELIMITER ;

