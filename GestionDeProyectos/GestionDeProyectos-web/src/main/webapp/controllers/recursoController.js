/**
    * Created by Gustavo on 23-02-2017.
    */
app.controller('recursoController',  function(      $uibModal
                                                ,recursosService
                                                ,proyectoService
                                                ,$routeParams
                                                ,$location ){


    var vm = this;
    vm.recurso= {};
    vm.recursos = [];
    vm.recursosPorProyecto = [];
    vm.Proyectos = [];
    vm.animationsEnabled = true;
    vm.mensaje = '';
    vm.deshabilitarGuardar = true;
    vm.idProyecto = 'todos';
    vm.idRecurso = 'todos';

    //Declaración de funciones
    vm.obtieneRecursos = obtieneRecursos;
    vm.buscaRecurso = buscaRecurso;
    vm.filtrarRecurso = filtrarRecurso;
    vm.showFormRecurso = showFormRecurso;
    vm.upsert = upsert;
    vm.getAction = getAction;
    vm.setAction = setAction;
    vm.setPath = setPath;
    vm.validaRut = validaRut;
    vm.evaluaBloqueo = evaluaBloqueo;
    vm.buscarRecursosPorProyecto = buscarRecursosPorProyecto;
    vm.obtenerProyectos = obtenerProyectos;


    function obtenerProyectos () {

        proyectoService.obtenerProyectos()
            .then(function (results) {

                if(results.data.length == 0){
                    vm.noData = true;
                }else{
                    vm.Proyectos = results.data;

                    console.log( vm.Proyectos  );
                }
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }


    function buscarRecursosPorProyecto(){

        recursosService.buscarRecursoPorProyecto( vm.idProyecto, vm.idRecurso )
        .then(function (results) {
            console.log( 'then' );
            vm.recursosPorProyecto = results.data;

            console.log(   vm.recursos );

        })
        .catch( function ( response ) {

            console.log(   response.data   );

        })
        .finally(function () {

        });
    }

    function evaluaBloqueo(){

        if(
                vm.recurso.rutRecurso == '' || vm.recurso.rutRecurso == null || vm.recurso.rutRecurso == undefined
            ||  vm.recurso.idRecurso == '' || vm.recurso.idRecurso == null || vm.recurso.idRecurso == undefined
            ||  vm.recurso.nombre == '' || vm.recurso.nombre == null || vm.recurso.nombre == undefined
            || vm.mensaje == ''
        ){
            vm.deshabilitarGuardar = true;
        }else{
            vm.deshabilitarGuardar = false;
        }

    }

    function validaRut( rut ){

        if( Fn.validaRut( rut ) == false  ){
            vm.mensaje = 'rut inválido';
        }else{
            vm.mensaje = '';
        }
    }

    function setPath( dir ) {
        $location.path(dir);
    }

    function filtrarRecurso( _rec ){
        vm.recursos = _rec;
    }

    function buscaRecurso() {

        recursosService.buscarRecurso($routeParams.rutRecurso)
            .then(function (results) {
                vm.recurso = results.data[0];

            })
            .catch(function (response) {

            })
            .finally(function () {

            });

    }

    //obtener recursos llamando al servicio, y luego a la base de datos a través de él
    function obtieneRecursos () {
        recursosService.obtenerRecursos()
            .then(function (results) {

                vm.noData = false;
                if(results.data.length == 0){
                    vm.noData = true;
                }else{
                    vm.recursos = results.data;
                }
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }

    function setAction(accion){
        recursosService.setAction(accion);
    }

    function getAction(){
        recursosService.getAction();

    }

    function upsert () {
        // if(validar()){
        recursosService.upsertRecurso(vm.recurso)
            .then(function (results) {
                vm.recursoAgregado = !vm.recursoAgregado;
                $timeout(function () {
                    vm.recursoAgregado = !vm.recursoAgregado;
                    vm.setAction(null);
                    $location.path("/recursos");
                }, 500);
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }

    function init() {

        console.log(  $location.path()   );

        obtenerProyectos();


        if( $routeParams.rutRecurso != null ) {
            buscaRecurso();
        }

        obtieneRecursos();

        if( $location.path() == '/recursosPorProyecto' ){
            buscarRecursosPorProyecto();
        }

    }

    init();

    //--------------------------------------------------- Modal --------------------------------------------------------
    //aquí se genera la ventana emergente a través de $uibModal.open en donde se carga el template con su respectivo controlador
    function showFormRecurso(recurso) {
        var modalInstance = $uibModal.open({
            animation: vm.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'views/mantenedores/eliminarRecurso.html',
            controller: 'ModalInstanceRecurso as vm',
            //le entregamos el id del proyecto que vamos a eliminar
            resolve: {
                recursoAEliminar: function () {
                    return recurso;
                }
            }
        });
    }

});

app.controller('ModalInstanceRecurso'
                , function (     $uibModalInstance
                                ,recursoAEliminar
                                ,recursosService
                                ,$route
                                ,$timeout
                                ,$location) {


    var vm = this;

    vm.mensaje;
    vm.problema = false;
    vm.exito = false;
    vm.botonesEliminar = true;


        vm.eliminar = eliminar;
    vm.cancel = cancel;
    vm.setPath = setPath;

    function setPath( dir ) {
        $location.path(dir);
    }


    function eliminar() {
        recursosService.eliminarRecurso(recursoAEliminar)
            .then(function () {
                $route.reload();
                vm.botonesEliminar = false;
                vm.mensaje = 'Recurso eliminado con exito';
                vm.exito = true;

            })
            .catch(function (response) {

                vm.botonesEliminar = false;
                vm.mensaje = response.data.mensaje;
                vm.problema = true;
            })
            .finally(function () {

                $timeout(function(){
                        $uibModalInstance.close();
                    }
                    ,3000);

            });
    }
    function cancel() {
        $uibModalInstance.close();
    }
});