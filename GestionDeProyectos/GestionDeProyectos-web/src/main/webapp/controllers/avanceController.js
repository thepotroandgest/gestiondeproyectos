app.controller('avanceController',
                        function (
                                     $uibModal
                                    ,$timeout
                                    ,$location
                                    ,avanceService
                                    ,$routeParams
                                    ,proyectoService
                                    ,utilsService
                        ) {

    var vm = this;

    vm.agregar = agregar;
    vm.obtenerAvances = obtenerAvances;
    vm.obtenerProyectos = obtenerProyectos;
    vm.obtenerAvance = obtenerAvance;
    vm.showFormAvance = showFormAvance;
    vm.setPath = setPath;
    vm.alertMsn = alertMsn;
    vm.evaluaBloqueo = evaluaBloqueo;

    vm.animationsEnabled = true;
    vm.avances = [];
    vm.avance = {};
    vm.idProyecto;
    vm.Proyectos;
    vm.mensaje = null;
    vm.problema = false;
    vm.exito = false;
    vm.habilitarGuardado = true;


    function evaluaBloqueo(){


        if(
                vm.avance.avanceEstimado == ''  || vm.avance.avanceEstimado == null || vm.avance.avanceEstimado == undefined
            ||  vm.avance.avanceReal == '' || vm.avance.avanceReal == null || vm.avance.avanceReal == undefined
            ||  vm.avance.esfuerzoReal == '' ||  vm.avance.esfuerzoReal == null ||  vm.avance.esfuerzoReal == undefined
        ){
            vm.habilitarGuardado = true;
        }else{
            vm.habilitarGuardado = false;
        }
    }

    function alertMsn(){

        if( vm.avance.avanceEstimado > 100 || vm.avance.avanceReal > 100 ){
            vm.mensaje = 'avance no puede ser mayor a 100%';
        }else{
            vm.mensaje = null;
        }
    }

    function setPath( dir ) {
        $location.path(dir);
    }

    function obtenerProyectos () {

        proyectoService.obtenerProyectos()
            .then(function (results) {

                if(results.data.length == 0){
                    vm.noData = true;
                }else{

                    vm.Proyectos = results.data;

                    for( var i = 0; i < vm.Proyectos.length; i++ ){
                        vm.Proyectos[ i ].esfuerzoVendido = Math.round( (vm.Proyectos[ i ].esfuerzoVendido / 9 ) * 10 ) / 10;
                    }
                }
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }


    function obtenerAvance(  ){

        vm.idProyecto = 'todos';
        vm.avance.idAvance = $routeParams.idAvance;

        avanceService.obtenerAvances( vm.avance.idAvance, vm.idProyecto )
            .then( function( results ){
                vm.avance = results.data[0];
                vm.avance.fecha = utilsService.stringToDdmmyyyyy(vm.avance.fecha);
            })
            .catch(

            )
            .finally(

            );

    }

    function obtenerAvances(){

        if(     vm.idProyecto == null
            ||  vm.idProyecto == undefined
            ||  vm.idProyecto == '' ){

            vm.idProyecto = 'todos'
        }
        if(     vm.avance.idAvance == null
            ||  vm.avance.idAvance == undefined
            ||  vm.avance.idAvance == '' ){

            vm.avance.idAvance = 0;
        }

        avanceService.obtenerAvances( vm.avance.idAvance, vm.idProyecto )
        .then( function( results ){
            vm.avances =results.data;


            if(  vm.avances.length == 0  ){
                vm.mensaje = 'No se encontraron datos';
                vm.problema = true;

                $timeout(function(){
                    vm.problema = false;
                },2000);

            }

        })
        .catch(

        )
        .finally(

        );

    }

    function agregar(){

        if( $routeParams.idProyecto != null ){
            vm.avance.fkIdProyecto = $routeParams.idProyecto;
        }

        avanceService.agregarAvance( vm.avance )
        .then(function (results) {
            vm.avanceAgregado = !vm.avanceAgregado;
            $location.path("/estadoProyecto");

            $timeout(function(){
                vm.avanceAgregado = !vm.avanceAgregado;
                //vm.setAction( null );
            },500);
        })
        .catch(function (response) {

        })
        .finally(function () {

        });

    };



    //Inicializar funciones
    function init(){
        //obtenerBitacoras();

        if( vm.idProyecto != undefined ){
            obtenerAvances();
        }

        if(  $routeParams.idAvance != null   ){
            obtenerAvance();
        }


        obtenerProyectos();

        if( $routeParams.idProyecto != null ){
            vm.idProyecto = $routeParams.idProyecto;
        }

    }

    init();


    //-----------------------------------------   datepickerpopup   ----------------------------------------------------

    vm.today = function() {
        vm.avance.fecha= new Date();
    };
    vm.today();

    vm.clear = function() {
        vm.avance.fecha = null;
    };
    vm.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };
    vm.dateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        minDate: new Date(),
        startingDay: 1
    };
    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }
    vm.toggleMin = function() {
        vm.inlineOptions.minDate = vm.inlineOptions.minDate ? null : new Date();
        vm.dateOptions.minDate = vm.inlineOptions.minDate;
    };
    vm.toggleMin();
    vm.open1 = function() {
        vm.popup1.opened = true;
    };
    vm.setDate = function(year, month, day) {
        vm.dt = new Date(year, month, day);
    };
    vm.formats = ['dd-MM-yyyy'];
    vm.format = vm.formats[0];
    vm.altInputFormats = ['M!/d!/yyyy'];

    vm.popup1 = {
        opened: false
    };
    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0,0,0,0);

            for (var i = 0; i < vm.events.length; i++) {
                var currentDay = new Date(vm.events[i].date).setHours(0,0,0,0);

                if (dayToCheck === currentDay) {
                    return vm.events[i].status;
                }
            }
        }
        return '';
    }

    //--------------------------------------------------- Modal --------------------------------------------------------
    //aquí se genera la ventana emergente a través de $uibModal.open en donde se carga el template con su respectivo controlador
    function showFormAvance(avance) {
        var modalInstance = $uibModal.open({
            animation: vm.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'views/mantenedores/eliminarAvance.html',
            controller: 'ModalInstanceAvance as vm',
            //le entregamos el id del proyecto que vamos a eliminar
            resolve: {
                avanceAEliminar: function () {
                    return avance;
                }
            }
        });
    }
});

app.controller('ModalInstanceAvance', function ( $uibModalInstance
                                                ,avanceAEliminar
                                                ,avanceService
                                                ,$route ) {

    //Declaración de variables
    var vm = this;//alias del scope
    //Declaración de funciones
    vm.eliminar = eliminar;
    vm.cancel = cancel;

    function eliminar() {

        avanceService.eliminarAvance( avanceAEliminar )
            .then(function () {
                $route.reload();
                $location.path( 'avance');


            })
            .catch(function (response) {

            })
            .finally(function () {

            });

        $uibModalInstance.close();

    }

    function cancel() {
        $uibModalInstance.close();
    }

});
