app.controller('bitacoraController',  function(  $scope
                                                ,$uibModal
                                                ,$location
                                                ,recursosService
                                                ,proyectoService
                                                ,bitacoraService
                                                ,utilsService
                                                ,$timeout
                                                ,$routeParams) {


    var vm = this;
    vm.obtenerBitacoras = obtenerBitacoras;
    vm.buscarBitacora = buscarBitacora;
    vm.showFormBitacora = showFormBitacora;
    vm.agregar = agregar;
    vm.obtenerProyectos = obtenerProyectos;
    vm.obtenerRecursos = obtenerRecursos;
    vm.getAction = getAction;
    vm.setAction = setAction;
    vm.setIdProyecto = setIdProyecto;
    vm.getIdProyecto = getIdProyecto;

    vm.Bitacoras = [];
    vm.bitacora = {};
    vm.animationsEnabled = true;
    vm.bitacora.fechaVencimiento = null;
    vm.rutResponsable;
    vm.idProyecto;


    vm.tipos = [
                     "Mail"
                    ,"Compromiso"
                    ,"Notas"
                    ,"Acuerdos"
                    ,"Observacion Estado"];



    vm.setPath = setPath;

    function setIdProyecto( id ){
        bitacoraService.setIdProyecto( id );
    }

    function getIdProyecto(){
        return bitacoraService.getIdProyecto();
    }

    function setPath( dir ) {
        $location.path(dir);
    }

    function getAction(){
        return utilsService.getAction();
    }

    function setAction( accion ){
        bitacoraService.setIdProyecto(  vm.idProyecto  );

        return utilsService.setAction( accion );
    }


    function obtenerProyectos () {
        proyectoService.obtenerProyectos()
            .then(function (results) {
                vm.Proyectos = results.data;
            })
            .catch(function (response) {

            })
            .finally(function () {

                if( bitacoraService.getIdProyecto() != undefined ) {
                    vm.bitacora.fkIdProyecto = bitacoraService.getIdProyecto();
                }

            });
    }

    function obtenerRecursos () {
        recursosService.obtenerRecursos()
            .then(function (results) {
                vm.Recursos = results.data;
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }

    function buscarBitacora(){


        recursosService.obtenerRecursos()
        .then(function (results) {
            vm.Recursos = results.data;
            return         bitacoraService.buscarBitacora( $routeParams.sec_bitacora  );
        })
        .then(function (results) {
            vm.bitacora = results.data[ 0 ];

            vm.bitacora.fechaIngreso = utilsService.stringToDdmmyyyyy( vm.bitacora.fechaIngreso );
            vm.bitacora.fechaVencimiento = utilsService.stringToDdmmyyyyy( vm.bitacora.fechaVencimiento );

        })
        .catch(function (response) {

        })
        .finally(function () {

            document.getElementById('recurso').value = vm.bitacora.rutResponsable;
        });

    }

    function agregar(){

        if( getAction() == 'Nueva'){
            vm.bitacora.rutResponsable = vm.rutResponsable;
        }

        bitacoraService.agregarBitacora(vm.bitacora)
        .then(function (results) {
            $timeout(function(){
                $location.path("/bitacora");
            },500);
        })
        .catch(function (response) {

        })
        .finally(function () {

        });

        vm.bitacoraNoAgregado = !vm.bitacoraNoAgregado;
        //$timeout(function(){vm.bitacoraNoAgregado = !vm.bitacoraNoAgregado},3000);

    }

    function obtenerBitacoras () {



        if(     vm.idProyecto == ''
            ||  vm.idProyecto == null
            ||  vm.idProyecto == undefined ){

            vm.idProyecto = 'todos';
        }



        bitacoraService.buscarBitacorasProyectos( vm.idProyecto )
            .then(function (results) {
                vm.noData = false;
                if(results.data.length == 0){
                    vm.noData = true;
                    vm.Bitacoras = null;

                }else{
                    vm.Bitacoras = results.data;
                }
            })
            .catch(function (response) {
            })
            .finally(function () {

            });
    }
    //Inicializar funciones
    function init(){

        //obtenerBitacoras();
        obtenerRecursos();
        obtenerProyectos();

        if( $routeParams.sec_bitacora != null ){
            buscarBitacora();
        }

    }
    init();
    //--------------------------------------------------- Modal --------------------------------------------------------
    //aquí se genera la ventana emergente a través de $uibModal.open en donde se carga el template con su respectivo controlador
    function showFormBitacora (bitacora) {
        var modalInstance = $uibModal.open({
            animation: vm.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'views/eliminarBitacora.html',
            controller: 'ModalInstanceBitacora as vm',
            //le entregamos el id del proyecto que vamos a eliminar
            resolve: {
                bitacoraAEliminar: function () {
                    return bitacora;
                }
            }
        });
    }

    //-----------------------------------------   datepickerpopup   ----------------------------------------------------

    vm.today = function() {
        vm.bitacora.fechaIngreso= new Date();
    };
    vm.today();

    vm.today2 = function() {

        if(   vm.bitacora.tipo == 'Compromiso' ){
            vm.bitacora.fechaVencimiento= new Date();
        }

    };
    vm.today2();

    vm.clear = function() {
        vm.nuevaBitacora.fechaIngreso = null;
    };
    vm.clear2 = function() {
        vm.nuevaBitacora.fechaVencimiento = null;
    };
    vm.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };
    vm.dateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        minDate: new Date(),
        startingDay: 1
    };
    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }
    vm.toggleMin = function() {
        vm.inlineOptions.minDate = vm.inlineOptions.minDate ? null : new Date();
        vm.dateOptions.minDate = vm.inlineOptions.minDate;
    };
    vm.toggleMin();
    vm.open1 = function() {
        vm.popup1.opened = true;
    };
    vm.open2 = function() {
        vm.popup2.opened = true;
    };
    vm.setDate = function(year, month, day) {
        vm.dt = new Date(year, month, day);
    };
    vm.formats = ['dd-MM-yyyy'];
    vm.format = vm.formats[0];
    vm.altInputFormats = ['M!/d!/yyyy'];

    vm.popup1 = {
        opened: false
    };
    vm.popup2 = {
        opened: false
    };
    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0,0,0,0);

            for (var i = 0; i < vm.events.length; i++) {
                var currentDay = new Date(vm.events[i].date).setHours(0,0,0,0);

                if (dayToCheck === currentDay) {
                    return vm.events[i].status;
                }
            }
        }
        return '';
    }

});
app.controller('ModalInstanceBitacora', function ($uibModalInstance, bitacoraAEliminar,bitacoraService,$route) {
    //Declaración de variables
    var vm = this;//alias del scope
    //Declaración de funciones
    vm.eliminar = eliminar;
    vm.cancel = cancel;
    function eliminar() {
        bitacoraService.eliminarBitacora(bitacoraAEliminar)
            .then(function () {
                $route.reload();
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
        $uibModalInstance.close();
    }
    function cancel() {
        $uibModalInstance.close();
    }
});
