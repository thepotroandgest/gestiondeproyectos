app.controller('cargasDeArchivoController', function(tareaService, horasHombreService, esfuerzoRealService, $timeout,$location) {

    var vm = this;
    vm.subirTarea = subirTarea;
    vm.getName = getName;
    vm.mensaje = mensaje;
    vm.subirTracking = subirTracking;
    vm.cargaExitosa = false;
    vm.cargaNoExitosa = false;
    vm.mensaje = "hola mundo";

    function mensaje(){

        alert('funciona');
    }

    function getName(){
        var f = document.getElementById('inputFile').files[0];

        document.getElementById('inputfile').value = f.name;
    }

    function subirTarea(){

        var f = document.getElementById('fileTarea').files[0];

        tareaService.insertarTarea( f )
        .then(function (data) {

            vm.cargaExitosa = !vm.cargaExitosa;
            vm.mensaje = data.data.mensaje;

            console.log( 'insertarTarea ok' );




            $timeout(function(){
                vm.cargaExitosa = !vm.cargaExitosa;

            },1500);

            console.log("data -- " + data.data.mensaje );

        }) .catch(function (response) {
            vm.cargaNoExitosa = !vm.cargaNoExitosa;
            vm.mensaje = response.data.mensaje;
            $timeout(function(){
                vm.cargaNoExitosa = !vm.cargaNoExitosa;
                $location.path("/cargasDeArchivo");
            },2500);

        })
        .finally(function () {

        });
    }

    function subirTracking( tipoArchivo ){
        var f = document.getElementById('fileTracking').files[0];

        if(tipoArchivo == null || tipoArchivo == ""){

            alert('No ha seleccionado ningún archivo');
        }
        else if (tipoArchivo == 'Horas Hombre'){

            horasHombreService.insertarHorasHombre(f)
                .then(function (data) {

                vm.cargaExitosa = !vm.cargaExitosa;
                vm.mensaje = data.data.mensaje;


                $timeout(function(){
                    vm.cargaExitosa = !vm.cargaExitosa;
                    $location.path("/estadoProyecto");
                },1500);

                console.log("data -- " + data.data.mensaje )
                ;

            }) .catch(function (response) {
                    vm.cargaNoExitosa = !vm.cargaNoExitosa;
                    vm.mensaje = response.data.mensaje;
                    $timeout(function(){
                        vm.cargaNoExitosa = !vm.cargaNoExitosa;
                        $location.path("/cargasDeArchivo");
                    },2500);

                })
                .finally(function () {

                });
        }
        else if(tipoArchivo == 'Esfuerzo'){


            console.log( f );


            esfuerzoRealService.insertarEsfuerzoReal(f)
                .then(function (data) {

                    vm.cargaExitosa = !vm.cargaExitosa;
                    vm.mensaje = data.data.statusMsj;

                    $timeout(function(){
                        vm.cargaExitosa = !vm.cargaExitosa;
                        $location.path("/estadoProyecto");
                    },1500);

                }) .catch(function (response) {

                    console.log( response );
                    vm.cargaNoExitosa = !vm.cargaNoExitosa;
                    vm.mensaje = response.data.mensaje;
                    $timeout(function(){
                        vm.cargaNoExitosa = !vm.cargaNoExitosa;
                        $location.path("/estadoProyecto");
                    },2500);

                })
                .finally(function () {

                });
        }

    }

    vm.limpiar = function() {
        document.getElementById('fileName2').value  = '';
    };

    function init(){

    }

    init();

});
