/**
    * Created by Gustavo on 16-02-2017.
    */
app.controller('modificarFacturaController',  function(facturaService,$timeout,$location,$routeParams) {
    //declaración de variables
    var vm = this;//alias del scope
    vm.disable = true;
    vm.factura = {};
    //Declaración de funciones
    vm.buscarFactura = buscarFactura;
    vm.upsert = upsert;
    //obtener el recurso llamando al servicio, y luego a la base de datos a través de él
    function buscarFactura () {
        facturaService.buscarFactura($routeParams.id_factura)
            .then(function (results) {
                vm.facturaList = results.data;
                vm.factura = vm.facturaList[0];
                vm.factura.fecha_emision = new Date(vm.facturaList[0].fecha_emision);
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }
    //Realizar actualización
    function upsert (){
        if(validar()){
            facturaService.upsertFactura(vm.factura)
                .then(function (results) {
                    vm.facturaModificado = !vm.facturaModificado;
                    $timeout(function(){
                        vm.facturaModificado = !vm.facturaModificado;
                        $location.path("/factura");
                    },500);
                })
                .catch(function (response) {

                })
                .finally(function () {

                });
        }else{
            vm.facturaNoModificado = !vm.facturaNoModificado;
            $timeout(function(){vm.facturaNoModificado = !vm.facturaNoModificado},3000);
        }
    };
    //Validar
    //función para validar los campos
    function validar (){
        vm.flagFecha_emision = false;
        vm.flagEstado = false;
        vm.flagObservacion = false;
        vm.facturaAgregado = false;
        vm.facturaNoAgregado = false;
        var rsp = true;
        if(!vm.factura.fecha_emision){
            vm.flagFecha_emision = true;
            rsp = false;
        }
        if(!vm.factura.estado){
            vm.flagEstado = true;
            rsp = false;
        }
        if(!vm.factura.observacion){
            vm.flagObservacion = true;
            rsp = false;
        }
        return rsp;
    }
    //Inicializar funciones
    function init(){
        buscarFactura();
    }
    init();
    //-----------------------------------------   datepickerpopup   ----------------------------------------------------

    vm.today = function() {
        vm.factura.fecha_emision= new Date();
    };

    vm.clear = function() {
        vm.factura.fecha_emision = null;
    };
    vm.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };
    vm.dateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        minDate: new Date(),
        startingDay: 1
    };
    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }
    vm.toggleMin = function() {
        vm.inlineOptions.minDate = vm.inlineOptions.minDate ? null : new Date();
        vm.dateOptions.minDate = vm.inlineOptions.minDate;
    };
    vm.toggleMin();
    vm.open1 = function() {
        vm.popup1.opened = true;
    };
    vm.setDate = function(year, month, day) {
        vm.dt = new Date(year, month, day);
    };
    vm.formats = ['yyyy-MM-dd'];
    vm.format = vm.formats[0];
    vm.altInputFormats = ['yyyy/M!/d!'];

    vm.popup1 = {
        opened: false
    };
    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0,0,0,0);

            for (var i = 0; i < vm.events.length; i++) {
                var currentDay = new Date(vm.events[i].date).setHours(0,0,0,0);

                if (dayToCheck === currentDay) {
                    return vm.events[i].status;
                }
            }
        }
        return '';
    }
});