app.controller('definicionHitosController',  function(    definicionHitosService
                                                        ,utilsService
                                                        ,$uibModal
                                                        ,$timeout
                                                        ,$location
                                                        ,$routeParams
){

    var vm = this;

    vm.animationsEnabled = true;
    vm.definicionHito = {};
    vm.DefinicionHitos = [];

    vm.obtenerDefinicionHitos = obtenerDefinicionHitos;
    vm.buscarDefinicionHito = buscarDefinicionHito;
    vm.showFormDefinicionHito = showFormDefinicionHito;
    vm.setAction = setAction;
    vm.getAction = getAction;
    vm.upsert = upsert;
    vm.setPath = setPath;

    function obtenerDefinicionHitos(){

        definicionHitosService.buscarDefinicionHitos()
        .then(function (results) {
            vm.noData = false;
            if(results.data.length == 0){
                vm.noData = true;
            }else{
                vm.DefinicionHitos = results.data;
            }
        })
        .catch(function (response) {

        })
        .finally(function () {

        });
    }


    function setPath( dir ) {
        $location.path(dir);
    }

    function buscarDefinicionHito () {



        definicionHitosService.buscarDefinicionHitos($routeParams.id)
        .then(function (results) {
            vm.definicionHito  = results.data[0];

        })
        .catch(function (response) {

        })
        .finally(function () {

        });
    }

    function setAction( accion ){
        utilsService.setAction( accion );
    }

    function getAction(  ){

        return utilsService.getAction(  );
    }

    function upsert (){


        definicionHitosService.upsertDefinicionHito( vm.definicionHito )
        .then(function (results) {

            $timeout(function(){
                $location.path("/definicionHitos");
            },500);

        })
        .catch(function (response) {

        })
        .finally(function () {

        });


    };


    //--------------------------------------------------- Modal --------------------------------------------------------
    //aquí se genera la ventana emergente a través de $uibModal.open en donde se carga el template con su respectivo controlador
    function showFormDefinicionHito( defHito ) {
        var modalInstance = $uibModal.open({
            animation: vm.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'views/mantenedores/eliminarDefinicionHito.html',
            controller: 'ModalInstanceDefinicionHito as vm',
            //le entregamos el id del proyecto que vamos a eliminar
            resolve: {
                definicionHitoAEliminar: function () {
                    return defHito;
                }
            }
        });
    }

    //Inicializar
    function init(){

        obtenerDefinicionHitos();

        if( getAction() == 'Modificar'){
            buscarDefinicionHito();
        }

    }

    init();

});

app.controller('ModalInstanceDefinicionHito', function ( $uibModalInstance
                                                        ,definicionHitoAEliminar
                                                        ,definicionHitosService
                                                        ,$route
                                                        ,$timeout) {

    var vm = this;

    vm.mensaje;
    vm.problema = false;
    vm.exito = false;
    vm.botonesEliminar = true;


    vm.eliminar = eliminar;
    vm.cancel = cancel;

    function eliminar() {

        definicionHitosService.eliminarDefinicionHito( definicionHitoAEliminar )
    .then(function () {
        $route.reload();
        vm.botonesEliminar = false;
        vm.mensaje = 'Definición Hito eliminado con exito';
        vm.exito = true;
    })
    .catch(function (response) {
        vm.botonesEliminar = false;
        vm.mensaje = response.data.mensaje;
        vm.problema = true;
    })
    .finally(function () {
        $timeout(function(){
                $uibModalInstance.close();
            }
            ,2000);
    });

    }
    function cancel() {
        $uibModalInstance.close();
    }
});