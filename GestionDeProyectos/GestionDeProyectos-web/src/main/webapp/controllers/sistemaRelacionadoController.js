app.controller('sistemaRelacionadoController',  function(sistemaRelacionadoService,$uibModal,$routeParams,$timeout,$location) {

    var vm = this;//alias del $scope

    vm.obtenerSistemas_relacionados = obtenerSistemas_relacionados;
    vm.showFormSistema_relacionado = showFormSistema_relacionado;
    vm.buscarSistema_relacionado = buscarSistema_relacionado;
    vm.setAction = setAction;
    vm.getAction = getAction;

    vm.animationsEnabled = true;
    vm.sistemaRelacionado = {};
    vm.upsert = upsert;

    vm.setPath = setPath;

    function setPath( dir ) {
        $location.path(dir);
    }


    function setAction( accion ){
        sistemaRelacionadoService.setAction( accion );
    }

    function getAction(  ){
        return sistemaRelacionadoService.getAction(  );
    }

    function buscarSistema_relacionado () {
        sistemaRelacionadoService.buscarSistema_relacionado($routeParams.idSistema)
            .then(function (results) {
                vm.sistemaRelacionado = results.data[0];
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }

    function upsert (){

        sistemaRelacionadoService.upsertSistema_relacionado( vm.sistemaRelacionado )
            .then(function (results) {
                vm.sistemaAgregado = !vm.sistemaAgregado;
                $timeout(function(){
                    vm.sistemaAgregado = !vm.sistemaAgregado;
                    $location.path("/sistemaRelacionado");
                },500);
            })
            .catch(function (response) {

            })
            .finally(function () {

            });

    };

    function obtenerSistemas_relacionados () {

        sistemaRelacionadoService.obtenerSistemas_relacionados()
            .then(function (results) {
                vm.noData = false;
                if(results.data.length == 0){
                    vm.noData = true;
                }else{
                    vm.sistemasRelacionados = results.data;
                }
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }

    function init(){
        obtenerSistemas_relacionados();

        if( $routeParams.idSistema != null ){
            buscarSistema_relacionado ();
        }

    }
    init();
    //--------------------------------------------------- Modal --------------------------------------------------------
    //aquí se genera la ventana emergente a través de $uibModal.open en donde se carga el template con su respectivo controlador
    function showFormSistema_relacionado(sistema) {
        var modalInstance = $uibModal.open({
            animation: vm.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'views/mantenedores/eliminarSistema_relacionado.html',
            controller: 'ModalInstanceSistemaRelacionado as vm',
            //le entregamos el id del proyecto que vamos a eliminar
            resolve: {
                sistemaAEliminar: function () {
                    return sistema;
                }
            }
        });
    }

});
app.controller('ModalInstanceSistemaRelacionado', function ($uibModalInstance
                                                            ,sistemaAEliminar
                                                            ,sistemaRelacionadoService
                                                            ,$route
                                                            ,$timeout) {

    var vm = this;

    vm.mensaje;
    vm.problema = false;
    vm.exito = false;
    vm.botonesEliminar = true;


    vm.eliminar = eliminar;
    vm.cancel = cancel;
    vm.setPath = setPath;

    function setPath( dir ) {
        $location.path(dir);
    }


    function eliminar() {

        sistemaRelacionadoService.eliminarSistemaRelacionado( sistemaAEliminar )
        .then(function () {
            $route.reload();

            vm.botonesEliminar = false;
            vm.mensaje = 'Recurso eliminado con exito';
            vm.exito = true;
        })
        .catch(function (response) {
            vm.botonesEliminar = false;
            vm.mensaje = response.data.mensaje;
            vm.problema = true;

        })
        .finally(function () {

            $timeout(function(){
                    $uibModalInstance.close();
                }
                ,2000);

        });
    }
    function cancel() {
        $uibModalInstance.close();
    }
});