app.controller('proyectosController',
                function
                (
                     $uibModal
                    ,$timeout
                    ,proyectoService
                    ,contraparteService
                    ,sistemaRelacionadoService
                    ,hitosComercialesService
                    ,parametroService
                    ,bitacoraService
                    ,definicionHitosService
                    ,utilsService
                    ,recursosService
                    ,$location
                    ,$routeParams
                    ,$window
                    ,Flash
                ) {

    var vm = this;
    var nombreInicial;
    var porcentajeAnterior;
    var valorUfAnterior;

    vm.animationsEnabled = true;
    vm.noData = false;
    vm.idProyectoExist = false;
    vm.valorVendidoExist = false;
    vm.estadoBotonGrabadoFinal = true;
    vm.mostrarAlertaPorcentaje = false;
    vm.pHDesabilitado = true;
    vm.nombreRepetido = false;
    vm.hitoActual = {};
    vm.proyecto={};

    vm.fase;
    vm.estado;
    vm.estadoDesarrolo;
    vm.todosLosRecursos;
    vm.todosLosRecursos;
    vm.idProyecto;
    vm.idContraparte;
    vm.rutRecurso;
    vm.idSistema;
    vm.Proyectos;
    vm.proyectoAgregado = false;
    vm.mensaje;
    vm.problema = false;

    vm.mails = [];
    vm.compromisos = [];
    vm.notas = [];
    vm.acuerdos = [];
    vm.observaciones = [];
    vm.hitosPorGrabar = [];
    vm.hitos = [];

    vm.nombresHitos = [ ];
    //     "Aceptación de la oferta"
    //    ,"Primera Entrega"
    //    ,"Entrega a Certificación"
    //    ,"Aceptación Conforme"
    //];

    vm.fases = [
                     "Ante-Proyecto"
                    ,"Desarrollo"
                    ,"Certificación"
                    ,"Liberación"
            ];

    vm.tipos = [
                     "ESFUERZO"
                    ,"HORAS HOMBRES"
                    ,"BOLSA DE HORAS"
            ];

    vm.estados = [
                 "EJECUCIÓN"
                ,"CERRADO"
                ];


    vm.alerta = {    'background-color' : 'red'
                    ,'color':'white'
                };

    vm.parametros = {};

    vm.total = {
        porcentaje:0
        ,valorUF: 0
        ,abono: 0
        ,saldo: 0
    };

    vm.totalHitosPorGrabar = {

         totalPorcentaje : 0
        ,totalValorUf : 0
    };


    vm.obtenerProyectos = obtenerProyectos;
    vm.buscarProyecto = buscarProyecto;
    vm.obtenerContrapartes = obtenerContrapartes;
    vm.obtenerSistemas_relacionados = obtenerSistemas_relacionados;
    vm.obtenerRecursos = obtenerRecursos;
    vm.showFormProyecto = showFormProyecto;
    vm.actualizaEsfuerzoReal = actualizaEsfuerzoReal;
    vm.getDefaultEsfuerzo = getDefaultEsfuerzo;
    vm.successAlert = successAlert;
    vm.obtenerEstado = obtenerEstado;
    vm.upsert = upsert;
    vm.setAction = setAction;
    vm.getAction = getAction;
    vm.upsertHito = upsertHito;
    vm.limpiarHito = limpiarHito;
    vm.eliminarHitoPorGrabar = eliminarHitoPorGrabar;
    vm.setIdProyectoExist = setIdProyectoExist;
    vm.setValorVendidoExist = setValorVendidoExist;
    vm.defineEstadoBotonHito = defineEstadoBotonHito;
    vm.cargarForaneos = cargarForaneos;
    vm.habilitarHitos = habilitarHitos;
    vm.desHabilitarHitos = desHabilitarHitos;
    vm.setValorUF = setValorUF;
    vm.validarPorcentaje = validarPorcentaje;
    vm.actualizarPorcentaje = actualizarPorcentaje;
    vm.obtenerHitos = obtenerHitos;
    vm.informeFacturacion = informeFacturacion;
    vm.buscarMailsProyecto = buscarMailsProyecto;
    vm.setPath = setPath;
    vm.nombreProyectoRepetido = nombreProyectoRepetido;
    vm.getStyle = getStyle;
    vm.obtenerDefinicionHitos = obtenerDefinicionHitos;

    var rojo;
    var rojoAmarillo;
    var amarilloVerde;
    var verdeAmarillo;
    var amarilloRojo;

    function obtenerDefinicionHitos(){

        definicionHitosService.buscarDefinicionHitos()
        .then( function( results ){

            vm.nombresHitos = results.data;
        });
    }

    function obtenerParametros(){

        parametroService.buscarParametros()
        .then( function( results ){
            vm.parametros = results.data;

            for( var i = 0; i < vm.parametros.length; i++ ){

                if(  vm.parametros[ i ].nombre == 'rojo'    ){
                    rojo =  vm.parametros[ i ].valor;
                }

                if(  vm.parametros[ i ].nombre == 'rojoAmarillo'   ){
                    rojoAmarillo = vm.parametros[ i ].valor;
                }

                if(  vm.parametros[ i ].nombre == 'amarilloVerde'  ){
                    amarilloVerde =  vm.parametros[ i ].valor;
                }

                if( vm.parametros[ i ].nombre == 'verdeAmarillo'  ){
                    verdeAmarillo =  vm.parametros[ i ].valor;
                }

                if( vm.parametros[ i ].nombre == 'amarilloRojo'  ){
                    amarilloRojo =  vm.parametros[ i ].valor;
                }

            }
        });
    }


    function getStyle( indice ){

        if( indice >  rojo && indice <= rojoAmarillo ){

            return  {    'background-color' : '#c83737'
                        ,'color':'#550000' };

        }else if( indice > rojoAmarillo && indice <= amarilloVerde ){

            return  {    'background-color' : '#ffe680'
                        ,'color':'#806600' };

        }else if( indice > amarilloVerde && indice <= verdeAmarillo  ){

            return  {    'background-color' : '#88aa00'
                        ,'color':'#445500' };
        }else if( indice > verdeAmarillo  && indice <= amarilloRojo ){

            return  {    'background-color' : '#ffe680'
                ,'color':'#445500' };
        }else if( indice > amarilloRojo  ){

            return  {    'background-color' : '#c83737'
                        ,'color':'#550000' };
        }

        return vm.alerta;
    }

    function nombreProyectoRepetido(){

        vm.nombreRepetido = false;

        for( var i = 0; i < vm.Proyectos.length; i++ ){

            if(     vm.Proyectos[ i ].nombre == vm.proyecto.nombre &&  nombreInicial != vm.proyecto.nombre      ){
                vm.nombreRepetido = true;
            }
        }
    }


    function setPath( dir ) {
        $location.path(dir);
    }

    function buscarMailsProyecto(){
        bitacoraService.buscarMailsProyecto( $routeParams.idProyecto )
            .then(function (results) {
                vm.mails = results.data;
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }

    function buscarCompromisosProyecto(){
        bitacoraService.buscarCompromisosProyecto( $routeParams.idProyecto )
            .then(function (results) {
                vm.compromisos = results.data;
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }

    function buscarNotasProyecto(){
        bitacoraService.buscarNotasProyecto( $routeParams.idProyecto )
            .then(function (results) {
                vm.notas = results.data;
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }

    function buscarAcuerdosProyecto(){
        bitacoraService.buscarAcuerdosProyecto( $routeParams.idProyecto )
            .then(function (results) {
                vm.acuerdos = results.data;
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }

    function buscarObservacionesProyecto(){

        bitacoraService.buscarObservacionesProyecto( $routeParams.idProyecto )
            .then(function (results) {
                vm.observaciones = results.data;
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }

    function informeFacturacion(){

        if(     vm.idProyecto == ''
            ||  vm.idProyecto == null
            ||  vm.idProyecto == undefined ){

            vm.idProyecto = 'todos';
        }

        if(     vm.idContraparte == ''
            ||  vm.idContraparte == null
            ||  vm.idContraparte == undefined ){

            vm.idContraparte = 'todos';
        }

        //vm.idContraparte;

        hitosComercialesService.obtenerHitosPorContraparte(  vm.idProyecto, vm.idContraparte )
        .then( function( results ){

            vm.total = {
                porcentaje:0
                ,valorUF: 0
                ,abono: 0
                ,saldo: 0
            };

            vm.hitosPorContraparte = results.data;

            for( var i = 0; i < vm.hitosPorContraparte.length; i++ ){
                vm.total.abono += vm.hitosPorContraparte[ i ].abono;
                vm.total.porcentaje += vm.hitosPorContraparte[ i ].porcentaje;
                vm.total.valorUF += vm.hitosPorContraparte[ i ].valorUf;
                vm.total.saldo += vm.hitosPorContraparte[ i ].saldo;
            }

            if(  vm.hitosPorContraparte.length == 0  ){
                vm.mensaje = 'No se encontraron datos';
                vm.problema = true;

                $timeout(function(){
                    vm.problema = false;
                },2000);
            }

        })
        .catch(

        ).finally(

        );
    }

    function obtenerHitos(){

        hitosComercialesService.buscarHitoComercial( $routeParams.idProyecto )
            .then( function( results ){
                vm.hitos = results.data;

                for( var i = 0; i < vm.hitos.length; i++ ){

                    vm.total.porcentaje = vm.total.porcentaje + vm.hitos[ i ].porcentaje;
                    vm.total.valorUF = vm.total.valorUF + vm.hitos[ i ].valorUf;
                    vm.total.abono = vm.total.abono + vm.hitos[ i ].abono;
                    vm.total.saldo = vm.total.saldo + vm.hitos[ i].saldo;

                    if( vm.hitos[ i ].abono == null  ){
                        vm.hitos[ i ].abono = 0;
                    }

                }
            })
            .catch( function (response) {

            });
    }

    function actualizarPorcentaje( id ){

        vm.hitosPorGrabar[ id ].valorUf =  Math.round( ( vm.hitosPorGrabar[ id ].porcentaje / 100 ) * vm.proyecto.valorVendido  * 100  ) / 100;


    }

    function setValorUF(){
        vm.hitoActual.valorUf =   Math.round ( ( vm.hitoActual.porcentaje / 100 ) *   vm.proyecto.valorVendido  * 100 ) / 100  ;
    }

    function successAlert(){
     var message = '<strong> Well done!</strong>  You successfully read this important alert message.';
     var id = Flash.create('success', message, 0, {class: 'custom-class', id: 'custom-id'}, true);
    }

    function getDefaultEsfuerzo( valor ){

        if( valor == null || valor == undefined ){
            return 0;
        }else{
            return valor;
        }

    }

    function setIdProyectoExist(   ){

        if(    vm.proyecto.idProyecto != null
            && vm.proyecto.idProyecto != ''
            && vm.proyecto.nombre != null
            && vm.proyecto.nombre != ''
            && vm.nombreRepetido != true){

            vm.idProyectoExist = true;
            vm.estadoBotonGrabadoFinal= false;

        }else{
            vm.idProyectoExist= false;
            vm.estadoBotonGrabadoFinal= true;
        }
    }

    function setValorVendidoExist(  ){

        if(    vm.proyecto.valorVendido != null
            && vm.proyecto.valorVendido != '' ){

            vm.valorVendidoExist = true;

        }else{

            vm.valorVendidoExist= false;
        }
    }

    function defineEstadoBotonHito(){

        var nombreSeleccionado = false;
        var porcentajeIngresado = false;

        if( document.getElementById( 'nombresHitos').selectedIndex != 0 ){
            nombreSeleccionado = true;
        }

        if(     vm.hitoActual.porcentaje != null
            &&  vm.hitoActual.porcentaje != '' ){

            porcentajeIngresado = true;
        }


        return  !(      vm.idProyectoExist
                    &&  vm.valorVendidoExist
                    && nombreSeleccionado
                    && porcentajeIngresado);

    }

    function habilitarHitos(indice){

        document.getElementById( indice + 'hp' ).disabled = false;
        document.getElementById( indice + 'hp' ).focus();

        porcentajeAnterior =  vm.hitosPorGrabar[ indice ].porcentaje;
        valorUfAnterior = vm.hitosPorGrabar[ indice ].valorUf;
    }

    function desHabilitarHitos( indice ){
        document.getElementById( indice + 'hp').disabled = true;
        vm.totalHitosPorGrabar.totalPorcentaje =  vm.totalHitosPorGrabar.totalPorcentaje - ( porcentajeAnterior - parseFloat( vm.hitosPorGrabar[ indice ].porcentaje ));
        vm.totalHitosPorGrabar.totalValorUf =  vm.totalHitosPorGrabar.totalValorUf - ( valorUfAnterior - parseFloat( vm.hitosPorGrabar[ indice ].valorUf ));

    }

    function setAction( action ){



        return utilsService.setAction( action );
    }

    function getAction(  ){
        return utilsService.getAction(  );
    }

    function buscarProyecto () {

        proyectoService.buscarProyecto($routeParams.idProyecto)
            .then(function (results) {
                vm.proyecto = results.data[0];



            })
            .catch(function (response) {

            })
            .finally(function () {


            });
    }

    function obtenerRecursos(){
        recursosService.obtenerRecursos()
        .then(
            function( results ){
                vm.Recursos = results.data;
            }
        ).catch( function (response) {

        }
        ).finally( function (response) {

        }
        );

    }

    function obtenerContrapartes () {
        contraparteService.obtenerContrapartes()
        .then(function (results) {
            vm.Contrapartes = results.data;

        })
        .catch(function (response) {

        })
        .finally(function () {

        });

    }

    function obtenerSistemas_relacionados () {
        sistemaRelacionadoService.obtenerSistemas_relacionados()
        .then(function (results) {
            vm.SistemasRelacionados = results.data;
        })
        .catch(function (response) {

        })
        .finally(function () {

        });
    }

    function cargarForaneos(){

        recursosService.obtenerRecursos()
        .then( function( results ){
            vm.Recursos = results.data;
            return contraparteService.obtenerContrapartes();
        })
        .then( function( results ){
            vm.Contrapartes = results.data;
            return sistemaRelacionadoService.obtenerSistemas_relacionados();
        })
        .then( function( results ){
            vm.SistemasRelacionados = results.data;
            return  proyectoService.buscarProyecto($routeParams.idProyecto);
        })
        .then( function( results ){

            vm.proyecto = results.data[0];

            vm.todosLosRecursos = '';

            for( var i = 0; i < vm.proyecto.recursosExtras.length;  i++  ){

                vm.todosLosRecursos = vm.todosLosRecursos + ' ' +  vm.proyecto.recursosExtras[ i ].idRecurso;
            }

            console.log( vm.todosLosRecursos  );

            vm.proyecto.esfuerzoVendido = Math.round( (vm.proyecto.esfuerzoVendido / 9) * 10 ) / 10;

        })
        .catch( function (response) {

        }
        ).finally( function (response) {


            if( getAction() == 'Modificar' ) {

                document.getElementById('contraparte').value = vm.proyecto.fkIdContraparte;
                document.getElementById('sistema').value = vm.proyecto.fkIdSistemaRelacionado;

                vm.fase = vm.proyecto.fase;
                vm.estadoDesarrolo= vm.proyecto.estado;
                nombreInicial = vm.proyecto.nombre;

                hitosComercialesService.buscarHitoComercial( $routeParams.idProyecto )
                .then( function( results ){

                    vm.hitosPorGrabar = results.data;

                    for( var i = 0; i < vm.hitosPorGrabar.length; i++ ){
                        vm.totalHitosPorGrabar.totalPorcentaje += vm.hitosPorGrabar[ i ].porcentaje;
                        vm.totalHitosPorGrabar.totalValorUf += vm.hitosPorGrabar[ i ].valorUf;
                    }

                })
                .catch( function (response) {

                });
            }

            if( getAction() != 'Detalle' ) {
                vm.proyecto.fechaInicio = utilsService.stringToDdmmyyyyy(vm.proyecto.fechaInicio);
                vm.proyecto.fechaTermino = utilsService.stringToDdmmyyyyy(vm.proyecto.fechaTermino);
            }

        });

    }

    function upsert (){


        vm.proyecto.esfuerzoVendido =  vm.proyecto.esfuerzoVendido * 9;

        if( vm.getAction() == 'Nuevo' ) {

            vm.proyecto.fkIdContraparte = vm.idContraparte;
            vm.proyecto.fkIdSistemaRelacionado = vm.idSistema;

        }else if( vm.getAction() == 'Modificar'  ){
            vm.proyecto.fkIdContraparte = document.getElementById( 'contraparte').value;
            vm.proyecto.fkIdSistemaRelacionado = document.getElementById( 'sistema').value;
        }

        vm.proyecto.fase = vm.fase;
        vm.proyecto.estado =  vm.estadoDesarrolo;

        proyectoService.upsertProyecto( vm.proyecto )
        .then(function (results) {
            vm.proyectoAgregado = !vm.proyectoAgregado;

            $timeout(function(){
                $location.path("/proyectos");
            },1500);


            hitosComercialesService.upsertHitos(vm.hitosPorGrabar);


        })
        .catch(function (response) {

        })
        .finally(function () {

        });

        vm.proyectoNoAgregado = !vm.proyectoNoAgregado;
        $timeout(function(){vm.proyectoNoAgregado = !vm.proyectoNoAgregado},4000);

    }

    //obtener proyectos llamando al servicio, y luego a la base de datos a través de él
    function obtenerProyectos () {

        proyectoService.obtenerProyectos()
        .then(function (results) {

            if(results.data.length == 0){
                vm.noData = true;
            }else{
                vm.Proyectos = results.data;


                for( var i = 0; i < vm.Proyectos.length; i++ ){

                    vm.Proyectos[ i ].esfuerzoVendido = Math.round( (vm.Proyectos[ i ].esfuerzoVendido / 9 ) * 10 ) / 10;

                }
            }
        })
        .catch(function (response) {

        })
        .finally(function () {

        });
    }

    //obtener proyectos llamando al servicio, y luego a la base de datos a través de él
    function obtenerEstado () {

        if(
                vm.idProyecto == null
            ||  vm.idProyecto == ''
            || vm.idProyecto == undefined
        ){
            vm.idProyecto = 'todos';

        }

        proyectoService.obtenerEstado(   vm.idProyecto   )
        .then(function (results) {
            if(results.data.length == 0){

                vm.estado = [];
                vm.mensaje = 'No se econtraron estados';


                $timeout(function(){
                        vm.mensaje = null;                    }
                    ,2000);

            }else{

                vm.estado = results.data;

                for( var i = 0; i < vm.estado.length; i++ ){
                    vm.estado[ i ].esfuerzoVendido = Math.round( ( vm.estado[ i ].esfuerzoVendido / 9) * 10 ) / 10;
                    vm.estado[ i ].esfuerzoReal = Math.round( ( vm.estado[ i ].esfuerzoReal / 9) * 10 ) / 10;
                }
            }
        })
        .catch(function (response) {

        })
        .finally(function () {

        });
    }

    function limpiarHito(){

        vm.hitoActual.hitoComercial= '';
        vm.hitoActual.porcentaje = '';
        vm.hitoActual.valorUf = '';
        vm.hitoActual.fkIdProyecto = '';

    }


    function validarPorcentaje(){

        var sumaPorcentaje = 0;
        vm.estadoBotonGrabadoFinal = true;

        for( var i = 0; i < vm.hitosPorGrabar.length; i++ ){
            sumaPorcentaje = parseFloat( sumaPorcentaje ) + parseFloat( vm.hitosPorGrabar[ i ].porcentaje );
        }

        if( sumaPorcentaje == 100 || sumaPorcentaje == 0 ){
            vm.estadoBotonGrabadoFinal = false;
            vm.mostrarAlertaPorcentaje = false;

        }else{
            vm.mostrarAlertaPorcentaje = true;
            vm.mensaje = 'El porcentaje total de hitos debe sumar 100%';
        }
    }

    function upsertHito(){

        var existeHito;


        for( var i = 0; i < vm.hitosPorGrabar.length; i++ ){

            if( vm.hitosPorGrabar[ i ].hitoComercial ==  vm.hitoActual.hitoComercial  ){
                existeHito = true;
            }
        }

        if( existeHito ){
            alert( 'hito ya existe para este proyecto' );
        }else{
            vm.hitosPorGrabar.push
            (
                {
                    hitoComercial: vm.hitoActual.hitoComercial
                    ,porcentaje: vm.hitoActual.porcentaje
                    ,valorUf: vm.hitoActual.valorUf
                    ,fkIdProyecto: vm.proyecto.idProyecto
                }
            );

            vm.totalHitosPorGrabar.totalPorcentaje = vm.totalHitosPorGrabar.totalPorcentaje + parseFloat( vm.hitoActual.porcentaje );
            vm.totalHitosPorGrabar.totalValorUf = vm.totalHitosPorGrabar.totalValorUf + parseFloat( vm.hitoActual.valorUf );

        }

        vm.validarPorcentaje();
        limpiarHito();
    }

    function eliminarHitoPorGrabar(hito) {

        vm.indiceHito = vm.hitosPorGrabar.indexOf(hito);

        if(vm.indiceHito > -1){
            vm.totalHitosPorGrabar.totalPorcentaje = vm.totalHitosPorGrabar.totalPorcentaje - vm.hitosPorGrabar[ vm.indiceHito ].porcentaje;
            vm.totalHitosPorGrabar.totalValorUf = vm.totalHitosPorGrabar.totalValorUf - vm.hitosPorGrabar[ vm.indiceHito ].valorUf;
            vm.hitosPorGrabar.splice(vm.indiceHito, 1);
        }

        hitosComercialesService.eliminarHitoComercial( hito )
        .then( function( results ){
            vm.results = results;
        }
        ).catch(

        );

        validarPorcentaje();
    }

    function init(){

        var indicePuerto = $location.absUrl().search( ':8080' );
        var dominio = $location.absUrl().substring( 0, indicePuerto + 5 );

        obtenerParametros();
        obtenerProyectos();
        obtenerProyectos();
        obtenerDefinicionHitos();

        if( vm.idProyecto != undefined ) {

            obtenerEstado();

            if (vm.idContraparte != undefined) {
                informeFacturacion();
            }
        }

        vm.obtenerHitos();

        if(
                $routeParams.idProyecto != null
            &&  $location.path() != '/proyectos'
        ){
            cargarForaneos();
            vm.idProyectoExist = true;
            vm.valorVendidoExist = true;
            vm.estadoBotonGrabadoFinal = false;
            buscarMailsProyecto();
            buscarCompromisosProyecto();
            buscarNotasProyecto();
            buscarAcuerdosProyecto();


        }else{
            obtenerContrapartes();
            obtenerSistemas_relacionados();
            obtenerRecursos();
        }

        if(   $location.path() != '/estadoProyecto'
            && $location.path() != '/estadoFacturacion'
            && getAction() != 'Nuevo'
            && getAction() != 'Modificar'
            && getAction() != 'Detalle'
        ){
            $location.path( 'proyectos')
        }

    }

    init();

    //--------------------------------------------------- Modal --------------------------------------------------------
    //aquí se genera la ventana emergente a través de $uibModal.open en donde se carga el template con su respectivo controlador
    function showFormProyecto (id_proyecto) {
        var modalInstance = $uibModal.open({
            animation: vm.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'views/eliminarProyecto.html',
            controller: 'ModalInstanceProyecto as vm',
            //le entregamos el id del proyecto que vamos a eliminar
            resolve: {
                proyectoAEliminar: function () {
                    return id_proyecto;
                }
            }
        });
    }


    function actualizaEsfuerzoReal(  id_proyecto ){
        var modalInstance = $uibModal.open({
            animation: vm.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'views/actualizarEsfuerzoReal.html',
            controller: 'ModalInstanceProyecto as vm',
            //le entregamos el id del proyecto que vamos a eliminar
            resolve: {
                proyectoAEliminar: function () {
                    return id_proyecto;
                }
            }
        });
    }




    //-----------------------------------------   datepickerpopup   ----------------------------------------------------

    vm.today = function() {
        vm.proyecto.fechaInicio= new Date();
    };
    vm.today();

    vm.today2 = function() {
        vm.proyecto.fechaTermino= new Date();
    };
    vm.today2();

    vm.clear = function() {
        vm.proyecto.fechaInicio = null;
    };
    vm.clear2 = function() {
        vm.proyecto.fechaTermino = null;
    };
    vm.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };
    vm.dateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        minDate: new Date(),
        startingDay: 1
    };
    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }
    vm.toggleMin = function() {
        vm.inlineOptions.minDate = vm.inlineOptions.minDate ? null : new Date();
        vm.dateOptions.minDate = vm.inlineOptions.minDate;
    };
    vm.toggleMin();
    vm.open1 = function() {
        vm.popup1.opened = true;
    };
    vm.open2 = function() {
        vm.popup2.opened = true;
    };
    vm.setDate = function(year, month, day) {
        vm.dt = new Date(year, month, day);
    };
    vm.formats = ['dd-MM-yyyy']; //['yyyy-MM-dd'];
    vm.format = vm.formats[0];
    vm.altInputFormats = ['M!/d!/yyyy'];

    vm.popup1 = {
        opened: false
    };

    vm.popup2 = {
        opened: false
    };

    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0,0,0,0);

            for (var i = 0; i < vm.events.length; i++) {
                var currentDay = new Date(vm.events[i].date).setHours(0,0,0,0);

                if (dayToCheck === currentDay) {
                    return vm.events[i].status;
                }
            }
        }
        return '';
    }








});

app.controller('ModalInstanceProyecto'
                , function (
                             $uibModalInstance
                            ,proyectoAEliminar
                            ,proyectoService
                            ,esfuerzoRealService
                            ,$route
                            ,$timeout
                        ) {

    var vm = this;

    vm.esfuerzo= {};

    vm.problema = false;
    vm.exito = false;
    vm.eliminar = eliminar;
    vm.cancel = cancel;
    vm.actualizarEsfuerzo = actualizarEsfuerzo;
    vm.mensaje;
    vm.botonesEliminar = true;
    vm.setPath = setPath;

    function setPath( dir ) {
        $location.path(dir);
    }

    function actualizarEsfuerzo(){

    vm.esfuerzo.proyecto = proyectoAEliminar;
    vm.esfuerzo.hora = vm.esfuerzo.hora * 9;


    esfuerzoRealService.actualizarEsfuerzo( vm.esfuerzo )
    .then(function () {
        $route.reload();
    })
    .catch(function (response) {

    })
    .finally(function () {

        $timeout(function(){
                $uibModalInstance.close();
        }
        ,500);

    });

    }

    function eliminar() {

        proyectoService.eliminarProyecto(proyectoAEliminar)
        .then(function ( results ) {

            vm.botonesEliminar = false;


            if(  results.data.error == 0 ){
                vm.exito = true;
                vm.mensaje = "Proyecto eliminado con exito...";

            }else{
                vm.problema = true;
                vm.mensaje = results.data.mensaje;

                if( results.data.error == -3  ){
                    vm.mensaje = vm.mensaje + ' es probable que esten facturados'
                }

            }

            $route.reload();

        })
        .catch(function (response) {

            vm.botonesEliminar = false;

            vm.problema = true;
            vm.mensaje = response.data.mensaje;

        })
        .finally(function () {
            $timeout(function(){
                    $uibModalInstance.close();
                }
                ,3000);        });

        vm.problema = false;


    }
    function cancel() {
        $uibModalInstance.close();
    }

});

