app.controller('parametrosController',  function(    parametroService
                                                    ,utilsService
                                                    ,$uibModal
                                                    ,$timeout
                                                    ,$location
                                                    ,$routeParams
){

    var vm = this;

    vm.animationsEnabled = true;
    vm.valorEsNulo = false;
    vm.parametro = {};
    vm.Parametros = [];
    vm.mensaje = '';


    vm.obtenerParametros = obtenerParametros;
    vm.showFormParametro = showFormParametro;
    vm.setAction = setAction;
    vm.getAction = getAction;
    vm.upsert = upsert;
    vm.setPath = setPath;
    vm.validaValor = validaValor;





    function validaValor(){

        if(     vm.parametro.valor == null
             || vm.parametro.valor == ''
             || vm.parametro.valor == undefined ){

            vm.valorEsNulo = true;
            vm.mensaje = 'El valor del parámetro no debe ser nulo'

        }else{
            vm.valorEsNulo = false;
        }

    }


    function setPath( dir ) {
        $location.path(dir);
    }

    function obtenerParametros () {

        parametroService.buscarParametros()
        .then(function (results) {
            vm.noData = false;

            if(results.data.length < 5){

                vm.Parametros = [

                    {    "id" : 1
                        ,"nombre" : "rojo"
                        ,"valor" : 0
                        ,"descripcion": "límite inferior primer tramo"
                    }
                    , {    "id" : 2
                        ,"nombre" : "rojoAmarillo"
                        ,"valor" : 0.4
                        ,"descripcion": "límite primer-segundo tramo"
                    }
                    ,{    "id" : 3
                        ,"nombre" : "amarilloVerde"
                        ,"valor" : 0.8
                        ,"descripcion": "límite segundo-tercer tramo"
                    }
                    ,{    "id" : 4
                        ,"nombre" : "verdeAmarillo"
                        ,"valor" : 1
                        ,"descripcion": "límite tercer-cuarto tramo"
                    }
                    ,{    "id" : 5
                        ,"nombre" : "amarilloRojo"
                        ,"valor" : 1.4
                        ,"descripcion": "límite cuarto-quinto tramo"
                    }
                ];

                for( var i = 0; i < 5; i++ ){

                    parametroService.upsertParametro( vm.Parametros[ i ] )
                        .then(function (results) {
                           console.log( results.data  );
                        })
                        .catch(function (response) {

                        })
                        .finally(function () {

                        });
                }

            }else{

                vm.Parametros = results.data;
            }
        })
        .catch(function (response) {

        })
        .finally(function () {

        });
    }

    function buscarParametro () {

        parametroService.buscarParametro($routeParams.id)
        .then(function (results) {
            vm.parametro  = results.data[0];
        })
        .catch(function (response) {

        })
        .finally(function () {

        });
    }

    function setAction( accion ){
        utilsService.setAction( accion );
    }

    function getAction(  ){

        return utilsService.getAction(  );
    }

    //
    //función para crear el usuario
    function upsert (){

        parametroService.upsertParametro( vm.parametro )
        .then(function (results) {
            $timeout(function(){
                $location.path("/parametros");
            },500);
        })
        .catch(function (response) {

        })
        .finally(function () {

        });


    };


    //--------------------------------------------------- Modal --------------------------------------------------------
    //aquí se genera la ventana emergente a través de $uibModal.open en donde se carga el template con su respectivo controlador
    function showFormParametro( parametro ) {
        var modalInstance = $uibModal.open({
            animation: vm.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'views/mantenedores/eliminarParametro.html',
            controller: 'ModalInstanceParametro as vm',
            //le entregamos el id del proyecto que vamos a eliminar
            resolve: {
                parametroAEliminar: function () {
                    return parametro;
                }
            }
        });
    }

    //Inicializar
    function init(){

        obtenerParametros();

        if( getAction() == 'Modificar'){
            buscarParametro();
        }

    }

    init();

});

app.controller('ModalInstanceParametro', function ($uibModalInstance
                                                    ,parametroAEliminar
                                                    ,parametroService
                                                    ,$route
                                                    ,$timeout) {

    var vm = this;


    vm.mensaje;
    vm.problema = false;
    vm.exito = false;
    vm.botonesEliminar = true;

    vm.eliminar = eliminar;
    vm.cancel = cancel;
    vm.setPath = setPath;

    function setPath( dir ) {
        $location.path(dir);
    }

    function eliminar() {

    parametroService.eliminarParametro(parametroAEliminar)
    .then(function () {
        $route.reload();
        vm.botonesEliminar = false;
        vm.mensaje = 'Parametro eliminado con exito';
        vm.exito = true;
    })
    .catch(function (response) {
        vm.botonesEliminar = false;
        vm.mensaje = response.data.mensaje;
        vm.problema = true;
    })
    .finally(function () {
        $timeout(function(){
                $uibModalInstance.close();
            }
            ,2000);
    });

    }
    function cancel() {
        $uibModalInstance.close();
    }
});