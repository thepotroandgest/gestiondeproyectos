/**
    * Created by Gustavo on 16-02-2017.
    */
app.controller('hitosComercialesController',  function($uibModal,hitosComercialesService) {
    //Declaración de variables
    var vm = this;//alias del scope
    vm.animationsEnabled = true;
    vm.hitoComercial = {};
    vm.nombresHitos=[
        {
            nombreHito: "Aceptación de la oferta"
        },
        {
            nombreHito: "Generación Certificado"
        },
        {
            nombreHito: "Aceptación Conforme"
        }

    ];


    //Declaración de funciones
    vm.obtenerHitosComerciales = obtenerHitosComerciales;
    vm.showFormHito_comercial = showFormHito_comercial;
    vm.upsertHito = upsertHito;

    //obtener hitos comerciales llamando al servicio, y luego a la base de datos a través de él
    function obtenerHitosComerciales() {
        console.log("lala");
        console.log(vm.nombresHitos);
        hitosComercialesService.obtenerHitosComerciales()
            .then(function (results) {
                vm.noData = false;
                if(results.data.length == 0){
                    vm.noData = true;
                }else{
                    vm.HitosComerciales = results.data;
                }
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }


    //función para crear el hito
    function upsertHito(){
        console.log("asd");


        }
    //Inicializar funciones
    function init(){
        obtenerHitosComerciales();
    }
    init();

    //--------------------------------------------------- Modal --------------------------------------------------------
    //aquí se genera la ventana emergente a través de $uibModal.open en donde se carga el template con su respectivo controlador
    function showFormHito_comercial(hito_comercial) {
        var modalInstance = $uibModal.open({
            animation: vm.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'views/eliminarHito_comercial.html',
            controller: 'ModalInstanceHitoComercial as vm',
            //le entregamos el id del proyecto que vamos a eliminar
            resolve: {
                hitoAEliminar: function () {
                    return hito_comercial;
                }
            }
        });
    }
});
app.controller('ModalInstanceHitoComercial', function ($uibModalInstance, hitoAEliminar,hitosComercialesService,$route) {
    //Declaración de variables
    var vm = this;//alias del scope
    //Declaración de funciones
    vm.eliminar = eliminar;
    vm.cancel = cancel;
    function eliminar() {
        hitosComercialesService.eliminarHito_comercial(hitoAEliminar)
            .then(function () {
                $route.reload();
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
        $uibModalInstance.close();
    }
    function cancel() {
        $uibModalInstance.close();
    }
});