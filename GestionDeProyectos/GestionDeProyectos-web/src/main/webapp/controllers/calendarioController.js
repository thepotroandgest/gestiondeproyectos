app.controller('calendarioController',
            function(        calendarioService
                            ,utilsService
                            ,$location) {

    var vm = this;//alias del scope

    function obtenerCalendario () {
        calendarioService.obtenerCalendario()
            .then(function (results) {
                vm.noData = false;
                if(results.data.length == 0){
                    vm.noData = true;
                }else{
                    vm.Calendario = results.data;
                    abrirCalendario();
                }
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }


    function obtenerCalendarioRecurso () {
        calendarioService.obtenerCalendarioRecurso()
            .then(function (results) {
                vm.noData = false;
                if(results.data.length == 0){
                    vm.noData = true;
                }else{
                    vm.Calendario = results.data;
                    abrirCalendario();
                }
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }


    function abrirCalendario(){

        console.log( utilsService.yyyymmddToStringFormat(new Date()) );


        $(document).ready(function() {
            $('#calendar').fullCalendar({
                defaultDate: utilsService.yyyymmddToStringFormat(new Date()), //'2017-03-06',
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                events: vm.Calendario
            });
        });
    }

    function init(){


        if( $location.path() == '/calendarioRecurso'){
            obtenerCalendarioRecurso();
        }else{
            obtenerCalendario();

        }
    }
    init();
});
