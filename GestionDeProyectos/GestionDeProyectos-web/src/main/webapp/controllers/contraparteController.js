app.controller
(
    'contraparteController'
    ,function
    (
         contraparteService
        ,$uibModal
        ,$timeout
        ,clienteService
        ,$location
        ,$routeParams
    ){

        var vm = this;

        vm.obtenerContrapartes = obtenerContrapartes;
        vm.showFormContraparte = showFormContraparte;
        vm.obtenerClientes = obtenerClientes;
        vm.buscarContraparte = buscarContraparte;
        vm.setAction = setAction;
        vm.getAction = getAction;
        vm.upsert = upsert;
        vm.habilitaBotonGuardar = habilitaBotonGuardar;
        vm.validaRut = validaRut;
        vm.setPath = setPath;



        function validaRut( rut ){

            console.log(  Fn.validaRut( rut )  );

        }


        function setPath( dir ) {
            $location.path(dir);
        }

        vm.animationsEnabled = true;
        vm.botonGuardarDeshabilitado = true;
        vm.contraparte = {};

        function  habilitaBotonGuardar(){

            if(      vm.contraparte.idContraparte != null
                  && vm.contraparte.idContraparte != undefined
                  &&  vm.contraparte.nombre != null
                  && vm.contraparte.nombre != undefined
                  && vm.contraparte.fkRutCliente != null
                    && vm.contraparte.fkRutCliente != undefined  ){

                vm.botonGuardarDeshabilitado = false;

            }

        }




        function setAction( accion ){
            contraparteService.setAction( accion );
        }

        function getAction(  ){
            return contraparteService.getAction(  );
        }


        function buscarContraparte () {
            contraparteService.buscarContraparte($routeParams.id_contraparte)
                .then(function (results) {
                    vm.contraparteList = results.data;
                    vm.contraparte = vm.contraparteList[0];
                })
                .catch(function (response) {

                })
                .finally(function () {

                });
        }

        function obtenerClientes () {

            clienteService.obtenerClientes()
                .then(function (results) {
                    vm.noData = false;
                    if(results.data.length == 0){
                        vm.noData = true;
                    }else{
                        vm.Clientes = results.data;
                    }
                })
                .catch(function (response) {

                })
                .finally(function () {

                });

        }

        function upsert (){


            console.log( 'contraparteService --- ' + vm.contraparte  );

            contraparteService.upsertContraparte(vm.contraparte)
                .then(function (results) {
                    vm.contraparteAgregado = !vm.contraparteAgregado;
                    $timeout(function(){
                        vm.contraparteAgregado = !vm.contraparteAgregado;
                        $location.path("/contraparte");
                    },500);
                })
                .catch(function (response) {

                })
                .finally(function () {

                });

        }

        //obtener contrapartes llamando al servicio, y luego a la base de datos a través de él
        function obtenerContrapartes () {

            contraparteService.obtenerContrapartes()
                .then(function (results) {
                    vm.noData = false;
                    if(results.data.length == 0){
                        vm.noData = true;
                    }else{
                        vm.Contrapartes = results.data;
                    }
                })
                .catch(function (response) {

                })
                .finally(function () {

                });

        }

        //--------------------------------------------------- Modal --------------------------------------------------------
        //aquí se genera la ventana emergente a través de $uibModal.open en donde se carga el template con su respectivo controlador
        function showFormContraparte(contraparte) {
            var modalInstance = $uibModal.open({
                animation: vm.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/mantenedores/eliminarContraparte.html',
                controller: 'ModalInstanceContraparte as vm',
                //le entregamos el id del proyecto que vamos a eliminar
                resolve: {
                    contraparteAEliminar: function () {
                        return contraparte;
                    }
                }
            });
        }
        //iniciar funciones
        function init(){
            obtenerContrapartes();
            obtenerClientes();

            if( $routeParams.id_contraparte != null) {
                buscarContraparte();
            }
        }
        init();
});

app.controller('ModalInstanceContraparte', function ($uibModalInstance
                                                    ,contraparteAEliminar
                                                    ,contraparteService
                                                    ,$route
                                                    ,$timeout
                                                    ,$location) {
    var vm = this;

    vm.mensaje;
    vm.problema = false;
    vm.exito = false;
    vm.botonesEliminar = true;


    vm.eliminar = eliminar;
    vm.cancel = cancel;
    vm.setPath = setPath;

    function setPath( dir ) {
        $location.path(dir);
    }

    function eliminar() {

        contraparteService.eliminarContraparte(contraparteAEliminar)
        .then(function ( results ) {
            $route.reload();
            vm.botonesEliminar = false;
            vm.mensaje = 'Contraparte Eliminada con exito';
            vm.exito = true;
        })
        .catch(function (response) {

            vm.botonesEliminar = false;
            vm.mensaje = response.data.mensaje;
            vm.problema = true;
        })
        .finally(function () {
            $timeout(function(){
                    $uibModalInstance.close();
                }
                ,2000);
        });
        }



    function cancel() {
        $uibModalInstance.close();
    }
});