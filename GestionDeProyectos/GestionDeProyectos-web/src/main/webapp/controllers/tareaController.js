/**
    * Created by Gustavo on 06-03-2017.
    */
app.controller('tareaController',  function($uibModal,tareaService, proyectoService, recursosService, $timeout) {
    //Declaración de variables
    var vm = this;//alias del scope
    vm.animationsEnabled = true;
    vm.tarea = {};
    vm.obtieneProyectos = obtieneProyectos;
    vm.obtieneRecursos = obtieneRecursos;
    vm.recursos;
    vm.obtRecursos = {};
    vm.flagSinTarea = false;


    //Declaración de funciones
    vm.obtenerTareas = obtenerTareas;
    vm.showFormTarea= showFormTarea;
    vm.upsertTarea = upsertTarea;



    //insertar o actualizar tarea

    function upsertTarea () {
        //if(validar()){


        //vm.tarea.id_proyecto = vm.tarea.id_proyecto.id_proyecto;
        //vm.tarea.rut_recurso = vm.tarea.rut_recurso.rut_recurso;
        console.log(   vm.tarea );

        tareaService.upsertTarea( vm.tarea )
                .then(function (results) {
                    vm.tareaAgregada = !vm.tareaAgregada;
                    $timeout(function () {
                        vm.tareaAgregada = !vm.tareaAgregada;
                        $location.path("/upsertTarea");
                    }, 500);

                })

                .catch(function (response) {

                })
                .finally(function () {

                });
     };

    //obtener recursos llamando al servicio, y luego a la base de datos a través de él
    function obtenerTareas() {

        tareaService.obtenerTareas()
            .then(function (results) {
                console.log( results );


                vm.noData = false;
                if(results.data.length == 0){

                    vm.noData = true;


                }else{
                    vm.obtenerTareas = results.data;


            }
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }

    function obtieneProyectos(){
        proyectoService.obtenerProyectos( ).
            then(function (results){
                vm.noData =false;
                if(results.data.length == 0){
                    vm.noData = true;
                }else{
                    vm.obtieneProyectos = results.data;
                }
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }

    function obtieneRecursos(){
        recursosService.obtenerRecursos().
            then(function(results){
                vm.recursos = results.data;
                vm.noData =false;
                if(results.data.length == 0){
                    vm.noData = true;
                }else{
                    vm.obtieneRecursos = results.data;
                }

            })
            .catch(function (response) {

            })
            .finally(function (){
        });

    }

   // función para validar que los campos no estén vacíos

    //function validar (){
    //    vm.flagSec_tarea = false;
    //    vm.flagId_traspaso = false;
    //    vm.flagdescripcion = false;
    //    vm.flagEsfuerzo_estimado = false;
    //    vm.flagEsfuerzo_real = false;
    //    vm.flagFecha_inicio_estimado = false;
    //    vm.flagFecha_inicio_real = false;
    //    vm.flagFecha_termino_estimado = false;
    //    vm.flagFecha_termino_real = false;
    //    vm.flagPorcentaje_estimado = false;
    //    vm.flagId_proyecto = false;
    //    vm.flagRut_recurso = false;
    //    vm.tareaAgregada = false;
    //    vm.tareaNoAgregada = false;
    //
    //    var rsp = true;
    //
    //    if(!vm.tarea.id_traspaso){
    //        vm.flagId_traspaso = true;
    //        rsp = false;
    //    }
    //    if(!vm.tarea.descripcion){
    //        vm.flagDescripcion = true;
    //        rsp = false;
    //    }
    //    if(!vm.tarea.esfuerzo_estimado){
    //        vm.flagEsfuerzo_estimado = true;
    //        rsp = false;
    //    }
    //    if(!vm.tarea.esfuerzo_real){
    //        vm.flagEsfuerzo_real = true;
    //        rsp = false;
    //    }
    //    if(!vm.tarea.fecha_inicio_estimado){
    //        vm.flagFecha_inicio_estimado = true;
    //        rsp = false;
    //    }
    //    if(!vm.fecha_inicio_real){
    //        vm.flagFecha_inicio_real = true;
    //        rsp = false;
    //    }
    //    if(!vm.fecha_termino_estimado){
    //        vm.flagFecha_termino_estimado = true;
    //        rsp = false;
    //    }
    //    if(!vm.tarea.porcentaje_estimado){
    //        vm.flagPorcentaje_estimado = true;
    //        rsp = false;
    //    }
    //    if(!vm.tarea.fk_id_proyecto){
    //        vm.flagId_proyecto = true;
    //        rsp=false;
    //    }
    //    if(!vm.tarea.fk_rut_recurso){
    //        vm.flagRut_recurso = true;
    //        rsp = false;
    //    }
    //    return rsp;
    //}

    //Iniciar funciones
    function init(){

        obtieneRecursos();
        obtieneProyectos();
    }

    init();

    //-----------------------------------------   datepickerpopup   ----------------------------------------------------

    vm.today = function() {
        vm.tarea.fecha_inicio_estimado = new Date();
    };
    vm.today();

    vm.today2 = function() {
        vm.tarea.fecha_inicio_real = new Date();
    };
    vm.today2();

    vm.clear = function() {
        vm.tarea.fecha_termino_estimado = null;
    };
    vm.clear2 = function() {
        vm.tarea.fecha_termino_real = null;
    };
    vm.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };
    vm.dateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        minDate: new Date(),
        startingDay: 1
    };
    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }
    vm.toggleMin = function() {
        vm.inlineOptions.minDate = vm.inlineOptions.minDate ? null : new Date();
        vm.dateOptions.minDate = vm.inlineOptions.minDate;
    };
    vm.toggleMin();
    vm.open1 = function() {
        vm.popup1.opened = true;
    };
    vm.open2 = function() {
        vm.popup2.opened = true;
    };
    vm.open3 = function(){
        vm.popup3.opened = true;
    }
    vm.open4 = function(){
        vm.popup4.opened = true;
    }
    vm.setDate = function(year, month, day) {
        vm.dt = new Date(year, month, day);
    };
    vm.formats = ['yyyy-MM-dd'];
    vm.format = vm.formats[0];
    vm.altInputFormats = ['M!/d!/yyyy'];

    vm.popup1 = {
        opened: false
    };
    vm.popup2 = {
        opened: false
    };

    vm.popup3 = {
        opened : false
    };

    vm.popup4 = {
        opened:false
    };
    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0,0,0,0);

            for (var i = 0; i < vm.events.length; i++) {
                var currentDay = new Date(vm.events[i].date).setHours(0,0,0,0);

                if (dayToCheck === currentDay) {
                    return vm.events[i].status;
                }
            }
        }
        return '';
    }




    //--------------------------------------------------- Modal --------------------------------------------------------
    //aquí se genera la ventana emergente a través de $uibModal.open en donde se carga el template con su respectivo controlador
    function showFormTarea(tarea) {
        var modalInstance = $uibModal.open({
            animation: vm.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'views/eliminarTarea.html',
            controller: 'ModalInstanceTarea as vm',
            //le entregamos el id del proyecto que vamos a eliminar
            resolve: {
                tareaAEliminar: function () {
                    return tarea;
                }
            }
        });
    }
});

app.controller('ModalInstanceTarea', function ($uibModalInstance, tareaService, tareaAEliminar, $route) {
    //Declaración de variables
    var vm = this;//alias del scope
    //Declaración de funciones
    vm.eliminar = eliminar;
    vm.cancel = cancel;
    function eliminar() {
        tareaService.eliminarTarea(tareaAEliminar)
            .then(function () {
                $route.reload();
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
        $uibModalInstance.close();
    }
    function cancel() {
        $uibModalInstance.close();
    }
});
