app.controller('clienteController',  function(clienteService,$uibModal,$timeout,$location, $routeParams) {

    var vm = this;

    vm.animationsEnabled = true;
    vm.deshabilitarBotonGuardar = true;

    vm.cliente = {};
    vm.Clientes = [];

    vm.obtenerClientes = obtenerClientes;
    vm.showFormCliente = showFormCliente;
    vm.setAction = setAction;
    vm.getAction = getAction;
    vm.upsert = upsert;
    vm.setPath = setPath;
    vm.habilitarBotonGuardar = habilitarBotonGuardar;
    vm.validaRut = validaRut;
    vm.mensaje = '';


    function validaRut( rut ){

        if( Fn.validaRut( rut ) == false  ){
            vm.mensaje = 'rut inválido';
        }else{
            vm.mensaje = '';
        }

    }

    function habilitarBotonGuardar(){

        if(    vm.cliente.rutCliente != null
            && vm.cliente.rutCliente != undefined
            && vm.cliente.nombre     != null
            && vm.cliente.nombre    != undefined ){
            vm.deshabilitarBotonGuardar = false;
        }

    }


    function setPath( dir ) {
        $location.path(dir);
    }

    //obtener clientes llamando al servicio, y luego a la base de datos a través de él
    function obtenerClientes () {


        console.log( "############################## ");


        clienteService.obtenerClientes()
        .then(  function (results) {

            vm.noData = false;

            if(results.data.length == 0){
                vm.noData = true;
            }else{
                vm.Clientes = results.data;
            }

        })
        .catch(function (response) {

            console.log(  response  );
        })
        .finally(function () {

        });
    }

    function buscarCliente () {




        console.log( 'buscarCliente'  );

        clienteService.buscarCliente($routeParams.rut_cliente)
            .then(function (results) {

                vm.cliente = results.data[0];

            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }


    function setAction( accion ){

        clienteService.setAction( accion );

    }

    function getAction(  ){

        return clienteService.getAction(  );
    }


    //función para crear el usuario
    function upsert (){

        console.log(  vm.cliente );

        //if(validar()){
        clienteService.upsertCliente(vm.cliente)
            .then(function (results) {
                vm.clienteAgregado = !vm.clienteAgregado;
                $timeout(function(){
                    vm.clienteAgregado = !vm.clienteAgregado;
                    vm.setAction( null );
                    $location.path("/clientes");
                },500);
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
        //}else{
        //    vm.clienteNoAgregado = !vm.clienteNoAgregado;
        //    $timeout(function(){vm.clienteNoAgregado = !vm.clienteNoAgregado},3000);
        //}
    };


    //--------------------------------------------------- Modal --------------------------------------------------------
    //aquí se genera la ventana emergente a través de $uibModal.open en donde se carga el template con su respectivo controlador
    function showFormCliente(cliente) {
        var modalInstance = $uibModal.open({
            animation: vm.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'views/mantenedores/eliminarCliente.html',
            controller: 'ModalInstanceCliente as vm',
            //le entregamos el id del proyecto que vamos a eliminar
            resolve: {
                clienteAEliminar: function () {
                    return cliente;
                }
            }
        });
    }

    //Inicializar
    function init(){

        console.log( "############################## ");

        obtenerClientes();

        if( $routeParams.rut_cliente != null ) {
            buscarCliente();
        }

    }

    init();

});

app.controller('ModalInstanceCliente', function ($uibModalInstance
                                                ,clienteAEliminar
                                                ,clienteService
                                                ,$route
                                                ,$timeout
                                                ,$location) {

    var vm = this;

    vm.mensaje;
    vm.problema = false;
    vm.exito = false;
    vm.botonesEliminar = true;

    vm.eliminar = eliminar;
    vm.cancel = cancel;
    vm.setPath = setPath;

    function setPath( dir ) {
        $location.path(dir);
    }



    function eliminar() {

    clienteService.eliminarCliente(clienteAEliminar)
        .then(function () {
            $route.reload();
            vm.botonesEliminar = false;
            vm.mensaje = 'Cliente eliminado con exito';
            vm.exito = true;
        })
        .catch(function (response) {
            vm.botonesEliminar = false;
            vm.mensaje = response.data.mensaje;
            vm.problema = true;

        })
        .finally(function () {
            $timeout(function(){
                    $uibModalInstance.close();
                }
                ,3000);
        });

    }
    function cancel() {
        $uibModalInstance.close();
    }
});