app.controller('modificarRecursoController',  function(recursosService,$routeParams,$timeout,$location) {
    //declaración de variables
    var vm = this;//alias del scope
    vm.disable = true;
    vm.recurso = {};
    //Declaración de funciones
    vm.buscarRecurso = buscarRecurso;
    vm.upsert = upsert;
    //obtener el recurso llamando al servicio, y luego a la base de datos a través de él
    function buscarRecurso () {
        recursosService.buscarRecurso($routeParams.rut_recurso)
            .then(function (results) {
                vm.recursoList = results.data;
                vm.recurso = vm.recursoList[0];
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }
    //Realizar actualización
    function upsert (){
        if(validar()){
            recursosService.upsertRecurso(vm.recurso)
                .then(function (results) {
                    vm.recursoModificado = !vm.recursoModificado;
                    $timeout(function(){
                        vm.recursoModificado = !vm.recursoModificado;
                        $location.path("/recurso");
                    },500);
                })
                .catch(function (response) {

                })
                .finally(function () {

                });
        }else{
            vm.recursoNoModificado = !vm.recursoNoModificado;
            $timeout(function(){vm.recursoNoModificado = !vm.recursoNoModificado},3000);
        }
    }
    //Validar
    //función para validar los campos
    function validar (){
        vm.flagId_recurso = false;
        vm.flagNombre = false;
        vm.recursoModificado = false;
        vm.recursoNoModificado = false;
        var rsp = true;
        if(!vm.recurso.id_recurso){
            vm.flagId_recurso = true;
            rsp = false;
        }
        if(!vm.recurso.nombre){
            vm.flagNombre = true;
            rsp = false;
        }
        return rsp;
    }
    //Inicializar funciones
    function init(){
        buscarRecurso();
    }
    init();
});
