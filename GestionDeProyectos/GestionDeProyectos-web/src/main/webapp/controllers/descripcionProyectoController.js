/**
    * Created by Gustavo on 10-02-2017.
    */
app.controller('descripcionProyectoController',  function($routeParams,proyectoService,contraparteService,bitacoraService,facturaService) {
    //declaración de variables
    var vm = this;//alias del scope
    vm.proyecto = {};
    //función para inicializar
    //obtener el proyecto llamando al servicio, y luego a la base de datos a través de él
    function buscarProyecto () {
        proyectoService.buscarProyecto($routeParams.id_proyecto)
            .then(function (results) {
                vm.proyectoList = results.data;
                vm.proyecto = vm.proyectoList[0];
                //obtener la contraparte llamando al servicio, y luego a la base de datos a través de él
                function buscarContraparte () {
                    contraparteService.buscarContraparte(vm.proyecto.fk_id_contraparte)
                        .then(function (results) {
                            vm.contraparteList = results.data;
                            vm.contraparte = vm.contraparteList[0];
                        })
                        .catch(function (response) {

                        })
                        .finally(function () {

                        });
                }
                buscarContraparte();
                //Obtener bitacora por proyectos
                function buscarBitacoraProyecto () {
                    bitacoraService.buscarBitacorasProyectos(vm.proyecto.id_proyecto)
                        .then(function (results) {
                            vm.bitacoraCompromiso = results.data;
                            vm.bitacoraEmail = results.data;
                            vm.bitacoraNota = results.data;
                        })
                        .catch(function (response) {

                        })
                        .finally(function () {

                        });
                }
                buscarBitacoraProyecto();
                //obtener el facturas por proyectos llamando al servicio, y luego a la base de datos a través de él
                function buscarFacturaProyecto () {
                    facturaService.buscarFacturaProyecto(vm.proyecto.id_proyecto)
                        .then(function (results) {
                            vm.facturas = results.data;
                        })
                        .catch(function (response) {

                        })
                        .finally(function () {

                        });
                }
                buscarFacturaProyecto();
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }
    //Inicializar funciones
    function init(){
        //Primero se buscar el proyecto
        buscarProyecto();
    }
    init();
});
