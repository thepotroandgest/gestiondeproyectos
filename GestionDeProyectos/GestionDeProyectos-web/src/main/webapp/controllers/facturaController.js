/**
 * Created by Gustavo on 16-02-2017.
 */
app.controller('facturaController'
    ,  function(
                 $uibModal
                ,facturaService
                ,proyectoService
                ,hitosComercialesService
                ,$routeParams
                ,utilsService
                ,$location
                ,$timeout
    ) {

    var vm = this;
    var sumaAbono = 0;
    var abonoAntiguo = 0;
    var diferenciaAbonos = 0;

    vm.factura = {};
    vm.animationsEnabled = true;
    vm.Proyectos = [];
    vm.idProyecto;
    vm.hito;
    vm.abono;
    vm.hitos = [];
    vm.hitosPorFacturar = [];
    vm.pHDesabilitado = true;
    vm.btnAgregarDeshabilitado = true;
    vm.total = {
                     porcentaje:0
                    ,valorUF: 0
                    ,abono: 0
                    ,saldo: 0
                };
    vm.Facturas = [];
    vm.facturaDetalle = {};
    vm.btnGuardarDeshabilitado = true;
    vm.facturaRepetida = false;

    vm.obtenerFacturas = obtenerFacturas;
    vm.showFormFactura = showFormFactura;
    vm.upsert = upsert;
    vm.buscarFactura = buscarFactura;
    vm.getAction = getAction;
    vm.setAction = setAction;
    vm.obtenerHitos = obtenerHitos;
    vm.upsertHito = upsertHito;
    vm.limpiarHito = limpiarHito;
    vm.limpiarHitosPorFacturar = limpiarHitosPorFacturar;
    vm.validarAbonoMax = validarAbonoMax;
    vm.habilitarBotonAgregado = habilitarBotonAgregado;
    vm.eliminarHitoPorGrabar = eliminarHitoPorGrabar;
    vm.habilitarHitos = habilitarHitos;
    vm.desHabilitarHitos = desHabilitarHitos;
    vm.setingHito = setingHito;
    vm.activaChecks = activaChecks;
    vm.pasarHitosPorModificar = pasarHitosPorModificar;
    vm.setPath = setPath;
    vm.validarFacturaEnCero = validarFacturaEnCero;
    vm.evaluarCampos = evaluarCampos;


    function evaluarCampos(){

        if(
                vm.factura.numeroFactura == '' || vm.factura.numeroFactura == null || vm.factura.numeroFactura == undefined
            ||  vm.factura.estado == '' || vm.factura.estado == null || vm.factura.estado == undefined
        ){
            vm.btnGuardarDeshabilitado = true;
        }else{
            vm.btnGuardarDeshabilitado = false;
        }


    }

    function validarFacturaEnCero(   ){

        for( var i = 0; i < vm.Facturas.length; i++   ){

            if(     vm.Facturas[ i ].numeroFactura == vm.factura.numeroFactura
                 && vm.Facturas[ i ].montoUF != 0      ){

                vm.facturaRepetida = true;
                vm.btnGuardarDeshabilitado = true;
            }else{

                vm.facturaRepetida = false;
                vm.btnGuardarDeshabilitado = false;
            }
        }
    }

    function setPath( dir ) {

        console.log( dir );

        $location.path(dir);
    }


    function getDetalleFactura(   ){


        facturaService.buscarFactura(   $routeParams.nFactura  )
        .then(function (results) {

            vm.facturaDetalle = results.data[0];

            return hitosComercialesService.buscarHitoPorFactura(  $routeParams.nFactura   );
        })
        .then(function(results){
              vm.hitos = results.data;
        })
        .catch(function (response) {

        })
        .finally(function () {

        });

    }

    function pasarHitosPorModificar(){

        vm.hitosPorFacturar = vm.hitos;

        for( var i = 0; i < vm.hitos.length; i++ ) {


            if (document.getElementById(i + 'chk').checked == false) {

                document.getElementById(i + 'chk').checked = true;

            } else {
                document.getElementById(i + 'chk').checked = false;
                vm.hitosPorFacturar = [];
            }
        }

    }

    function activaChecks(){

        for( var i = 0; i < vm.hitos.length; i++ ){

            if( document.getElementById( i + 'chk').checked == false){

                document.getElementById( i + 'chk').checked = true;

                if(    !validarHitoRepetido( vm.hito[i] )
                    && vm.hitos[ i ].saldo != null
                    && vm.hitos[ i ].saldo > 0  ){

                    sumaAbono = sumaAbono + vm.hitos[ i ].saldo;

                    vm.hitosPorFacturar.push
                    (
                        {
                            fkIdProyecto: vm.idProyecto
                            ,hitoComercial: vm.hitos[ i ].hitoComercial
                            ,abono: vm.hitos[ i ].saldo
                        }
                    );
                }

            }else{
                document.getElementById( i + 'chk').checked = false;
                vm.hitosPorFacturar = [];
                sumaAbono = 0;
            }
        }

        vm.factura.montoUF = sumaAbono;

    }

    function setingHito( hito ){

        document.getElementById( 'abonoTxt').disabled = false;
        vm.hito = hito.hitoComercial;
        vm.abono = hito.saldo;
        document.getElementById( 'abonoTxt').focus();
        habilitarBotonAgregado();
    }


    function habilitarHitos(indice){

        abonoAntiguo = parseFloat(document.getElementById( indice).value.replace(',', '.'));

        document.getElementById( indice ).disabled = false;
        document.getElementById( indice ).focus();

    }

    function desHabilitarHitos( indice ){

        vm.factura.montoUF = vm.factura.montoUF - ( abonoAntiguo - parseFloat(document.getElementById( indice).value.replace( ',', '.') ) );
        sumaAbono = vm.factura.montoUF;

        document.getElementById( indice ).disabled = true;

    }

    function eliminarHitoPorGrabar(hito) {

        vm.indiceHito = vm.hitosPorFacturar.indexOf(hito);
        vm.factura.montoUF = vm.factura.montoUF - hito.abono;
        sumaAbono = sumaAbono - hito.abono;

        if(vm.indiceHito > -1){
            vm.hitosPorFacturar.splice(vm.indiceHito, 1);
        }

        facturaService.elimiarHitoFactura( parseInt(vm.factura.numeroFactura) ,  vm.idProyecto, hito.hitoComercial  )
        .then( function(  results   ){

        })
        .catch(

        )
            .finally(

            );

    }

    function habilitarBotonAgregado(){

        if( vm.abono == null || vm.abono == '' || isNaN( vm.abono) ){
            document.getElementById( 'btnAgregado').disabled = true;
        }else{
            document.getElementById( 'btnAgregado').disabled = false;

        }
    }

    function validarAbonoMax(){

        var maxSaldo = -1;
        var maxValorUF = -1;
        var indiceHitoSeleccionado = -1;

        for( var i = 0; i < vm.hitos.length; i++ ){

            if(  vm.hitos[ i ].hitoComercial == vm.hito   ){
                indiceHitoSeleccionado = i;
            }
        }

        if( typeof    vm.hitos[ indiceHitoSeleccionado] != 'undefined' ) {

            maxSaldo = vm.hitos[indiceHitoSeleccionado].saldo;
            maxValorUF = vm.hitos[indiceHitoSeleccionado].valorUf;

            if (    maxSaldo != null
                &&  maxSaldo > -1
                && vm.abono > maxSaldo ) {

                vm.abono = '';

            } else if (     maxValorUF != null
                        &&    maxValorUF > -1
                        && vm.abono > maxValorUF ) {
                vm.abono = '';
            }
        }

    }

    function limpiarHito(){
        vm.hito = '';
        vm.abono = '';
    }

    function limpiarHitosPorFacturar(){
        vm.hitosPorFacturar = [];
        vm.factura.montoUF = '';
        limpiarHito();
        sumaAbono = 0;

        document.getElementById( 'chkTodo').checked = false;

        vm.total = {
            porcentaje:0
            ,valorUF: 0
            ,abono: 0
            ,saldo: 0
        };

    }

    function validarHitoRepetido( nombreHito ){

        var repetido;

        for( var i = 0; i < vm.hitosPorFacturar.length; i++ ){

            if( vm.hitosPorFacturar[ i ].hitoComercial ==  nombreHito ){
                repetido = true;
                break;
            }else{
                repetido = false;
            }
        }

        return repetido;
    }


    function upsertHito(){

        if( validarHitoRepetido( vm.hito ) ){
            alert( 'hito ya existe para este proyecto' );
        }else{

            sumaAbono = sumaAbono + vm.abono;
            vm.factura.montoUF = sumaAbono;

            vm.hitosPorFacturar.push
            (
                {
                    fkIdProyecto: vm.idProyecto
                    ,hitoComercial: vm.hito
                    ,abono: vm.abono
                }
            );
        }

        limpiarHito();
    }

    function obtenerHitos(  ){

        hitosComercialesService.buscarHitoComercial( vm.idProyecto )
            .then( function( results ){


                vm.hitos = results.data;

                for( var i = 0; i < vm.hitos.length; i++ ){


                    vm.total.porcentaje = vm.total.porcentaje + vm.hitos[ i ].porcentaje;
                    vm.total.valorUF = vm.total.valorUF + vm.hitos[ i ].valorUf;
                    vm.total.abono = vm.total.abono + vm.hitos[ i ].abono;
                    vm.total.saldo = vm.total.saldo + vm.hitos[ i].saldo;
                }

            })
            .catch( function (response) {

            });
    }

    function obtenerProyectos () {

    proyectoService.obtenerProyectos()
        .then(function (results) {

            if(results.data.length == 0){vm.factura = results.data[0];

                vm.noData = true;
            }else{
                vm.Proyectos = results.data;
            }

        })
        .catch(function (response) {

        })
        .finally(function () {

        });
    }

    function upsert () {

        vm.factura.abonosHitos =  vm.hitosPorFacturar;

        facturaService.upsertFactura(vm.factura)
            .then(function (results) {
                vm.facturaAgregado = !vm.facturaAgregado;

                $timeout(function () {
                    vm.facturaAgregado = !vm.facturaAgregado;
                    $location.path("/factura");
                }, 500);

            })
            .catch(function (response) {

            })
            .finally(function () {

            });



    }

    //obtener facturas llamando al servicio, y luego a la base de datos a través de él
    function obtenerFacturas () {
        facturaService.obtenerFacturas()
            .then(function (results) {
                vm.noData = false;
                if(results.data.length == 0){
                    vm.noData = true;
                }else{
                    vm.Facturas = results.data;
                }
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }

    function buscarFactura(){

        proyectoService.obtenerProyectos()
        .then(function(results){
            vm.Proyectos = results.data;
            return facturaService.buscarFactura( $routeParams.idFactura );
        })
        .then( function( results ){
            vm.factura = results.data[ 0 ];
            vm.factura.fechaEmision =  utilsService.stringToDdmmyyyyy(vm.factura.fechaEmision);
            vm.idProyecto = vm.factura.idProyecto;
            return hitosComercialesService.buscarHitoComercial( vm.idProyecto );
        })
        .then( function( results ){

            vm.hitos = results.data;

            for( var i = 0; i < vm.hitos.length; i++ ){

                vm.total.porcentaje = vm.total.porcentaje + vm.hitos[ i ].porcentaje;
                vm.total.valorUF = vm.total.valorUF + vm.hitos[ i ].valorUf;
                vm.total.abono = vm.total.abono + vm.hitos[ i ].abono;
                vm.total.saldo = vm.total.saldo + vm.hitos[ i].saldo;
            }


        })
        .catch(function(response){
        })
        .finally(function(){

        });
    }

    function setAction(accion){
        facturaService.setAction(accion);
    }

    function getAction(){
        return facturaService.getAction();
    }

    function init() {

        obtenerFacturas();
        obtenerProyectos();

        if($routeParams.idFactura != null )
        {
            buscarFactura();
        }

        if( $routeParams.nFactura != null  ){
            getDetalleFactura();
        }

    }
    init();


    //-----------------------------------------   datepickerpopup   ----------------------------------------------------

    vm.today = function() {
        vm.factura.fechaEmision= new Date();
    };
    vm.today();

    vm.clear = function() {
        vm.factura.fechaEmision = null;
    };
    vm.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };
    vm.dateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        minDate: new Date(),
        startingDay: 1
    };
    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }
    vm.toggleMin = function() {
        vm.inlineOptions.minDate = vm.inlineOptions.minDate ? null : new Date();
        vm.dateOptions.minDate = vm.inlineOptions.minDate;
    };
    vm.toggleMin();
    vm.open1 = function() {
        vm.popup1.opened = true;
    };
    vm.setDate = function(year, month, day) {
        vm.dt = new Date(year, month, day);
    };
    vm.formats = ['dd-MM-yyyy'];
    vm.format = vm.formats[0];
    vm.altInputFormats = ['M!/d!/yyyy'];

    vm.popup1 = {
        opened: false
    };
    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0,0,0,0);

            for (var i = 0; i < vm.events.length; i++) {
                var currentDay = new Date(vm.events[i].date).setHours(0,0,0,0);

                if (dayToCheck === currentDay) {
                    return vm.events[i].status;
                }
            }
        }
        return '';
    }






        //--------------------------------------------------- Modal --------------------------------------------------------
        //aquí se genera la ventana emergente a través de $uibModal.open en donde se carga el template con su respectivo controlador
        function showFormFactura(factura) {
            var modalInstance = $uibModal.open({
                animation: vm.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/eliminarFactura.html',
                controller: 'ModalInstanceFactura as vm',
                //le entregamos el id del proyecto que vamos a eliminar
                resolve: {
                    facturaAEliminar: function () {
                        return factura;
                    }
                }
            });
        }





});


app.controller('ModalInstanceFactura', function ($uibModalInstance
                                                ,facturaAEliminar
                                                ,facturaService
                                                ,$route) {
    //Declaración de variables
    var vm = this;//alias del scope
    //Declaración de funciones
    vm.eliminar = eliminar;
    vm.cancel = cancel;
    function eliminar() {
        facturaService.eliminarFactura(facturaAEliminar)
            .then(function () {
                $route.reload();
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
        $uibModalInstance.close();
    }
    function cancel() {
        $uibModalInstance.close();
    }
});
