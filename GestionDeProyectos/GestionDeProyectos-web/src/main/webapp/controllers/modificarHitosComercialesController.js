/**
    * Created by Gustavo on 16-02-2017.
    */
app.controller('modificarHitosComercialesController',  function(hitosComercialesService,$routeParams,$timeout,$location) {
    //declaración de variables
    var vm = this;//alias del scope
    vm.disable = true;
    vm.hito_comercial = {};
    //Declaración de funciones
    vm.buscarHito_comercial = buscarHito_comercial;
    vm.actualizar = actualizar;
    //obtener el recurso llamando al servicio, y luego a la base de datos a través de él
    function buscarHito_comercial () {
        hitosComercialesService.buscarHito_comercial($routeParams.sec_hito)
            .then(function (results) {
                vm.hitoList = results.data;
                vm.hito_comercial = vm.hitoList[0];
            })
            .catch(function (response) {

            })
            .finally(function () {

            });
    }
    //Realizar actualización
    function actualizar (){
        if(validar()){
            hitosComercialesService.actualizarHito_comercial(vm.hito_comercial)
                .then(function (results) {
                    vm.hitoModificado = !vm.hitoModificado;
                    $timeout(function(){
                        vm.hitoModificado = !vm.hitoModificado;
                        $location.path("/hitosComerciales");
                    },700);
                })
                .catch(function (response) {

                })
                .finally(function () {

                });
        }else{
            vm.hitoNoModificado = !vm.hitoNoModificado;
            $timeout(function(){vm.hitoNoModificado = !vm.hitoNoModificado},3000);
        }
    }
    //Validar
    //función para validar los campos
    function validar (){
        vm.flagTipo = false;
        vm.flagPorcentaje = false;
        vm.flagValor_uf = false;
        vm.hitoModificado = false;
        vm.hitoNoModificado = false;
        var rsp = true;
        if(!vm.hito_comercial.tipo){
            vm.flagTipo = true;
            rsp = false;
        }
        if(!vm.hito_comercial.porcentaje){
            vm.flagPorcentaje = true;
            rsp = false;
        }
        if(!vm.hito_comercial.valor_uf){
            vm.flagValor_uf = true;
            rsp = false;
        }
        return rsp;
    }
    //Inicializar funciones
    function init(){
        buscarHito_comercial();
    }
    init();
});
