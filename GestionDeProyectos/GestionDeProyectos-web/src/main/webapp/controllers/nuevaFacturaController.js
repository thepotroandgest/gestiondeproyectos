/**
* Created by Gustavo on 16-02-2017.
*/
app.controller('nuevaFacturaController',  function(facturaService,$timeout,$location) {
    //Declaración de variables
    var vm = this;//alias del scope
    vm.factura = {};

    //Declaración de funciones
    vm.upsert = upsert;

    //función para crear el usuario
    function upsert (){
        //if(validar()){
            console.log(vm.factura);
            facturaService.upsertFactura(vm.factura)
                .then(function (results) {
                    vm.facturaAgregado = !vm.facturaAgregado;
                    $timeout(function(){
                        vm.facturaAgregado = !vm.facturaAgregado;
                        $location.path("/factura");
                    },700);
                })
                .catch(function (response) {

                })
                .finally(function () {

                });



        //}else{
        //    vm.facturaNoAgregado = !vm.facturaNoAgregado;
        //    $timeout(function(){vm.facturaNoAgregado = !vm.facturaNoAgregado},3000);
        //}
    }
    //función para validar los campos
    //function validar (){
    //    vm.flagId_factura = false;
    //    vm.flagFecha_emision = false;
    //    vm.flagEstado = false;
    //    vm.flagObservacion = false;
    //    vm.flagValor_uf_real = false;
    //    vm.facturaAgregado = false;
    //    vm.facturaNoAgregado = false;
    //    var rsp = true;
    //    if(!vm.factura.id_factura){
    //        vm.flagId_factura = true;
    //        rsp = false;
    //    }
    //    if(!vm.factura.fecha_emision){
    //        vm.flagFecha_emision = true;
    //        rsp = false;
    //    }
    //    if(!vm.factura.estado){
    //        vm.flagEstado = true;
    //        rsp = false;
    //    }
    //    if(!vm.factura.observacion){
    //        vm.flagObservacion = true;
    //        rsp = false;
    //    }
    //    if(!vm.factura.valor_uf_real){
    //        vm.flagValor_uf_real = true;
    //        rsp = false;
    //    }
    //    return rsp;
    //}
    //-----------------------------------------   datepickerpopup   ----------------------------------------------------

    vm.today = function() {
        vm.factura.fecha_emision= new Date();
    };
    vm.today();

    vm.clear = function() {
        vm.factura.fecha_emision = null;
    };
    vm.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };
    vm.dateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        minDate: new Date(),
        startingDay: 1
    };
    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }
    vm.toggleMin = function() {
        vm.inlineOptions.minDate = vm.inlineOptions.minDate ? null : new Date();
        vm.dateOptions.minDate = vm.inlineOptions.minDate;
    };
    vm.toggleMin();
    vm.open1 = function() {
        vm.popup1.opened = true;
    };
    vm.setDate = function(year, month, day) {
        vm.dt = new Date(year, month, day);
    };
    vm.formats = ['yyyy-MM-dd'];
    vm.format = vm.formats[0];
    vm.altInputFormats = ['M!/d!/yyyy'];

    vm.popup1 = {
        opened: false
    };
    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0,0,0,0);

            for (var i = 0; i < vm.events.length; i++) {
                var currentDay = new Date(vm.events[i].date).setHours(0,0,0,0);

                if (dayToCheck === currentDay) {
                    return vm.events[i].status;
                }
            }
        }
        return '';
    }
});