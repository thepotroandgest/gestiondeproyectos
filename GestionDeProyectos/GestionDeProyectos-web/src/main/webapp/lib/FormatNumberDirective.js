

function formatNumberDirective(val, decimales, negative) { 
    var clean = '';
    if (negative) {
        console.log(clean)
        clean = val.replace(/[^-0-9\,]/g, '');
        console.log(clean)
        var tieneSigno = false;
        if (clean.match(/^-/))
            tieneSigno = true;
        clean = clean.replace(/-/g, '');
        clean = (tieneSigno ? "-" : "") + clean;
    } else {
        clean = val.replace(/[^0-9\,]/g, '');
    }
   
    var negativeCheck = clean.split('-');
    var decimalCheck = clean.split(',');
    if (!angular.isUndefined(negativeCheck[1])) {
        negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
        clean = negativeCheck[0] + '-' + negativeCheck[1];
        if (negativeCheck[0].length > 0) {
            clean = negativeCheck[0];
        }
    }
    var entero = decimalCheck[0];
    var enteroForm = '';
    var a = 0;
    for (var i = entero.length - 1; i >= 0; i--) {
        if (a == 3) {
            enteroForm = '.' + enteroForm;
            a = 0;
        } a++;
        enteroForm = entero.charAt(i) + enteroForm;
    }
    clean = enteroForm;
    if (!angular.isUndefined(decimalCheck[1])) {
        if (decimales >= 0) {
            decimalCheck[1] = decimalCheck[1].slice(0, decimales);
            clean = decimales == 0 ? enteroForm : enteroForm + ',' + decimalCheck[1];
        } else {
            clean = enteroForm + ',' + decimalCheck[1];
        }
    }

    return clean;
}