//ui.bootstrap para solucionar los problemas de compatibilidad entre jquery y angularjs (menú collapse)
//ngRoute para el enrutamiento en app.js
//ui.select para seleccionar varios campos en un mismo input en un formulario
//ngsanitize para que se vean los campos en el formuluario (uiselect)
//rte angular para un editor de texto
var app=angular.module("myApp",['ngSanitize','ui.bootstrap','ngRoute', 'ui.select','satellizer','rte-angular', 'ngFlash']);
var ip = 'http://localhost:8080';
//login
app.factory("autenticadoService", function() {
    return {
        autenticado: true
    };
});

//Rutas
app.config(function ($routeProvider) {
    $routeProvider
        .when('/login',{
            controller: 'loginController as vm',
            templateUrl: 'views/login.html'
        })
        .when('/proyectos',{
            controller: 'proyectosController as vm',
            templateUrl: 'views/proyectos.html'
        })
        .when('/bitacora',{
            controller: 'bitacoraController as vm',
            templateUrl: 'views/bitacora.html'
        })
        .when('/cargasDeArchivo',{
            controller: 'cargasDeArchivoController as vm',
            templateUrl: 'views/cargasDeArchivo.html'
        })
        .when('/contraparte',{
            controller: 'contraparteController as vm',
            templateUrl: 'views/mantenedores/contraparte.html'
        })
        .when('/clientes',{
            controller: 'clienteController as vm',
            templateUrl: 'views/mantenedores/clientes.html'
        })

        .when('/definicionHitos',{
            controller: 'definicionHitosController as vm',
            templateUrl: 'views/mantenedores/definicion_hitos.html'
        })
        .when('/parametros',{
            controller: 'parametrosController as vm',
            templateUrl: 'views/mantenedores/parametros.html'
        })
        .when('/sistemaRelacionado',{
            controller: 'sistemaRelacionadoController as vm',
            templateUrl: 'views/mantenedores/sistemaRelacionado.html'
        })
        .when('/recurso',{
            controller: 'recursoController as vm',
            templateUrl: 'views/mantenedores/recurso.html'
        })



        .when('/calendario',{
            controller: 'calendarioController as vm',
            templateUrl: 'views/informes/calendario.html'
        })


        .when('/calendarioRecurso',{
            controller: 'calendarioController as vm',
            templateUrl: 'views/informes/calendario.html'
        })



        .when('/estadoProyecto',{
            controller: 'proyectosController as vm',
            templateUrl: 'views/informes/estadoProyecto.html'
        })
        .when('/estadoFacturacion',{
            controller: 'proyectosController as vm',
            templateUrl: 'views/informes/estadoFacturacion.html'
        })





        .when('/recursosPorProyecto',{
            controller: 'recursoController as vm',
            templateUrl: 'views/informes/recursoPorProyecto.html'
        })




        .when('/nuevoProyecto',{
            controller: 'proyectosController as vm',
            templateUrl: 'views/nuevoProyecto.html'
        })
        .when('/nuevaBitacora',{
            controller: 'bitacoraController as vm',
            templateUrl: 'views/nuevaBitacora.html'
        })
        .when('/nuevaContraparte',{
            controller: 'contraparteController as vm',
            templateUrl: 'views/mantenedores/nuevaContraparte.html'
        })
        .when('/nuevoCliente/',{
            controller: 'clienteController as vm',
            templateUrl: 'views/mantenedores/nuevoCliente.html'
        })

        .when('/nuevoParametro/',{
            controller: 'parametrosController as vm',
            templateUrl: 'views/mantenedores/nuevoParametro.html'
        })


        .when('/nuevaDefinicionHito/',{
            controller: 'definicionHitosController as vm',
            templateUrl: 'views/mantenedores/nuevaDefinicionHito.html'
        })




        .when('/nuevoSistemaRelacionado',{
            controller: 'sistemaRelacionadoController as vm',
            templateUrl: 'views/mantenedores/nuevoSistemaRelacionado.html'
        })

        .when('/nuevoRecurso/',{
            controller: 'recursoController as vm',
            templateUrl: 'views/mantenedores/nuevoRecurso.html'
        })
        .when('/modificarProyecto/:idProyecto',{
            controller: 'proyectosController as vm',
            templateUrl: 'views/nuevoProyecto.html'
        })
        .when('/modificarBitacora/:sec_bitacora',{
            controller: 'bitacoraController as vm',
            templateUrl: 'views/nuevaBitacora.html'
        })
        .when('/modificarContraparte/:id_contraparte',{
            controller: 'contraparteController as vm',
            templateUrl: 'views/mantenedores/nuevaContraparte.html'
        })
        .when('/modificarRecurso/:rutRecurso',{
            controller: 'recursoController as vm',
            templateUrl: 'views/mantenedores/nuevoRecurso.html'
        })
        .when('/modificarClientes/:rut_cliente',{
            controller: 'clienteController as vm',
            templateUrl: 'views/mantenedores/nuevoCliente.html'
        })

        .when('/modificarParametro/:id',{
            controller: 'parametrosController as vm',
            templateUrl: 'views/mantenedores/nuevoParametro.html'
        })

        .when('/modificarDefinicionHito/:id',{
            controller: 'definicionHitosController as vm',
            templateUrl: 'views/mantenedores/nuevaDefinicionHito.html'
        })

        .when('/modificarSistemaRelacionado/:idSistema',{
            controller: 'sistemaRelacionadoController as vm',
            templateUrl: 'views/mantenedores/nuevoSistemaRelacionado.html'

        })
        .when('/descripcionProyecto/:idProyecto',{
            controller: 'proyectosController as vm',
            templateUrl: 'views/descripcionProyecto.html'
        })
        .when('/descripcionFactura/:nFactura',{
            controller: 'facturaController as vm',
            templateUrl: 'views/descripcionFactura.html'
        })
        .when('/hitosComerciales',{
            controller: 'hitosComercialesController as vm',
            templateUrl: 'views/hitosComerciales.html'
        })
        .when('/factura',{
            controller: 'facturaController as vm',
            templateUrl: 'views/factura.html'
        })
        .when('/modificarHitosComerciales/:sec_hito',{
            controller: 'modificarHitosComercialesController as vm',
            templateUrl: 'views/modificarHitosComerciales.html'
        })
        .when('/nuevoHitoComercial',{
            controller: 'nuevoHitoComercialController as vm',
            templateUrl: 'views/nuevoHitoComercial.html'
        })
        .when('/modificarFactura/:idFactura',{
            controller: 'facturaController as vm',
            templateUrl: 'views/modificarFactura.html'
        })
        .when('/nuevaFactura',{
            controller: 'facturaController as vm',
            templateUrl: 'views/nuevaFactura.html'
        })
        .when('/tareas',{
            controller: 'tareaController as vm',
            templateUrl: 'views/tareas.html'
        })
        .when('/mantenedorTareas',{
            controller: 'tareaController as vm',
            templateUrl: 'views/tareas.html'
        })
        .when('/modificarTarea',{
            controller: 'tareaController as vm',
            templateUrl: 'views/modificarTarea.html'
        })
        .when('/nuevaTarea',{
            controller: 'tareaController as vm',
            templateUrl: 'views/nuevaTarea.html'
        })
        .when('/nuevoAvance/:idProyecto',{
            controller: 'avanceController as vm',
            templateUrl: 'views/nuevoAvance.html'
        })
        .when('/avance',{
            controller: 'avanceController as vm',
            templateUrl: 'views/mantenedores/avance.html'
        })
        .when('/modificarAvance/:idAvance',{
            controller: 'avanceController as vm',
            templateUrl: 'views/mantenedores/modificarAvance.html'
        })
        .otherwise({
            redirectTo: '/login'
        });

});

app.directive('formatNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }
            var maxValue = parseFloat(attrs.max);
            var minValue = parseFloat(attrs.min);
            var negative = attrs.negative=="true";

            ngModelCtrl.$parsers.push(function (val) {
                //console.log(attrs);
                if (angular.isUndefined(val) || val=="-") {
                    return val;
                }
                var clean = formatNumberDirective(val, attrs.formatNumber, negative);
                if (val != clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                var valor = parseFloat(clean.replace(/[\.]/g, '').replace(",", "."));
                var flgVal = (angular.isUndefined(attrs.max) || valor <= maxValue) && (angular.isUndefined(attrs.min) || valor <= maxValue);
                ngModelCtrl.$setValidity('validNumber', flgVal);
                return valor;
            });

            ngModelCtrl.$formatters.push(function (val) {
                if (angular.isUndefined(val)) {
                    return val;
                }
                var clean = val.toString().replace('.', ',');
                clean = formatNumberDirective(clean, attrs.formatNumber,negative);
                ngModelCtrl.$setViewValue(clean);
                ngModelCtrl.$render();
                var valor = parseFloat(val);
                var flgVal = (angular.isUndefined(attrs.max) || valor <= maxValue) && (angular.isUndefined(attrs.min) || valor <= maxValue);
                ngModelCtrl.$setValidity('validNumber', flgVal);
                return clean;

            });
            element.bind('keydown', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                    return false;
                }
                if (event.keyCode === 190 || event.keyCode === 110 || event.keyCode === 188) {
                    event.target.value = event.target.value + ',';
                    return true;
                }
            });
            element.bind('blur', function (event, o) {
                if (event.target.value == "-") {
                    event.target.value = "";
                }
            });

        }
    }
});



app.constant('ipServer', {
    ip: ip

});



var Fn = {
    // Valida el rut con su cadena completa "XXXXXXXX-X"
    validaRut: function (rutCompleto) {
        console.log(rutCompleto);
        if (!/^[0-9]+-[0-9kK]{1}$/.test(rutCompleto))
            return false;
        var tmp = rutCompleto.split('-');
        var digv = tmp[1];
        var rut = tmp[0];
        if (digv == 'K') digv = 'k';
        return (Fn.dv(rut) == digv);
    },
    dv: function (T) {
        var M = 0, S = 1;
        for (; T; T = Math.floor(T / 10))
            S = (S + T % 10 * (9 - M++ % 6)) % 11;
        return S ? S - 1 : 'k';
    }
}




