/**
    * Created by Gustavo on 22-02-2017.
    */

app.factory('clienteService', function ($http, ipServer, utilsService) {

    var clienteServiceFactory = {};
    var _action;

    var _getDominio = function(){
        return utilsService.getDominio();
    }

    var _setAction = function ( action ){
        _action = action;
    }

    var _getAction = function( ){
        return _action;
    }


    var _obtenerClientes = function () {

        console.log( "####################");


        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/clientes';
        return $http.get(url, function (results) {
            return results;
        });

    };

    var _upsertCliente = function (cliente){

        var config = {
            method: 'POST',
            url: ipServer.ip + '/GestionDeProyectos-rest-1.0/clientes',
            header: {'Content-Type': 'application/json'},
            data:cliente
        }

        return $http(config);

    };
    var _buscarCliente = function (rut_cliente){
        if(rut_cliente){
            var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/clientes/cliente/' + rut_cliente;
            return $http.get(url, function (results){
                return results;
            });
        }
    };
    var _eliminarCliente = function ( rutCliente ){

        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/clientes/cliente/' +rutCliente ;



        return $http.delete(url, function (results){
            return results;
        });

    };

    clienteServiceFactory.upsertCliente = _upsertCliente;
    clienteServiceFactory.buscarCliente = _buscarCliente;
    clienteServiceFactory.eliminarCliente = _eliminarCliente;
    clienteServiceFactory.obtenerClientes = _obtenerClientes;
    clienteServiceFactory.setAction = _setAction;
    clienteServiceFactory.getAction = _getAction;

    return clienteServiceFactory;

});
