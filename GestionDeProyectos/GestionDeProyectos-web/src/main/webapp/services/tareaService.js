/**
    * Created by Gustavo on 22-02-2017.
    */

app.factory('tareaService', function ($http, ipServer, utilsService) {
    var vm = this;
    var tareasServiceFactory = {};

    var _getDominio = function(){
        return utilsService.getDominio();
    }

    var _obtenerTareas = function () {
        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/tareas';
        return $http.get(url, function (results) {
            return results;
        });
    };

    var _upsertTarea = function ( tarea  ) {


        console.log(tarea);


        var vm = this;
        var config = {
            method: 'POST',
            url: ipServer.ip + '/GestionDeProyectos-rest-1.0/tareas/upsert',
            header: {'Content-Type': 'application/json'},
            data: {

                "id_traspaso": tarea.id_traspaso,
                "descripcion": tarea.descripcion,
                "esfuerzo_estimado": tarea.esfuerzo_estimado,
                "esfuerzo_real": tarea.esfuerzo_real,
                "fecha_inicio_estimado":tarea.fecha_inicio_estimado,
                "fecha_inicio_real":tarea.fecha_inicio_real,
                "fecha_termino_estimado":tarea.fecha_termino_estimado,
                "fecha_termino_real":tarea.fecha_termino_real,
                "porcentaje_estimado":tarea.porcentaje_estimado,
                "id_proyecto":tarea.id_proyecto,
                "rut_recurso":tarea.rut_recurso
            }
        }

        return $http(config);
    };



    var _insertarTarea = function( file ) {
        var vm = this;
        var formData = new FormData();
        var url = ipServer.ip +'/GestionDeProyectos-rest-1.0/tareas/insert';

        formData.append('file', file);

        return $http.post(      url
            ,formData
            ,{
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined},
                responseType: "json"
            });


    };

    var _eliminarTarea = function (tarea) {

        console.log(tarea.id_traspaso);


        var vm = this;

        console.log( tarea.id_proyecto + ' - ' + tarea.id_traspaso );
        var url = ipServer.ip +'/GestionDeProyectos-rest-1.0/tareas/'+tarea.id_traspaso+'/proyecto/'+tarea.id_proyecto;


        return $http.delete(   url    );

    };

    tareasServiceFactory.obtenerTareas = _obtenerTareas;
    tareasServiceFactory.insertarTarea = _insertarTarea;
    tareasServiceFactory.upsertTarea = _upsertTarea;
    tareasServiceFactory.eliminarTarea = _eliminarTarea;
    return tareasServiceFactory;

});
