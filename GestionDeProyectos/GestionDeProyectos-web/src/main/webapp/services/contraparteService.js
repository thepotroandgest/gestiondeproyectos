/**
    * Created by Gustavo on 22-02-2017.
    */

app.factory('contraparteService', function ( $http
                                            ,ipServer
                                            ,utilsService) {

    var contraparteServiceFactory = {};
    var _action;

    var _getDominio = function(){
        return utilsService.getDominio();
    }

    var _setAction = function( action ){
        _action = action;
    }

    var _getAction = function(){
        return _action;
    }


    var _obtenerContrapartes = function () {
        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/contrapartes';
        return $http.get(url, function (results) {
            return results;
        });
    };

    var _upsertContraparte = function (contraparte){

        var config = {
            method: 'POST',
            url: ipServer.ip + '/GestionDeProyectos-rest-1.0/contrapartes',
            header: {'Content-Type': 'application/json'},
            data:contraparte
        }

        return $http(config);

    };

    var _buscarContraparte = function (id_contraparte){
        if(id_contraparte){
            var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/contrapartes/contraparte/' + id_contraparte;
            return $http.get(url, function (results){
                return results;
            });
        }
    };

    var _eliminarContraparte = function (idContraparte){

            var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/contrapartes/contraparte/' + idContraparte;
            return $http.delete(url, function (results){
                return results;
            });

    };

    contraparteServiceFactory.upsertContraparte = _upsertContraparte;
    contraparteServiceFactory.buscarContraparte = _buscarContraparte;
    contraparteServiceFactory.eliminarContraparte = _eliminarContraparte;
    contraparteServiceFactory.obtenerContrapartes = _obtenerContrapartes;
    contraparteServiceFactory.setAction = _setAction;
    contraparteServiceFactory.getAction = _getAction;

    return contraparteServiceFactory;

});
