/**
    * Created by Gustavo on 23-02-2017.
    */
app.factory('recursosService', function ($http, ipServer, utilsService) {
    var recursoServiceFactory = {};
    var _action;

    var _getDominio = function(){
        return utilsService.getDominio();
    }


    var _setAction = function ( action ){
        _action = action;
    }

    var _getAction = function( ){
        return _action;
    }

    var _obtenerRecursos = function () {
        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/recursos';
        return $http.get(url, function (results) {
            return results;
        });

    };
    var _upsertRecurso = function (recurso){

        var config = {
            method: 'POST',
            url: ipServer.ip + '/GestionDeProyectos-rest-1.0/recursos',
            header: {'Content-Type': 'application/json'},
            data: recurso
        };

        console.log( ' dspeus de var ' );

        return $http(config);
    };

    var _buscarRecurso = function (rutRecurso){
        if(rutRecurso){
            var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/recursos/recurso/' + rutRecurso;
            return $http.get(url, function (results){
                return results;
            });
        }
    };

    var _buscarRecursoPorProyecto = function (idProyecto, idRecurso){

        console.log( '_buscarRecursoPorProyecto'  );


        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/recursos/idProyecto/' + idProyecto + '/idRecurso/' + idRecurso;

        return $http.get(url, function (results){
            return results;
        });

    };



    var _eliminarRecurso = function (idRecurso){

            var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/recursos/recurso/' + idRecurso;
            return $http.delete(url, function (results){
                return results;
            });

    };

    recursoServiceFactory.obtenerRecursos = _obtenerRecursos;
    recursoServiceFactory.upsertRecurso = _upsertRecurso;
    recursoServiceFactory.buscarRecurso = _buscarRecurso;
    recursoServiceFactory.eliminarRecurso = _eliminarRecurso;
    recursoServiceFactory.getAction = _getAction;
    recursoServiceFactory.setAction = _setAction;
    recursoServiceFactory.buscarRecursoPorProyecto = _buscarRecursoPorProyecto;


    return recursoServiceFactory;
});
