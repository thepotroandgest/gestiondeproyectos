/**
 * Created by cvera on 13-06-17.
 */


app.factory('utilsService', function ( $http, $location  ) {

    var utilsServiceFactory = {};
    var _action;

    var _getDominio = function(){

        return $location.protocol() + "://" + $location.host() + ":" + $location.port();
    }


    var _setAction = function ( action ){
        _action = action;
    }

    var _getAction = function( ){
        return _action;
    }



    var _ddmmyyyyToStringFormat = function( date  ){

        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();

        if( dd < 10 ){
            dd = '0' + dd.toString();
        }

        if( mm < 10 ){
            mm = '0' + mm.toString();
        }

        return dd + '-' + mm + '-' + yyyy;

    }


    var _yyyymmddToStringFormat = function( date  ){

        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();

        if( dd < 10 ){
            dd = '0' + dd.toString();
        }

        if( mm < 10 ){
            mm = '0' + mm.toString();
        }

        return yyyy + '-' +mm  + '-' + dd;

    }

    var _stringToDdmmyyyyy = function( cadena ){

        var dd = cadena.substring( 0, 2 );
        var mm = cadena.substring( 3, 5 );
        var yyyy = cadena.substring( 6, 10 );
        var fecha = new Date( yyyy + '-' + mm + '-' + dd );

        //console.log( cadena)
        //console.log(  yyyy + '-' + mm + '-' + dd  );
        //console.log(  fecha  );
        fecha.setDate( fecha.getDate() + 1 );

        return fecha;
    }

    utilsServiceFactory.ddmmyyyyToStringFormat = _ddmmyyyyToStringFormat;
    utilsServiceFactory.stringToDdmmyyyyy = _stringToDdmmyyyyy;
    utilsServiceFactory.yyyymmddToStringFormat = _yyyymmddToStringFormat;
    utilsServiceFactory.setAction = _setAction;
    utilsServiceFactory.getAction = _getAction;
    utilsServiceFactory.getDominio = _getDominio;

    return utilsServiceFactory;
});


