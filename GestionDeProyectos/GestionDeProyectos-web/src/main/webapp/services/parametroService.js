/**
    * Created by Gustavo on 22-02-2017.
    */

app.factory('parametroService', function ($http, ipServer, utilsService) {

    var parametroServiceFactory = {};
    var _action;

    var _setAction = function ( action ){
        _action = action;
    }

    var _getAction = function( ){
        return _action;
    }

    var _getDominio = function(){
        return utilsService.getDominio();
    }



    var _upsertParametro = function ( param ){

        var config = {
            method: 'POST',
            url: ipServer.ip+ '/GestionDeProyectos-rest-1.0/parametros',
            header: {'Content-Type': 'application/json'},
            data: param
        }

        return $http(config);
    };

    var _buscarParametros = function (){

        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/parametros';
        return $http.get(url, function (results){
            return results;
        });
    };

    var _buscarParametro = function ( id ){

        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/parametros/parametro/' + id;
        return $http.get(url, function (results){
            return results;
        });
    };

    var _eliminarParametro= function ( id ){

        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/parametros/parametro/' + id;

        return $http.delete(url, function (results){
            return results;
        });
    };

    parametroServiceFactory.buscarParametros = _buscarParametros;
    parametroServiceFactory.buscarParametro = _buscarParametro;
    parametroServiceFactory.upsertParametro = _upsertParametro;
    parametroServiceFactory.eliminarParametro = _eliminarParametro;

    return parametroServiceFactory;
});
