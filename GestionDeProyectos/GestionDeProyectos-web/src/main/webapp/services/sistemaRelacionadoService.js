/**
    * Created by Gustavo on 22-02-2017.
    */

app.factory('sistemaRelacionadoService', function ($http, ipServer, utilsService) {

    var sistemasRelacionadosServiceFactory = {};
    var _action;

    var _getDominio = function(){
        return utilsService.getDominio();
    }

    var _setAction = function ( action ){
        _action = action;
    }

    var _getAction = function( ){
        return _action;
    }

    var _obtenerSistemas_relacionados = function () {
        var url =ipServer.ip + '/GestionDeProyectos-rest-1.0/sistemasRelacionados';
        return $http.get(url, function (results) {
            return results;
        });

    };

    var _upsertSistema_relacionado = function ( sistema ){

        console.log( sistema  );

        var config = {
            method: 'POST',
            url: ipServer.ip + '/GestionDeProyectos-rest-1.0/sistemasRelacionados',
            header: {'Content-Type': 'application/json'},
            data:sistema
        }

        return $http( config );

    };

    var _buscarSistema_relacionado = function (id_sistema_relacionado){
        if(id_sistema_relacionado){
            var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/sistemasRelacionados/id/'+id_sistema_relacionado;
            return $http.get(url, function (results){
                return results;
            });
        }
    };


    var _eliminarSistemaRelacionado = function (idSistemaRelacionado){

            var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/sistemasRelacionados/sistema/' + idSistemaRelacionado;
            return $http.delete(url, function (results){
                return results;
            });

    };

    sistemasRelacionadosServiceFactory.upsertSistema_relacionado = _upsertSistema_relacionado;
    sistemasRelacionadosServiceFactory.buscarSistema_relacionado = _buscarSistema_relacionado;
    sistemasRelacionadosServiceFactory.obtenerSistemas_relacionados = _obtenerSistemas_relacionados;
    sistemasRelacionadosServiceFactory.setAction = _setAction;
    sistemasRelacionadosServiceFactory.getAction = _getAction;
    sistemasRelacionadosServiceFactory.eliminarSistemaRelacionado = _eliminarSistemaRelacionado;


    return sistemasRelacionadosServiceFactory;

});
