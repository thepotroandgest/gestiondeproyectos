/**
    * Created by Gustavo on 22-02-2017.
    */

app.factory('esfuerzoRealService', function ($http, ipServer, utilsService) {

    var esfuerzoRealServiceFactory = {};

    var _getDominio = function(){
        return utilsService.getDominio();
    }

    var _obtenerEsfuerzoReal = function () {
        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/esfuerzoReal';
        return $http.get(url, function (results) {
            return results;
        });
    };

    var _insertarEsfuerzoReal = function (file){

        console.log( file );


        var vm = this;
        var formData = new FormData();
        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/esfuerzoReal';
        formData.append('file', file);

        return $http.post( url, formData,
            {
                transformRequest: angular.identity,
                headers: {'Content-type': undefined},
                responseType: "json"
            }

        );
    };

    var _actualizarEsfuerzo = function ( esfuerzo ){

        var config = {
            method: 'POST',
            url: ipServer.ip + '/GestionDeProyectos-rest-1.0/esfuerzoReal/actualizarEsfuerzo',
            header: {'Content-Type': 'application/json'},
            data: esfuerzo
        };

        return $http( config );
    };

    esfuerzoRealServiceFactory.obtenerEsfuerzoReal = _obtenerEsfuerzoReal;
    esfuerzoRealServiceFactory.insertarEsfuerzoReal = _insertarEsfuerzoReal;
    esfuerzoRealServiceFactory.actualizarEsfuerzo = _actualizarEsfuerzo;

    return esfuerzoRealServiceFactory;

});
