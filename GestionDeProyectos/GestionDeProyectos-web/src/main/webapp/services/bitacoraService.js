/**
    * Created by Gustavo on 22-02-2017.
    */

app.factory('bitacoraService', function (    $http
                                            ,ipServer
                                            ,utilsService) {

    var bitacoraServiceFactory = {};
    var idProyecto;


    var _getDominio = function(){
        return utilsService.getDominio();
    }


    var _setIdProyecto = function( id ){
        idProyecto = id;
    }

    var _getIdProyecto = function(){
        return idProyecto;
    }


    var _obtenerBitacoras = function () {
        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/bitacoras';
        return $http.get(url, function (results) {
            return results;
        });

    };


    var _buscarBitacora = function (sec_bitacora){

        if(sec_bitacora){

            var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/bitacoras/secBitacora/' + sec_bitacora+'/fkIdProyecto/todos';

            return $http.get(url, function (results){
                return results;
            });
        }
    };




    var _buscarBitacorasProyecto = function (id_proyecto) {
        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/bitacoras/tipo/todos/fkIdProyecto/'+id_proyecto;
        return $http.get(url, function (results) {
            return results;
        });

    };

    var _buscarMailsProyecto = function (id_proyecto) {

        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/bitacoras/tipo/Mail/fkIdProyecto/'+id_proyecto;
        return $http.get(url, function (results) {
            return results;
        });
    };

    var _buscarCompromisosProyecto = function (id_proyecto) {

        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/bitacoras/tipo/Compromiso/fkIdProyecto/'+id_proyecto;
        return $http.get(url, function (results) {
            return results;
        });
    };

    var _buscarNotasProyecto = function (id_proyecto) {

        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/bitacoras/tipo/Notas/fkIdProyecto/'+id_proyecto;
        return $http.get(url, function (results) {
            return results;
        });

    };

    var _buscarAcuerdosProyecto = function (id_proyecto) {

        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/bitacoras/tipo/Acuerdos/fkIdProyecto/'+id_proyecto;
        return $http.get(url, function (results) {
            return results;
        });

    };

    var _buscarObservacionesProyecto = function (id_proyecto) {

        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/bitacoras/tipo/Observacion%20Estado/fkIdProyecto/'+id_proyecto;
        return $http.get(url, function (results) {
            return results;
        });

    };


    var _agregarBitacora = function (bitacora){

        if( bitacora.fechaIngreso != null ){
            bitacora.fechaIngreso = utilsService.ddmmyyyyToStringFormat( bitacora.fechaIngreso );
        }

        if( bitacora.fechaVencimiento != null ){
            bitacora.fechaVencimiento = utilsService.ddmmyyyyToStringFormat( bitacora.fechaVencimiento );
        }


        var config = {
            method: 'POST',
            url: ipServer.ip + '/GestionDeProyectos-rest-1.0/bitacoras',
            header: {'Content-Type': 'application/json'},
            data: bitacora
        };

        return $http(config);

    };
    var _actualizarBitacora = function (bitacora){

        if(bitacora){
            var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/bitacoras/actualizar?sec_bitacora='+ bitacora.sec_bitacora + '&tipo='+ bitacora.tipo + '&asunto='+bitacora.asunto+'&descripcion='+bitacora.descripcion+'&rut_responsable='+bitacora.rut_responsable+'&fecha_ingreso='+bitacora.fecha_ingreso+'&fecha_vencimiento='+bitacora.fecha_vencimiento+'&fk_id_proyecto='+bitacora.fk_id_proyecto;
            return $http.post(url, function (results) {
                return results;
            });
        }
    };

    var _eliminarBitacora = function (sec_bitacora){
        if(sec_bitacora){
            var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/bitacoras?sec_bitacora=' + sec_bitacora;
            return $http.delete(url, function (results){
                return results;
            });
        }
    };

    bitacoraServiceFactory.actualizarBitacora = _actualizarBitacora;
    bitacoraServiceFactory.agregarBitacora = _agregarBitacora;
    bitacoraServiceFactory.buscarBitacora = _buscarBitacora;
    bitacoraServiceFactory.eliminarBitacora = _eliminarBitacora;
    bitacoraServiceFactory.obtenerBitacoras = _obtenerBitacoras;
    bitacoraServiceFactory.buscarBitacorasProyectos =_buscarBitacorasProyecto;
    bitacoraServiceFactory.buscarMailsProyecto = _buscarMailsProyecto;
    bitacoraServiceFactory.buscarCompromisosProyecto = _buscarCompromisosProyecto;
    bitacoraServiceFactory.buscarNotasProyecto = _buscarNotasProyecto;
    bitacoraServiceFactory.buscarAcuerdosProyecto  = _buscarAcuerdosProyecto;
    bitacoraServiceFactory.buscarObservacionesProyecto  = _buscarObservacionesProyecto;
    bitacoraServiceFactory.setIdProyecto  = _setIdProyecto;
    bitacoraServiceFactory.getIdProyecto  = _getIdProyecto;


    return bitacoraServiceFactory;

});
