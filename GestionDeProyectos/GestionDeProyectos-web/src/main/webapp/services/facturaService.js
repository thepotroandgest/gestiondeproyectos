/**
    * Created by Gustavo on 22-02-2017.
    */

app.factory('facturaService', function ( $http
                                        ,ipServer
                                        ,utilsService) {


    var facturaServiceFactory = {};
    var _action;
    var _factura = {};

    var _getDominio = function(){
        return utilsService.getDominio();
    }


    var _setFactura = function( factura ){
        _factura = factura;
    }

    var _getFactura = function(){
        return _factura;
    }

    var _setAction = function ( action ){
        _action = action;
    }

    var _getAction = function( ){
        return _action;
    }

    var _obtenerFacturas = function () {
        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/facturas';
        return $http.get(url, function (results) {
            return results;
        });
    };

    var _buscarFacturaProyecto = function (id_proyecto) {
        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/facturas/proyecto?id_proyecto='+id_proyecto;
        return $http.get(url, function (results) {
            return results;
        });

    };

    var _upsertFactura = function (factura){

        factura.fechaEmision = utilsService.ddmmyyyyToStringFormat( factura.fechaEmision );


        console.log( factura  );


        var config = {
            method : 'POST',
            url : ipServer.ip + '/GestionDeProyectos-rest-1.0/facturas',
            header : {'Content-Type': 'application/json'},
            data : factura
        };

        return $http(config);
    };

    var _buscarFactura = function (nFactura){
            var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/facturas/factura/' + nFactura;
            return $http.get(url, function (results){
                return results;
            });
        }

    var _eliminarFactura = function (nFactura){

            var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/facturas/nFactura/' + nFactura;
            return $http.delete(url, function (results){
                return results;
            });

    };

    var _elimiarHitoFactura = function(nFactura,idProyecto, hito){

        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/facturas/nFactura/' + nFactura + '/idProyecto/'+idProyecto + '/hito/'+hito;
        return $http.delete(url, function (results){
            return results;
        });

    }


    facturaServiceFactory.upsertFactura = _upsertFactura;
    facturaServiceFactory.buscarFactura = _buscarFactura;
    facturaServiceFactory.eliminarFactura = _eliminarFactura;
    facturaServiceFactory.obtenerFacturas = _obtenerFacturas;
    facturaServiceFactory.buscarFacturaProyecto = _buscarFacturaProyecto;
    facturaServiceFactory.getAction = _getAction;
    facturaServiceFactory.setAction =_setAction;
    facturaServiceFactory.setFactura = _setFactura;
    facturaServiceFactory.getFactura = _getFactura;
    facturaServiceFactory.elimiarHitoFactura = _elimiarHitoFactura;

    return facturaServiceFactory;

});
