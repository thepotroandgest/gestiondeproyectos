/**
    * Created by Gustavo on 22-02-2017.
    */

app.factory('hitosComercialesService', function ($http, ipServer, utilsService) {

    var hitosComercialesServiceFactory = {};
    var _action;


    var _getDominio = function(){
        return utilsService.getDominio();
    }


    var _setAction = function(action){
        _action = action;
    }

    var _getAction = function (){
        return _action;
    }

    var _obtenerHitosComerciales = function () {
        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/hitosComerciales';
        return $http.get(url, function (results) {
            return results;
        });

    };
    var _upsertHitoComercial = function (hitoComercial){

        var config = {
            method:'POST',
            url: ipServer.ip + '/GestionDeProyectos-rest-1.0/hitosComerciales',
            header :  {'Content-Type': 'application/json'},
            data: hitoComercial

        };

        return $http(config);

    };
    //-----------------------------BGN VVQ-20-06-2017 ------------------------------
    var _upsertHitos = function (hitosComerciales){

        var config ={
            method:'POST',
            url: ipServer.ip +'/GestionDeProyectos-rest-1.0/hitosComerciales/hitos',
            header :  {'Content-Type': 'application/json'},
            data: hitosComerciales
        };

        return $http(config);
    }
    //-----------------------------END VVQ-20-06-2017 ------------------------------

    var _buscarHitoComercial = function (idFkProyecto){

            var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/hitosComerciales/hitoComercial/' + idFkProyecto;
            return $http.get(url, function (results){
                return results;

        });
    }


    var _buscarHitoPorFactura = function (nFactura){

        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/hitosComerciales/factura/' + nFactura;
        return $http.get(url, function (results){
            return results;

        });
    }

    var _obtenerHitosPorContraparte = function( proyecto, contraparte){

        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/hitosComerciales/proyecto/' + proyecto + '/contraparte/' + contraparte ;
        return $http.get(url, function (results){
            return results;

        });

    }


    var _eliminarHitoComercial = function (hito){


        http://localhost:8080/GestionDeProyectos-rest-1.0/hitosComerciales/hito/Primera%20Entrega/proyecto/PPL-01-TEST2
            var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/hitosComerciales/hito/' + hito.hitoComercial + '/proyecto/'+ hito.fkIdProyecto;
            return $http.delete(url, function (results){
                return results;
            });
        }



    hitosComercialesServiceFactory.obtenerHitosComerciales =_obtenerHitosComerciales;
    hitosComercialesServiceFactory.buscarHitoComercial = _buscarHitoComercial;
    hitosComercialesServiceFactory.upsertHitoComercial = _upsertHitoComercial;
    hitosComercialesServiceFactory.eliminarHitoComercial = _eliminarHitoComercial;
    hitosComercialesServiceFactory.upsertHitos= _upsertHitos;
    hitosComercialesServiceFactory.getAction = _getAction;
    hitosComercialesServiceFactory.setAction = _setAction;
    hitosComercialesServiceFactory.buscarHitoPorFactura = _buscarHitoPorFactura;
    hitosComercialesServiceFactory.obtenerHitosPorContraparte = _obtenerHitosPorContraparte;


    return hitosComercialesServiceFactory;

});
