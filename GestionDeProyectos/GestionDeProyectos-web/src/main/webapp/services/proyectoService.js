    /**
    * Created by Gustavo on 22-02-2017.
    */

app.factory('proyectoService', function (    $http
                                            ,ipServer
                                            ,utilsService) {

    var proyectosServiceFactory = {};

    var _getDominio = function(){
        return utilsService.getDominio();
    }

    var _obtenerProyectos = function () {

        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/proyectos';

        return $http.get(url, function (results) {
            return results;
        });

    };

    var _upsertProyecto = function (_proyecto){

        var proyecto = {};
        Object.assign( proyecto, _proyecto);

        proyecto.fechaInicio = utilsService.ddmmyyyyToStringFormat( proyecto.fechaInicio );
        proyecto.fechaTermino = utilsService.ddmmyyyyToStringFormat( proyecto.fechaTermino );

        var config = {
            method: 'POST',
            url: ipServer.ip + '/GestionDeProyectos-rest-1.0/proyectos',
            header: {'Content-Type': 'application/json'},
            data: proyecto
        };

        return $http(config);
    };

    var _buscarProyecto = function (id_proyecto){

            var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/proyectos/proyecto/' + id_proyecto;
            return $http.get(url, function (results){
                return results;
            });
    };

    var _eliminarProyecto= function (id_proyecto){

        var vm = this;
        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/proyectos/proyecto/'+id_proyecto;

        return $http.delete( url );
    };

    var _obtenerEstadoProyectos = function( idProyecto ){

        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/proyectos/estado/'+idProyecto;

        return $http.get(url, function (results){
            return results;
        });
    }

    proyectosServiceFactory.obtenerProyectos = _obtenerProyectos;
    proyectosServiceFactory.upsertProyecto = _upsertProyecto;
    proyectosServiceFactory.buscarProyecto = _buscarProyecto;
    proyectosServiceFactory.eliminarProyecto = _eliminarProyecto;
    proyectosServiceFactory.obtenerEstado = _obtenerEstadoProyectos;

    return proyectosServiceFactory;

});
