/**
 * Created by cvera on 13-06-17.
 */


app.factory('avanceService', function (  $http
                                        ,ipServer
                                        ,utilsService ) {

    var avanceServiceFactory = {};
    var _action;


    var _getDominio = function(){
        return utilsService.getDominio();
    }


    var _setAction = function ( action ){
        _action = action;
    }

    var _getAction = function( ){
        return _action;
    }

    var _obtenerAvances = function ( avance, idProyecto ) {

        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/avance/proyecto/' + idProyecto + '/idAvance/' + avance;

        return $http.get(url, function (results) {
            return results;
        });

    };

    var _eliminarAvance = function ( idAvance ){

        var vm = this;
        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/avance/idAvance/' + idAvance;

        return $http.delete( url );
    };

    var _agregarAvance = function ( avance ){

        avance.fecha = utilsService.ddmmyyyyToStringFormat( avance.fecha  );

        var config = {
            method: 'POST',
            url: ipServer.ip + '/GestionDeProyectos-rest-1.0/avance',
            header: {'Content-Type': 'application/json'},
            data: avance
        };

        return $http( config );
    };

    avanceServiceFactory.setAction = _setAction;
    avanceServiceFactory.getAction = _getAction;
    avanceServiceFactory.agregarAvance = _agregarAvance;
    avanceServiceFactory.obtenerAvances = _obtenerAvances;
    avanceServiceFactory.eliminarAvance = _eliminarAvance;

    return avanceServiceFactory;

});


