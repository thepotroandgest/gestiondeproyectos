/**
 * Created by daniel on 16-05-2017.
 */
app.factory('horasHombreService', function ($http, ipServer, utilsService) {

    var horasHombreServiceFactory = {};

    var _getDominio = function(){
        return utilsService.getDominio();
    }


    var _obtenerHorasHombre = function () {
        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/HorasHombre';
        return $http.get(url, function (results) {
            return results;
        });

    };


    var _insertarHorasHombre = function( file ) {
        var vm = this;
        var formData = new FormData();
        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/HorasHombre/insert';
        formData.append('file', file);

        return $http.post(      url
            ,formData
            ,{
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined},
                responseType: "json"
            });


    };

    horasHombreServiceFactory.obtenerHorasHombre = _obtenerHorasHombre;
    horasHombreServiceFactory.insertarHorasHombre = _insertarHorasHombre;
    return horasHombreServiceFactory;

});

