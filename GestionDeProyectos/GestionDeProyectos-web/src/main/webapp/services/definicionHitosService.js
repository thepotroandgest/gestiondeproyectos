/**
    * Created by Gustavo on 22-02-2017.
    */

app.factory('definicionHitosService', function ( $http
                                                ,ipServer
                                                ,utilsService) {

    var definicionHitoServiceFactory = {};
    var _action;


    var _getDominio = function(){
        return utilsService.getDominio();
    }


    var _setAction = function ( action ){
        _action = action;
    }

    var _getAction = function( ){
        return _action;
    }

    var _upsertDefinicionHito = function ( defHito ){

        var config = {
            method: 'POST',
            url: ipServer.ip + '/GestionDeProyectos-rest-1.0/definicionHitos',
            header: {'Content-Type': 'application/json'},
            data: defHito
        }

        return $http(config);
    };

    var _buscarDefinicionHitos = function (){

        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/definicionHitos';
        return $http.get(url, function (results){
            return results;
        });
    };

    var _buscarDefinicionHito = function ( id ){

        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/definicionHitos/hito/' + id;
        return $http.get(url, function (results){
            return results;
        });

    };

    var _eliminarDefinicionHito= function ( id ){

        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/definicionHitos/hito/' + id;

        return $http.delete(url, function (results){
            return results;
        });

    };

    definicionHitoServiceFactory.buscarDefinicionHitos = _buscarDefinicionHitos;
    definicionHitoServiceFactory.buscarDefinicionHito = _buscarDefinicionHito;
    definicionHitoServiceFactory.upsertDefinicionHito = _upsertDefinicionHito;
    definicionHitoServiceFactory.eliminarDefinicionHito = _eliminarDefinicionHito;

    return definicionHitoServiceFactory;

});
