/**
 * Created by Gustavo on 22-02-2017.
 */

app.factory('calendarioService', function ($http, ipServer, utilsService) {

    var calendarioServiceFactory = {};

    var _getDominio = function(){
        return utilsService.getDominio();
    }


    var _obtenerCalendario = function () {
        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/calendario';
        return $http.get(url, function (results) {
            return results;
        });

    };


    var _obtenerCalendarioRecurso = function () {
        var url = ipServer.ip + '/GestionDeProyectos-rest-1.0/calendario/recursos';
        return $http.get(url, function (results) {
            return results;
        });

    };


    calendarioServiceFactory.obtenerCalendario= _obtenerCalendario;
    calendarioServiceFactory.obtenerCalendarioRecurso= _obtenerCalendarioRecurso;


    return calendarioServiceFactory;

});
