package com.gestion.controller;

import com.gestion.dominio.Avance;
import com.gestion.dominio.Cliente;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.service.AvanceService;
import com.gestion.service.ClienteService;
import com.gestion.utils.ResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
@RestController
@RequestMapping("/avance")
public class AvanceController {

    private static final Logger logger = LoggerFactory.getLogger( AvanceController.class );


    @Autowired
    private AvanceService avanceService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Avance> obtenerAvance(){
        return avanceService.obtenerAvance( null, null );
    }

    //Buscar cliente
    @RequestMapping(value = "/proyecto/{proyecto}/idAvance/{avance}",method = RequestMethod.GET)
    public List<Avance> buscarCliente(   @PathVariable("proyecto")  String proyecto
                                        ,@PathVariable("avance")  Long avance){


        logger.info(  "proyecto --> " + proyecto   );
        logger.info(  "avance --> " + avance   );


        if( proyecto.equals( "todos" ) ){
            proyecto = null;
        }
        if( avance == 0 ){
            avance = null;
        }

        return avanceService.obtenerAvance( avance, proyecto );
    }

    //insertar o actualizar
    @RequestMapping(method = RequestMethod.POST,headers = {"Content-type=application/json"})
    public ResponseEntity upsert(@RequestBody Avance avance) {
        ResponseServicio salida;

        logger.info( avance.toString()  );

        salida = avanceService.agregarAvance( avance );


        if (salida != null || salida.isOk()){
            return ResponseUtils.responseCreated(salida);
        }
        else{
            return ResponseUtils.responseConflict(salida);
        }


    }


    //Eliminar recurso
    @RequestMapping(value ="/idAvance/{idAvance}",  method = RequestMethod.DELETE)
    public void eliminarProyecto(

            @PathVariable("idAvance") Long idAvance

    ){

        avanceService.eliminarAvance( idAvance );
    }

}











