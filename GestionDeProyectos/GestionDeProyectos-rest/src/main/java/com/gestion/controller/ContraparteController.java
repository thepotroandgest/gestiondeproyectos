package com.gestion.controller;

import com.gestion.dominio.Contraparte;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.service.ContraparteService;
import com.gestion.utils.ResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
@RestController
@RequestMapping("/contrapartes")
public class ContraparteController {

    private static final Logger logger = LoggerFactory.getLogger( ClienteController.class );

    @Autowired
    private ContraparteService contraparteService;

    //Obtener todo
    @RequestMapping(method = RequestMethod.GET)
    public List<Contraparte> obtenerContrapartes(){

        return contraparteService.obtenerContrapartes( null );
    }

    //Buscar
    @RequestMapping(value = "/contraparte/{idContraparte}",method = RequestMethod.GET)
    public List<Contraparte> buscarContraparte(@PathVariable("idContraparte") String idContraparte){
        return contraparteService.buscarContraparte(idContraparte);
    }

    //insertar o actualizar
    @RequestMapping(method = RequestMethod.POST,headers = {"Content-type=application/json"})
    public ResponseEntity upsert(@RequestBody Contraparte pCon) {

        ResponseServicio salida;

        logger.info( pCon.toString()  );

        salida = contraparteService.upsert(pCon);

        if (salida != null || salida.isOk()){
            return ResponseUtils.responseCreated(salida);
        }
        else
        {
            return ResponseUtils.responseConflict(salida);
        }
    }


    @RequestMapping(value ="/contraparte/{idContraparte}", method = RequestMethod.DELETE)
    public void eliminarContraparte( @PathVariable("idContraparte") String idContraparte ){

        contraparteService.eliminarContraparte( idContraparte );
    }

}






















