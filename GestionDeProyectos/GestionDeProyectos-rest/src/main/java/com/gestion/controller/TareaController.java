package com.gestion.controller;

import com.gestion.dominio.Tarea;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.dominio.response.TareaPostResponse;
import com.gestion.service.TareaService;
import com.gestion.utils.ResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
@RestController
@RequestMapping("/tareas")
public class TareaController {
    private static final Logger logger = LoggerFactory.getLogger(TareaController.class);

    @Autowired
    private TareaService tareaService;

    @RequestMapping(value="/proyecto/{id_proyecto}",method = RequestMethod.GET)
    public List<Tarea> obtenerTarea(
        @PathVariable("id_proyecto") String id_proyecto

    ){
        return tareaService.obtenerTareas(id_proyecto);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Tarea> obtenerTareas(){

        String id_proyecto = null;
        return tareaService.obtenerTareas(id_proyecto);
    }

    @RequestMapping(value = "/insert",method = RequestMethod.POST )
    public ResponseEntity<TareaPostResponse> insertarTarea(@RequestParam("file") MultipartFile file ) {

        ResponseServicio salida;
        String completeData = null;
        byte[] latin = null;

        if(  !file.isEmpty()  ) {

            try{

                //Versión Windows
//                byte[] bytes = file.getBytes();
//                completeData = new String(bytes);

                //Version Linux
                byte[] bytes = file.getBytes();
                latin = new String(bytes, "ISO-8859-1").getBytes(  "UTF-8" );
                completeData = new String(latin);



            }catch( Exception e ){
                logger.info("excepcio  ---> " + e );

            }

        }

        salida = tareaService.insert( completeData );

        if (salida == null || !salida.isOk()) {
            return ResponseUtils.responseConflict(salida);
        }
        return ResponseUtils.responseCreated(salida);

    }

    //insertar o actualizar
    @RequestMapping(value = "/upsert",method = RequestMethod.POST,headers = {"Content-type=application/json"})
    public ResponseEntity insertarActualizarTareas (@RequestBody Tarea tTarea){
        ResponseServicio salida;

        logger.info(tTarea.toString());


        salida = tareaService.insertarActualizarTareas(tTarea);

        if (salida != null || salida.isOk()){
            return ResponseUtils.responseCreated(salida);
        }
        else
        {
            return ResponseUtils.responseConflict(salida);
        }
    }

    //Eliminar tarea
    @RequestMapping( value="/{id_traspaso}/proyecto/{id_proyecto}", method = RequestMethod.DELETE)
    public void eliminarTarea(
                                     @PathVariable("id_traspaso") Integer id_traspaso
                                    ,@PathVariable("id_proyecto") String id_proyecto

    ){

        Tarea tTarea = new Tarea();

        tTarea.setId_traspaso( id_traspaso );
        tTarea.setId_proyecto( id_proyecto  );

        logger.info( "" + id_traspaso + " - " + id_proyecto );
        tareaService.eliminarTarea(tTarea);
    }

}
