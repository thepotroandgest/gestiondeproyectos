package com.gestion.controller;

import com.gestion.dominio.EstadoProyecto;
import com.gestion.dominio.Proyecto;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.service.ProyectoService;
import com.gestion.utils.ResponseUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import org.slf4j.Logger;


import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
@RestController
@RequestMapping("/proyectos")
public class ProyectoController {

    private static final Logger logger = LoggerFactory.getLogger( ProyectoController.class );

    @Autowired
    private ProyectoService proyectoService;
    //Obtener todo
    @RequestMapping(method = RequestMethod.GET)
    public List<Proyecto> obtenerProyectos(){
        return proyectoService.obtenerProyectos( null );
    }

    @RequestMapping(value = "/estado/{idProyecto}",method = RequestMethod.GET)
    public List<EstadoProyecto> obtenerEstado(@PathVariable( "idProyecto" ) String idProyecto){


        if( idProyecto.equals( "todos" ) ){
            idProyecto = null;
        }

        return proyectoService.obtenerEstado(idProyecto);
    }

    //Buscar recurso
    @RequestMapping(value = "/proyecto/{idProyecto}",method = RequestMethod.GET)
    public List<Proyecto> buscarRecurso( @PathVariable( "idProyecto" ) String idProyecto ){
        return proyectoService.obtenerProyectos( idProyecto );
    }

    //insertar o actualizar
    @RequestMapping(method = RequestMethod.POST,headers = {"Content-type=application/json"})
    public ResponseEntity upsert(@RequestBody Proyecto pPro) {

        ResponseServicio salida;
        logger.info( pPro.toString()   );
        salida = proyectoService.upsert(pPro);

        if (salida != null || salida.isOk()){
            return ResponseUtils.responseCreated(salida);
        }
        else
        {
            return ResponseUtils.responseConflict(salida);
        }

    }

    //Eliminar recurso
    @RequestMapping(value ="/proyecto/{idProyecto}",  method = RequestMethod.DELETE)
    public ResponseEntity eliminarProyecto(

            @PathVariable("idProyecto") String idProyecto
    ){

        ResponseServicio salida;
        logger.info( idProyecto.toString()   );
        salida = proyectoService.eliminarProyecto( idProyecto );

        if (salida != null || salida.isOk()){
            return ResponseUtils.responseCreated(salida);
        }
        else
        {
            return ResponseUtils.responseConflict(salida);
        }


    }
}
