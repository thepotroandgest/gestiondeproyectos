package com.gestion.controller;

import com.gestion.dominio.Cliente;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.service.ClienteService;
import com.gestion.utils.ResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
@RestController
@RequestMapping("/clientes")
public class ClienteController {

    private static final Logger logger = LoggerFactory.getLogger( ClienteController.class );


    @Autowired
    private ClienteService clienteService;

    //Obtener todo
    @RequestMapping(method = RequestMethod.GET)
    public List<Cliente> obtenerClientes(){
        return clienteService.obtenerClientes( null );
    }


    //Buscar cliente
    @RequestMapping(value = "/cliente/{rutCliente}",method = RequestMethod.GET)
    public List<Cliente> buscarCliente(@PathVariable("rutCliente")  String rutCliente){
        return clienteService.obtenerClientes(rutCliente);
    }

    //insertar o actualizar
    @RequestMapping(method = RequestMethod.POST,headers = {"Content-type=application/json"})
    public ResponseEntity upsert(@RequestBody Cliente pCli) {

        ResponseServicio salida;

        logger.info( pCli.toString()  );

        salida = clienteService.upsert(pCli);

        if (salida != null || salida.isOk()){
            return ResponseUtils.responseCreated(salida);
        }
        else
        {
            return ResponseUtils.responseConflict(salida);
        }

    }

    //Eliminar recurso
    @RequestMapping(value ="/cliente/{rutCliente}", method = RequestMethod.DELETE)
    public void eliminarRecurso( @PathVariable("rutCliente") String rutCliente ){

        clienteService.eliminarCliente( rutCliente );
    }
}











