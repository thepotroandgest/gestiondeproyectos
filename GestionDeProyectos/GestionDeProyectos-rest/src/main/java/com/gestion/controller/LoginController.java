package com.gestion.controller;

import com.gestion.dominio.Login;
import com.gestion.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
@RestController
@RequestMapping("/login")
public class LoginController {
    @Autowired
    private LoginService loginService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Login> obtenerLogin(){
        return loginService.obtenerLogin();
    }
}
