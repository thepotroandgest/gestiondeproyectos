package com.gestion.controller;

import com.gestion.dominio.Bitacora;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.service.BitacoraService;
import com.gestion.utils.ResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
@RestController
@RequestMapping("/bitacoras")
public class BitacoraController {

    private static final Logger logger = LoggerFactory.getLogger(BitacoraController.class);

    @Autowired
    private BitacoraService bitacoraService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Bitacora> obtenerClientes(){

        return bitacoraService.obtenerBitacoras( null, null, null );
    }

    @RequestMapping(value = "/tipo/{tipo}/fkIdProyecto/{fkIdProyecto}",method = RequestMethod.GET)
    public List<Bitacora> buscarBitacorasProyecto(
            @PathVariable("tipo") String tipo
            ,@PathVariable("fkIdProyecto") String fkIdProyecto){

        if( tipo.equals("todos") ){
            tipo = null;
        }

        if( fkIdProyecto.equals("todos") ){
            fkIdProyecto = null;
        }

        return bitacoraService.obtenerBitacoras(  null , fkIdProyecto, tipo);
    }

    @RequestMapping(value = "/secBitacora/{secBitacora}/fkIdProyecto/{fkIdProyecto}",method = RequestMethod.GET)
    public List<Bitacora> buscarBitacoras(
                                                 @PathVariable("secBitacora") Long secBitacora
                                                ,@PathVariable("fkIdProyecto") String fkIdProyecto){

        if( secBitacora == 0 ){
            secBitacora = null;
        }

        if( fkIdProyecto.equals("todos") ){
            fkIdProyecto = null;
        }

        return bitacoraService.obtenerBitacoras(  secBitacora, fkIdProyecto, null);
    }

    //insertar o actualizar
    @RequestMapping(method = RequestMethod.POST,headers = {"Content-type=application/json"})
    public ResponseEntity upsert(@RequestBody Bitacora pBit) {

        logger.info(  pBit.toString()   );


        ResponseServicio salida;
        salida = bitacoraService.agregarBitacora( pBit );

        if (salida != null || salida.isOk()){
            return ResponseUtils.responseCreated(salida);
        }
        else
        {
            return ResponseUtils.responseConflict(salida);
        }

    }

    //Eliminar recurso
    @RequestMapping(method = RequestMethod.DELETE)
    public void eliminarBitacora(@RequestParam(value="sec_bitacora",required=true) long set_bitacora) {
        bitacoraService.eliminarBitacora(set_bitacora);
    }



}
