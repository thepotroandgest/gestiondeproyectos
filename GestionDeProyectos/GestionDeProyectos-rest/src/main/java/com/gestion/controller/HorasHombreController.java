package com.gestion.controller;

import com.gestion.dominio.Result;
import com.gestion.dominio.Tarea;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.service.HorasHombreService;
import com.gestion.service.TareaService;
import com.gestion.utils.ResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by cvera on 15-05-17.
 */

@RestController
@RequestMapping("/HorasHombre")
public class HorasHombreController {

    private static final Logger logger = LoggerFactory.getLogger( HorasHombreController.class );

    @Autowired
    private HorasHombreService hHService;



    @RequestMapping(value = "/insert",method = RequestMethod.POST )
    public ResponseEntity insertarTraspasoTarea(@RequestParam("file") MultipartFile file ) {


        ResponseServicio salida;
        String completeData = null;


        if(  !file.isEmpty()  ) {

            try{
                byte[] bytes = file.getBytes();
                completeData = new String(bytes);

            }catch( Exception e ){
                logger.info("excepcio  ---> " + e );
            }

        }

        salida = hHService.insert( completeData );

        if (salida == null || !salida.isOk()) {
            return ResponseUtils.responseConflict(salida);
        }
        return ResponseUtils.responseCreated(salida);
    }



}
