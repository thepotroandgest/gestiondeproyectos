package com.gestion.controller;

import com.gestion.dominio.Sistema_Relacionado;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.service.Sistema_RelacionadoService;
import com.gestion.utils.ResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
@RestController
@RequestMapping("/sistemasRelacionados")
public class Sistema_RelacionadoController {

    private static final Logger logger = LoggerFactory.getLogger( ClienteController.class );

    @Autowired
    private Sistema_RelacionadoService sistema_relacionadoService;

    //Obtener todo
    @RequestMapping(method = RequestMethod.GET)
    public List<Sistema_Relacionado> obtenerSistema_Relacionados(){
        return sistema_relacionadoService.obtenerSistema_Relacionados(null);
    }

    //Buscar recurso
    @RequestMapping(value = "/id/{idSistema}",method = RequestMethod.GET)
    public List<Sistema_Relacionado> buscarSistema_relacionado( @PathVariable("idSistema") String idSistema){
        return sistema_relacionadoService.obtenerSistema_Relacionados( idSistema );
    }

    //insertar o actualizar
    @RequestMapping(method = RequestMethod.POST,headers = {"Content-type=application/json"})
    public ResponseEntity upsert(@RequestBody Sistema_Relacionado pSiRe) {
        ResponseServicio salida;

        logger.info( pSiRe.toString()  );

        salida = sistema_relacionadoService.upsert(pSiRe);

        if (salida != null || salida.isOk()){
            return ResponseUtils.responseCreated(salida);
        }
        else
        {
            return ResponseUtils.responseConflict(salida);
        }
    }

    @RequestMapping(value ="/sistema/{idSistema}", method = RequestMethod.DELETE)
    public void eliminarRecurso( @PathVariable("idSistema") String idSistema ){

        sistema_relacionadoService.eliminarSistema_relacionado( idSistema );
    }

}


























