package com.gestion.controller;

import com.gestion.dominio.Cliente;
import com.gestion.dominio.Parametro;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.service.ClienteService;
import com.gestion.service.ParametroService;
import com.gestion.utils.ResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
@RestController
@RequestMapping("/parametros")
public class ParametroController {

    private static final Logger logger = LoggerFactory.getLogger( ParametroController.class );


    @Autowired
    private ParametroService parametroService;

    //Obtener todo
    @RequestMapping(method = RequestMethod.GET)
    public List<Parametro> obtenerParametros(){
        return parametroService.obtenerParametros( null );
    }


    @RequestMapping(value = "/parametro/{id}",method = RequestMethod.GET)
    public List<Parametro> buscarParametro(@PathVariable("id")  Integer id){
        return parametroService.obtenerParametros(id);
    }


    @RequestMapping(method = RequestMethod.POST,headers = {"Content-type=application/json"})
    public ResponseEntity upsert( @RequestBody Parametro param ) {

        ResponseServicio salida;

        logger.info( param.toString()  );

        salida = parametroService.upsert( param );

        if (salida != null || salida.isOk()){
            return ResponseUtils.responseCreated(salida);
        }
        else
        {
            return ResponseUtils.responseConflict(salida);
        }

    }


    //Eliminar recurso
    @RequestMapping(value ="/parametro/{id}", method = RequestMethod.DELETE)
    public void eliminarRecurso( @PathVariable("id") Integer id ){

        parametroService.eliminarParametro( id );
    }
}











