package com.gestion.controller;

import com.gestion.dominio.Recurso;
import com.gestion.dominio.RecursosPorProyecto;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.service.RecursoService;
import com.gestion.utils.ResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/recursos")
public class RecursoController {

    @Autowired
    private RecursoService recursoService;

    private static final Logger logger = LoggerFactory.getLogger(TareaController.class);

    //Obtener recursos
    @RequestMapping(method = RequestMethod.GET)
    public List<Recurso> obtenerRecursos(){

        String rutRecurso = null;
        return recursoService.obtenerRecursos(rutRecurso);
    }

    //Obtener recursos
    @RequestMapping( value="/idProyecto/{idProyecto}/idRecurso/{idRecurso}", method = RequestMethod.GET)
    public List<RecursosPorProyecto> obtenerRecursosPorProyecto(

                     @PathVariable("idProyecto") String idProyecto
                    ,@PathVariable("idRecurso") String idRecurso
    ){

        if( idProyecto.equals( "todos" )  ){
            idProyecto = null;
        }

        if( idRecurso.equals( "todos" )   ){
            idRecurso = null;
        }

        return recursoService.obtenerRecursosPorProyecto(idProyecto, idRecurso);
    }

    //Buscar recurso
    @RequestMapping(value = "/recurso/{rut_recurso}",method = RequestMethod.GET)
    public List<Recurso> buscarRecurso(

            @PathVariable("rut_recurso") String rutRecurso
    ){
        return recursoService.obtenerRecursos(rutRecurso);
    }

    //insertar o actualizar
    @RequestMapping(method = RequestMethod.POST,headers = {"Content-type=application/json"})
    public ResponseEntity upsert(@RequestBody  Recurso pRec){
        ResponseServicio salida;

        logger.info(pRec.toString());

        salida = recursoService.upsert(pRec);


        if(salida != null || salida.isOk()){
            return ResponseUtils.responseCreated(salida);
        }else{
            return ResponseUtils.responseConflict(salida);
        }

    }

    @RequestMapping(value ="/recurso/{idRecurso}", method = RequestMethod.DELETE)
    public void eliminarRecurso( @PathVariable("idRecurso") String idRecurso ){

        recursoService.eliminarRecurso(idRecurso);
    }



}















