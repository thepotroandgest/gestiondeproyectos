package com.gestion.controller;

import com.gestion.dominio.Esfuerzo_Real;
import com.gestion.dominio.Proyecto;
import com.gestion.dominio.Result;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.service.EsfuerzoRealService;
import com.gestion.utils.ResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
@RestController
@RequestMapping("/esfuerzoReal")
public class EsfuerzoRealController {

    private static final Logger logger = LoggerFactory.getLogger(EsfuerzoRealController.class);


    @Autowired
    private EsfuerzoRealService esfuerzoRealService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Esfuerzo_Real> obtenerEsfuerzo_Real() {
        return esfuerzoRealService.obtenerEsfuerzo_Real();
    }


    @RequestMapping( method = RequestMethod.POST )
    public ResponseEntity insertarTraspasoEsfuerzo(@RequestParam("file") MultipartFile file) {

        Result result = new Result();
        ResponseServicio salida;
        String completeData = null;
        byte[] latin = null;

        if (!file.isEmpty()) {

            try {

                //Versión Windows
//                byte[] bytes = file.getBytes();
//                completeData = new String(bytes);

                byte[] bytes = file.getBytes();
                latin = new String(bytes, "ISO-8859-1").getBytes(  "UTF-8" );
                completeData = new String(latin);

                logger.info( completeData );

            } catch (Exception e) {
                logger.info("excepcio  ---> " + e);
            }

        }

        salida = esfuerzoRealService.insert( completeData );

        if (salida == null || !salida.isOk()) {
            return ResponseUtils.responseConflict(salida);
        }



        return ResponseUtils.responseCreated(salida);
    }



    //insertar o actualizar
    @RequestMapping( value = "/actualizarEsfuerzo", method = RequestMethod.POST,headers = {"Content-type=application/json"})
    public void actualizarEsfuerzo(@RequestBody Esfuerzo_Real esfuerzo) {

        ResponseServicio salida;


        logger.info( esfuerzo.toString() );

        esfuerzoRealService.actualizarEsfuerzoManual(esfuerzo);

    }



}
