package com.gestion.controller;

import com.gestion.dominio.DefinicionHito;
import com.gestion.dominio.Parametro;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.service.DefinicionHitoService;
import com.gestion.service.ParametroService;
import com.gestion.utils.ResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
@RestController
@RequestMapping("/definicionHitos")
public class DefinicionHitoController {

    private static final Logger logger = LoggerFactory.getLogger( DefinicionHitoController.class );


    @Autowired
    private DefinicionHitoService definicionHitoService;

    //Obtener todo
    @RequestMapping(method = RequestMethod.GET)
    public List<DefinicionHito> obtenerParametros(){
        return definicionHitoService.obtenerDefinicionHitos( null );
    }


    @RequestMapping(value = "/hito/{id}",method = RequestMethod.GET)
    public List<DefinicionHito> buscarParametro(@PathVariable("id")  Integer id){
        return definicionHitoService.obtenerDefinicionHitos( id );
    }


    @RequestMapping(method = RequestMethod.POST,headers = {"Content-type=application/json"})
    public ResponseEntity upsert( @RequestBody DefinicionHito defHito ) {

        ResponseServicio salida;

        logger.info( defHito.toString()  );

        salida = definicionHitoService.upsert( defHito );

        if (salida != null || salida.isOk()){
            return ResponseUtils.responseCreated(salida);
        }
        else
        {
            return ResponseUtils.responseConflict(salida);
        }

    }


    //Eliminar recurso
    @RequestMapping(value ="/hito/{id}", method = RequestMethod.DELETE)
    public void eliminarRecurso( @PathVariable("id") Integer id ){

        definicionHitoService.eliminarDefinicionHito( id );
    }
}











