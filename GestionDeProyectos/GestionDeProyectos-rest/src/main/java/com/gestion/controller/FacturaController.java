package com.gestion.controller;

import com.gestion.dominio.Factura;
import com.gestion.dominio.HitoComercial;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.service.FacturaService;
import com.gestion.utils.ResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
@RestController
@RequestMapping("/facturas")
public class FacturaController {

    private static final Logger logger= LoggerFactory.getLogger( FacturaController.class );

    @Autowired
    private FacturaService facturaService;


    //Obtener todo
    @RequestMapping(method = RequestMethod.GET)
    public List<Factura> obtenerFacturas() {

        return facturaService.obtenerFacturas(null, null);
    }

    //Buscar Factura
    @RequestMapping(value = "/factura/{nFactura}",method = RequestMethod.GET)
    public List<Factura> buscarFactura(@PathVariable("nFactura") Integer nFactura){
        logger.info("buscar Factura" + nFactura);

        return facturaService.buscarFactura(nFactura);
    }

    //insertar o actualizar
    @RequestMapping(method = RequestMethod.POST,headers = {"Content-type=application/json"})
    public ResponseEntity upsert(@RequestBody Factura pFact) {

        ResponseServicio salida;
        logger.info(pFact.toString());

        salida = facturaService.upsert(pFact);

        if (salida != null || salida.isOk()) {
            return ResponseUtils.responseCreated(salida);
        } else {
            return ResponseUtils.responseConflict(salida);
        }
    }

    //Eliminar recurso
    @RequestMapping(value="/nFactura/{nFactura}/idProyecto/{idProyecto}/hito/{hito}" , method = RequestMethod.DELETE)
    public void eliminarHitoComercial
    (
         @PathVariable Integer  nFactura
        ,@PathVariable String  idProyecto
        ,@PathVariable String hito


    ) {

        facturaService.eliminarFacturaHito( nFactura, idProyecto, hito );
    }


    //Eliminar recurso
    @RequestMapping(value="/nFactura/{nFactura}" , method = RequestMethod.DELETE)
    public void eliminarFactura
    (
            @PathVariable Integer  nFactura

    ) {

        facturaService.eliminarFactura( nFactura  );
    }





}
