package com.gestion.controller.error;

import com.gestion.dominio.error.ErrorDTO;
import com.gestion.exceptions.HTTPServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Sergio Puas (sjpuas) - Bennu Ltda.
 * Project: rest-core
 */
@ControllerAdvice
public class RestExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(RestExceptionHandler.class);
    private static final String MSG_TAREA_DUPLICADA = "Tarea duplicada";
    private static final String MSG_ARCHIVO_INCORRECTO = "Formato de archivo tarea incorrecto";
    private static final String MSG_ARCHIVO_TRACKING_INCORRECTO = "Formato de archivo Tracking incorrecto";
    private static final String MSG_ELIMINA_PROYECTO = "Este Proyecto tiene elementos asociadas, no se puede eliminar";
    private static final String MSG_ELIMINA_RECURSO = "Recurso asignado";
    private static final String MSG_ELIMINA_CONTRAPARTE = "Contraparte asiganda, no se puede eliminar";
    private static final String MSG_ELIMINA_CLIENTE = "Cliente asigando, no se puede eliminar";
    private static final String MSG_ELIMINA_SISTEMA = "Sistema asigando, no se puede eliminar";


  /**
   * Errores generales
   **/

  @ExceptionHandler({Exception.class})
  @RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
  public
  @ResponseBody
  ResponseEntity<ErrorDTO> handleUncaughtException(Exception ex) throws IOException {
    log.error("Ha ocurrido un error interno al realizar la petición " + ex.getMessage(), ex);
    HttpStatus code;
    ErrorDTO errorDTO;
    if (ex instanceof HTTPServiceException) {
      code = ((HTTPServiceException) ex).getHttpStatus();
      errorDTO = new ErrorDTO(code.value(), ex.getMessage());
    } else{

        code = HttpStatus.INTERNAL_SERVER_ERROR;

        if (ex.getMessage().equals(   MSG_TAREA_DUPLICADA ) ){
          errorDTO = new ErrorDTO(code.value(), MSG_TAREA_DUPLICADA );

        }else if(    ex.getMessage().matches(  "(.*)"+MSG_ARCHIVO_INCORRECTO+"(.*)" )  ){
            errorDTO = new ErrorDTO(code.value(),  ex.getMessage());

        }else if(    ex.getMessage().matches(  "(.*)"+MSG_ARCHIVO_TRACKING_INCORRECTO+"(.*)" )  ){
            errorDTO = new ErrorDTO(code.value(),  ex.getMessage());

        }else if (ex.getMessage().equals(MSG_ELIMINA_PROYECTO)){
            errorDTO = new ErrorDTO(code.value(), MSG_ELIMINA_PROYECTO);
        }else if ( ex.getMessage().matches( "(.*)"+MSG_ELIMINA_PROYECTO+"(.*)" )){
            errorDTO = new ErrorDTO(code.value(), ex.getMessage());
        }else if ( ex.getMessage().matches( "(.*)"+MSG_ELIMINA_RECURSO+"(.*)" )){
            errorDTO = new ErrorDTO(code.value(), ex.getMessage());
        }else if ( ex.getMessage().matches( "(.*)"+MSG_ELIMINA_CONTRAPARTE+"(.*)" )){
            errorDTO = new ErrorDTO(code.value(), ex.getMessage());
        }else if ( ex.getMessage().matches( "(.*)"+MSG_ELIMINA_CLIENTE+"(.*)" )){
            errorDTO = new ErrorDTO(code.value(), ex.getMessage());
        }else if ( ex.getMessage().matches( "(.*)"+MSG_ELIMINA_SISTEMA+"(.*)" )){
            errorDTO = new ErrorDTO(code.value(), ex.getMessage());
        }else{
            errorDTO = new ErrorDTO(code.value(), "Ha ocurrido un error interno al realizar la petición " + ex.getMessage());

        }


//        if (ex.getMessage().equals(MSG_ELIMINA_PROYECTO)){
//            errorDTO = new ErrorDTO(code.value(), MSG_ELIMINA_PROYECTO);
//        }else if ( ex.getMessage().matches( "(.*)"+MSG_ELIMINA_PROYECTO+"(.*)" )){
//            errorDTO = new ErrorDTO(code.value(), ex.getMessage());
//        }else{
//
//            errorDTO = new ErrorDTO(code.value(), "fallo acción");
//        }


    }

    return new ResponseEntity<ErrorDTO>(errorDTO,code);
  }

  @RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
  @ExceptionHandler({MissingServletRequestParameterException.class,
    UnsatisfiedServletRequestParameterException.class,
    ServletRequestBindingException.class
  })
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public
  @ResponseBody
  ErrorDTO handleRequestException(Exception ex) {
    return new ErrorDTO(HttpStatus.BAD_REQUEST.value(),ex.getMessage());
  }


  @RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
  @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
  @ResponseStatus(value = HttpStatus.METHOD_NOT_ALLOWED)
  public
  @ResponseBody
  ErrorDTO handleMethodNotSupported(Exception ex) {
    return new ErrorDTO(HttpStatus.METHOD_NOT_ALLOWED.value(),"Metodo no soportado");
  }

  @RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
  @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
  @ResponseStatus(value = HttpStatus.UNSUPPORTED_MEDIA_TYPE)
  public
  @ResponseBody
  ErrorDTO handleUnsupportedMediaTypeException(HttpMediaTypeNotSupportedException ex) throws IOException {
    return new ErrorDTO(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value(),"Content-type no soportado");
  }

}
