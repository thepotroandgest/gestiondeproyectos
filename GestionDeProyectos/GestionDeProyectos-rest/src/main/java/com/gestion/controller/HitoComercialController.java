package com.gestion.controller;

import com.gestion.dominio.EstadoFacturacion;
import com.gestion.dominio.HitoComercial;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.service.HitoComercialService;
import com.gestion.utils.ResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
@RestController
@RequestMapping("/hitosComerciales")
public class HitoComercialController {

    private static final Logger logger= LoggerFactory.getLogger(HitoComercialController.class);

    @Autowired
    private HitoComercialService hitoComercialService;

    //obtener todo
    @RequestMapping(method = RequestMethod.GET)
    public List<HitoComercial> obtenerHitoComercial(){
        return hitoComercialService.obtenerHitoComercial(null);
    }

    @RequestMapping(value = "/factura/{nFactura}",method = RequestMethod.GET)
    public List<HitoComercial> obtenerHitosPorFactura(@PathVariable("nFactura") Integer nFactura){
        return hitoComercialService.obtenerHitosPorFactura( nFactura, null, null );
    }


    @RequestMapping(value = "/proyecto/{proyecto}/contraparte/{contraparte}",method = RequestMethod.GET)
    public List<EstadoFacturacion> obtenerHitosPorContraparte(@PathVariable("proyecto") String proyecto
                                                            , @PathVariable("contraparte") String contraparte ){

        if( proyecto.equals( "todos" )  ){
            proyecto = null;
        }if( contraparte.equals( "todos" ) ){
            contraparte = null;
        }

        return hitoComercialService.obtenerHitosPorContraparte( null, proyecto, contraparte );

    }


    @RequestMapping(value = "/factura",method = RequestMethod.GET)
    public List<HitoComercial> obtenerHitosPorFactura( ){
        return hitoComercialService.obtenerHitosPorFactura( null, null, null );
    }


    //Buscar
    @RequestMapping(value = "/hitoComercial/{fkIdProyecto}",method = RequestMethod.GET)
    public List<HitoComercial> buscarHitoComercial(@PathVariable("fkIdProyecto") String fkIdProyecto){
        logger.info("Buscar Hito" + fkIdProyecto);
        return hitoComercialService.buscarHitoComercial(fkIdProyecto);
    }
    //insertar
    @RequestMapping(method = RequestMethod.POST,headers = {"Content-type=application/json"})
    public ResponseEntity upsertHitoComercial (@RequestBody HitoComercial hito) {

        ResponseServicio salida;
        logger.info(hito.toString());
        salida = hitoComercialService.upsertHitoComercial(hito);

        if(salida != null || salida.isOk()){
            return ResponseUtils.responseCreated(salida);
        } else{
            return ResponseUtils.responseConflict(salida);
        }

    }
    //-----------------------------BGN VVQ-20-06-2017 ------------------------------
    @RequestMapping(value="/hitos", method = RequestMethod.POST)
    public void upsertHitos(@RequestBody ArrayList<HitoComercial> hitos){

        logger.info(hitos.toString());
        hitoComercialService.upsertHitos(hitos);

    }
    //-----------------------------END VVQ-20-06-2017 ------------------------------

    //Eliminar recurso
    @RequestMapping(value="/hito/{hitoComercial}/proyecto/{nombreProyecto}" , method = RequestMethod.DELETE)
    public void eliminarHitoComercial
    (
             @PathVariable String  hitoComercial
            ,@PathVariable String  nombreProyecto


    ) {

        HitoComercial hito = new HitoComercial();

        hito.setHitoComercial( hitoComercial );
        hito.setFkIdProyecto( nombreProyecto );

        hitoComercialService.eliminarHitoComercial( hito );
    }
}
