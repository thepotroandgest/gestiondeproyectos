package com.gestion.controller;

import com.gestion.dominio.Calendario;
import com.gestion.service.CalendarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Gustavo on 06-03-2017.
 */
@RestController
@RequestMapping("/calendario")
public class CalendarioController {
    @Autowired
    private CalendarioService calendarioService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Calendario> obtenerCalendario(){
        return calendarioService.obtenerCalendario();
    }

    @RequestMapping(value = "/recursos", method = RequestMethod.GET)
    public List<Calendario> obtenerCalendarioRecursos(){
        return calendarioService.obtenerCalendarioRecursos();
    }

}
