package com.gestion.repository;

import com.gestion.dominio.Bitacora;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
public interface BitacoraDao {
    List<Bitacora> obtenerBitacoras( Long secBitacora, String idProyecto, String tipo );
    GeneralPostResponse agregarBitacora(Bitacora pBit);
    void eliminarBitacora(long sec_bitacora);
}
