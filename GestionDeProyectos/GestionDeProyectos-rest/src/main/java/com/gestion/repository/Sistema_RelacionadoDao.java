package com.gestion.repository;

import com.gestion.dominio.Sistema_Relacionado;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
public interface Sistema_RelacionadoDao {
    List<Sistema_Relacionado> obtenerSistema_Relacionados(String idSistema);
    GeneralPostResponse upsert(Sistema_Relacionado pSiRe);
    void eliminarSistema_relacionado(String id_sistema_relacionado);
    public List<Sistema_Relacionado> buscarSistema_relacionado(String id_sistema_relacionado);
}
