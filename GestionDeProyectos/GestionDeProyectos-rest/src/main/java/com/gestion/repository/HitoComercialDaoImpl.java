package com.gestion.repository;

import com.gestion.dominio.EstadoFacturacion;
import com.gestion.dominio.HitoComercial;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.exceptions.GestionDatabaseException;
import com.gestion.utils.SqlDateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Gustavo on 21-02-2017.
 */
@Repository
public class HitoComercialDaoImpl implements HitoComercialDao {

    private static final Logger logger= LoggerFactory.getLogger(HitoComercialDaoImpl.class);

    public static final String SP_OBTENER_HITO_COMERCIAL = "obtenerHitos";
    public static final String SP_BUSCAR_HITO_COMERCIAL= "buscarHito";
    public static final String SP_UPSERT_HITO_COMERCIAL= "upsertHito";
    public static final String SP_ELIMINAR_HITO_COMERCIAL = "eliminarHito";
    public static final String SP_OBTENER_HITOS_POR_FACTURA = "obtenerHitosPorFactura";

    private static final String DATE_FORMAT = "dd-MM-yyyy";

    // SImpleJDBC
    private SimpleJdbcCall procReadCatalogo;
    private SimpleJdbcCall procAgregarCatalogo;
    private SimpleJdbcCall procUpsertCatalogo;
    private SimpleJdbcCall procEliminarCatalogo;
    private SimpleJdbcCall procBuscarCatalogo;
    private SimpleJdbcCall procObtenerHitosPorFactura;
   // private SimpleJdbcCall procSumaCatalogo;

    @PostConstruct
    public void postConstruct() {
        //logger.debug("CatalogoDaoImpl [{}] inicializado", SP_CONSULTA_TABLA_001);
    }
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.procReadCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_OBTENER_HITO_COMERCIAL);
        this.procUpsertCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_UPSERT_HITO_COMERCIAL);
        this.procEliminarCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_ELIMINAR_HITO_COMERCIAL);
        this.procBuscarCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_BUSCAR_HITO_COMERCIAL);
        this.procObtenerHitosPorFactura = new SimpleJdbcCall(dataSource).withProcedureName(SP_OBTENER_HITOS_POR_FACTURA);

        // this.procSumaCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_OBTENER_SUMA_UF);
    }

    //obtener todo
    @Override
    public List<HitoComercial> obtenerHitoComercial(String fkIdProyecto) {
        try {
            logger.info(fkIdProyecto);
            SqlParameterSource in = new MapSqlParameterSource().addValue("_fk_id_proyecto", fkIdProyecto);
            Map out = procReadCatalogo.execute(in);
            logger.info(fkIdProyecto);
            return convierteToHitoComercialList((List) out.get("#result-set-1"));
        } catch (DataAccessException exception) {
            throw new GestionDatabaseException("Error al ejecutar " + SP_OBTENER_HITO_COMERCIAL,"SP001",exception);
        }
    }

    @Override
    public List<HitoComercial> obtenerHitosPorFactura(Integer nFactura, String proyecto, String contraparte) {
        try {
            SqlParameterSource in = new MapSqlParameterSource()
                                        .addValue("_numero_factura", nFactura)
                                        .addValue("_id_proyecto", proyecto)
                                        .addValue("_contraparte", contraparte);
            ;
            Map out = procObtenerHitosPorFactura.execute(in);
            return convierteToHitoComercialList((List) out.get("#result-set-1"));
        } catch (DataAccessException exception) {
            throw new GestionDatabaseException("Error al ejecutar " + SP_OBTENER_HITO_COMERCIAL,"SP001",exception);
        }
    }

    @Override
    public List<EstadoFacturacion> obtenerHitosPorContraparte(Integer nFactura, String proyecto, String contraparte) {
        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("_numero_factura", nFactura)
                    .addValue("_id_proyecto", proyecto)
                    .addValue("_contraparte", contraparte);
            ;
            Map out = procObtenerHitosPorFactura.execute(in);
            return convierteToHitoEstadoFacturacionList((List) out.get("#result-set-1"));
        } catch (DataAccessException exception) {
            throw new GestionDatabaseException("Error al ejecutar " + SP_OBTENER_HITO_COMERCIAL,"SP001",exception);
        }
    }


    //insertar
    @Override
        public GeneralPostResponse upsertHitoComercial(HitoComercial hito) throws GestionDatabaseException {
            GeneralPostResponse generalResponse;
            Integer codError;
            String mensaje;
            Map out;

        try {

            logger.info(hito.toString());

            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("_sec_hito", hito.getSecHito())
                    .addValue("_nombre_hito", hito.getHitoComercial())
                    .addValue("_porcentaje", hito.getPorcentaje())
                    .addValue("_valor_uf", hito.getValorUf())
                    .addValue("_fk_id_proyecto", hito.getFkIdProyecto());


            procUpsertCatalogo.declareParameters(new SqlInOutParameter("codError", Types.INTEGER));
            procUpsertCatalogo.declareParameters(new SqlInOutParameter("mensaje", Types.VARCHAR));

             out = procUpsertCatalogo.execute(in);

            codError = (Integer) out.get("codError");
            mensaje = (String) out.get("mensaje");

            if (codError != 0) {
                generalResponse = new GeneralPostResponse( -1 );
                generalResponse.setMensajeNOK("Fallo insertar Ingresar Hito");
            }else{
                generalResponse = new GeneralPostResponse( 0 );
                generalResponse.setMensajeOK("Hito agregado correctamente");
            }
            generalResponse.setError( codError  );
            generalResponse.setMensaje(  mensaje );


        } catch (DataAccessException exception) {

            String msg = exception.getMessage();
            logger.info("excepción" + exception);

            if(msg.matches("(.*)Duplicate entry(.*)")) {
                msg= "Hito Duplicado";
                throw new GestionDatabaseException(msg, "SP001", exception);
            }
            throw new GestionDatabaseException("Error al ejecutar " + SP_UPSERT_HITO_COMERCIAL,"SP003",exception);
        }

        return generalResponse;
    }

    //-----------------------------BGN VVQ-20-06-2017 ------------------------------

    public void upsertHitos(ArrayList<HitoComercial> hitos){

        logger.info(hitos.toString());
        for(HitoComercial hito: hitos){

            upsertHitoComercial(hito);

        }
    }

    //-----------------------------END VVQ-20-06-2017 ------------------------------


    //Eliminar
    @Override
    public void eliminarHitoComercial(HitoComercial hito){

        try{
            SqlParameterSource in = new MapSqlParameterSource()
                                        .addValue( "_nombre_hito", hito.getHitoComercial() )
                                        .addValue( "_fk_id_proyecto", hito.getFkIdProyecto() );

            Map out = procEliminarCatalogo.execute(in);

        }catch(DataAccessException exception){
            throw new GestionDatabaseException("Error al ejecutar " + SP_ELIMINAR_HITO_COMERCIAL,"SP005",exception);
        }

    }

    private static List<HitoComercial> convierteToHitoComercialList(List rs) {
        List<HitoComercial> resultado = new ArrayList<HitoComercial>();
        for (Object obj : rs) {
            Map fila = (Map) obj;

            HitoComercial hito = new HitoComercial();

            hito.setSecHito((Long) fila.get("sec_hito"));
            hito.setHitoComercial((String) fila.get("nombre_hito"));
            hito.setPorcentaje((Double) fila.get("porcentaje"));
            hito.setValorUf((Double) fila.get("valor_uf"));
            hito.setFkIdProyecto((String) fila.get("fk_id_proyecto"));
            hito.setAbono(  (Double) fila.get( "abono"  ) );
            hito.setSaldo( (Double) fila.get( "saldo" ) );

            logger.info(hito.toString());

            resultado.add(hito);

        }
        return resultado;
    }

    private static List<EstadoFacturacion> convierteToHitoEstadoFacturacionList(List rs) {

        List<EstadoFacturacion> resultado = new ArrayList<>();

        try{


            for (Object obj : rs) {

                Map fila = (Map) obj;

                EstadoFacturacion estado = new EstadoFacturacion();

                estado.setAbono( (Double)fila.get( "abono" )  );
                estado.setEstadoFactura( (String)fila.get( "estado_factura" ) );
                estado.setEstadoHito( (String)fila.get( "estado_hito" ) );
                estado.setEstadoProyecto( (String)fila.get( "estado_proyecto" ) );


                if( fila.get( "fecha_emision" ) != null ) {
                    estado.setFechaEmision(SqlDateUtils.timestam2String(new Timestamp(((Date) fila.get("fecha_emision")).getTime()), DATE_FORMAT));
                }else{
                    estado.setFechaEmision( null);
                }

                estado.setFkIdContraparte( (String)fila.get( "fk_id_contraparte" ) );
                estado.setNombreHito( (String)fila.get( "nombre_hito" ) );
                estado.setValorUf( (Double)fila.get( "valor_uf" )  );
                estado.setSaldo( (Double)fila.get( "saldo" ) );
                estado.setPorcentaje( (Double)fila.get( "porcentaje" ) );
                estado.setNumeroFactura( (Integer)fila.get( "numero_factura" ) );
                estado.setFkIdProyecto(  ( String )fila.get( "fk_id_proyecto" )     );

                logger.info( estado.toString() );

                resultado.add( estado );
            }

            return resultado;

        }catch( Exception e ){
            logger.info( e.getMessage() );
            throw e;
        }



    }


}
