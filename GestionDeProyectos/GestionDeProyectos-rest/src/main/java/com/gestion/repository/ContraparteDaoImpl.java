package com.gestion.repository;

import com.gestion.dominio.Contraparte;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.exceptions.GestionDatabaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Gustavo on 21-02-2017.
 */
@Repository
public class ContraparteDaoImpl implements ContraparteDao {

    private static final Logger logger = LoggerFactory.getLogger( ContraparteDaoImpl.class );


    // NOMBRES DE PROCEDIMIENTOS UTILIZADOS
    public static final String SP_OBTENER_CONTRAPARTES = "obtenerContrapartes";
    public static final String SP_BUSCAR_CONTRAPARTE = "buscarContraparte";
    public static final String SP_UPSERT_CONTRAPARTE = "upsertContraparte";
    public static final String SP_ELIMINAR_CONTRAPARTE = "eliminarContraparte";
    // SImpleJDBC
    private SimpleJdbcCall procReadCatalogo;
    private SimpleJdbcCall procUpSertCatalogo;
    private SimpleJdbcCall procEliminarCatalogo;
    private SimpleJdbcCall procBuscarCatalogo;
    @PostConstruct
    public void postConstruct(){
        //logger.
    }
    //generar la conexión al procedimiento almacenado
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.procReadCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_OBTENER_CONTRAPARTES);
        this.procUpSertCatalogo = new SimpleJdbcCall(dataSource)
                .withProcedureName(SP_UPSERT_CONTRAPARTE);
        this.procEliminarCatalogo = new SimpleJdbcCall(dataSource)
                .withProcedureName(SP_ELIMINAR_CONTRAPARTE);
        this.procBuscarCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_BUSCAR_CONTRAPARTE);
    }

    //Obtener todas las contrapartes
    @Override
    public List<Contraparte> obtenerContrapartes( String idContraparte) {
        try{
            SqlParameterSource in =  new MapSqlParameterSource().addValue("_id_contraparte", idContraparte);

            Map out = procReadCatalogo.execute( in );
            return convierteToContraparteList((List) out.get("#result-set-1"));
        }catch(DataAccessException e){
            throw new GestionDatabaseException("Error al ejecutar " + SP_OBTENER_CONTRAPARTES,"SPCONTRAPARTES001",e);
        }
    }
    //Obtener una contraparte
    @Override
    public List<Contraparte> buscarContraparte(String id_contraparte){
        try{
            SqlParameterSource in =  new MapSqlParameterSource().addValue("id_contraparte", id_contraparte);
            Map out = procBuscarCatalogo.execute(in);
            return convierteToContraparteList((List) out.get("#result-set-1"));
        }catch (DataAccessException exception){
            throw new GestionDatabaseException("Error al ejecutar " + SP_BUSCAR_CONTRAPARTE,"SPCONTRAPARTEBUSACAR002",exception);
        }
    }
    //Actualizar o insertar
    @Override
    public GeneralPostResponse upsert(Contraparte pCon) {

        GeneralPostResponse generalResponse;
        Integer codError;
        String mensaje;
        Map out;

        try {

            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("_id_contraparte", pCon.getIdContraparte())
                    .addValue("_nombre", pCon.getNombre())
                    .addValue("_telefono", pCon.getTelefono())
                    .addValue("_correo", pCon.getCorreo())
                    .addValue("_fk_rut_cliente", pCon.getFkRutCliente());

            procUpSertCatalogo.declareParameters( new SqlInOutParameter( "codError", Types.INTEGER ) );
            procUpSertCatalogo.declareParameters( new SqlInOutParameter( "mensaje", Types.VARCHAR ) );

            out = procUpSertCatalogo.execute(in);

            codError = (Integer) out.get("codError");
            mensaje = (String) out.get("mensaje");

            if (codError != 0) {
                generalResponse = new GeneralPostResponse( -1 );
                generalResponse.setMensajeNOK("Fallo insertar Contraparte");
            }else{
                generalResponse = new GeneralPostResponse( 0 );
                generalResponse.setMensajeOK("Contraparte agregado correctamente");

            }

            generalResponse.setError( codError  );
            generalResponse.setMensaje(  mensaje );


        } catch (DataAccessException exception) {

            String msg = exception.getMessage();
            logger.info("excepcion "+ exception  );


            if( msg.matches(  "(.*)Duplicate entry(.*)"  )  ){
                msg = "Contraparte duplicado";
                throw new GestionDatabaseException( msg ,"SP001",exception);
            }


            throw new GestionDatabaseException("Error al ejecutar " + SP_UPSERT_CONTRAPARTE,"SP003",exception);
        }


        return generalResponse;
    }
    //Eliminar
    @Override
    public void eliminarContraparte(String idContraparte){
        try{
            SqlParameterSource in = new MapSqlParameterSource().addValue("_id_contraparte", idContraparte);
            Map out = procEliminarCatalogo.execute(in);
        }catch(DataAccessException exception){



            String msg = exception.getMessage();

            if( msg.matches(  "(.*)Cannot delete or update a parent row: a foreign key(.*)"  )  ){
                msg = "Contraparte asiganda, no se puede eliminar";
                throw new GestionDatabaseException( msg ,"SP001",exception);
            }



            throw new GestionDatabaseException("Error al ejecutar " + SP_ELIMINAR_CONTRAPARTE,"SP004",exception);
        }
    }

    //Mapear las contrapartes a utilizar
    private List<Contraparte> convierteToContraparteList(List rs) {
        List<Contraparte> resultado = new ArrayList<Contraparte>();
        for (Object obj : rs) {
            Map fila = (Map) obj;

            Contraparte contraparte = new Contraparte();

            contraparte.setIdContraparte( (String) fila.get("id_contraparte") );
            contraparte.setNombre( (String) fila.get("nombre") );
            contraparte.setTelefono(  (String) fila.get("telefono") );
            contraparte.setCorreo(  (String) fila.get("correo")  );
            contraparte.setFkRutCliente( (String) fila.get("fk_rut_cliente") );

            resultado.add( contraparte );

        }
        return resultado;
    }
}
