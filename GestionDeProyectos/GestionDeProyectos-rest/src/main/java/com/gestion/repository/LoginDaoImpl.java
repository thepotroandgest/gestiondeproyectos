package com.gestion.repository;

import com.gestion.dominio.Login;
import com.gestion.exceptions.GestionDatabaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Gustavo on 21-02-2017.
 */
@Repository
public class LoginDaoImpl implements LoginDao {
    // NOMBRES DE PROCEDIMIENTOS UTILIZADOS
    public static final String SP_OBTENER_LOGIN = "obtenerLogin";
    private SimpleJdbcCall procReadCatalogo;
    // SImpleJDBC
    @PostConstruct
    public void postConstruct(){
        //algo hace
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.procReadCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_OBTENER_LOGIN);
    }

    @Override
    public List<Login> obtenerLogin() {
        try {
            Map out = procReadCatalogo.execute();
            return convierteTobuscarProcesoLista((List) out.get("#result-set-1"));
        } catch (DataAccessException exception){
            throw new GestionDatabaseException("Error al ejecutar " + SP_OBTENER_LOGIN,"SPRECURSO001",exception);
        }
    }

    private static List<Login> convierteTobuscarProcesoLista(List rs) {
        List<Login> resultado = new ArrayList<Login>();
        for (Object obj : rs) {
            Map fila = (Map) obj;
            resultado.add(new Login.Builder()
                    .setRut((String) fila.get("rut"))
                    .setPassword((String) fila.get("password"))
                    .build());
        }
        return resultado;
    }

}
