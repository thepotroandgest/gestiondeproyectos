package com.gestion.repository;

import com.gestion.dominio.EstadoProyecto;
import com.gestion.dominio.Proyecto;
import com.gestion.dominio.Recurso;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
public interface ProyectoDao {
    public List<Proyecto> obtenerProyectos( String idProyecto );
    public List<Recurso> obtenerRecursosExtras( String idProyecto );
    public List<EstadoProyecto> obtenerEstado( String idProyecto );
    GeneralPostResponse upsert( Proyecto pPro );
    GeneralPostResponse eliminarProyecto( String id_proyecto );
    public List<Proyecto> buscarProyecto( String id_proyecto );
}
