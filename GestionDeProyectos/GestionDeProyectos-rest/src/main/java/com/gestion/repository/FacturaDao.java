package com.gestion.repository;

import com.gestion.dominio.Factura;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
public interface FacturaDao {
    public List<Factura> obtenerFacturas(String idProyecto, Integer nFactura);
    GeneralPostResponse upsert(Factura pFact);
    void eliminarFactura(Integer nFactura);
    void eliminarFacturaHito(Integer nFactura, String idProyecto, String hito );

}
