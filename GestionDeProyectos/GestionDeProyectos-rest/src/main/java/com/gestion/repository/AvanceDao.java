package com.gestion.repository;

import com.gestion.dominio.Avance;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.List;

/**
 * Created by cvera on 16-06-17.
 */
public interface AvanceDao{

    GeneralPostResponse agregarAvance(Avance avance );
    List<Avance> obtenerAvance(Long idAvance, String idProyecto);
    void eliminarAvance(Long idAvance);

}
