package com.gestion.repository;

import com.gestion.dominio.Recurso;
import com.gestion.dominio.RecursosPorProyecto;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.exceptions.GestionDatabaseException;
import com.gestion.utils.SqlDateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * Created by Gustavo on 15-02-2017.
 */

@Repository
public class RecursoDaoImpl implements RecursoDao {

    private static final Logger logger = LoggerFactory.getLogger( RecursoDaoImpl.class );

    private static final String DATE_FORMAT = "dd-MM-yyyy";


    // NOMBRES DE PROCEDIMIENTOS UTILIZADOS
    public static final String SP_OBTENER_RECURSOS = "obtenerRecursos";
    public static final String SP_BUSCAR_RECURSO = "buscarRecurso";
    public static final String SP_UPSERT_RECURSO = "upsertRecurso";
    public static final String SP_ELIMINAR_RECURSO = "eliminarRecurso";
    public static final String SP_OBTENER_RECURSO_POR_PROYECTO = "obtenerRecursosPorProyecto";


    // SImpleJDBC
    private SimpleJdbcCall procReadCatalogo;
    private SimpleJdbcCall procUpSertCatalogo;
    private SimpleJdbcCall procEliminarCatalogo;
    private SimpleJdbcCall procBuscarCatalogo;
    private SimpleJdbcCall procObteberRecursoPorProyecto;

    @PostConstruct
    public void postConstruct(){
        //algo hace
    }

    //generar la conexión al procedimiento almacenado+
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.procReadCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_OBTENER_RECURSOS);
        this.procUpSertCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_UPSERT_RECURSO);
        this.procEliminarCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_ELIMINAR_RECURSO);
        this.procBuscarCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_BUSCAR_RECURSO);
        this.procObteberRecursoPorProyecto = new SimpleJdbcCall(dataSource).withProcedureName(SP_OBTENER_RECURSO_POR_PROYECTO);
    }

    @Override
    public List<RecursosPorProyecto> obtenerRecursosPorProyecto(String idProyecto, String idRecurso){

        SqlParameterSource in;

        try {

            in = new MapSqlParameterSource()
                    .addValue( "_id_proyecto", idProyecto )
                    .addValue( "_id_recurso", idRecurso);

            Map out = procObteberRecursoPorProyecto.execute(in);

            //Mapear recursos
            return convierteToRecursosPorProyectoLista( ( List ) out.get( "#result-set-1" ) );

        } catch (DataAccessException exception){

            throw new GestionDatabaseException("Error al ejecutar " + SP_OBTENER_RECURSO_POR_PROYECTO,"SPRECURSO001",exception);
        }

    }


    //Obtener Recursos
    @Override
        public List<Recurso> obtenerRecursos(String rutRecurso) {

            SqlParameterSource in;

            try {
                in = new MapSqlParameterSource()
                        .addValue("_rut_recurso", rutRecurso);

                Map out = procReadCatalogo.execute(in);

                //Mapear recursos
                return convierteToRecursosLista((List) out.get("#result-set-1"));

            } catch (DataAccessException exception){

                throw new GestionDatabaseException("Error al ejecutar " + SP_OBTENER_RECURSOS,"SPRECURSO001",exception);

        }


    }

    //Actualizar o insertar
    @Override
    public GeneralPostResponse upsert(Recurso pRec) {
        GeneralPostResponse generalResponse = null;
        Integer codError;
        String mensaje;
        Map out;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("rut_recurso", pRec.getRutRecurso())
                    .addValue("id_recurso", pRec.getIdRecurso())
                    .addValue("nombre", pRec.getNombre())
                    ;

            procUpSertCatalogo.declareParameters(new SqlOutParameter("codError", Types.INTEGER));
            procUpSertCatalogo.declareParameters(new SqlOutParameter("mensaje",  Types.VARCHAR));

            out = procUpSertCatalogo.execute(in);

            codError = (Integer) out.get("codError");
            mensaje = (String) out.get("mensaje");
            
//
            if(codError != 0){
                generalResponse = new GeneralPostResponse( -1 );
                generalResponse.setMensajeNOK("fallo al insertar recurso");
            }else{
                generalResponse = new GeneralPostResponse( 0 );
                generalResponse.setMensajeOK("Recurso agregado exitosamente");

            }

            generalResponse.setError(codError);
            generalResponse.setMensaje(mensaje);

        } catch (DataAccessException exception) {
            String msg = exception.getMessage();
            logger.info("excepcion "+ exception  );


            if( msg.matches(  "(.*)Duplicate entry(.*)"  )  ){
                msg = "Recurso duplicado";
                throw new GestionDatabaseException( msg ,"SP001",exception);
            }

            throw new GestionDatabaseException("Error al ejecutar " + SP_UPSERT_RECURSO,"SP003",exception);
        }catch( Exception e ){
            logger.info("excepcion "+ e  );
        }

        return generalResponse;
    }


    //Eliminar
    @Override
    public void eliminarRecurso(String idRecurso){
        try{
            SqlParameterSource in = new MapSqlParameterSource().addValue("_id_recurso", idRecurso);
            Map out = procEliminarCatalogo.execute(in);
        }catch(DataAccessException exception){

            String msg = exception.getMessage();

            if( msg.matches(  "(.*)Cannot delete or update a parent row: a foreign key(.*)"  )  ){
                msg = "Recurso asignado, no se puede eliminar";
                throw new GestionDatabaseException( msg ,"SP001",exception);
            }


            throw new GestionDatabaseException("Error al ejecutar " + SP_ELIMINAR_RECURSO,"SP004",exception);
        }
    }


    //Mapear los recursos para mostrar
    private static List<Recurso> convierteToRecursosLista(List rs) {
        List<Recurso> resultado = new ArrayList<Recurso>();
        for (Object obj : rs) {
            Map fila = (Map) obj;
            Recurso recurso = new Recurso();

            recurso.setRutRecurso((String) fila.get("rut_recurso"));
            recurso.setNombre((String) fila.get("nombre"));
            recurso.setIdRecurso((String) fila.get ("id_recurso"));

            resultado.add(recurso);

        }

        return resultado;
    }


    //Mapear los recursos para mostrar
    private static List<RecursosPorProyecto> convierteToRecursosPorProyectoLista(List rs) {

        List<RecursosPorProyecto> resultado = new ArrayList<>();

        for (Object obj : rs) {

            Map fila = (Map) obj;
            RecursosPorProyecto recurso = new RecursosPorProyecto();

            recurso.setNombre( (String)fila.get("nombre") );
            recurso.setRutRecurso( (String)fila.get("rut_recurso") );
            recurso.setIdRecurso( (String)fila.get("id_recurso") );
            recurso.setFechaInicio(  SqlDateUtils.timestam2String( new Timestamp( ((Date) fila.get( "fecha_inicio" ) ).getTime()),DATE_FORMAT )   );
            recurso.setFechaTermino(  SqlDateUtils.timestam2String( new Timestamp( ((Date) fila.get( "fecha_termino" ) ).getTime()),DATE_FORMAT ) );
            recurso.setFkIdProyecto( (String)fila.get("fk_id_proyecto") );

            resultado.add(recurso);
        }

        return resultado;
    }


}
