package com.gestion.repository;

import com.gestion.dominio.Sistema_Relacionado;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.exceptions.GestionDatabaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Gustavo on 21-02-2017.
 */
@Repository
public class Sistema_RelacionadoDaoImpl implements Sistema_RelacionadoDao {


    private static final Logger logger = LoggerFactory.getLogger( Sistema_RelacionadoDaoImpl.class );



    // NOMBRES DE PROCEDIMIENTOS UTILIZADOS
    public static final String SP_OBTENER_SISTEMAS_RELACIONADOS="obtenerSistema_relacionado";
    public static final String SP_BUSCAR_SISTEMA_RELACIONADO = "buscarSistema_relacionado";
    public static final String SP_UPSERT_SISTEMA_RELACIONADO  = "upsertSistemaRelacionado";
    public static final String SP_ELIMINAR_SISTEMA_RELACIONADO  = "eliminarSistemaRelacionado";
    // SImpleJDBC
    private SimpleJdbcCall procReadCatalogo;
    private SimpleJdbcCall procUpSertCatalogo;
    private SimpleJdbcCall procEliminarCatalogo;
    private SimpleJdbcCall procBuscarCatalogo;
    @PostConstruct
    public void postConstruct() {
        //logger.debug("CatalogoDaoImpl [{}] inicializado", SP_CONSULTA_TABLA_001);
    }
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.procReadCatalogo = new SimpleJdbcCall(dataSource)
                .withProcedureName(SP_OBTENER_SISTEMAS_RELACIONADOS);
        this.procUpSertCatalogo = new SimpleJdbcCall(dataSource)
                .withProcedureName(SP_UPSERT_SISTEMA_RELACIONADO);
        this.procEliminarCatalogo = new SimpleJdbcCall(dataSource)
                .withProcedureName(SP_ELIMINAR_SISTEMA_RELACIONADO);
        this.procBuscarCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_BUSCAR_SISTEMA_RELACIONADO);
    }
    //obtener todo
    @Override
    public List<Sistema_Relacionado> obtenerSistema_Relacionados(String idSistema) {
        try {

            SqlParameterSource in =  new MapSqlParameterSource().addValue( "_id_sistema_relacionado", idSistema );
            Map out = procReadCatalogo.execute(in);
            return convierteToSistemaRelacionadoList((List) out.get("#result-set-1"));
        } catch (DataAccessException exception) {
            throw new GestionDatabaseException("Error al ejecutar " + SP_OBTENER_SISTEMAS_RELACIONADOS,"SPSISTEMARELACIONADO001",exception);
        }
    }
    //Obtener un recurso
    @Override
    public List<Sistema_Relacionado> buscarSistema_relacionado(String id_sistema_relacionado){
        try{
            SqlParameterSource in =  new MapSqlParameterSource().addValue("id_sistema_relacionado", id_sistema_relacionado);
            Map out = procBuscarCatalogo.execute(in);
            return convierteToSistemaRelacionadoList((List) out.get("#result-set-1"));
        }catch (DataAccessException exception){
            throw new GestionDatabaseException("Error al ejecutar " + SP_BUSCAR_SISTEMA_RELACIONADO,"SPSISTEMARELACIONADOBUSACAR002",exception);
        }
    }
    //Actualizar o insertar
    @Override
    public GeneralPostResponse upsert(Sistema_Relacionado pSiRe) {

        GeneralPostResponse generalResponse;
        Integer codError;
        String mensaje;
        Map out;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("_id_sistema_relacionado", pSiRe.getIdSistemaRelacionado())
                    .addValue("_nombre", pSiRe.getNombre());

            procUpSertCatalogo.declareParameters( new SqlInOutParameter( "codError", Types.INTEGER ) );
            procUpSertCatalogo.declareParameters( new SqlInOutParameter( "mensaje", Types.VARCHAR ) );

            out = procUpSertCatalogo.execute(in);

            codError = (Integer) out.get("codError");
            mensaje = (String) out.get("mensaje");

            if (codError != 0) {
                generalResponse = new GeneralPostResponse( -1 );
                generalResponse.setMensajeNOK("Fallo insertar Sistema Relacionado");
            }else{
                generalResponse = new GeneralPostResponse( 0 );
                generalResponse.setMensajeOK("Sistema Relacionado agregado correctamente");

            }

            generalResponse.setError( codError  );
            generalResponse.setMensaje(  mensaje );

        } catch (DataAccessException exception) {


            String msg = exception.getMessage();
            logger.info("excepcion "+ exception  );


            if( msg.matches(  "(.*)Duplicate entry(.*)"  )  ){
                msg = "Contraparte duplicado";
                throw new GestionDatabaseException( msg ,"SP001",exception);
            }


            throw new GestionDatabaseException("Error al ejecutar " + SP_UPSERT_SISTEMA_RELACIONADO,"SP003",exception);
        }

        return generalResponse;
    }
    //Eliminar
    @Override
    public void eliminarSistema_relacionado(String idSistemaRelacionado){
        try{
            SqlParameterSource in = new MapSqlParameterSource().addValue("_id_sistema_relacionado", idSistemaRelacionado);
            Map out = procEliminarCatalogo.execute(in);
        }catch(DataAccessException exception){

            String msg = exception.getMessage();

            if( msg.matches(  "(.*)Cannot delete or update a parent row: a foreign key(.*)"  )  ){
                msg = "Sistema asignado, no se puede eliminar";
                throw new GestionDatabaseException( msg ,"SP001",exception);
            }

            throw new GestionDatabaseException("Error al ejecutar " + SP_ELIMINAR_SISTEMA_RELACIONADO,"SP004",exception);
        }
    }
    private List<Sistema_Relacionado> convierteToSistemaRelacionadoList(List rs) {
        List<Sistema_Relacionado> resultado = new ArrayList<Sistema_Relacionado>();
        for (Object obj : rs) {
            Map fila = (Map) obj;

            Sistema_Relacionado sr = new Sistema_Relacionado();

            sr.setIdSistemaRelacionado( ( String )fila.get("id_sistema_relacionado") );
            sr.setNombre( ( String )fila.get( "nombre" ) );

            resultado.add( sr );
        }
        return resultado;
    }
}
