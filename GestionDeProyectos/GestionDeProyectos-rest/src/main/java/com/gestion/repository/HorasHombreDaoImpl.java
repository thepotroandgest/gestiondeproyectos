package com.gestion.repository;

import com.gestion.dominio.Result;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.dominio.response.TareaPostResponse;
import com.gestion.exceptions.GestionDatabaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Types;
import java.util.Map;

/**
 * Created by Gustavo on 10-05-2017.
 */
@Repository
public class HorasHombreDaoImpl implements HorasHombreDao{

    private static final Logger logger = LoggerFactory.getLogger(HorasHombreDaoImpl.class);

    // NOMBRES DE PROCEDIMIENTOS UTILIZADOS
    public static final String SP_INSERTAR_ESFUERZO_REAL = "insertarTraspasoEsfuerzoReal";
    public static final String SP_TRASPASAR_HORAS_HOMBRE = "traspasar_traspaso_horas_hombre";
    public static final String SP_LIMPIAR_TRASPASO_ESFUERZO =  "limpiaTraspasoEsfuerzoReal";

    private static final String DATE_FORMAT = "dd-MM-yy HH:mm";

    private SimpleJdbcCall procInsertTraspasoEsfuerzoReal;
    private SimpleJdbcCall procTraspasarTraspaso;
    private SimpleJdbcCall procLimpiarTraspasoEsfuerzo;



    // SImpleJDBC
    @PostConstruct
    public void postConstruct(){
        //algo hace
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {

        this.procInsertTraspasoEsfuerzoReal = new SimpleJdbcCall(dataSource).withProcedureName( SP_INSERTAR_ESFUERZO_REAL );
        this.procTraspasarTraspaso = new SimpleJdbcCall(dataSource).withProcedureName( SP_TRASPASAR_HORAS_HOMBRE );
        this.procLimpiarTraspasoEsfuerzo = new SimpleJdbcCall(dataSource).withProcedureName( SP_LIMPIAR_TRASPASO_ESFUERZO );
    }

    @Override
    public ResponseServicio insert(){

        TareaPostResponse resultado = new TareaPostResponse();
        Map out;
        Integer codError;
        String mensaje;

        try {

            procTraspasarTraspaso.declareParameters( new SqlInOutParameter( "codError", Types.INTEGER ) );
            procTraspasarTraspaso.declareParameters( new SqlInOutParameter( "mensaje", Types.VARCHAR ) );

            MapSqlParameterSource in = new MapSqlParameterSource()
                                            .addValue( "codError", -1  )
                                            .addValue( "mensaje", "default"  );

            out = procTraspasarTraspaso.execute( in );

            codError = (Integer) out.get("codError");
            mensaje = (String) out.get("mensaje");

            if (codError != 0) {
                resultado = new TareaPostResponse( -1 );
            }else{
                resultado = new TareaPostResponse( 0 );
            }

            resultado.setError( codError  );
            resultado.setMensaje(  mensaje );


        } catch (DataAccessException exception) {

            String msg = exception.getMessage();
            logger.info("excepcion "+ exception  );


            if( msg.matches(  "(.*)Duplicate entry(.*)"  )  ){
                msg = "Tarea duplicada";
                procLimpiarTraspasoEsfuerzo.execute();
                throw new GestionDatabaseException( msg ,"SP001",exception);
            }

            throw new GestionDatabaseException("Error al ejecutar " + SP_TRASPASAR_HORAS_HOMBRE +" " + msg ,"SP001",exception);
        }catch( Exception e ){
            logger.info("excepcion "+ e  );
        }


        return resultado;
    }





}
