package com.gestion.repository;

import com.gestion.dominio.Bitacora;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.exceptions.GestionDatabaseException;
import com.gestion.utils.SqlDateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Gustavo on 21-02-2017.
 */
@Repository
public class BitacoraDaoImpl implements BitacoraDao {

    private static final Logger logger = LoggerFactory.getLogger(BitacoraDaoImpl.class);


    public static final String SP_OBTENER_BITACORAS= "obtenerBitacora";
    public static final String SP_AGREGAR_BITACORA= "upsertBitacora";
    public static final String SP_ELIMINAR_BITACORA = "eliminarBitacora";
    public static final String SP_BITACORA_PROYECTO = "buscarBitacoraPorProyecto";

    private SimpleJdbcCall procReadCatalogo;
    private SimpleJdbcCall procAgregarCatalogo;
    private SimpleJdbcCall procEliminarCatalogo;
    private SimpleJdbcCall procBuscarCatalogo;
    private SimpleJdbcCall procBuscarBitacoraProyectoCatalogo;

    private static final String DATE_FORMAT = "dd-MM-yyyy";



    @PostConstruct
    public void postConstruct(){
        //algo hace
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.procReadCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_OBTENER_BITACORAS);
        this.procAgregarCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_AGREGAR_BITACORA);
        this.procEliminarCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_ELIMINAR_BITACORA);
        this.procBuscarBitacoraProyectoCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_BITACORA_PROYECTO);
    }

    @Override
    public List<Bitacora> obtenerBitacoras( Long secBitacora, String idProyecto, String tipo ) {

        try {

            SqlParameterSource in = new MapSqlParameterSource()
                                        .addValue( "_fk_id_proyecto", idProyecto )
                                        .addValue( "_sec_bitacora", secBitacora )
                                        .addValue( "_tipo", tipo );


            Map out = procReadCatalogo.execute( in );
            return convierteToBitacoraList((List) out.get("#result-set-1"));
        } catch (DataAccessException exception){
            throw new GestionDatabaseException("Error al ejecutar " + SP_OBTENER_BITACORAS,"SPCLIENTE001",exception);
        }
    }

    //insertar
    @Override
    public GeneralPostResponse agregarBitacora(Bitacora pBit ) {

        GeneralPostResponse generalResponse;
        Integer codError;
        String mensaje;
        Map out;

        try {

            logger.info( pBit.toString() );



            SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_tipo", pBit.getTipo())
                .addValue("_asunto", pBit.getAsunto())
                .addValue("_descripcion", pBit.getDescripcion())
                .addValue("_rut_responsable", pBit.getRutResponsable() )
                .addValue("_fecha_ingreso",   SqlDateUtils.string2Timestamp( pBit.getFechaIngreso() , DATE_FORMAT)    )
                .addValue("_fecha_vencimiento",  SqlDateUtils.string2Timestamp( pBit.getFechaVencimiento() , DATE_FORMAT) )
                .addValue("_fk_id_proyecto", pBit.getFkIdProyecto())
                .addValue("_link", pBit.getLink());


            procAgregarCatalogo.declareParameters( new SqlInOutParameter( "codError", Types.INTEGER ) );
            procAgregarCatalogo.declareParameters( new SqlInOutParameter( "mensaje", Types.VARCHAR ) );

            out = procAgregarCatalogo.execute(in);

            codError = (Integer) out.get("codError");
            mensaje = (String) out.get("mensaje");

            if (codError != 0) {
                generalResponse = new GeneralPostResponse( -1 );
                generalResponse.setMensajeNOK("Fallo insertar Cliente");
            }else{
                generalResponse = new GeneralPostResponse( 0 );
                generalResponse.setMensajeOK("Cliente agregado correctamente");
            }

            generalResponse.setError( codError  );
            generalResponse.setMensaje(  mensaje );

        } catch (DataAccessException exception) {
            String msg = exception.getMessage();
            logger.info("excepcion "+ exception  );


            if( msg.matches(  "(.*)Duplicate entry(.*)"  )  ){
                msg = "Cliente duplicado";
                throw new GestionDatabaseException( msg ,"SP001",exception);
            }

            throw new GestionDatabaseException("Error al ejecutar " + SP_AGREGAR_BITACORA +" " + msg ,"SP001",exception);
        }catch(Exception e ){
            logger.info("excepcion "+ e  );
            throw e;
        }

        return generalResponse;

    }

    //Eliminar
    @Override
    public void eliminarBitacora(long sec_bitacora){
        try{
            SqlParameterSource in = new MapSqlParameterSource().addValue("sec_bitacora", sec_bitacora);
            Map out = procEliminarCatalogo.execute(in);
        }catch(DataAccessException exception){
            throw new GestionDatabaseException("Error al ejecutar " + SP_ELIMINAR_BITACORA,"SP004",exception);
        }
    }
    //Mapear
    private static List<Bitacora> convierteToBitacoraList(List rs) {
        List<Bitacora> resultado = new ArrayList<>();


        for (Object obj : rs) {
            Map fila = (Map) obj;

            Bitacora bitacora = new Bitacora();

            bitacora.setFkIdProyecto( ( String )fila.get( "fk_id_proyecto" )   );
            bitacora.setAsunto( ( String )fila.get( "asunto" )  );
            bitacora.setDescripcion( ( String )fila.get( "descripcion" )   );
            bitacora.setFechaIngreso(  SqlDateUtils.timestam2String( new Timestamp( ((Date) fila.get( "fecha_ingreso" ) ).getTime()),DATE_FORMAT )   );

            if( fila.get( "fecha_vencimiento" )  != null ){
                bitacora.setFechaVencimiento(  SqlDateUtils.timestam2String( new Timestamp( ((Date) fila.get( "fecha_vencimiento" ) ).getTime()),DATE_FORMAT )  );
            }else{
                bitacora.setFechaVencimiento( null );
            }

            bitacora.setIdRecurso( (String)fila.get( "id_recurso" ) );
            bitacora.setTipo( (String)fila.get( "tipo" ) );
            bitacora.setSecBitacora( (Long)fila.get( "sec_bitacora" )  );
            bitacora.setLink( (String)fila.get( "link" ) );
            bitacora.setRutResponsable( (String)fila.get( "rut_responsable" ) );

            resultado.add( bitacora );
        }

        return resultado;
    }
}




















