package com.gestion.repository;

import com.gestion.dominio.Tarea;
import com.gestion.dominio.TraspasoTarea;
import com.gestion.dominio.response.TareaPostResponse;
import com.gestion.exceptions.GestionDatabaseException;
import com.gestion.utils.SqlDateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Gustavo on 21-02-2017.
 */
@Repository
public class TareaDaoImpl implements TareaDao {

    // NOMBRES DE PROCEDIMIENTOS UTILIZADOS
    private static final Logger logger = LoggerFactory.getLogger(TareaDaoImpl.class);

    public static  final String SP_INSERTAR_TAREAS = "guardarActualizarTarea";
    public static final String SP_OBTENER_TAREAS = "listarTareas";
    public static final String SP_TRASPASAR_TAREA = "traspasarTraspasoTarea";
    public static final String SP_INSERTAR_PROYECTOS = "insertarTraspasoTarea";
    public static final String SP_LIMPIAR_TRASPASO_TAREA =  "limpiaTraspasoTarea";
    public static final String SP_ELIMINAR_TAREA = "eliminarTarea";
    private static final String DATE_FORMAT = "dd-MM-yy";
    // SImpleJDBC
    private SimpleJdbcCall procEliminarTarea;
    private SimpleJdbcCall procInsertarActualizarTareas;
    private SimpleJdbcCall procReadTarea;
    private SimpleJdbcCall procInsertTraspaso;
    private SimpleJdbcCall procTraspasarTarea;
    private SimpleJdbcCall procLimpiarTraspasoTarea;



    @PostConstruct
    public void postConstruct(){
        //algo hace
    }
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.procInsertarActualizarTareas = new SimpleJdbcCall(dataSource).withProcedureName(SP_INSERTAR_TAREAS);
        this.procEliminarTarea = new SimpleJdbcCall(dataSource).withProcedureName(SP_ELIMINAR_TAREA);
        this.procReadTarea = new SimpleJdbcCall(dataSource).withProcedureName( SP_OBTENER_TAREAS );
        this.procInsertTraspaso = new SimpleJdbcCall(dataSource).withProcedureName( SP_INSERTAR_PROYECTOS );
        this.procTraspasarTarea = new SimpleJdbcCall(dataSource).withProcedureName( SP_TRASPASAR_TAREA );
        this.procLimpiarTraspasoTarea = new SimpleJdbcCall(dataSource).withProcedureName( SP_LIMPIAR_TRASPASO_TAREA );
    }


    @Override
    public TareaPostResponse insertarActualizarTareas(Tarea tTarea) {

            TareaPostResponse resultado = new TareaPostResponse();
            Integer codError;
            String mensaje;

        try {

            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("_id_traspaso", tTarea.getId_traspaso())
                    .addValue("_descripcion", tTarea.getDescripcion())
                    .addValue("_esfuerzo_estimado", tTarea.getEsfuerzo_estimado())
                    .addValue("_esfuerzo_real", tTarea.getEsfuerzo_real())
                    .addValue("_fecha_inicio_estimado", tTarea.getFecha_inicio_estimado())
                    .addValue("_fecha_inicio_real", tTarea.getFecha_inicio_real())
                    .addValue("_fecha_termino_estimado", tTarea.getFecha_termino_estimado())
                    .addValue("_fecha_termino_real", tTarea.getFecha_termino_real())
                    .addValue("_porcentaje_estimado", tTarea.getPorcentaje_estimado())
                    .addValue("_porcentaje_real", tTarea.getPorcentaje_real())
                    .addValue("_fk_id_proyecto", tTarea.getId_proyecto())
                    .addValue("_fk_rut_recurso", tTarea.getRut_recurso())
                    ;

            Map out;

            procInsertarActualizarTareas.declareParameters( new SqlInOutParameter( "codError", Types.INTEGER ) );
            procInsertarActualizarTareas.declareParameters( new SqlInOutParameter( "mensaje", Types.VARCHAR ) );

            out = procInsertarActualizarTareas.execute( in );

            codError = (Integer) out.get("codError");
            mensaje = (String) out.get("mensaje");

            if (codError != 0) {
                resultado = new TareaPostResponse( -1 );
            }else{
                resultado = new TareaPostResponse( 0 );
            }

            resultado.setError( codError  );
            resultado.setMensaje(  mensaje );

        } catch (DataAccessException exception) {

            String msg = exception.getMessage();
            logger.info("excepcion "+ exception  );


            if( msg.matches(  "(.*)Duplicate entry(.*)"  )  ){
                msg = "Tarea duplicada";
                throw new GestionDatabaseException( msg ,"SP001",exception);
            }

            throw new GestionDatabaseException("Error al ejecutar " + SP_INSERTAR_TAREAS +" " + msg ,"SP001",exception);
        }catch( Exception e ){
            logger.info("excepcion "+ e  );
        }

        return resultado;
    }

    @Override
    public List<Tarea> obtenerTareas(String id_proyecto) {

        SqlParameterSource in;

        try {


            in = new MapSqlParameterSource()
                    .addValue( "_fk_id_proyecto", id_proyecto  );

            Map out = procReadTarea.execute( in );

            return convierteTobuscarProcesoLista( (List) out.get("#result-set-1"));


        } catch (DataAccessException exception) {

            throw new GestionDatabaseException("Error al ejecutar " + SP_OBTENER_TAREAS, "SPTAREA001", exception);
        }

    }


    private static List<Tarea> convierteTobuscarProcesoLista(List rs) {
        List<Tarea> resultado = new ArrayList<Tarea>();

        for (Object obj : rs) {

            Map fila = (Map) obj;
            resultado.add(new Tarea.Builder()
                    .setSec_Tarea((Long) fila.get("sec_tarea"))
                    .setId_traspaso((Integer) fila.get("id_traspaso"))
                    .setDescripcion((String) fila.get("descripcion"))
                    .setEsfuerzo_Estimado((Float) fila.get("esfuerzo_estimado"))
                    .setEsfuerzo_Real((Float) fila.get("esfuerzo_real"))
                    .setFecha_Inicio_Estimado((Date) fila.get("fecha_inicio_estimado"))
                    .setFecha_Inicio_Real((Date) fila.get("fecha_inicio_real"))
                    .setFecha_Termino_Estimado((Date) fila.get("fecha_termino_estimado"))
                    .setFecha_Inicio_Real((Date) fila.get("fecha_termino_real"))
                    .setPorcentaje_Estimado((Float) fila.get("porcentaje_estimado"))
                    .setPorcentaje_Real((Float) fila.get("porcentaje_real"))
                    .setId_proyecto((String) fila.get("fk_id_proyecto"))
                    .setRut_recurso((String) fila.get("fk_rut_recurso"))
                    .setId_recurso((String)fila.get("id_recurso"))

                    .build());
        }
//        logger.info(resultado.get(0).toString());
        return resultado;
    }

    @Override
    public void eliminarTarea(Tarea tTarea){

        logger.info( tTarea.toString() );


        try{
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("_id_traspaso", tTarea.getId_traspaso())
                    .addValue("_id_proyecto", tTarea.getId_proyecto());

            Map out = procEliminarTarea.execute(in);
        }catch(DataAccessException exception){

            logger.info( exception.getMessage()  );
            throw new GestionDatabaseException("Error al ejecutar " + SP_ELIMINAR_TAREA,"SP004",exception);

        }catch( Exception e ){

            logger.info( e.getMessage()  );
            throw new GestionDatabaseException("Error al ejecutar " + SP_ELIMINAR_TAREA,"SP004", e );

        }
    }

    @Override
    public TareaPostResponse insert(ArrayList<TraspasoTarea> listaTraspasoTarea) {

        String idProyecto = listaTraspasoTarea.get( 0 ).getNombre();
        TareaPostResponse resultado = new TareaPostResponse();
        SqlParameterSource in;
        Map out;

        Integer codError;
        String mensaje;

        try {

            for( int i = 1; i < listaTraspasoTarea.size(); i ++ ) {

                if( listaTraspasoTarea.get( i ).getResourceInitials() != null ) {


                    logger.info(  listaTraspasoTarea.get( i ).toString() );

                    in = new MapSqlParameterSource()
                            .addValue("_id", listaTraspasoTarea.get(i).getId())
                            .addValue("_nombre", listaTraspasoTarea.get(i).getNombre())
                            .addValue("_duration", listaTraspasoTarea.get(i).getDuration())
                            .addValue("_scheduled_Work", listaTraspasoTarea.get(i).getScheduledWork())
                            .addValue("_start_Date", SqlDateUtils.string2Timestamp(listaTraspasoTarea.get(i).getStartDate(), DATE_FORMAT))
                            .addValue("_finish_Date", SqlDateUtils.string2Timestamp(listaTraspasoTarea.get(i).getFinishDate(), DATE_FORMAT))
                            .addValue("_predecessors", listaTraspasoTarea.get(i).getPredecessors())
                            .addValue("_resource_Initials", listaTraspasoTarea.get(i).getResourceInitials());

                    out = procInsertTraspaso.execute(in);

                }
            }

            procTraspasarTarea.declareParameters( new SqlInOutParameter( "codError", Types.INTEGER ) );
            procTraspasarTarea.declareParameters( new SqlInOutParameter( "mensaje", Types.VARCHAR ) );


            /// agregar esto en insertarActualizarTarea
            in = new MapSqlParameterSource()
                    .addValue( "idProyectoTraspaso", idProyecto  )
                    .addValue( "codError", -1  )
                    .addValue( "mensaje", "default"  );

            out = procTraspasarTarea.execute( in );

            codError = (Integer) out.get("codError");
            mensaje = (String) out.get("mensaje");

            if (codError != 0) {
                resultado = new TareaPostResponse( -1 );
            }else{
                resultado = new TareaPostResponse( 0 );
            }

            resultado.setError( codError  );
            resultado.setMensaje(  mensaje );

        } catch (DataAccessException exception) {

            String msg = exception.getMessage();
            logger.info("excepcion "+ exception  );


            if( msg.matches(  "(.*)Duplicate entry(.*)"  )  ){
                msg = "Tarea duplicada";
                procLimpiarTraspasoTarea.execute();
                throw new GestionDatabaseException( msg ,"SP001",exception);
            }

            throw new GestionDatabaseException("Error al ejecutar " + SP_TRASPASAR_TAREA +" " + msg ,"SP001",exception);
        }catch( Exception e ){
            logger.info("excepcion "+ e  );
        }

        return resultado;
    }


}















