package com.gestion.repository;

import com.gestion.dominio.DefinicionHito;
import com.gestion.dominio.HitoComercial;
import com.gestion.dominio.Parametro;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.exceptions.GestionDatabaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Repository
public class DefinicionHitoDaoImpl implements DefinicionHitoDao{


    private static final Logger logger = LoggerFactory.getLogger( DefinicionHitoDaoImpl.class );

    public static final String SP_OBTENER_DEFINICION_HITOS = "obtenerDefinicionHitos";
    public static final String SP_UPSERT_DEFINICION_HITOS = "upsertDefinicionHitos";
    public static final String SP_ELIMINAR_DEFINICION_HITOS  = "eliminarDefrinicionHito";

    private SimpleJdbcCall procReadCatalogo;
    private SimpleJdbcCall procUpSertCatalogo;
    private SimpleJdbcCall procEliminarCatalogo;

    @PostConstruct
    public void postConstruct(){

    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.procReadCatalogo = new SimpleJdbcCall(dataSource).withProcedureName( SP_OBTENER_DEFINICION_HITOS );
        this.procUpSertCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_UPSERT_DEFINICION_HITOS);
        this.procEliminarCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_ELIMINAR_DEFINICION_HITOS);
    }

    @Override
    public List<DefinicionHito> obtenerDefinicionHitos(Integer id ) {
        try {

            SqlParameterSource in = new MapSqlParameterSource().addValue( "_id", id );

            Map out = procReadCatalogo.execute( in );
            return convierteToDefinicionHitoLista((List) out.get("#result-set-1"));
        } catch (DataAccessException exception){
            throw new GestionDatabaseException("Error al ejecutar " + SP_OBTENER_DEFINICION_HITOS,"SPPARAMETRO001",exception);
        }catch( Exception e){

            logger.info(  e.getMessage() );
            throw e;
        }
    }

    //Actualizar o insertar
    @Override
    public GeneralPostResponse upsert(DefinicionHito  defHito ) {

        logger.info(  defHito.toString()   );

        GeneralPostResponse generalResponse;
        Integer codError;
        String mensaje;
        Map out;

        try {

            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue( "_id", defHito.getId() )
                    .addValue( "_nombre", defHito.getNombre() );

            procUpSertCatalogo.declareParameters( new SqlInOutParameter( "codError", Types.INTEGER ) );
            procUpSertCatalogo.declareParameters( new SqlInOutParameter( "mensaje", Types.VARCHAR ) );

            out = procUpSertCatalogo.execute( in );

            codError = (Integer) out.get("codError");
            mensaje = (String) out.get("mensaje");

            if (codError != 0) {
                generalResponse = new GeneralPostResponse( -1 );
                generalResponse.setMensajeNOK("Fallo insertar Parámetro");
            }else{
                generalResponse = new GeneralPostResponse( 0 );
                generalResponse.setMensajeOK("Parámetro agregado correctamente");
            }

            generalResponse.setError( codError  );
            generalResponse.setMensaje(  mensaje );

        } catch (DataAccessException exception) {
            String msg = exception.getMessage();
            logger.info("excepcion "+ exception  );


            if( msg.matches(  "(.*)Duplicate entry(.*)"  )  ){
                msg = "Parametro duplicado";
                throw new GestionDatabaseException( msg ,"SP001",exception);
            }

            throw new GestionDatabaseException("Error al ejecutar " + SP_UPSERT_DEFINICION_HITOS +" " + msg ,"SP001",exception);
        }

        return generalResponse;
    }

    //Eliminar
    @Override
    public void eliminarDefinicionHito(Integer id){
        try{
            SqlParameterSource in = new MapSqlParameterSource().addValue("_id", id);
            Map out = procEliminarCatalogo.execute(in);
        }catch(DataAccessException exception){

            String msg = exception.getMessage();

            if( msg.matches(  "(.*)Cannot delete or update a parent row: a foreign key(.*)"  )  ){
                msg = "Cliente asigando, no se puede eliminar";
                throw new GestionDatabaseException( msg ,"SP001",exception);
            }


            throw new GestionDatabaseException("Error al ejecutar " + SP_ELIMINAR_DEFINICION_HITOS,"SP004",exception);
        }
    }

    private static List<DefinicionHito> convierteToDefinicionHitoLista(List rs) {

        List<DefinicionHito> resultado = new ArrayList<>();

        for (Object obj : rs) {

            Map fila = (Map) obj;
            DefinicionHito param = new DefinicionHito();

            param.setId(  ( Integer ) fila.get("id")  );
            param.setNombre(  ( String )fila.get("nombre")  );

            resultado.add( param );
        }

        return resultado;
    }
}