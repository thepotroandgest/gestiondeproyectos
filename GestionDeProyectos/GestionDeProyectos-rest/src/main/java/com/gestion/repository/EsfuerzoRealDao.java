package com.gestion.repository;

import com.gestion.dominio.Esfuerzo_Real;
import com.gestion.dominio.Result;
import com.gestion.dominio.TraspasoEsfuerzoReal;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.dominio.response.TareaPostResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
public interface EsfuerzoRealDao {
    public List<Esfuerzo_Real> obtenerEsfuerzo_Real();
    ResponseServicio insert();
    void actualizarEsfuerzoManual( Esfuerzo_Real esfuerzoReal);
    GeneralPostResponse insertarTraspasoEsfuerzoReal(ArrayList<TraspasoEsfuerzoReal> listaTraspasoEsfuerzoReal );
    void setearEsfuerzoReal();
}
