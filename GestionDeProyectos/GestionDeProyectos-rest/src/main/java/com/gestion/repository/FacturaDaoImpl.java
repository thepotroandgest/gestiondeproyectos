package com.gestion.repository;

import com.gestion.dominio.AbonoHito;
import com.gestion.dominio.Factura;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.exceptions.GestionDatabaseException;
import com.gestion.utils.SqlDateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Gustavo on 21-02-2017.
 */
@Repository
public class FacturaDaoImpl implements FacturaDao {

    private static final Logger  logger= LoggerFactory.getLogger(FacturaDaoImpl.class);

    // NOMBRES DE PROCEDIMIENTOS UTILIZADOS
    public static final String SP_FACTURA_GET = "obtenerFacturas";
    public static final String SP_BUSCAR_FACTURA = "buscarFactura";
    public static final String SP_UPSERT_FACTURA = "upsertFactura";
    public static final String SP_ELIMINAR_FACTURA = "eliminarFactura";
    public static final String SP_UPSERT_FACTURA_HITO = "upsertFacturaHito";
    public static final String SP_ELIMINAR_FACTURA_HITO = "eliminarFacturaHito";



//    public static final String SP_FACTURA_PROYECTO = "buscarFacturaPorProyecto";
    // SImpleJDBC
    private SimpleJdbcCall procReadCatalogo;
    private SimpleJdbcCall procUpSertCatalogo;
    private SimpleJdbcCall procEliminarCatalogo;
    private SimpleJdbcCall procBuscarCatalogo;
    private SimpleJdbcCall procUpsertFacturaHito;
    private SimpleJdbcCall procEliminarFacturaHito;


//    private SimpleJdbcCall procBuscarFacturaProyectoCatalogo;

    private static final String DATE_FORMAT = "dd-MM-yyyy";

    @PostConstruct
    public void postConstruct() {
        //logger.debug("CatalogoDaoImpl [{}] inicializado", SP_CONSULTA_TABLA_001);
    }
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.procReadCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_FACTURA_GET);
        this.procUpSertCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_UPSERT_FACTURA);
        this.procEliminarCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_ELIMINAR_FACTURA);
        this.procBuscarCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_BUSCAR_FACTURA);
        this.procUpsertFacturaHito = new SimpleJdbcCall( dataSource ).withProcedureName( SP_UPSERT_FACTURA_HITO  );
        this.procEliminarFacturaHito = new SimpleJdbcCall( dataSource ).withProcedureName( SP_ELIMINAR_FACTURA_HITO  );
//      this.procBuscarFacturaProyectoCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_FACTURA_PROYECTO);
    }


    @Override
    public void eliminarFacturaHito(Integer nFactura, String idProyecto, String hito ){
        try{
            SqlParameterSource in = new MapSqlParameterSource()
                                        .addValue( "_numero_factura", nFactura)
                                        .addValue( "_id_proyecto", idProyecto )
                                        .addValue( "_nombre_hito", hito  );

            procEliminarFacturaHito.execute(in);
        }catch(DataAccessException exception){
            throw new GestionDatabaseException("Error al ejecutar " + SP_ELIMINAR_FACTURA_HITO,"SP004",exception);
        }
    }



    //Obtener todo
    @Override
    public List<Factura> obtenerFacturas( String idProyecto, Integer nFactura) {
        try {
            SqlParameterSource in =  new MapSqlParameterSource()
                                    .addValue( "_fk_id_proyecto", idProyecto )
                                    .addValue( "_numero_factura", nFactura );

            Map out = procReadCatalogo.execute(in);
            return convierteToFacturaList((List) out.get("#result-set-1"));
        } catch (DataAccessException exception) {
            throw new GestionDatabaseException("Error al ejecutar " + SP_FACTURA_GET,"SPFACTURA001",exception);
        }
    }


    //Actualizar o insertar
    @Override
    public GeneralPostResponse upsert(Factura pFact) throws GestionDatabaseException {
        GeneralPostResponse generalResponse = new GeneralPostResponse();
        Integer codError;
        String mensaje;
        Map out;

        try {


            logger.info(  "upsert"    );
            logger.info( pFact.toString() );

            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("_n_factura", pFact.getNumeroFactura())
                    .addValue("_fecha_emision", SqlDateUtils.string2Timestamp( pFact.getFechaEmision(), DATE_FORMAT))
                    .addValue("_estado", pFact.getEstado())
                    .addValue("_glosa", pFact.getGlosa())
                    .addValue("_monto_uf", pFact.getMontoUF());

            procUpSertCatalogo.declareParameters(new SqlInOutParameter("codError", Types.INTEGER));
            procUpSertCatalogo.declareParameters(new SqlInOutParameter("mensaje", Types.VARCHAR));

            out = procUpSertCatalogo.execute(in);

            codError = (Integer) out.get("codError");
            mensaje = (String) out.get("mensaje");


            for(AbonoHito abono : pFact.getAbonosHitos() ){


                //TODO usar para probar excepcioens
                if( abono.getAbono()  == null ){
                    abono.setAbono( 0.0  );
                }


                in = new MapSqlParameterSource()
                        .addValue( "_n_factura", pFact.getNumeroFactura() )
                        .addValue( "_id_proyecto", abono.getFkIdProyecto()  )
                        .addValue( "_nombre_hito", abono.getHitoComercial() )
                        .addValue( "_abono", abono.getAbono() );


                procUpsertFacturaHito.declareParameters(new SqlInOutParameter("codError", Types.INTEGER));
                procUpsertFacturaHito.declareParameters(new SqlInOutParameter("mensaje", Types.VARCHAR));

                out = procUpsertFacturaHito.execute(in);

                codError = (Integer) out.get("codError");
                mensaje = (String) out.get("mensaje");


            }

            if (codError != 0) {
                generalResponse = new GeneralPostResponse( -1 );
                    generalResponse.setMensajeNOK("Fallo insertar Factura");
            }else{
                generalResponse = new GeneralPostResponse( 0 );
                generalResponse.setMensajeOK("Factura agregada correctamente");
            }
            generalResponse.setError( codError  );
            generalResponse.setMensaje(  mensaje );

        } catch (DataAccessException exception) {

            String msg = exception.getMessage();
            logger.info("excepción" + exception);

            if(msg.matches("(.*)Duplicate entry(.*)")) {
                msg= "Factura Duplicada";
                throw new GestionDatabaseException(msg, "SP001", exception);
            }


            throw new GestionDatabaseException("Error al ejecutar " + SP_UPSERT_FACTURA,"SP003",exception);
        }

        return generalResponse;
    }
    //Eliminar
    @Override
    public void eliminarFactura(Integer nFactura){
        try{
            SqlParameterSource in = new MapSqlParameterSource().addValue("_n_factura", nFactura);
            Map out = procEliminarCatalogo.execute(in);
        }catch(DataAccessException exception){
            throw new GestionDatabaseException("Error al ejecutar " + SP_ELIMINAR_FACTURA,"SP004",exception);
        }
    }

    //Mapear factura
    private static List<Factura> convierteToFacturaList(List rs) {
        List<Factura> resultado = new ArrayList<Factura>();
        for (Object obj : rs) {
            Map fila = (Map) obj;

            Factura factura = new Factura();

            factura.setIdFactura( (Long) fila.get("id_factura"));
            factura.setNumeroFactura((Integer) fila.get("numero_factura"));
            factura.setIdProyecto( (String) fila.get( "fk_id_proyecto" )    );
            factura.setFechaEmision( SqlDateUtils.timestam2String( new Timestamp( ((Date) fila.get( "fecha_emision" ) ).getTime()),DATE_FORMAT ) );
            factura.setEstado( (String) fila.get("estado"));
            factura.setMontoUF( (Double) fila.get("monto"));
            factura.setGlosa( (String) fila.get("glosa"));

            resultado.add(factura);

        }
        return resultado;
    }
}
