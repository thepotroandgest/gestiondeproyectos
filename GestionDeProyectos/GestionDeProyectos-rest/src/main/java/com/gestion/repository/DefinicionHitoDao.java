package com.gestion.repository;

import com.gestion.dominio.DefinicionHito;
import com.gestion.dominio.Parametro;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.List;

/**
 * Created by Gustavo on 20-02-2017.
 */
public interface DefinicionHitoDao {
     List<DefinicionHito> obtenerDefinicionHitos(Integer id);
    GeneralPostResponse upsert(DefinicionHito defHito);
    void eliminarDefinicionHito(Integer id);
}
