package com.gestion.repository;

import com.gestion.dominio.Cliente;
import com.gestion.dominio.Parametro;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.List;

/**
 * Created by Gustavo on 20-02-2017.
 */
public interface ParametroDao {
    public List<Parametro> obtenerParametros( Integer id  );
    GeneralPostResponse upsert( Parametro param );
    void eliminarParametro(Integer id);
}
