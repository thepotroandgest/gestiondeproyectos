package com.gestion.repository;

import com.gestion.dominio.Esfuerzo_Real;
import com.gestion.dominio.Result;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.dominio.response.TareaPostResponse;
import com.gestion.dominio.TraspasoEsfuerzoReal;
import com.gestion.exceptions.GestionDatabaseException;
import com.gestion.utils.SqlDateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Gustavo on 21-02-2017.
 */
@Repository
public class EsfuerzoRealDaoImpl implements EsfuerzoRealDao {

    private static final Logger logger = LoggerFactory.getLogger(EsfuerzoRealDaoImpl.class);

    // NOMBRES DE PROCEDIMIENTOS UTILIZADOS
    public static final String SP_OBTENER_ESFUERZO_REAL = "obtenerEsfuerzo_Real";
    public static final String SP_INSERTAR_ESFUERZO_REAL = "insertarTraspasoEsfuerzoReal";
    public static final String SP_TRASPASAR_ESFUERZO_REAL = "traspasar_traspaso_esfuerzo_real";
    public static final String SP_LIMPIAR_TRASPASO = "limpiaTraspasoEsfuerzoReal";
    public static final String SP_SETEAR_ESFUERZO_REAL = "setearEsfuerzoReal";
    public static final String CARGAR_ESFUERZO_MANUAL = "cargaEsfuerzoManual";

    private static final String DATE_FORMAT = "dd-MM-yy HH:mm";

    private SimpleJdbcCall procReadCatalogo;
    private SimpleJdbcCall procInsertTraspasoEsfuerzoReal;
    private SimpleJdbcCall procTraspasarTraspaso;
    private SimpleJdbcCall procLimpiarTraspaso;
    private SimpleJdbcCall procSetearEsfuerzoReal;
    private SimpleJdbcCall procCargarEsfuerzoManual;

    // SImpleJDBC
    @PostConstruct
    public void postConstruct(){
        //algo hace
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.procReadCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_OBTENER_ESFUERZO_REAL);
        this.procInsertTraspasoEsfuerzoReal = new SimpleJdbcCall(dataSource).withProcedureName( SP_INSERTAR_ESFUERZO_REAL );
        this.procTraspasarTraspaso = new SimpleJdbcCall(dataSource).withProcedureName( SP_TRASPASAR_ESFUERZO_REAL );
        this.procLimpiarTraspaso = new SimpleJdbcCall(dataSource).withProcedureName( SP_LIMPIAR_TRASPASO );
        this.procSetearEsfuerzoReal = new SimpleJdbcCall(dataSource).withProcedureName( SP_SETEAR_ESFUERZO_REAL );
        this.procCargarEsfuerzoManual = new SimpleJdbcCall(dataSource).withProcedureName( CARGAR_ESFUERZO_MANUAL );
    }


    @Override
    public List<Esfuerzo_Real> obtenerEsfuerzo_Real() {
        try {
            Map out = procReadCatalogo.execute();
            return convierteTobuscarProcesoLista((List) out.get("#result-set-1"));
        } catch (DataAccessException exception){
            throw new GestionDatabaseException("Error al ejecutar " + SP_OBTENER_ESFUERZO_REAL,"SPESFUERZOREAL001",exception);
        }
    }

    private static List<Esfuerzo_Real> convierteTobuscarProcesoLista(List rs) {
        List<Esfuerzo_Real> resultado = new ArrayList<Esfuerzo_Real>();
        for (Object obj : rs) {
            Map fila = (Map) obj;
            resultado.add(new Esfuerzo_Real.Builder()
                    .setFecha_Inicio((Date) fila.get("fecha_inicio"))
                    .setFecha_Fin((Date) fila.get("tipo"))
                    .setUsuario((String) fila.get("usuario"))
                    .setCliente((String) fila.get("cliente"))
                    .setHora((Double) fila.get("hora"))
                    .setProyecto((String) fila.get("proyecto"))
                    .build());
        }
        return resultado;
    }

    @Override
    public GeneralPostResponse insertarTraspasoEsfuerzoReal( ArrayList<TraspasoEsfuerzoReal> listaTraspasoEsfuerzoReal ){

        GeneralPostResponse resultado ;
        SqlParameterSource in;
        Map out;


        resultado = new GeneralPostResponse( 0 );

        try {

            for( int i = 0; i < listaTraspasoEsfuerzoReal.size(); i ++ ) {


                logger.info( "---->" + listaTraspasoEsfuerzoReal.get(i).getTarea()  );

                in = new MapSqlParameterSource()
                        .addValue("_servicio", listaTraspasoEsfuerzoReal.get(i).getServicio())
                        .addValue("_cliente", listaTraspasoEsfuerzoReal.get(i).getCliente())
                        .addValue("_proyecto", listaTraspasoEsfuerzoReal.get(i).getProyecto())
                        .addValue("_usuario", listaTraspasoEsfuerzoReal.get(i).getUsuario())
                        .addValue("_tarea", listaTraspasoEsfuerzoReal.get(i).getTarea())
                        .addValue("_fechaEntrega", SqlDateUtils.string2Timestamp(listaTraspasoEsfuerzoReal.get(i).getFechaEntrega(), DATE_FORMAT))
                        .addValue("_estimatedHours", listaTraspasoEsfuerzoReal.get(i).getEstimatedHours())
                        .addValue("_facturable", listaTraspasoEsfuerzoReal.get(i).getFacturable())
                        .addValue("_precioHora", listaTraspasoEsfuerzoReal.get(i).getPrecioHora())
                        .addValue("_archivado", listaTraspasoEsfuerzoReal.get(i).getArchivado())
                        .addValue("_fechaInicio", SqlDateUtils.string2Timestamp( listaTraspasoEsfuerzoReal.get(i).getFechaInicio(), DATE_FORMAT) )
                        .addValue("_fechaFin",    SqlDateUtils.string2Timestamp( listaTraspasoEsfuerzoReal.get(i).getFechaFin(), DATE_FORMAT) )
                        .addValue("_zonaHoraria", listaTraspasoEsfuerzoReal.get(i).getZonaHoraria())
                        .addValue("_duracion", listaTraspasoEsfuerzoReal.get(i).getDuracion() )
                        .addValue("_horas", listaTraspasoEsfuerzoReal.get(i).getHoras())
                        .addValue("_notas", listaTraspasoEsfuerzoReal.get(i).getNotas())
                        .addValue("_total", listaTraspasoEsfuerzoReal.get(i).getTotal())
                        .addValue("_moneda", listaTraspasoEsfuerzoReal.get(i).getMoneda());

                out = procInsertTraspasoEsfuerzoReal.execute(in);

            }

            resultado.setMensajeOK( "Carga de archivo Tracking exitosa" );
            resultado.setMensajeNOK( "Fallo carga de archivo tracking" );

            resultado.setError( 0 );
            resultado.setMensaje( "exito" );

        } catch (DataAccessException e ) {

            resultado.setError( 1 );
            resultado.setMensaje( "error" + e.getMessage());
            logger.info("excepcion "+ e  );
            throw new GestionDatabaseException( "Error al ejecutar " + SP_INSERTAR_ESFUERZO_REAL,"SP001", e );
        }catch( Exception e ){
            logger.info("excepcion "+ e  );
            resultado.setError( 1 );
            resultado.setMensaje( "error" + e.getMessage());
            logger.info("excepcion "+ e  );
            throw new GestionDatabaseException( "Error al ejecutar " + SP_INSERTAR_ESFUERZO_REAL,"SP001", e );
        }

        return resultado;
    }



    @Override
    public void setearEsfuerzoReal() {

        GeneralPostResponse generalResponse = new GeneralPostResponse();
        Integer codError;
        String mensaje;
        Map out;

        try {

            out = procSetearEsfuerzoReal.execute(  );

        }catch( DataAccessException exception ){

            String msg = exception.getMessage();
            logger.info("excepcion "+ exception  );


            if( msg.matches(  "(.*)Duplicate entry(.*)"  )  ){
                msg = "Tarea duplicada";
                procLimpiarTraspaso.execute();
                throw new GestionDatabaseException( msg ,"SP001",exception);
            }

            throw new GestionDatabaseException("Error al ejecutar " + SP_SETEAR_ESFUERZO_REAL +" " + msg ,"SP001",exception);

        }catch( Exception e ){
            logger.info("excepcion "+ e.getMessage()  );

        }

    }

    @Override
    public void actualizarEsfuerzoManual( Esfuerzo_Real esfuerzoReal) {


        try {

            MapSqlParameterSource in = new MapSqlParameterSource()
                    .addValue( "_avance_real", esfuerzoReal.getHora()  )
                    .addValue( "_id_proyecto", esfuerzoReal.getProyecto()  );

            procCargarEsfuerzoManual.execute(  in );

        }catch( DataAccessException exception ){

            String msg = exception.getMessage();
            logger.info("excepcion "+ exception  );


            if( msg.matches(  "(.*)Duplicate entry(.*)"  )  ){
                msg = "Tarea duplicada";
                procLimpiarTraspaso.execute();
                throw new GestionDatabaseException( msg ,"SP001",exception);
            }

            throw new GestionDatabaseException("Error al ejecutar " + SP_SETEAR_ESFUERZO_REAL +" " + msg ,"SP001",exception);

        }catch( Exception e ){
            logger.info("excepcion "+ e.getMessage()  );

        }

    }








    @Override
    public TareaPostResponse insert() {

        TareaPostResponse resultado = new TareaPostResponse();
        Integer codError;
        String mensaje;
        Map out;

        try {

            procTraspasarTraspaso.declareParameters( new SqlInOutParameter( "codError", Types.INTEGER ) );
            procTraspasarTraspaso.declareParameters( new SqlInOutParameter( "mensaje", Types.VARCHAR ) );

            MapSqlParameterSource in = new MapSqlParameterSource()
                                            .addValue( "codError", -1  )
                                            .addValue( "mensaje", "default"  );

            out = procTraspasarTraspaso.execute( in );

            codError = (Integer) out.get("codError");
            mensaje = (String) out.get("mensaje");

            if (codError != 0) {
                resultado = new TareaPostResponse( -1 );
            }else{
                resultado = new TareaPostResponse( 0 );
            }

            resultado.setError( codError  );
            resultado.setMensaje(  mensaje );


        }catch( DataAccessException exception ){
            String msg = exception.getMessage();
            logger.info("excepcion "+ exception  );


            if( msg.matches(  "(.*)Duplicate entry(.*)"  )  ){
                msg = "Tarea duplicada";
                procLimpiarTraspaso.execute();
                throw new GestionDatabaseException( msg ,"SP001",exception);
            }

            throw new GestionDatabaseException("Error al ejecutar " + SP_TRASPASAR_ESFUERZO_REAL +" " + msg ,"SP001",exception);
        }catch( Exception e ){
            logger.info("excepcion "+ e  );

        }

        return resultado;

    }

}
