package com.gestion.repository;

import com.gestion.dominio.Avance;
import com.gestion.dominio.Bitacora;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.exceptions.GestionDatabaseException;
import com.gestion.utils.SqlDateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Gustavo on 21-02-2017.
 */
@Repository
public class AvanceDaoImpl implements AvanceDao {

    private static final Logger logger = LoggerFactory.getLogger(AvanceDaoImpl.class);

    public static final String SP_AGREGAR_AVANCE= "insertAvance";

    private SimpleJdbcCall procAgregarCatalogo;



    // NOMBRES DE PROCEDIMIENTOS UTILIZADOS
    public static final String SP_OBTENER_BITACORAS = "obtenerBitacoras";
    public static final String SP_BUSCAR_BITACORA= "buscarBitacora";
    public static final String SP_ACTUALIZAR_BITACORA= "actualizarBitacora";
    public static final String SP_ELIMINAR_AVANCE = "eliminarAvance";
    public static final String SP_BITACORA_PROYECTO = "buscarBitacoraPorProyecto";
    public static final String SP_OBTENER_AVANCE = "obtenerAvance";

    // SImpleJDBC
    private SimpleJdbcCall procReadCatalogo;
    private SimpleJdbcCall procActualizarCatalogo;
    private SimpleJdbcCall procEliminarAvance;
    private SimpleJdbcCall procBuscarCatalogo;
    private SimpleJdbcCall procBuscarBitacoraProyectoCatalogo;
    private SimpleJdbcCall procObtenerAvance;

    private static final String DATE_FORMAT = "dd-MM-yyyy";

    @PostConstruct
    public void postConstruct(){
        //algo hace
    }

    //Conectar a la base de datos con los procedimientos
    @Autowired
    public void setDataSource(DataSource dataSource) {

        this.procAgregarCatalogo = new SimpleJdbcCall(dataSource).withProcedureName( SP_AGREGAR_AVANCE );
        this.procReadCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_OBTENER_BITACORAS);
        this.procActualizarCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_ACTUALIZAR_BITACORA);
        this.procEliminarAvance = new SimpleJdbcCall(dataSource).withProcedureName(SP_ELIMINAR_AVANCE);
        this.procBuscarCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_BUSCAR_BITACORA);
        this.procBuscarBitacoraProyectoCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_BITACORA_PROYECTO);
        this.procObtenerAvance = new SimpleJdbcCall(dataSource).withProcedureName(SP_OBTENER_AVANCE);


    }

    @Override
    public void eliminarAvance( Long idAvance ){
        try{

            SqlParameterSource in = new MapSqlParameterSource().addValue("_id_avance", idAvance);
            Map out = procEliminarAvance.execute(in);

        }catch(DataAccessException exception){

            throw new GestionDatabaseException("Error al ejecutar " + SP_ELIMINAR_AVANCE,"SP004",exception);

        }

    }


    @Override
    public GeneralPostResponse agregarAvance(Avance avance ) {

        GeneralPostResponse generalResponse;
        Integer codError;
        String mensaje;
        Map out;


        logger.info(  avance.toString()    );

        try {

            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("_avance_estimado", avance.getAvanceEstimado())
                    .addValue("_avance_real", avance.getAvanceReal())
                    .addValue("_fk_id_proyecto", avance.getFkIdProyecto())
                    .addValue("_observacion", avance.getObservacion() )
                    .addValue("_fecha", SqlDateUtils.string2Timestamp( avance.getFecha() ,DATE_FORMAT  )  )
                    .addValue( "_id",  avance.getId()  )
                    .addValue( "_esfuerzo_real", avance.getEsfuerzoReal()  );

            procAgregarCatalogo.declareParameters( new SqlInOutParameter( "codError", Types.INTEGER ) );
            procAgregarCatalogo.declareParameters( new SqlInOutParameter( "mensaje", Types.VARCHAR ) );

            out = procAgregarCatalogo.execute(in);

            codError = (Integer) out.get( "codError" );
            mensaje = (String) out.get("mensaje");

            if (codError != 0) {
                generalResponse = new GeneralPostResponse( -1 );
                generalResponse.setMensajeNOK("Fallo insertar Avance");
            }else{
                generalResponse = new GeneralPostResponse( 0 );
                generalResponse.setMensajeOK("Avance agregado correctamente");
            }

            generalResponse.setError( codError  );
            generalResponse.setMensaje(  mensaje );


        } catch (DataAccessException exception) {
            throw new GestionDatabaseException("Error al ejecutar " + SP_AGREGAR_AVANCE,"SP003",exception);
        }catch (Exception e){
            logger.info( e.getMessage() );

            throw e;
        }

        return generalResponse;
    }


    //Obtener todas las contrapartes
    @Override
    public List<Avance> obtenerAvance( Long idAvance, String idProyecto) {
        try{
            SqlParameterSource in =  new MapSqlParameterSource()
                                    .addValue( "_fk_id_proyecto", idProyecto )
                                    .addValue( "_id", idAvance );

            Map out = procObtenerAvance.execute( in );
            return convierteToAvanceList((List) out.get("#result-set-1"));
        }catch(DataAccessException e){
            throw new GestionDatabaseException("Error al ejecutar " + procObtenerAvance,"SPCONTRAPARTES001",e);
        }
    }

    //Mapear las contrapartes a utilizar
    private List<Avance> convierteToAvanceList(List rs) {

        List<Avance> resultado = new ArrayList<Avance>();

        for (Object obj : rs) {
            Map fila = (Map) obj;

            Avance avance = new Avance();

            avance.setId(  (Long)fila.get( "id" )    );
            avance.setFkIdProyecto( (String)fila.get( "fk_id_proyecto" ) );
            avance.setAvanceEstimado( (Double)fila.get( "avance_estimado" ) );
            avance.setAvanceReal(  (Double)fila.get( "avance_real" )  );
            avance.setEsfuerzoReal( (Double)fila.get( "esfuerzo_real" )  );
            avance.setFecha( SqlDateUtils.timestam2String( new Timestamp( ((Date) fila.get( "fecha" ) ).getTime()),DATE_FORMAT )    );
            avance.setObservacion( (String)fila.get( "observacion" )  );
            resultado.add( avance );

        }

        return resultado;
    }

}




















