package com.gestion.repository;

import com.gestion.dominio.Calendario;

import java.util.List;

/**
 * Created by Gustavo on 06-03-2017.
 */
public interface CalendarioDao {
     List<Calendario> obtenerCalendario();
     List<Calendario> obtenerCalendarioRecursos();

}
