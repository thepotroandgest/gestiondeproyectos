package com.gestion.repository;

import com.gestion.dominio.Cliente;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.exceptions.GestionDatabaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Gustavo on 20-02-2017.
 */

@Repository
public class ClienteDaoImpl implements ClienteDao{


    private static final Logger logger = LoggerFactory.getLogger( ClienteDaoImpl.class );


    // NOMBRES DE PROCEDIMIENTOS UTILIZADOS
    public static final String SP_OBTENER_CLIENTES = "obtenerClientes";
    public static final String SP_UPSERT_CLIENTE = "upsertCliente";
    public static final String SP_ELIMINAR_CLIENTE = "eliminarCliente";
    // SImpleJDBC
    private SimpleJdbcCall procReadCatalogo;
    private SimpleJdbcCall procUpSertCatalogo;
    private SimpleJdbcCall procEliminarCatalogo;

    @PostConstruct
    public void postConstruct(){
        //algo hace
    }

    //generar la conexión al procedimiento almacenado
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.procReadCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_OBTENER_CLIENTES);
        this.procUpSertCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_UPSERT_CLIENTE);
        this.procEliminarCatalogo = new SimpleJdbcCall(dataSource).withProcedureName(SP_ELIMINAR_CLIENTE);
    }

    //Obtener todo
    @Override
    public List<Cliente> obtenerClientes( String rutCliente  ) {
        try {
            SqlParameterSource in =  new MapSqlParameterSource().addValue( "_rut_cliente", rutCliente );
            Map out = procReadCatalogo.execute( in );
            return convierteToClientesLista((List) out.get("#result-set-1"));
        } catch (DataAccessException exception){
            throw new GestionDatabaseException("Error al ejecutar " + SP_OBTENER_CLIENTES,"SPCLIENTE001",exception);
        }
    }

    //Actualizar o insertar
    @Override
    public GeneralPostResponse upsert(Cliente pCli) {
        logger.info(  pCli.toString()   );

        GeneralPostResponse generalResponse;
        Integer codError;
        String mensaje;
        Map out;

        try {

            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue( "_nombre", pCli.getNombre() )
                    .addValue( "_rut_cliente", pCli.getRutCliente() )
                    .addValue( "_fono", pCli.getFono()  )
                    .addValue( "_correo", pCli.getCorreo()  )
                    .addValue( "_direccion", pCli.getDireccion() );

            procUpSertCatalogo.declareParameters( new SqlInOutParameter( "codError", Types.INTEGER ) );
            procUpSertCatalogo.declareParameters( new SqlInOutParameter( "mensaje", Types.VARCHAR ) );

            out = procUpSertCatalogo.execute( in );

            codError = (Integer) out.get("codError");
            mensaje = (String) out.get("mensaje");

            if (codError != 0) {
                generalResponse = new GeneralPostResponse( -1 );
                generalResponse.setMensajeNOK("Fallo insertar Cliente");
            }else{
                generalResponse = new GeneralPostResponse( 0 );
                generalResponse.setMensajeOK("Cliente agregado correctamente");
            }

            generalResponse.setError( codError  );
            generalResponse.setMensaje(  mensaje );

        } catch (DataAccessException exception) {
            String msg = exception.getMessage();
            logger.info("excepcion "+ exception  );


            if( msg.matches(  "(.*)Duplicate entry(.*)"  )  ){
                msg = "Cliente duplicado";
                throw new GestionDatabaseException( msg ,"SP001",exception);
            }

            throw new GestionDatabaseException("Error al ejecutar " + SP_UPSERT_CLIENTE +" " + msg ,"SP001",exception);
        }


        return generalResponse;
    }

    //Eliminar
    @Override
    public void eliminarCliente(String rut_cliente){
        try{
            SqlParameterSource in = new MapSqlParameterSource().addValue("_rut_cliente", rut_cliente);
            Map out = procEliminarCatalogo.execute(in);
        }catch(DataAccessException exception){

            String msg = exception.getMessage();

            if( msg.matches(  "(.*)Cannot delete or update a parent row: a foreign key(.*)"  )  ){
                msg = "Cliente asigando, no se puede eliminar";
                throw new GestionDatabaseException( msg ,"SP001",exception);
            }


            throw new GestionDatabaseException("Error al ejecutar " + SP_ELIMINAR_CLIENTE,"SP004",exception);
        }
    }

    private static List<Cliente> convierteToClientesLista(List rs) {

        List<Cliente> resultado = new ArrayList<Cliente>();

        for (Object obj : rs) {

            Map fila = (Map) obj;

            Cliente cliente = new Cliente();

            cliente.setNombre( (String) fila.get("nombre") );
            cliente.setCorreo(  (String) fila.get("correo") );
            cliente.setFono(   (String) fila.get("fono")   );
            cliente.setRutCliente(  (String) fila.get("rut_cliente")       );
            cliente.setDireccion( (String) fila.get("direccion")  );

            resultado.add( cliente );

        }

        return resultado;
    }
}