package com.gestion.repository;


import com.gestion.dominio.EstadoProyecto;
import com.gestion.dominio.Tarea;
import com.gestion.dominio.TraspasoTarea;
import com.gestion.dominio.response.TareaPostResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
public interface TareaDao {
    List<Tarea> obtenerTareas(String id_proyecto);
    TareaPostResponse insert(ArrayList<TraspasoTarea> listaTraspasoTarea);
    TareaPostResponse insertarActualizarTareas (Tarea tTarea);
    void eliminarTarea(Tarea tTarea);

}
