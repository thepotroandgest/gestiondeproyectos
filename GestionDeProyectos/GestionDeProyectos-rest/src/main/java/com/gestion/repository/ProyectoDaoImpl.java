package com.gestion.repository;

import com.gestion.dominio.EstadoProyecto;
import com.gestion.dominio.Proyecto;
import com.gestion.dominio.Recurso;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.exceptions.GestionDatabaseException;
import com.gestion.utils.SqlDateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Gustavo on 21-02-2017.
 */
@Repository
public class ProyectoDaoImpl implements ProyectoDao {

    private static final Logger logger = LoggerFactory.getLogger( ProyectoDaoImpl.class );

    // NOMBRES DE PROCEDIMIENTOS UTILIZADOS
    public static final String SP_OBTENER_PROYECTOS = "obtenerProyectos";
    public static final String SP_BUSCAR_PROYECTO = "buscarProyecto";
    public static final String SP_UPSERT_PROYECTO = "upsertProyecto";
    public static final String SP_ELIMINAR_PROYECTO = "eliminarProyecto";
    public static final String SP_ESTADO_PROYECTO = "obtenerEstadoProyecto";
    public static final String SP_OBTENER_RECURSOS_EXTRAS = "obtenerRecursosExtras";

    // SImpleJDBC
    private SimpleJdbcCall procReadCatalogo;
    private SimpleJdbcCall procUpSertCatalogo;
    private SimpleJdbcCall procEliminarCatalogo;
    private SimpleJdbcCall procBuscarCatalogo;
    private SimpleJdbcCall procEstadoProyecto;
    private SimpleJdbcCall procObtieneRecExtras;

    private static final String DATE_FORMAT = "dd-MM-yyyy";
    //private static final String DATE_FORMAT_INPUT = "yyyy-MM-dd";

    @PostConstruct
    public void postConstruct(){
        //algo hace
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.procReadCatalogo = new SimpleJdbcCall(dataSource).withProcedureName( SP_OBTENER_PROYECTOS );
        this.procUpSertCatalogo = new SimpleJdbcCall(dataSource).withProcedureName( SP_UPSERT_PROYECTO );
        this.procEliminarCatalogo = new SimpleJdbcCall(dataSource).withProcedureName( SP_ELIMINAR_PROYECTO );
        this.procBuscarCatalogo = new SimpleJdbcCall(dataSource).withProcedureName( SP_BUSCAR_PROYECTO );
        this.procEstadoProyecto = new SimpleJdbcCall(dataSource).withProcedureName( SP_ESTADO_PROYECTO );
        this.procObtieneRecExtras = new SimpleJdbcCall(dataSource).withProcedureName( SP_OBTENER_RECURSOS_EXTRAS );
    }

    @Override
    public List<EstadoProyecto> obtenerEstado( String idProyecto){

        try{

            SqlParameterSource in =  new MapSqlParameterSource().addValue("_id_proyecto", idProyecto);

            Map out = procEstadoProyecto.execute( in );

            return convierteToEstadoLista((List) out.get("#result-set-1"));
        }catch(   DataAccessException exception  ){
            throw new GestionDatabaseException("Error al ejecutar " + SP_ESTADO_PROYECTO,"SPPROYECTO001",exception);
        }
    }

    //Obtener todo
    @Override
    public List<Proyecto> obtenerProyectos( String idProyecto ) {
        try {

            SqlParameterSource in =  new MapSqlParameterSource().addValue("_id_proyecto", idProyecto);

            Map out = procReadCatalogo.execute( in );
            return convierteToProyectoLista((List) out.get("#result-set-1"));
        } catch (DataAccessException exception){
            throw new GestionDatabaseException("Error al ejecutar " + SP_OBTENER_PROYECTOS,"SPPROYECTO001",exception);
        }
    }


    @Override
    public List<Recurso> obtenerRecursosExtras(String idProyecto ) {
        try {

            SqlParameterSource in =  new MapSqlParameterSource().addValue("_id_proyecto", idProyecto);

            Map out = procObtieneRecExtras.execute( in );
            return convierteRecursosList((List) out.get("#result-set-1"));
        } catch (DataAccessException exception){
            throw new GestionDatabaseException("Error al ejecutar " + SP_OBTENER_PROYECTOS,"SPPROYECTO001",exception);
        }
    }


    //Obtener un recurso
    @Override
    public List<Proyecto> buscarProyecto(String id_proyecto){
        try{
            SqlParameterSource in =  new MapSqlParameterSource().addValue("id_proyecto", id_proyecto);
            Map out = procBuscarCatalogo.execute(in);
            return convierteToProyectoLista((List) out.get("#result-set-1"));
        }catch (DataAccessException exception){
            throw new GestionDatabaseException("Error al ejecutar " + SP_BUSCAR_PROYECTO,"SP002",exception);
        }
    }
    //Actualizar o insertar
    @Override
    public GeneralPostResponse upsert(Proyecto pPro) {

        GeneralPostResponse generalResponse;
        Integer codError;
        String mensaje;
        Map out;

        try {

            logger.info( pPro.getIdProyecto());
            logger.info(pPro.getNombre());

            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue( "_id_proyecto", pPro.getIdProyecto() )
                    .addValue( "_nombre", pPro.getNombre() )
                    .addValue( "_tipo", pPro.getTipo() )
                    .addValue( "_esfuerzo_vendido", pPro.getEsfuerzoVendido() )
                    .addValue( "_fecha_inicio",  SqlDateUtils.string2Timestamp( pPro.getFechaInicio(),DATE_FORMAT  ) )
                    .addValue( "_fecha_termino", SqlDateUtils.string2Timestamp( pPro.getFechaTermino(),DATE_FORMAT ) )
                    .addValue( "_descripcion", pPro.getDescripcion() )
                    .addValue( "_valor_vendido", pPro.getValorVendido() )
                    .addValue( "_fase", pPro.getFase()  )
                    .addValue( "_estado", pPro.getEstado() )
                    .addValue( "_fk_id_contraparte", pPro.getFkIdContraparte() )
                    .addValue( "_fk_id_sistema_relacionado", pPro.getFkIdSistemaRelacionado() );

            procUpSertCatalogo.declareParameters( new SqlInOutParameter( "codError", Types.INTEGER ) );
            procUpSertCatalogo.declareParameters( new SqlInOutParameter( "mensaje", Types.VARCHAR ) );


            out = procUpSertCatalogo.execute(in);

            codError = (Integer) out.get("codError");
            mensaje = (String) out.get("mensaje");

            if (codError != 0) {
                generalResponse = new GeneralPostResponse( -1 );
                generalResponse.setMensajeNOK("Fallo insertar Proyecto");
            }else{
                generalResponse = new GeneralPostResponse( 0 );
                generalResponse.setMensajeOK("Proyecto agregado correctamente");

            }

            generalResponse.setError( codError  );
            generalResponse.setMensaje(  mensaje );

        } catch (DataAccessException exception) {
            String msg = exception.getMessage();
            logger.info("excepcion "+ exception  );


            if( msg.matches(  "(.*)Duplicate entry(.*)"  )  ){
                msg = "Proyecto duplicado";
                throw new GestionDatabaseException( msg ,"SP001",exception);
            }

            throw new GestionDatabaseException("Error al ejecutar " + SP_UPSERT_PROYECTO +" " + msg ,"SP001",exception);
        }

        return generalResponse;
    }

    //Eliminar
    @Override
    public GeneralPostResponse eliminarProyecto(String id_proyecto){

        GeneralPostResponse generalResponse;
        Integer codError;
        String mensaje;
        Map out;

        try{
            SqlParameterSource in = new MapSqlParameterSource().addValue("_id_proyecto", id_proyecto);


            procEliminarCatalogo.declareParameters( new SqlInOutParameter( "codError", Types.INTEGER ) );
            procEliminarCatalogo.declareParameters( new SqlInOutParameter( "mensaje", Types.VARCHAR ) );

            out = procEliminarCatalogo.execute(in);

            codError = (Integer) out.get("codError");
            mensaje = (String) out.get("mensaje");

            if (codError != 0) {
                generalResponse = new GeneralPostResponse( -1 );
                generalResponse.setMensajeNOK("Fallo eliminar proyecto: " + mensaje  );
            }else{
                generalResponse = new GeneralPostResponse( 0 );
                generalResponse.setMensajeOK("Proyecto eliminado correctamente");

            }

            generalResponse.setError( codError  );
            generalResponse.setMensaje(  mensaje );

        }catch(DataAccessException exception){

            logger.info(exception.getMessage());
            throw new GestionDatabaseException("Error al ejecutar " + SP_ELIMINAR_PROYECTO,"SP004",exception);

        }catch( Exception e ){

            logger.info( e.getMessage()  );
            throw new GestionDatabaseException("Error al ejecutar" + SP_ELIMINAR_PROYECTO,"SP004", e );

        }

        return generalResponse;
    }

    //Mapear
    private  List<Proyecto> convierteToProyectoLista(List rs) {
        List<Proyecto> resultado = new ArrayList<Proyecto>();
        for (Object obj : rs) {
            Map fila = (Map) obj;

            Proyecto proyecto = new Proyecto();

            proyecto.setIdProyecto( (String) fila.get("id_proyecto") );
            proyecto.setNombre( (String) fila.get("nombre") );
            proyecto.setTipo((String) fila.get("tipo") );
            proyecto.setEsfuerzoVendido( (Double) fila.get("esfuerzo_vendido") );

            if(    fila.get( "fecha_inicio" ) != null  ){
                proyecto.setFechaInicio( SqlDateUtils.timestam2String( new Timestamp( ((Date) fila.get( "fecha_inicio" ) ).getTime()),DATE_FORMAT    )  );
            }

            if(   fila.get( "fecha_termino" ) != null ){
                proyecto.setFechaTermino( SqlDateUtils.timestam2String( new Timestamp( ((Date) fila.get( "fecha_termino" ) ).getTime()),DATE_FORMAT ) );
            }

            proyecto.setDescripcion( (String) fila.get( "descripcion" )  );
            proyecto.setValorVendido( (Double) fila.get("valor_vendido") );
            proyecto.setFase( (String ) fila.get( "fase" )     );
            proyecto.setEstado(  (String) fila.get( "estado" )   );
            proyecto.setFkIdContraparte(  (String) fila.get("fk_id_contraparte") );
            proyecto.setFkIdSistemaRelacionado( (String) fila.get( "fk_id_sistema_relacionado" )   );
//            proyecto.setFkRutRecurso( (String) fila.get( "fk_rut_recurso" )   );
            proyecto.setIdRecurso( (String) fila.get( "id_recurso" )   );
            proyecto.setNombreContraparte( (String) fila.get( "nombre_contraparte" )   );
            proyecto.setNombreSistema( (String) fila.get( "nombre_sistema" )   );

            proyecto.setRecursosExtras(  (ArrayList<Recurso> ) obtenerRecursosExtras( (String) fila.get("id_proyecto")    )    );

            resultado.add( proyecto );
        }


        return resultado;
    }

    private static List<Recurso> convierteRecursosList(List rs) {
        List<Recurso> resultado = new ArrayList<>();
        for (Object obj : rs) {
            Map fila = (Map) obj;

            Recurso recurso = new Recurso();

            recurso.setRutRecurso( ( String )fila.get(  "rut_recurso" )  );
            recurso.setIdRecurso( ( String )fila.get( "id_recurso" ) );
            recurso.setNombre( ( String )fila.get( "nombre" ) );

            resultado.add( recurso );
        }

        return resultado;
    }

    //Mapear
    private static List<EstadoProyecto> convierteToEstadoLista(List rs) {
        List<EstadoProyecto> resultado = new ArrayList<>();

        for (Object obj : rs) {
            Map fila = (Map) obj;

            EstadoProyecto estadoProyecto = new EstadoProyecto();

            estadoProyecto.setId(  ( Long )fila.get( "id" )  );
            estadoProyecto.setFkIdProyecto(  ( String )fila.get( "fk_id_proyecto" ) );
            estadoProyecto.setFechaActual( SqlDateUtils.timestam2String( new Timestamp( ((Date) fila.get( "fecha_actual" ) ).getTime()),DATE_FORMAT ) );
            estadoProyecto.setFechaInicio( SqlDateUtils.timestam2String( new Timestamp( ((Date) fila.get( "fecha_inicio" ) ).getTime()),DATE_FORMAT ) );
            estadoProyecto.setFechaTermino( SqlDateUtils.timestam2String( new Timestamp( ((Date) fila.get( "fecha_termino" ) ).getTime()),DATE_FORMAT )  );
            estadoProyecto.setEsfuerzoVendido(  ( Double )fila.get( "esfuerzo_vendido" )  );
            estadoProyecto.setAvanceEstimado(    ( Double )fila.get( "avance_estimado" )  );
            estadoProyecto.setAvanceReal(    ( Double )fila.get( "avance_real" )  );
            estadoProyecto.setEsfuerzoReal(   ( Double )fila.get( "esfuerzo_real" )  );
            estadoProyecto.setIndiceEsfuerzo(  ( Double )fila.get( "indice_esfuerzo" )  );
            estadoProyecto.setIndiceAvance(    ( Double )fila.get( "indice_avance" )      );

            resultado.add( estadoProyecto );

        }

        return resultado;
    }

}
