package com.gestion.repository;

import com.gestion.dominio.EstadoFacturacion;
import com.gestion.dominio.HitoComercial;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
public interface HitoComercialDao {
    public List<HitoComercial> obtenerHitoComercial(String fkIdProyecto);
    GeneralPostResponse upsertHitoComercial(HitoComercial hito);
    void eliminarHitoComercial(HitoComercial hito);
    void upsertHitos(ArrayList<HitoComercial> hito);
    List<HitoComercial> obtenerHitosPorFactura(Integer nFactura, String estado, String contraparte);
    List<EstadoFacturacion> obtenerHitosPorContraparte( Integer nFactura, String proyecto, String contraparte );

}
