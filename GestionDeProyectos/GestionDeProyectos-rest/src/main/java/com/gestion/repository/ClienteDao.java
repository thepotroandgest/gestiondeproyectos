package com.gestion.repository;

import com.gestion.dominio.Cliente;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.List;

/**
 * Created by Gustavo on 20-02-2017.
 */
public interface ClienteDao {
    public List<Cliente> obtenerClientes( String rutCliente );
    GeneralPostResponse upsert(Cliente pCli);
    void eliminarCliente(String rut_cliente);
}
