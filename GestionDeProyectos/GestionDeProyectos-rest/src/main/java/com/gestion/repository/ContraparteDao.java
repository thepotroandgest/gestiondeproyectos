package com.gestion.repository;

import com.gestion.dominio.Contraparte;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
public interface ContraparteDao {
    List<Contraparte> obtenerContrapartes( String idContraparte );
    GeneralPostResponse upsert(Contraparte pCon);
    void eliminarContraparte(String id_contraparte);
    public List<Contraparte> buscarContraparte(String id_contraparte);
}
