package com.gestion.repository;

import com.gestion.dominio.Recurso;
import com.gestion.dominio.RecursosPorProyecto;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.List;

/**
 * Created by daniel on 15-02-2017.
 */
public interface RecursoDao {

    public List<Recurso> obtenerRecursos(String rutRecurso);
    GeneralPostResponse upsert(Recurso pRec);
    void eliminarRecurso(String rutRecurso);
    public List<RecursosPorProyecto> obtenerRecursosPorProyecto(String idProyecto,String idRecurso);


}
