package com.gestion.repository;

import com.gestion.dominio.Calendario;
import com.gestion.exceptions.GestionDatabaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Gustavo on 06-03-2017.
 */
@Repository
public class CalendarioDaoImpl implements CalendarioDao {

    public static final String SP_OBTENER_CALENDARIO = "obtenerCalendario";
    public static final String SP_OBTENER_CALENDARIO_RECURSOS = "obtenerCalendarioRecursos";

    private SimpleJdbcCall procReadCatalogo;
    private SimpleJdbcCall obtenerCalendarioRecurso;


    @PostConstruct
    public void postConstruct(){
        //algo hace
    }
    //generar la conexión al procedimiento almacenado
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.procReadCatalogo = new SimpleJdbcCall(dataSource).withProcedureName( SP_OBTENER_CALENDARIO );
        this.obtenerCalendarioRecurso = new SimpleJdbcCall(dataSource).withProcedureName( SP_OBTENER_CALENDARIO_RECURSOS );

    }
    //Obtener todo
    @Override
    public List<Calendario> obtenerCalendario() {
        try {
            Map out = procReadCatalogo.execute();
            return convierteToCalendarioLista((List) out.get("#result-set-1"));
        } catch (DataAccessException exception){
            throw new GestionDatabaseException("Error al ejecutar " + SP_OBTENER_CALENDARIO,"SP001",exception);
        }
    }


    @Override
    public List<Calendario> obtenerCalendarioRecursos(){
        try {
            Map out = obtenerCalendarioRecurso.execute();
            return convierteToCalendarioLista( ( List) out.get("#result-set-1") );
        } catch ( DataAccessException exception ){
            throw new GestionDatabaseException("Error al ejecutar " + SP_OBTENER_CALENDARIO_RECURSOS,"SP001",exception);
        }

    }

    private static List<Calendario> convierteToCalendarioLista(List rs) {
        List<Calendario> resultado = new ArrayList<Calendario>();
        for (Object obj : rs) {
            Map fila = (Map) obj;
            resultado.add(new Calendario.Builder()
                    .setTitle((String) fila.get("title"))
                    .setStart((Date) fila.get("start"))
                    .setEnd((Date) fila.get("end"))
                    .build());
        }
        return resultado;
    }
}
