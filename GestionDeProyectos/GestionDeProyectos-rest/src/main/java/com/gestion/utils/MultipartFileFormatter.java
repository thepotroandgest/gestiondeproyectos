package com.gestion.utils;

import com.gestion.exceptions.GestionDatabaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by Gustavo on 27-03-2017.
 */
public class MultipartFileFormatter {
    private static final Logger logger = LoggerFactory.getLogger(MultipartFileFormatter.class);
    private static final String PCOMA = ";";
    private static final String COMA = ",";

    private ArrayList<String[]> listaDatos = new ArrayList();
    private String dataCompleta;

    public MultipartFileFormatter( String dataCompleta ){
        this.dataCompleta = dataCompleta;
    }

    public ArrayList<String[]> getListaRegistroTareas(  ){

        String[] lines;
        String[] colName;

        lines = dataCompleta.split("\\r\\n|\\r|\\n");

        //validar cantidad de columnas
        colName = lines[0].split( PCOMA );

        if(
                    colName.length == 8
                &&  colName[ 0 ].equals( "ID" )
                &&  colName[ 1 ].equals( "Name" )
                &&  colName[ 2 ].equals( "Duration" )
                &&  colName[ 3 ].equals( "Scheduled_Work" )
                &&  colName[ 4 ].equals( "Start_Date" )
                &&  colName[ 5 ].equals( "Finish_Date" )
                &&  colName[ 6 ].equals( "Predecessors" )
                &&  colName[ 7 ].equals( "Resource_Initials" )
        ){

                for( int  i = 0; i < lines.length; i++ ){
                    listaDatos.add( lines[ i ].split( PCOMA ) );
                }

        }else{

            logger.info( "Formato de archivo tarea incorrecto" );
            throw new GestionDatabaseException("Formato de archivo tarea incorrecto " ,"MultipartFileFormatter",  new Exception("Formato de archivo tarea incorrecto "));
        }

        return listaDatos;
    }

    public ArrayList<String[]> getListaRegistroEsfuerzoReal(  ){

        String[] lines;
        String[] colName;

        lines = dataCompleta.split("\\r\\n|\\r|\\n");

        //validar cantidad de columnas
        colName = lines[0].split( PCOMA );

        colName = trimpColNames( colName );

        if(
                    colName.length == 18
                &&  colName[ 0 ].equals( "Servicio" )
                &&  colName[ 1 ].equals( "Cliente" )
                &&  colName[ 2 ].equals( "Proyecto" )
                &&  colName[ 3 ].equals( "Usuario" )
                &&  colName[ 4 ].equals( "Tarea" )
                &&  colName[ 5 ].equals( "Fecha de entrega" )
                &&  colName[ 6 ].equals( "Estimated Hours" )
                &&  colName[ 7 ].equals( "Facturable" )
                &&  colName[ 8 ].equals( "Precio por hora" )
                &&  colName[ 9 ].equals( "Archivado" )
                &&  colName[ 10 ].equals( "Fecha de inicio" )
                &&  colName[ 11 ].equals( "Fecha de fin" )
                &&  colName[ 12 ].equals( "Zona horaria" )
                &&  colName[ 13 ].equals( "Duracion" )
                &&  colName[ 14 ].equals( "Horas" )
                &&  colName[ 15 ].equals( "Notas" )
                &&  colName[ 16 ].equals( "Total" )
                &&  colName[ 17 ].equals( "Moneda" )

        ){

            for( int  i = 0; i < lines.length; i++ ){
                listaDatos.add( lines[ i ].split( PCOMA ) );
            }

        }else{
            logger.info( "Formato de archivo tarea incorrecto" );
            throw new GestionDatabaseException("Formato de archivo Tracking incorrecto " ,"MultipartFileFormatter",  new Exception("Formato de archivo Tracking incorrecto "));
        }

        return listaDatos;
    }


    private String[] trimpColNames(String[] colNames){

        String[] result = new String[ colNames.length ];

        for( int i = 0; i < colNames.length; i++){

             result[ i ] = colNames[ i ].trim();

        }

        return result;
    }


}
