package com.gestion.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * User: GRN - Seiza SpA.
 * Date: AGO-2016
 * Project: template api rest
 */
public class SqlDateUtils {

  private static final Logger logger = LoggerFactory.getLogger(SqlDateUtils.class);

  public static Timestamp string2Timestamp(String fecha, String formato) {


    Timestamp d = null;

    if (fecha != null) {
      try {
        d = new Timestamp((new SimpleDateFormat(formato).parse(fecha)).getTime());
      } catch (ParseException e) {
        logger.info("ParseException --> " + e);
      } catch (Exception e) {
        logger.info("Exception --> " + e);
      }

    }

    return d;
  }
  public static String timestam2String(Timestamp fecha, String formato) {
    String d = null;
    if (fecha != null) {
      d = new SimpleDateFormat(formato).format(fecha);
    }
    return d;
  }
}
