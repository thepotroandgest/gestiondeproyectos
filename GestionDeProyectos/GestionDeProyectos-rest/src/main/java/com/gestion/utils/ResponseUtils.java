package com.gestion.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * User: GRN - Seiza SpA.
 * Date: AGO-2016
 * Project: template api rest
 */
public class ResponseUtils {
  /**
   * GET, PUT, PATCH, DELETE
   *
   * @param body
   * @return
   */
  public static ResponseEntity responseOK(Object body) {
    return new ResponseEntity<Object>(body, HttpStatus.OK);
  }

  /**
   * POST
   *
   * @param body
   * @return
   */
  public static ResponseEntity responseCreated(Object body) {
    return new ResponseEntity<Object>(body, HttpStatus.CREATED);
  }

  /**
   * GET
   *
   * @param body
   * @return
   */
  public static ResponseEntity responseNotContent(Object body) {
    return new ResponseEntity<Object>(body, HttpStatus.NO_CONTENT);
  }

  /**
   * GET, POST, PUT, PATCH, DELETE
   *
   * @param body
   * @return
   */
  public static ResponseEntity responseBadRequest(Object body) {
    return new ResponseEntity<Object>(body, HttpStatus.BAD_REQUEST);
  }

  /**
   * GET, POST, PUT, PATCH, DELETE
   *
   * @param body
   * @return
   */
  public static ResponseEntity responseUnauthorized(Object body) {
    return new ResponseEntity<Object>(body, HttpStatus.UNAUTHORIZED);
  }

  /**
   * POST, PUT, PATCH, DELETE
   *
   * @param body
   * @return
   */
  public static ResponseEntity responseConflict(Object body) {
    return new ResponseEntity<Object>(body, HttpStatus.CONFLICT);
  }
}
