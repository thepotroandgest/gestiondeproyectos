package com.gestion.service;

import com.gestion.dominio.Recurso;
import com.gestion.dominio.RecursosPorProyecto;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.repository.RecursoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecursoServiceImpl implements RecursoService {

    @Autowired
    private RecursoDao recursoDao;


    @Override
    public List<RecursosPorProyecto> obtenerRecursosPorProyecto(String idProyecto, String idRecurso){

        return recursoDao.obtenerRecursosPorProyecto( idProyecto, idRecurso  );
    }

    @Override
    public List<Recurso> obtenerRecursos(String rutRecurso) {
        return recursoDao.obtenerRecursos(rutRecurso);
    }

    @Override
    public GeneralPostResponse upsert(Recurso pRec){
        return recursoDao.upsert(pRec);
    }

    @Override
    public void eliminarRecurso(String idRecurso){
        recursoDao.eliminarRecurso(idRecurso);
    }


}
