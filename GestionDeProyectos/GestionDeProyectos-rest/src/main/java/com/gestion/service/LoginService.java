package com.gestion.service;

import com.gestion.dominio.Login;

import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
public interface LoginService {
    List<Login> obtenerLogin();
}
