package com.gestion.service;

import com.gestion.dominio.EstadoFacturacion;
import com.gestion.dominio.HitoComercial;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.repository.HitoComercialDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
@Service
public class HitoComercialServiceImpl implements HitoComercialService {

    private static final Logger logger= LoggerFactory.getLogger(HitoComercialService.class);

    @Autowired
    private HitoComercialDao hitoComercialDao;


    @Override
    public List<HitoComercial> obtenerHitoComercial(String fkIdProyecto) {
        return hitoComercialDao.obtenerHitoComercial(fkIdProyecto);
    }

    @Override
    public List<HitoComercial> obtenerHitosPorFactura(Integer nFactura, String estado, String contraparte) {
        return hitoComercialDao.obtenerHitosPorFactura(nFactura, estado, contraparte);
    }

    @Override
    public List<EstadoFacturacion> obtenerHitosPorContraparte(Integer nFactura, String proyecto, String contraparte) {
        return hitoComercialDao.obtenerHitosPorContraparte(nFactura, proyecto, contraparte);
    }

    @Override
    public GeneralPostResponse upsertHitoComercial (HitoComercial hito){
        logger.info(hito.toString());
        return hitoComercialDao.upsertHitoComercial(hito);
    }

    @Override
    public void eliminarHitoComercial(HitoComercial hito){
        hitoComercialDao.eliminarHitoComercial(hito);
    }

    //-----------------------------BGN VVQ-20-06-2017 ------------------------------
    @Override
    public void upsertHitos(ArrayList<HitoComercial> hito) {
        logger.info(hito.toString());
        hitoComercialDao.upsertHitos(hito);
    }
    //-----------------------------END VVQ-20-06-2017 ------------------------------

    @Override
    public List<HitoComercial> buscarHitoComercial(String fkIdProyecto) {
        logger.info("Hello" + fkIdProyecto);
        return hitoComercialDao.obtenerHitoComercial(fkIdProyecto);
    }
}
