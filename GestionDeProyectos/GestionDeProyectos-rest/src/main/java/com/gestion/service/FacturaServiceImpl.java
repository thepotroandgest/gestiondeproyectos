package com.gestion.service;

import com.gestion.dominio.Factura;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.repository.FacturaDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Gustavo on 20-02-2017.
 */
@Service
public class FacturaServiceImpl implements FacturaService {

    private static final Logger logger= LoggerFactory.getLogger(FacturaService.class);

    @Autowired
    private FacturaDao facturaDao;
    @Override
    public List<Factura> obtenerFacturas(String idProyecto, Integer nFactura){
        return facturaDao.obtenerFacturas(idProyecto, nFactura);
    }
    @Override
    public GeneralPostResponse upsert(Factura pFact)
    {
        logger.info(pFact.toString());
        return facturaDao.upsert(pFact);

    }

    @Override
    public void eliminarFactura(Integer nFactura){
        facturaDao.eliminarFactura(nFactura);
    }

    @Override
    public void eliminarFacturaHito(Integer nFactura, String idProyecto, String hito ){
        facturaDao.eliminarFacturaHito(  nFactura, idProyecto, hito   );
    }


    @Override
    public List<Factura> buscarFactura(Integer nFactura) {
        logger.info("Hello" + nFactura);
        return facturaDao.obtenerFacturas(null, nFactura);
    }


}
