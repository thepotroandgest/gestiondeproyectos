package com.gestion.service;

import com.gestion.dominio.Avance;
import com.gestion.dominio.Bitacora;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.repository.AvanceDao;
import com.gestion.repository.BitacoraDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
@Service
public class AvanceServiceImpl implements AvanceService{
    @Autowired
    private AvanceDao avanceDao;


//    @Override
//    public List<Bitacora> obtenerBitacoras() {
//        return bitacoraDao.obtenerBitacoras();
//    }



    @Override
    public GeneralPostResponse agregarAvance( Avance avance ){

        return avanceDao.agregarAvance( avance );
    }

    @Override
    public List<Avance> obtenerAvance( Long idAvance, String idProyecto) {

        return avanceDao.obtenerAvance( idAvance, idProyecto );
    }

    @Override
    public void eliminarAvance( Long idAvance ){

         avanceDao.eliminarAvance( idAvance );
    }




//    @Override
//    public void actualizarBitacora(Bitacora pBit){
//        bitacoraDao.actualizarBitacora(pBit);
//    }
//
//    @Override
//    public void eliminarBitacora(long sec_bitacora){
//        bitacoraDao.eliminarBitacora(sec_bitacora);
//    }
//
//    @Override
//    public List<Bitacora> buscarBitacora(long sec_bitacora) {
//        return bitacoraDao.buscarBitacora(sec_bitacora);
//    }
//
//    @Override
//    public List<Bitacora> buscarBitacoraProyecto(String id_proyecto) {
//        return bitacoraDao.buscarBitacoraProyecto(id_proyecto);
//    }
}
