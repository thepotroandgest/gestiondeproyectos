package com.gestion.service;

import com.gestion.dominio.Bitacora;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.repository.BitacoraDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
@Service
public class BitacoraServiceImpl implements BitacoraService{
    @Autowired
    private BitacoraDao bitacoraDao;


    @Override
    public List<Bitacora> obtenerBitacoras( Long secBitacora, String idProyecto, String tipo ) {
        return bitacoraDao.obtenerBitacoras(secBitacora, idProyecto, tipo);
    }

    @Override
    public GeneralPostResponse agregarBitacora(Bitacora pBit){
         return bitacoraDao.agregarBitacora(pBit);
    }

    @Override
    public void eliminarBitacora(long sec_bitacora){
        bitacoraDao.eliminarBitacora(sec_bitacora);
    }


}
