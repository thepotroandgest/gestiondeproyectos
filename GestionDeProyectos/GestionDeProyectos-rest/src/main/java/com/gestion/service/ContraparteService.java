package com.gestion.service;

import com.gestion.dominio.Contraparte;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
public interface ContraparteService {
    List<Contraparte> obtenerContrapartes( String idContraparte );
    GeneralPostResponse upsert(Contraparte pCon);
    void eliminarContraparte(String idContraparte);
    List<Contraparte> buscarContraparte(String id_contraparte);
}
