package com.gestion.service;


import com.gestion.dominio.Esfuerzo_Real;
import com.gestion.dominio.Result;
import com.gestion.dominio.TraspasoEsfuerzoReal;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.repository.EsfuerzoRealDao;
import com.gestion.utils.MultipartFileFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
@Service
public class EsfuerzoRealServiceImpl implements EsfuerzoRealService {

    private static final Logger logger = LoggerFactory.getLogger( EsfuerzoRealServiceImpl.class );

    @Autowired
    private EsfuerzoRealDao esfuerzoRealDao;

    @Override
    public List<Esfuerzo_Real> obtenerEsfuerzo_Real() {
        return esfuerzoRealDao.obtenerEsfuerzo_Real();
    }


    @Override
    public GeneralPostResponse insertTraspaso(String  dataCompleta  ){
        ResponseServicio result;
        MultipartFileFormatter formateador = new MultipartFileFormatter( dataCompleta );
        ArrayList<TraspasoEsfuerzoReal> listaTraspasoEsfuerzoReal = new ArrayList<>();

        try {
            ArrayList<String[]> listaEsfuerzo = formateador.getListaRegistroEsfuerzoReal();

            for( int i = 1; i < listaEsfuerzo.size(); i++ ) {

                TraspasoEsfuerzoReal traspaso = new TraspasoEsfuerzoReal();

                if( listaEsfuerzo.get( i ).length > 0 && !listaEsfuerzo.get( i )[ 0 ].equals("") ){
                    traspaso.setServicio(  listaEsfuerzo.get( i )[ 0 ] );
                }else{
                    traspaso.setServicio( null );
                }
                if( listaEsfuerzo.get( i ).length >  1  && !listaEsfuerzo.get( i )[ 1 ].equals("") ){
                    traspaso.setCliente( listaEsfuerzo.get( i )[ 1 ] );
                }else{
                    traspaso.setCliente( null );
                }
                if( listaEsfuerzo.get( i ).length >  2 && !listaEsfuerzo.get( i )[ 2 ].equals("")  ){
                    traspaso.setProyecto( listaEsfuerzo.get( i )[ 2 ] );
                }else{
                    traspaso.setProyecto( null );
                }
                if( listaEsfuerzo.get( i ).length >  3 && !listaEsfuerzo.get( i )[ 3 ].equals("")  ){
                    traspaso.setUsuario( listaEsfuerzo.get( i )[ 3 ] );
                }
                else{
                    traspaso.setUsuario( null );
                }
                if( listaEsfuerzo.get( i ).length >  4  && !listaEsfuerzo.get( i )[ 4 ].equals("") ){
                    traspaso.setTarea(  listaEsfuerzo.get( i )[ 4 ]  );
                }
                else{
                    traspaso.setTarea( null );
                }
                if( listaEsfuerzo.get( i ).length >  5  && !listaEsfuerzo.get( i )[ 5 ].equals("") ){
                    traspaso.setFechaEntrega(  listaEsfuerzo.get( i )[ 5 ] );
                }else{
                    traspaso.setFechaEntrega( null );
                }
                if( listaEsfuerzo.get( i ).length >  6  && !listaEsfuerzo.get( i )[ 6 ].equals("")  ){
                    traspaso.setEstimatedHours(  new Double(listaEsfuerzo.get( i )[ 6 ]) );
                }else{
                    traspaso.setEstimatedHours( null );
                }
                if( listaEsfuerzo.get( i ).length >  7 && !listaEsfuerzo.get( i )[ 7 ].equals("") ){
                    traspaso.setFacturable( new Boolean( listaEsfuerzo.get( i )[ 7 ] ));
                }else{
                    traspaso.setFacturable( null );
                }
                if( listaEsfuerzo.get( i ).length >  8 && !listaEsfuerzo.get( i )[ 8 ].equals("") ){
                    traspaso.setPrecioHora( new Double( listaEsfuerzo.get( i )[ 8 ] ));
                }else{
                    traspaso.setPrecioHora( null );
                }
                if( listaEsfuerzo.get( i ).length >  9 && !listaEsfuerzo.get( i )[ 9 ].equals("") ){
                    traspaso.setArchivado( new Boolean( listaEsfuerzo.get( i )[ 9 ] ));
                }else{
                    traspaso.setArchivado( null );
                }
                if( listaEsfuerzo.get( i ).length >  10 && !listaEsfuerzo.get( i )[ 10 ].equals("") ){

                    logger.info( "-"+listaEsfuerzo.get( i )[ 10 ].trim()+"-"  );
                    traspaso.setFechaInicio(  listaEsfuerzo.get( i )[ 10 ].trim() );
                }else{
                    traspaso.setFechaInicio( null );
                }
                if( listaEsfuerzo.get( i ).length >  11 && !listaEsfuerzo.get( i )[ 11 ].equals("") ){
                    traspaso.setFechaFin(  listaEsfuerzo.get( i )[ 11 ].trim() );
                }else{
                    traspaso.setFechaFin( null );
                }
                if( listaEsfuerzo.get( i ).length >  12 && !listaEsfuerzo.get( i )[ 12 ].equals("") ){
                    traspaso.setZonaHoraria(  listaEsfuerzo.get( i )[ 12 ] );
                }else{
                    traspaso.setZonaHoraria( null );
                }
                if( listaEsfuerzo.get( i ).length >  13 && !listaEsfuerzo.get( i )[ 13 ].equals("") ){
                    traspaso.setDuracion(  listaEsfuerzo.get( i )[ 13 ]  );
                }else{
                    traspaso.setDuracion( null );
                }
                if( listaEsfuerzo.get( i ).length >  14 && !listaEsfuerzo.get( i )[ 14 ].equals("") ){
                    traspaso.setHoras(  new Double(listaEsfuerzo.get( i )[ 14 ].replace(',', '.') ) );
                }else{
                    traspaso.setHoras( null );
                }
                if( listaEsfuerzo.get( i ).length >  15 && !listaEsfuerzo.get( i )[ 15 ].equals("") ){
                    traspaso.setNotas(  listaEsfuerzo.get( i )[ 15 ] );
                }else{
                    traspaso.setNotas( null );
                }
                if( listaEsfuerzo.get( i ).length >  16 && !listaEsfuerzo.get( i )[ 16 ].equals("") ){
                    traspaso.setTotal(  new Double( listaEsfuerzo.get( i )[ 16 ] ));
                }else{
                    traspaso.setTotal( null );
                }
                if( listaEsfuerzo.get( i ).length >  17 && !listaEsfuerzo.get( i )[ 17 ].equals("") ){
                    traspaso.setMoneda(   listaEsfuerzo.get( i )[ 17 ]  );
                }else{
                    traspaso.setMoneda( null );
                }

                listaTraspasoEsfuerzoReal.add( traspaso );
            }

        }catch( NumberFormatException e){
            logger.info( " excepcion --> " + e );
            logger.info( " excepcion --> " + e.getStackTrace() );
        }catch ( Exception e){
            logger.info( " excepcion --> " + e.getMessage() );
            throw e;
        }

        return  esfuerzoRealDao.insertarTraspasoEsfuerzoReal( listaTraspasoEsfuerzoReal );

    }


    @Override
    public GeneralPostResponse insert(String  dataCompleta) {

        GeneralPostResponse result =   insertTraspaso( dataCompleta  );
//        esfuerzoRealDao.setearEsfuerzoReal();

        return result;
    }

    @Override
    public void actualizarEsfuerzoManual( Esfuerzo_Real esfuerzoReal ){

        esfuerzoRealDao.actualizarEsfuerzoManual( esfuerzoReal );

    }


}





























