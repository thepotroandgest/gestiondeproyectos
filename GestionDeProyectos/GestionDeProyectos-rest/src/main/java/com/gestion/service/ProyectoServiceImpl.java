package com.gestion.service;

import com.gestion.dominio.EstadoProyecto;
import com.gestion.dominio.Proyecto;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.repository.ProyectoDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
@Service
public class ProyectoServiceImpl implements ProyectoService {

    private static final Logger logger = LoggerFactory.getLogger( ProyectoServiceImpl.class );

    @Autowired
    private ProyectoDao proyectoDao;
    @Override
    public List<Proyecto> obtenerProyectos( String idProyecto ) {
        return proyectoDao.obtenerProyectos( idProyecto );
    }

    @Override
    public GeneralPostResponse upsert(Proyecto pPro){

        pPro.setFechaInicio( pPro.getFechaInicio().substring( 0, 10 )   );
        pPro.setFechaTermino( pPro.getFechaTermino().substring( 0, 10 ) );

        logger.info(pPro.toString());
        return proyectoDao.upsert(pPro);
    }

    @Override
    public GeneralPostResponse eliminarProyecto(String id_proyecto){
        return proyectoDao.eliminarProyecto(id_proyecto);
    }

    @Override
    public List<Proyecto> buscarProyecto(String id_proyecto) {
        return proyectoDao.buscarProyecto(id_proyecto);
    }

    @Override
    public List<EstadoProyecto> obtenerEstado( String idProyecto){
        return proyectoDao.obtenerEstado(idProyecto);
    }

}
