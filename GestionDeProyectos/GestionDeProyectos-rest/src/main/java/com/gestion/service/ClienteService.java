package com.gestion.service;

import com.gestion.dominio.Cliente;
import com.gestion.dominio.response.GeneralPostResponse;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
@Service
public interface ClienteService {
    List<Cliente> obtenerClientes( String rutCliente );
    GeneralPostResponse upsert(Cliente pCli);
    void eliminarCliente(String rut_cliente);

}
