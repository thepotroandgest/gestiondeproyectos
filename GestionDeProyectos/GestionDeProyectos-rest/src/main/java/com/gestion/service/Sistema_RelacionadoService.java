package com.gestion.service;

import com.gestion.dominio.Sistema_Relacionado;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
public interface Sistema_RelacionadoService {
    List<Sistema_Relacionado> obtenerSistema_Relacionados( String idSistema );
    GeneralPostResponse upsert(Sistema_Relacionado pSiRe);
    void eliminarSistema_relacionado(String id_sistema_relacionado);
    List<Sistema_Relacionado> buscarSistema_relacionado(String id_sistema_relacionado);
}
