package com.gestion.service;

import com.gestion.dominio.EstadoProyecto;
import com.gestion.dominio.Proyecto;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
public interface ProyectoService {
    List<Proyecto> obtenerProyectos( String idProyecto );
    List<EstadoProyecto> obtenerEstado( String idProyecto );
    GeneralPostResponse upsert(Proyecto pPro);
    GeneralPostResponse eliminarProyecto(String id_proyecto);
    List<Proyecto> buscarProyecto(String id_proyecto);


}
