package com.gestion.service;

import com.gestion.dominio.Cliente;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.repository.ClienteDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
@Service
public class ClienteServiceImpl implements ClienteService{
    @Autowired
    private ClienteDao clienteDao;


    @Override
    public List<Cliente> obtenerClientes( String rutClientes ) {
        return clienteDao.obtenerClientes( rutClientes );
    }

    @Override
    public GeneralPostResponse upsert(Cliente pCli){
        return clienteDao.upsert(pCli);
    }

    @Override
    public void eliminarCliente(String rut_cliente){
        clienteDao.eliminarCliente(rut_cliente);
    }

}
