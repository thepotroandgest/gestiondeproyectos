package com.gestion.service;

import com.gestion.dominio.Avance;
import com.gestion.dominio.Bitacora;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
public interface AvanceService {
//    List<Bitacora> obtenerBitacoras();
    GeneralPostResponse agregarAvance( Avance avance );
    List<Avance> obtenerAvance( Long idAvance, String idProyecto);
    void eliminarAvance( Long idAvance );
//    void actualizarBitacora(Bitacora pBit);
//    void eliminarBitacora(long sec_bitacora);
//    List<Bitacora> buscarBitacora(long sec_bitacora);
//    List<Bitacora> buscarBitacoraProyecto(String id_proyecto);
}
