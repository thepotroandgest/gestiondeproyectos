package com.gestion.service;

import com.gestion.dominio.Bitacora;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
public interface BitacoraService {
    List<Bitacora> obtenerBitacoras(Long secBitacora, String idProyecto, String tipo);
    GeneralPostResponse agregarBitacora(Bitacora pBit);
    void eliminarBitacora(long sec_bitacora);

}
