package com.gestion.service;

import com.gestion.dominio.Contraparte;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.repository.ContraparteDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
@Service
public class ContraparteServiceImpl implements ContraparteService {
    @Autowired
    private ContraparteDao contraparteDao;

    @Override
    public List<Contraparte> obtenerContrapartes( String idContraparte){
        return contraparteDao.obtenerContrapartes( idContraparte );
    }

    @Override
    public GeneralPostResponse upsert(Contraparte pCon){
        return contraparteDao.upsert(pCon);
    }

    @Override
    public void eliminarContraparte(String idContraparte){
        contraparteDao.eliminarContraparte(idContraparte);
    }

    @Override
    public List<Contraparte> buscarContraparte(String id_contraparte) {
        return contraparteDao.buscarContraparte(id_contraparte);
    }
}
