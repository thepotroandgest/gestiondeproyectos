package com.gestion.service;

import com.gestion.dominio.Recurso;
import com.gestion.dominio.RecursosPorProyecto;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.List;

public interface RecursoService {

    List<Recurso> obtenerRecursos(String rutRecurso);
    List<RecursosPorProyecto> obtenerRecursosPorProyecto(String idProyecto,  String idRecurso);
    GeneralPostResponse upsert(Recurso pRec);
    void eliminarRecurso(String rutRecurso);



}
