package com.gestion.service;

import com.gestion.dominio.Result;
import com.gestion.dominio.response.ResponseServicio;
import com.gestion.repository.EsfuerzoRealDao;
import com.gestion.repository.HorasHombreDao;
import com.gestion.repository.TareaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by cvera on 15-05-17.
 */

@Service
public class HorasHombreServiceImpl implements HorasHombreService {

    @Autowired
    private HorasHombreDao horasHombreDao;

    @Autowired
    private EsfuerzoRealService esfuerzoRealService;

    @Override
    public ResponseServicio insert(String  dataHorasHombre){

        ResponseServicio result;

        esfuerzoRealService.insertTraspaso( dataHorasHombre );
        result = horasHombreDao.insert();

        return result;

    }


}
