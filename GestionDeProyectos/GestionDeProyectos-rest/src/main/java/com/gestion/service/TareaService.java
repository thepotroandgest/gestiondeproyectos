package com.gestion.service;

import com.gestion.dominio.Tarea;
import com.gestion.dominio.response.TareaPostResponse;

import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
public interface TareaService {
    List<Tarea> obtenerTareas(String id_proyecto);
    TareaPostResponse insert(String  dataCompleta);
    TareaPostResponse insertarActualizarTareas(Tarea tTarea);
    void eliminarTarea(Tarea tTarea);
}
