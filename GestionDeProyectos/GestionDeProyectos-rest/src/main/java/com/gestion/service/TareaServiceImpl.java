package com.gestion.service;

import com.gestion.dominio.Tarea;
import com.gestion.dominio.TraspasoTarea;
import com.gestion.dominio.response.TareaPostResponse;
import com.gestion.repository.TareaDao;
import com.gestion.utils.MultipartFileFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
@Service
public class TareaServiceImpl implements TareaService {
    @Autowired
    private TareaDao tareaDao;

    @Override
    public List<Tarea> obtenerTareas(String id_proyecto) {
        return tareaDao.obtenerTareas(id_proyecto);
    }

    private static final Logger logger = LoggerFactory.getLogger( TareaServiceImpl.class );

    @Override
    public TareaPostResponse insertarActualizarTareas(Tarea tTarea){

        return tareaDao.insertarActualizarTareas(tTarea);
    }

    @Override
    public TareaPostResponse insert(String  dataCompleta) {

        MultipartFileFormatter formateador = new MultipartFileFormatter( dataCompleta );
        ArrayList<TraspasoTarea> listaTraspasoTarea = new ArrayList<>();

        try {
            ArrayList<String[]> listaTareas = formateador.getListaRegistroTareas();

            for( int i = 1; i < listaTareas.size(); i++ ) {

                TraspasoTarea traspaso = new TraspasoTarea();

                if( listaTareas.get( i ).length > 0 && !listaTareas.get( i )[ 0 ].equals("") ){
                    traspaso.setId(  new Integer( listaTareas.get( i )[ 0 ].trim() ));
                }else{
                    traspaso.setId( null );
                }
                if( listaTareas.get( i ).length >  1  && !listaTareas.get( i )[ 1 ].equals("") ){
                    traspaso.setNombre( listaTareas.get( i )[ 1 ] );
                }else{
                    traspaso.setNombre( null );
                }
                if( listaTareas.get( i ).length >  2 && !listaTareas.get( i )[ 2 ].equals("")  ){
                    traspaso.setDuration( new Double( cutDuration(listaTareas.get( i )[ 2 ].trim() ).replace( ',', '.'  ) ) );
                }else{
                    traspaso.setDuration( null );
                }
                if( listaTareas.get( i ).length >  3 && !listaTareas.get( i )[ 3 ].equals("")  ){
                    traspaso.setScheduledWork( new Double( cutDuration(listaTareas.get( i )[ 3 ].trim()).replace( ',', '.'  ) ));
                }
                else{
                    traspaso.setScheduledWork( null );
                }
                if( listaTareas.get( i ).length >  4  && !listaTareas.get( i )[ 4 ].equals("") ){
                    traspaso.setStartDate( cutDate( listaTareas.get( i )[ 4 ] ) );
                }
                else{
                    traspaso.setStartDate( null );
                }
                if( listaTareas.get( i ).length >  5  && !listaTareas.get( i )[ 5 ].equals("") ){
                    traspaso.setFinishDate( cutDate( listaTareas.get( i )[ 5 ] ) );
                }else{
                    traspaso.setFinishDate( null );
                }
                if( listaTareas.get( i ).length >  6  && !listaTareas.get( i )[ 6 ].equals("")  ){
                    traspaso.setPredecessors(  new Integer(listaTareas.get( i )[ 6 ].trim()) );
                }else{
                    traspaso.setPredecessors( null );
                }
                if( listaTareas.get( i ).length >  7 && !listaTareas.get( i )[ 7 ].equals("")  && listaTareas.get( i )[ 7 ] != null ){
                    traspaso.setResourceInitials( listaTareas.get( i )[ 7 ] );
                }else{
                    traspaso.setResourceInitials( null );
                }

                logger.info( traspaso.toString()  );
                listaTraspasoTarea.add( traspaso );
            }

        }catch( NumberFormatException e){
            logger.info( " excepcion --> " + e.getMessage() );
        }catch ( Exception e){
            logger.info( " excepcion --> " + e );
            throw e;
        }

        return tareaDao.insert( listaTraspasoTarea );
    }

    private String cutDate( String rawDate ){
        String coockedDate;
        int indiceEspacio;

        indiceEspacio = rawDate.indexOf( ' ' );
        coockedDate = rawDate.substring( indiceEspacio, rawDate.length() );

        return coockedDate;
    }

    private String cutDuration( String rawDate ){
        String coockedDate;
        int indiceEspacio;

        indiceEspacio = rawDate.indexOf( ' ' );
        coockedDate = rawDate.substring( 0, indiceEspacio );

        return coockedDate;
    }


    @Override
    public void eliminarTarea(Tarea tTarea){
        tareaDao.eliminarTarea(tTarea);
    }


}
