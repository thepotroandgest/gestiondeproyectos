package com.gestion.service;

import com.gestion.dominio.Esfuerzo_Real;
import com.gestion.dominio.Result;
import com.gestion.dominio.response.ResponseServicio;

import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
public interface EsfuerzoRealService {
    List<Esfuerzo_Real> obtenerEsfuerzo_Real();
    ResponseServicio insert(String  dataCompleta);
    ResponseServicio insertTraspaso( String  dataCompleta  );
    void actualizarEsfuerzoManual( Esfuerzo_Real esfuerzoReal );

}
