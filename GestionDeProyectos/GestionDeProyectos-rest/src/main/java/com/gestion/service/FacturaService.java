package com.gestion.service;

import com.gestion.dominio.Factura;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.List;

/**
 * Created by Gustavo on 20-02-2017.
 */
public interface FacturaService {
    List<Factura> obtenerFacturas(String idProyecto, Integer nFactura);
    GeneralPostResponse upsert(Factura pFact);
    void eliminarFactura(Integer nFactura);
    void eliminarFacturaHito(Integer nFactura, String idProyecto, String hito );
    List<Factura> buscarFactura(Integer nFactura);
//    List<Factura> buscarFacturaProyecto(String id_proyecto);
}
