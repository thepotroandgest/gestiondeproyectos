package com.gestion.service;

import com.gestion.dominio.Login;
import com.gestion.repository.LoginDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
@Service
public class LoginServiceImpl implements LoginService{
    @Autowired
    private LoginDao loginDao;


    @Override
    public List<Login> obtenerLogin() {
        return loginDao.obtenerLogin();
    }
}
