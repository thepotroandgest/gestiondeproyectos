package com.gestion.service;

import com.gestion.dominio.DefinicionHito;
import com.gestion.dominio.Parametro;
import com.gestion.dominio.response.GeneralPostResponse;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
@Service
public interface DefinicionHitoService {
    List<DefinicionHito> obtenerDefinicionHitos(Integer id);
    GeneralPostResponse upsert(DefinicionHito defHito);
    void eliminarDefinicionHito(Integer id);
}
