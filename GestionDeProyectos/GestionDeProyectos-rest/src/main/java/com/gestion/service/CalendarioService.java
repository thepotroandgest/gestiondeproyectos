package com.gestion.service;

import com.gestion.dominio.Calendario;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Gustavo on 06-03-2017.
 */
@Service
public interface CalendarioService {
    List<Calendario> obtenerCalendario();
     List<Calendario> obtenerCalendarioRecursos();

}
