package com.gestion.service;

import com.gestion.dominio.EstadoFacturacion;
import com.gestion.dominio.HitoComercial;
import com.gestion.dominio.response.GeneralPostResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gustavo on 21-02-2017.
 */
public interface HitoComercialService {
    List<HitoComercial> obtenerHitoComercial(String fkIdProyecto);
    List<HitoComercial> obtenerHitosPorFactura(Integer nFactura, String estado, String contraparte );
    List<EstadoFacturacion> obtenerHitosPorContraparte(Integer nFactura, String proyecto, String contraparte );
    GeneralPostResponse upsertHitoComercial (HitoComercial hito);
    void eliminarHitoComercial( HitoComercial hito );
    //-----------------------------BGN VVQ-20-06-2017 ------------------------------
    void upsertHitos(ArrayList<HitoComercial> hito);
    //-----------------------------END VVQ-20-06-2017 ------------------------------
    List<HitoComercial> buscarHitoComercial(String fkIdProyecto);


}
