package com.gestion.service;

import com.gestion.dominio.DefinicionHito;
import com.gestion.dominio.Parametro;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.repository.DefinicionHitoDao;
import com.gestion.repository.ParametroDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
@Service
public class DefinicionHitoServiceImpl implements DefinicionHitoService{
    @Autowired
    private DefinicionHitoDao definicionHitoDao;


    @Override
    public List<DefinicionHito> obtenerDefinicionHitos( Integer id  ) {
        return definicionHitoDao.obtenerDefinicionHitos( id );
    }

    @Override
    public GeneralPostResponse upsert(DefinicionHito defHito){
        return definicionHitoDao.upsert(defHito);
    }

    @Override
    public void eliminarDefinicionHito(Integer id){
        definicionHitoDao.eliminarDefinicionHito( id );
    }

}
