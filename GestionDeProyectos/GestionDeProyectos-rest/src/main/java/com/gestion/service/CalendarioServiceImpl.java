package com.gestion.service;


import com.gestion.dominio.Calendario;
import com.gestion.repository.CalendarioDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Gustavo on 06-03-2017.
 */
@Service
public class CalendarioServiceImpl implements CalendarioService{
    @Autowired
    private CalendarioDao calendarioDao;


    @Override
    public List<Calendario> obtenerCalendario() {
        return calendarioDao.obtenerCalendario();
    }

    @Override
    public List<Calendario> obtenerCalendarioRecursos(){

        return calendarioDao.obtenerCalendarioRecursos();
    }




}
