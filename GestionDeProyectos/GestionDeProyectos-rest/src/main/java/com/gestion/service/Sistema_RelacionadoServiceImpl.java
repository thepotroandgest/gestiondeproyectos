package com.gestion.service;


import com.gestion.dominio.Sistema_Relacionado;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.repository.Sistema_RelacionadoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
@Service
public class Sistema_RelacionadoServiceImpl implements Sistema_RelacionadoService {
    @Autowired
    private Sistema_RelacionadoDao sistema_relacionadoDao;


    @Override
    public List<Sistema_Relacionado> obtenerSistema_Relacionados(String idSistema) {
        return sistema_relacionadoDao.obtenerSistema_Relacionados( idSistema);
    }

    @Override
    public GeneralPostResponse upsert(Sistema_Relacionado pSiRe){
        return sistema_relacionadoDao.upsert(pSiRe);
    }

    @Override
    public void eliminarSistema_relacionado(String id_sistema_relacionado){
        sistema_relacionadoDao.eliminarSistema_relacionado(id_sistema_relacionado);
    }

    @Override
    public List<Sistema_Relacionado> buscarSistema_relacionado(String id_sistema_relacionado) {
        return sistema_relacionadoDao.buscarSistema_relacionado(id_sistema_relacionado);
    }
}
