package com.gestion.service;

import com.gestion.dominio.Cliente;
import com.gestion.dominio.Parametro;
import com.gestion.dominio.response.GeneralPostResponse;
import com.gestion.repository.ClienteDao;
import com.gestion.repository.ParametroDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
@Service
public class ParametroServiceImpl implements ParametroService{
    @Autowired
    private ParametroDao parametroDao;


    @Override
    public List<Parametro> obtenerParametros( Integer id  ) {
        return parametroDao.obtenerParametros( id );
    }

    @Override
    public GeneralPostResponse upsert(Parametro param){
        return parametroDao.upsert(param);
    }

    @Override
    public void eliminarParametro(Integer id){
        parametroDao.eliminarParametro( id );
    }

}
