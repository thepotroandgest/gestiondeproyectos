package com.gestion.service;

import com.gestion.dominio.Cliente;
import com.gestion.dominio.Parametro;
import com.gestion.dominio.response.GeneralPostResponse;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Gustavo on 27-01-2017.
 */
@Service
public interface ParametroService {
    List<Parametro> obtenerParametros( Integer id );
    GeneralPostResponse upsert( Parametro param );
    void eliminarParametro( Integer id );
}
