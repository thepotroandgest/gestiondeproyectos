package com.gestion.dominio;

/**
 * Created by cvera on 31-07-17.
 */
public class DefinicionHito {

    Integer id;
    String nombre;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "DefinicionHito{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                '}';
    }

}
