package com.gestion.dominio.error;

/**
 * Created with IntelliJ IDEA.
 * User: Sergio Puas (sjpuas) - Bennu Ltda.
 * Date: May-2016
 * Project: new-resumen
 */
public class ErrorDTO {
  private Integer codigo;
  private String mensaje;

  public ErrorDTO(Integer codigo, String mensaje) {
    this.codigo = codigo;
    this.mensaje = mensaje;
  }

  public String getMensaje() {
    return mensaje;
  }

  public void setMensaje(String mensaje) {
    this.mensaje = mensaje;
  }

  public Integer getCodigo() {
    return codigo;
  }

  public void setCodigo(Integer codigo) {
    this.codigo = codigo;
  }
}
