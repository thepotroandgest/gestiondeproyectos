package com.gestion.dominio;

/**
 * Created by cvera on 27-04-17.
 */
public class TraspasoEsfuerzoReal {


    private String servicio;
    private String cliente;
    private String proyecto;
    private String usuario;
    private String tarea;
    private String fechaEntrega;
    private Double estimatedHours;
    private Boolean facturable;
    private Double precioHora;
    private Boolean archivado;
    private String fechaInicio;
    private String fechaFin;
    private String zonaHoraria;
    private String duracion;
    private Double horas;
    private String notas;
    private Double total;
    private String moneda;

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getProyecto() {
        return proyecto;
    }

    public void setProyecto(String proyecto) {
        this.proyecto = proyecto;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTarea() {
        return tarea;
    }

    public void setTarea(String tarea) {
        this.tarea = tarea;
    }

    public String getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public Double getEstimatedHours() {
        return estimatedHours;
    }

    public void setEstimatedHours(Double estimatedHours) {
        this.estimatedHours = estimatedHours;
    }

    public Boolean getFacturable() {
        return facturable;
    }

    public void setFacturable(Boolean facturable) {
        this.facturable = facturable;
    }

    public Double getPrecioHora() {
        return precioHora;
    }

    public void setPrecioHora(Double precioHora) {
        this.precioHora = precioHora;
    }

    public Boolean getArchivado() {
        return archivado;
    }

    public void setArchivado(Boolean archivado) {
        this.archivado = archivado;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getZonaHoraria() {
        return zonaHoraria;
    }

    public void setZonaHoraria(String zonaHoraria) {
        this.zonaHoraria = zonaHoraria;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public Double getHoras() {
        return horas;
    }

    public void setHoras(Double horas) {
        this.horas = horas;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    @Override
    public String toString() {
        return "TraspasoEsfuerzoReal{" +
                "servicio='" + servicio + '\'' +
                ", cliente='" + cliente + '\'' +
                ", proyecto='" + proyecto + '\'' +
                ", usuario='" + usuario + '\'' +
                ", tarea='" + tarea + '\'' +
                ", fechaEntrega='" + fechaEntrega + '\'' +
                ", estimatedHours=" + estimatedHours +
                ", facturable=" + facturable +
                ", precioHora=" + precioHora +
                ", archivado=" + archivado +
                ", fechaInicio='" + fechaInicio + '\'' +
                ", fechaFin='" + fechaFin + '\'' +
                ", zonaHoraria='" + zonaHoraria + '\'' +
                ", duracion='" + duracion + '\'' +
                ", horas=" + horas +
                ", notas='" + notas + '\'' +
                ", total=" + total +
                ", moneda='" + moneda + '\'' +
                '}';
    }
}

