package com.gestion.dominio;


import java.util.ArrayList;

/**
 * Created by Gustavo on 21-02-2017.
 */
public class Proyecto {
    //atributos

    private String idProyecto;
    private String nombre;
    private String tipo;
    private Double esfuerzoVendido;
    private Double esfuerzoReal;
    private String fechaInicio;
    private String fechaTermino;
    private String descripcion;
    private Double valorVendido;
    private String fkIdContraparte;
    private String fkIdSistemaRelacionado;
    private String fkRutRecurso;
    private String idRecurso;
    private String nombreContraparte;
    private String nombreSistema;
    private String fase;
    private String estado;
    private ArrayList<Recurso> recursosExtras;




    //Constructor
    public Proyecto() {
    }

    public String getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(String idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getEsfuerzoVendido() {
        return esfuerzoVendido;
    }

    public void setEsfuerzoVendido(Double esfuerzoVendido) {
        this.esfuerzoVendido = esfuerzoVendido;
    }

    public Double getEsfuerzoReal() {
        return esfuerzoReal;
    }

    public void setEsfuerzoReal(Double esfuerzoReal) {
        this.esfuerzoReal = esfuerzoReal;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaTermino() {
        return fechaTermino;
    }

    public void setFechaTermino(String fechaTermino) {
        this.fechaTermino = fechaTermino;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getValorVendido() {
        return valorVendido;
    }

    public void setValorVendido(Double valorVendido) {
        this.valorVendido = valorVendido;
    }

    public String getFkIdContraparte() {
        return fkIdContraparte;
    }

    public void setFkIdContraparte(String fkIdContraparte) {
        this.fkIdContraparte = fkIdContraparte;
    }

    public String getFkIdSistemaRelacionado() {
        return fkIdSistemaRelacionado;
    }

    public void setFkIdSistemaRelacionado(String fkIdSistemaRelacionado) {
        this.fkIdSistemaRelacionado = fkIdSistemaRelacionado;
    }

    public String getFkRutRecurso() {
        return fkRutRecurso;
    }

    public void setFkRutRecurso(String fkRutRecurso) {
        this.fkRutRecurso = fkRutRecurso;
    }

    public String getIdRecurso() {
        return idRecurso;
    }

    public void setIdRecurso(String idRecurso) {
        this.idRecurso = idRecurso;
    }

    public String getNombreContraparte() {
        return nombreContraparte;
    }

    public void setNombreContraparte(String nombreContraparte) {
        this.nombreContraparte = nombreContraparte;
    }

    public String getNombreSistema() {
        return nombreSistema;
    }

    public void setNombreSistema(String nombreSistema) {
        this.nombreSistema = nombreSistema;
    }

    public String getFase() {
        return fase;
    }

    public void setFase(String fase) {
        this.fase = fase;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public ArrayList<Recurso> getRecursosExtras() {
        return recursosExtras;
    }

    public void setRecursosExtras(ArrayList<Recurso> recursosExtras) {
        this.recursosExtras = recursosExtras;
    }

    @Override
    public String toString() {
        return "Proyecto{" +
                "idProyecto='" + idProyecto + '\'' +
                ", nombre='" + nombre + '\'' +
                ", tipo='" + tipo + '\'' +
                ", esfuerzoVendido=" + esfuerzoVendido +
                ", esfuerzoReal=" + esfuerzoReal +
                ", fechaInicio='" + fechaInicio + '\'' +
                ", fechaTermino='" + fechaTermino + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", valorVendido=" + valorVendido +
                ", fkIdContraparte='" + fkIdContraparte + '\'' +
                ", fkIdSistemaRelacionado='" + fkIdSistemaRelacionado + '\'' +
                ", fkRutRecurso='" + fkRutRecurso + '\'' +
                ", idRecurso='" + idRecurso + '\'' +
                ", nombreContraparte='" + nombreContraparte + '\'' +
                ", nombreSistema='" + nombreSistema + '\'' +
                ", fase='" + fase + '\'' +
                ", estado='" + estado + '\'' +
                ", recursosExtras=" + recursosExtras +
                '}';
    }
}
