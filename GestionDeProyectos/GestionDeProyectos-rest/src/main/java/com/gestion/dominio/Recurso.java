package com.gestion.dominio;

public class Recurso {

    //Atributos
    private String rutRecurso;
    private String idRecurso;
    private String nombre;

    //Constructor
    public Recurso(){

    }

    public String getRutRecurso() {
        return rutRecurso;
    }

    public void setRutRecurso(String rutRecurso) {
        this.rutRecurso = rutRecurso;
    }

    public String getIdRecurso() {
        return idRecurso;
    }

    public void setIdRecurso(String idRecurso) {
        this.idRecurso = idRecurso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Recurso{" +
                "rutRecurso='" + rutRecurso + '\'' +
                ", idRecurso='" + idRecurso + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}