package com.gestion.dominio;

/**
 * Created by cvera on 29-05-17.
 */
public class EstadoProyecto {



    private Long id;
    private String fkIdProyecto;
    private String fechaInicio;
    private String fechaTermino;
    private String fechaActual;
    private Double avanceEstimado;
    private Double avanceReal;
    private Double esfuerzoVendido;
    private Double esfuerzoReal;
    private Double indiceAvance;
    private Double indiceEsfuerzo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFkIdProyecto() {
        return fkIdProyecto;
    }

    public void setFkIdProyecto(String fkIdProyecto) {
        this.fkIdProyecto = fkIdProyecto;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaTermino() {
        return fechaTermino;
    }

    public void setFechaTermino(String fechaTermino) {
        this.fechaTermino = fechaTermino;
    }

    public String getFechaActual() {
        return fechaActual;
    }

    public void setFechaActual(String fechaActual) {
        this.fechaActual = fechaActual;
    }

    public Double getAvanceEstimado() {
        return avanceEstimado;
    }

    public void setAvanceEstimado(Double avanceEstimado) {
        this.avanceEstimado = avanceEstimado;
    }

    public Double getAvanceReal() {
        return avanceReal;
    }

    public void setAvanceReal(Double avanceReal) {
        this.avanceReal = avanceReal;
    }

    public Double getEsfuerzoVendido() {
        return esfuerzoVendido;
    }

    public void setEsfuerzoVendido(Double esfuerzoVendido) {
        this.esfuerzoVendido = esfuerzoVendido;
    }

    public Double getEsfuerzoReal() {
        return esfuerzoReal;
    }

    public void setEsfuerzoReal(Double esfuerzoReal) {
        this.esfuerzoReal = esfuerzoReal;
    }

    public Double getIndiceAvance() {
        return indiceAvance;
    }

    public void setIndiceAvance(Double indiceAvance) {
        this.indiceAvance = indiceAvance;
    }

    public Double getIndiceEsfuerzo() {
        return indiceEsfuerzo;
    }

    public void setIndiceEsfuerzo(Double indiceEsfuerzo) {
        this.indiceEsfuerzo = indiceEsfuerzo;
    }

    @Override
    public String toString() {
        return "EstadoProyecto{" +
                "id=" + id +
                ", fkIdProyecto='" + fkIdProyecto + '\'' +
                ", fechaInicio='" + fechaInicio + '\'' +
                ", fechaTermino='" + fechaTermino + '\'' +
                ", fechaActual='" + fechaActual + '\'' +
                ", avanceEstimado=" + avanceEstimado +
                ", avanceReal=" + avanceReal +
                ", esfuerzoVendido=" + esfuerzoVendido +
                ", esfuerzoReal=" + esfuerzoReal +
                ", indiceAvance=" + indiceAvance +
                ", indiceEsfuerzo=" + indiceEsfuerzo +
                '}';
    }
}
