package com.gestion.dominio.response;

/**
 * User: DZC - Seiza SpA.
 * Date: NOV-2016
 * Project: Participe API REST
 */
public interface ResponseServicio {
  public String getStatusMsj();
  public boolean isOk();
}
