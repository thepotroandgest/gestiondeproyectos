package com.gestion.dominio;

/**
 * Created by Gustavo on 23-03-2017.
 */
public class Result {

    private Integer result;
    private String message;

    public Result(){}


    public Integer getResult() {
        return result;
    }

    public Result setResult(Integer result) {
        this.result = result;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Result setMessage(String message) {
        this.message = message;
        return this;
    }
}
