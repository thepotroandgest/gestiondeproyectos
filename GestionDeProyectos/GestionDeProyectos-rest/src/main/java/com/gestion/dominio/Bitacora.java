package com.gestion.dominio;

import java.util.Date;

/**
 * Created by Gustavo on 27-01-2017.
 */
public class Bitacora {

    //Atributos
    private Long secBitacora;
    private String tipo;
    private String asunto;
    private String descripcion;
    private String rutResponsable;
    private String fechaIngreso;
    private String fechaVencimiento;
    private String fkIdProyecto;
    private String idRecurso;
    private String link;


    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getIdRecurso() {
        return idRecurso;
    }

    public void setIdRecurso(String idRecurso) {
        this.idRecurso = idRecurso;
    }

    public Long getSecBitacora() {
        return secBitacora;
    }

    public void setSecBitacora(Long secBitacora) {
        this.secBitacora = secBitacora;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRutResponsable() {
        return rutResponsable;
    }

    public void setRutResponsable(String rutResponsable) {
        this.rutResponsable = rutResponsable;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getFkIdProyecto() {
        return fkIdProyecto;
    }

    public void setFkIdProyecto(String fkIdProyecto) {
        this.fkIdProyecto = fkIdProyecto;
    }

    @Override
    public String toString() {
        return "Bitacora{" +
                "secBitacora=" + secBitacora +
                ", tipo='" + tipo + '\'' +
                ", asunto='" + asunto + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", rutResponsable='" + rutResponsable + '\'' +
                ", fechaIngreso='" + fechaIngreso + '\'' +
                ", fechaVencimiento='" + fechaVencimiento + '\'' +
                ", fkIdProyecto='" + fkIdProyecto + '\'' +
                ", idRecurso='" + idRecurso + '\'' +
                ", link='" + link + '\'' +
                '}';
    }
}
