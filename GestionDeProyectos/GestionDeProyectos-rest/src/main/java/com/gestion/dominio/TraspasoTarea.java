package com.gestion.dominio;

/**
 * Created by Gustavo on 20-03-2017.
 */

public class TraspasoTarea {

    //Atributos
    private Integer id;
    private String nombre;
    private Double duration;
    private Double scheduledWork;
    private String startDate;
    private String finishDate;
    private Integer predecessors;
    private String resourceInitials;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public Double getScheduledWork() {
        return scheduledWork;
    }

    public void setScheduledWork(Double scheduledWork) {
        this.scheduledWork = scheduledWork;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public Integer getPredecessors() {
        return predecessors;
    }

    public void setPredecessors(Integer predecessors) {
        this.predecessors = predecessors;
    }

    public String getResourceInitials() {
        return resourceInitials;
    }

    public void setResourceInitials(String resourceInitials) {
        this.resourceInitials = resourceInitials;
    }


    @Override
    public String toString() {
        return "TraspasoTarea{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", duration=" + duration +
                ", scheduledWork=" + scheduledWork +
                ", startDate='" + startDate + '\'' +
                ", finishDate='" + finishDate + '\'' +
                ", predecessors=" + predecessors +
                ", resourceInitials='" + resourceInitials + '\'' +
                '}';
    }
}
