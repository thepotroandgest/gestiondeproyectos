package com.gestion.dominio;

/**
 * Created by cvera on 28-08-17.
 */
public class RecursosPorProyecto {


    private String idRecurso;
    private String rutRecurso;
    private String nombre;
    private String fkIdProyecto;
    private String fechaInicio;
    private String fechaTermino;

    public String getIdRecurso() {
        return idRecurso;
    }

    public void setIdRecurso(String idRecurso) {
        this.idRecurso = idRecurso;
    }

    public String getRutRecurso() {
        return rutRecurso;
    }

    public void setRutRecurso(String rutRecurso) {
        this.rutRecurso = rutRecurso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFkIdProyecto() {
        return fkIdProyecto;
    }

    public void setFkIdProyecto(String fkIdProyecto) {
        this.fkIdProyecto = fkIdProyecto;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaTermino() {
        return fechaTermino;
    }

    public void setFechaTermino(String fechaTermino) {
        this.fechaTermino = fechaTermino;
    }


    @Override
    public String toString() {
        return "RecursosPorProyecto{" +
                "idRecurso='" + idRecurso + '\'' +
                ", rutRecurso='" + rutRecurso + '\'' +
                ", nombre='" + nombre + '\'' +
                ", fkIdProyecto='" + fkIdProyecto + '\'' +
                ", fechaInicio='" + fechaInicio + '\'' +
                ", fechaTermino='" + fechaTermino + '\'' +
                '}';
    }
}
