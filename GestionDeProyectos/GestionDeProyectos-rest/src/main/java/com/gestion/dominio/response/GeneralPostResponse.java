package com.gestion.dominio.response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by cvera on 06-06-17.
 */
public class GeneralPostResponse implements ResponseServicio {



    private static final Logger logger = LoggerFactory.getLogger( GeneralPostResponse.class );
    public String mensajeOK;
    public String mensajeNOK;
    private int resultado = -1;
    private int error;
    private String mensaje;

    public GeneralPostResponse(){
    }

    public GeneralPostResponse(int resultado) {
        this.resultado = resultado;
    }

    public GeneralPostResponse( String mensajeOK, String mensajeNOK  ){

        this.mensajeOK = mensajeOK;
        this.mensajeNOK = mensajeNOK;
    }


    @Override
    public String getStatusMsj() {
        if (resultado == 0){
            return mensajeOK;
        }else{
            return mensajeNOK;
        }
    }

    public String getMensajeOK() {
        return mensajeOK;
    }

    public void setMensajeOK(String mensajeOK) {
        this.mensajeOK = mensajeOK;
    }

    public String getMensajeNOK() {
        return mensajeNOK;
    }

    public void setMensajeNOK(String mensajeNOK) {
        this.mensajeNOK = mensajeNOK;
    }

    @Override
    public boolean isOk() {
        return resultado == 0;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }











}
