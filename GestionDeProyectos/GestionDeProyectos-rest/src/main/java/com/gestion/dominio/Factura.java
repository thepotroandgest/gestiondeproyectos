package com.gestion.dominio;

import java.util.ArrayList;

/**
 * Created by Gustavo on 20-02-2017.
 */
public class Factura {

    //Atributos
    private Long idFactura;
    private Integer numeroFactura;
    private String fechaEmision;
    private String glosa;
    private String estado;
    private Double montoUF;
    private String idProyecto;
    private ArrayList<AbonoHito> abonosHitos;


    //Getter and Setters

    public Long getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Long idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(Integer numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Double getMontoUF() {
        return montoUF;
    }

    public void setMontoUF(Double montoUF) {
        this.montoUF = montoUF;
    }

    public ArrayList<AbonoHito> getAbonosHitos() {
        return abonosHitos;
    }

    public String getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(String idProyecto) {
        this.idProyecto = idProyecto;
    }

    public void setAbonosHitos(ArrayList<AbonoHito> abonosHitos) {
        this.abonosHitos = abonosHitos;
    }

    @Override
    public String toString() {
        return "Factura{" +
                "idFactura=" + idFactura +
                ", numeroFactura=" + numeroFactura +
                ", fechaEmision='" + fechaEmision + '\'' +
                ", glosa='" + glosa + '\'' +
                ", estado='" + estado + '\'' +
                ", montoUF=" + montoUF +
                ", idProyecto='" + idProyecto + '\'' +
                ", abonosHitos=" + abonosHitos +
                '}';
    }
}
