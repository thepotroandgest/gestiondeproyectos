package com.gestion.dominio.response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User: DZC - Seiza SpA.
 * Date: NOV-2016
 * Project: Participe API REST
 */
public class TareaPostResponse implements ResponseServicio {

  private static final Logger logger = LoggerFactory.getLogger(TareaPostResponse.class);
  private static final String MENSAJEOK = "Tarea grabada exitosamente";
  private static final String MENSAJENOK = "Error al grabar tarea";
  private int resultado = -1;
  private int error;
  private String mensaje;

  public TareaPostResponse(){
  }

  public TareaPostResponse(int resultado) {
    this.resultado = resultado;
  }

  @Override
  public String getStatusMsj() {
    if (resultado == 0){
      return MENSAJEOK;
    }else{
      return MENSAJENOK;
    }
  }

  @Override
  public boolean isOk() {
    return resultado == 0;
  }

  public int getError() {
    return error;
  }

  public void setError(int error) {
    this.error = error;
  }

  public String getMensaje() {
    return mensaje;
  }

  public void setMensaje(String mensaje) {
    this.mensaje = mensaje;
  }
}
