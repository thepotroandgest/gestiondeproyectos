package com.gestion.dominio;

import java.util.LinkedList;

/**
 * Created by Gustavo on 27-01-2017.
 */
public class Contraparte {

    //Atributos
    private String idContraparte;
    private String nombre;
    private String telefono;
    private String correo;
    private String fkRutCliente;

    //Constructor
    public Contraparte() {
    }


    public String getIdContraparte() {
        return idContraparte;
    }

    public void setIdContraparte(String idContraparte) {
        this.idContraparte = idContraparte;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getFkRutCliente() {
        return fkRutCliente;
    }

    public void setFkRutCliente(String fkRutCliente) {
        this.fkRutCliente = fkRutCliente;
    }

    @Override
    public String toString() {
        return "Contraparte{" +
                "idContraparte='" + idContraparte + '\'' +
                ", nombre='" + nombre + '\'' +
                ", telefono='" + telefono + '\'' +
                ", correo='" + correo + '\'' +
                ", fkRutCliente='" + fkRutCliente + '\'' +
                '}';
    }
}
