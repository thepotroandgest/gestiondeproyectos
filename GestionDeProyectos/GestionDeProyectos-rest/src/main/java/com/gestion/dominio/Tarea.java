package com.gestion.dominio;

import java.util.Date;

/**
 * Created by Gustavo on 21-02-2017.
 */
public class Tarea {
    //Atributos
    private Long sec_tarea;
    private Integer id_traspaso;
    private String descripcion;
    private Float esfuerzo_estimado;
    private Float esfuerzo_real;
    private Date fecha_inicio_estimado;
    private Date fecha_inicio_real;
    private Date fecha_termino_estimado;
    private Date fecha_termino_real;
    private Float porcentaje_estimado;
    private Float porcentaje_real;
    private String id_proyecto;
    private String rut_recurso;
    private String id_recurso;

    //Constructor
    public Tarea() {
    }

    //Constructor Builder

    //Constructor Builder
    private Tarea(Builder builder) {
        setSec_tarea(builder.sec_tarea);
        setId_traspaso(builder.id_traspaso);
        setDescripcion(builder.descripcion);
        setEsfuerzo_estimado(builder.esfuerzo_estimado);
        setEsfuerzo_real(builder.esfuerzo_real);
        setFecha_inicio_estimado(builder.fecha_inicio_estimado);
        setFecha_inicio_real(builder.fecha_inicio_real);
        setFecha_termino_estimado(builder.fecha_termino_estimado);
        setFecha_termino_real(builder.fecha_termino_real);
        setPorcentaje_estimado(builder.porcentaje_estimado);
        setPorcentaje_real(builder.porcentaje_real);
        setId_proyecto(builder.id_proyecto);
        setRut_recurso(builder.rut_recurso);
        setId_recurso(builder.id_recurso);
    }

    //Getters and Setters
    public Long getSec_tarea() {
        return sec_tarea;
    }
    public void setSec_tarea(Long sec_tarea) {
        this.sec_tarea = sec_tarea;
    }
    public Integer getId_traspaso() {return id_traspaso;}
    public void setId_traspaso(Integer id_traspaso) {this.id_traspaso = id_traspaso;}
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public Float getEsfuerzo_estimado() { return esfuerzo_estimado;}
    public void setEsfuerzo_estimado(Float esfuerzo_estimado) {this.esfuerzo_estimado = esfuerzo_estimado;}
    public Float getEsfuerzo_real() {
        return esfuerzo_real;
    }
    public void setEsfuerzo_real(Float esfuerzo_real) {
        this.esfuerzo_real = esfuerzo_real;
    }
    public Date getFecha_inicio_estimado() {
        return fecha_inicio_estimado;
    }
    public void setFecha_inicio_estimado(Date fecha_inicio_estimado) {
        this.fecha_inicio_estimado = fecha_inicio_estimado;
    }
    public Date getFecha_inicio_real() {
        return fecha_inicio_real;
    }
    public void setFecha_inicio_real(Date fecha_inicio_real) {
        this.fecha_inicio_real = fecha_inicio_real;
    }
    public Date getFecha_termino_estimado() {
        return fecha_termino_estimado;
    }
    public void setFecha_termino_estimado(Date fecha_termino_estimado) {
        this.fecha_termino_estimado = fecha_termino_estimado;
    }
    public Date getFecha_termino_real() {
        return fecha_termino_real;
    }
    public void setFecha_termino_real(Date fecha_termino_real) {
        this.fecha_termino_real = fecha_termino_real;
    }
    public Float getPorcentaje_estimado() {
        return porcentaje_estimado;
    }
    public void setPorcentaje_estimado(Float porcentaje_estimado) {
        this.porcentaje_estimado = porcentaje_estimado;
    }
    public Float getPorcentaje_real() {
        return porcentaje_real;
    }
    public void setPorcentaje_real(Float porcentaje_real) {
        this.porcentaje_real = porcentaje_real;
    }
    public String getId_proyecto(){return id_proyecto;}
    public void setId_proyecto(String id_proyecto){this.id_proyecto = id_proyecto;}
    public String getRut_recurso(){return rut_recurso;}
    public void setRut_recurso(String rut_recurso){this.rut_recurso = rut_recurso;}
    public String getId_recurso(){return id_recurso;}
    public void setId_recurso(String id_recurso){this.id_recurso = id_recurso;}
    //Builder
    public static final class Builder {
        private Long sec_tarea;
        private Integer id_traspaso;
        private String descripcion;
        private Float esfuerzo_estimado;
        private Float esfuerzo_real;
        private Date fecha_inicio_estimado;
        private Date fecha_inicio_real;
        private Date fecha_termino_estimado;
        private Date fecha_termino_real;
        private Float porcentaje_estimado;
        private Float porcentaje_real;
        private String id_proyecto;
        private String rut_recurso;
        private String id_recurso;

        public Builder() {
        }
        public Builder setFecha_Inicio_RealNULL(){
            this.fecha_inicio_real = null;
            return this;
        }
        public Builder setFecha_termino_RealNULL(){
            this.fecha_termino_real = null;
            return this;
        }
        public Builder setSec_Tarea(Long sec_tarea) {
            this.sec_tarea = sec_tarea;
            return this;
        }
        public Builder setId_traspaso(Integer id_traspaso){
            this.id_traspaso = id_traspaso;
            return this;
        }
        public Builder setDescripcion(String descripcion) {
            this.descripcion = descripcion;
            return this;
        }
        public Builder setEsfuerzo_Estimado(Float esfuerzo_estimado) {
            this.esfuerzo_estimado = esfuerzo_estimado;
            return this;
        }
        public Builder setEsfuerzo_Real(Float esfuerzo_real) {
            this.esfuerzo_real = esfuerzo_real;
            return this;
        }
        public Builder setFecha_Inicio_Estimado(Date fecha_inicio_estimado) {
            this.fecha_inicio_estimado = fecha_inicio_estimado;
            return this;
        }
        public Builder setFecha_Inicio_Real(Date fecha_inicio_real) {
            this.fecha_inicio_real = fecha_inicio_real;
            return this;
        }
        public Builder setFecha_Termino_Estimado(Date fecha_termino_estimado) {
            this.fecha_termino_estimado = fecha_termino_estimado;
            return this;
        }
        public Builder setFecha_Termino_Real(Date fecha_termino_real) {
            this.fecha_termino_real = fecha_termino_real;
            return this;
        }
        public Builder setPorcentaje_Estimado(Float porcentaje_estimado) {
            this.porcentaje_estimado = porcentaje_estimado;
            return this;
        }
        public Builder setPorcentaje_Real(Float porcentaje_real) {
            this.porcentaje_real = porcentaje_real;
            return this;
        }

        public Builder setId_proyecto(String id_proyecto){
            this.id_proyecto = id_proyecto;
            return this;
        }

        public Builder setRut_recurso(String rut_recurso){
            this.rut_recurso = rut_recurso;
            return this;
        }

        public Builder setId_recurso(String id_recurso){
            this.id_recurso = id_recurso;
            return this;
        }

        public Tarea build() {
            return new Tarea(this);
        }
    }

    @Override
    public String toString() {
        return "Tarea{" +
                "  id_traspaso=" + id_traspaso +
                ", descripcion='" + descripcion + '\'' +
                ", esfuerzo_estimado=" + esfuerzo_estimado +
                ", esfuerzo_real=" + esfuerzo_real +
                ", fecha_inicio_estimado=" + fecha_inicio_estimado +
                ", fecha_inicio_real=" + fecha_inicio_real +
                ", fecha_termino_estimado=" + fecha_termino_estimado +
                ", fecha_termino_real=" + fecha_termino_real +
                ", porcentaje_estimado=" + porcentaje_estimado +
                ", porcentaje_real=" + porcentaje_real +
                ", id_proyecto='" + id_proyecto + '\'' +
                ", rut_recurso='" + rut_recurso + '\'' +
                ", id_recurso='" + id_recurso + '\'' +
                '}';
    }
}
