package com.gestion.dominio;

/**
 * Created by cvera on 16-06-17.
 */
public class Avance{

    private Long id;
    private Double avanceEstimado;
    private Double avanceReal;
    private String fecha;
    private Double esfuerzoReal;
    private String fkIdProyecto;
    private String observacion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAvanceEstimado() {
        return avanceEstimado;
    }

    public void setAvanceEstimado(Double avanceEstimado) {
        this.avanceEstimado = avanceEstimado;
    }

    public Double getAvanceReal() {
        return avanceReal;
    }

    public void setAvanceReal(Double avanceReal) {
        this.avanceReal = avanceReal;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Double getEsfuerzoReal() {
        return esfuerzoReal;
    }

    public void setEsfuerzoReal(Double esfuerzoReal) {
        this.esfuerzoReal = esfuerzoReal;
    }

    public String getFkIdProyecto() {
        return fkIdProyecto;
    }

    public void setFkIdProyecto(String fkIdProyecto) {
        this.fkIdProyecto = fkIdProyecto;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @Override
    public String toString() {
        return "Avance{" +
                "id=" + id +
                ", avanceEstimado=" + avanceEstimado +
                ", avanceReal=" + avanceReal +
                ", fecha=" + fecha +
                ", esfuerzoReal=" + esfuerzoReal +
                ", fkIdProyecto='" + fkIdProyecto + '\'' +
                ", observacion='" + observacion + '\'' +
                '}';
    }
}
