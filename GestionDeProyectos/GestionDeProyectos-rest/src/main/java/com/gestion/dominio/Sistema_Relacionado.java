package com.gestion.dominio;

/**
 * Created by Gustavo on 27-01-2017.
 */
public class Sistema_Relacionado {

    //Atributos
    private String idSistemaRelacionado;
    private String nombre;


    public String getIdSistemaRelacionado() {
        return idSistemaRelacionado;
    }

    public void setIdSistemaRelacionado(String idSistemaRelacionado) {
        this.idSistemaRelacionado = idSistemaRelacionado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
