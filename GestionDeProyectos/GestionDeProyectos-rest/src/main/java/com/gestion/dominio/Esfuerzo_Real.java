package com.gestion.dominio;

import java.util.Date;

/**
 * Created by Gustavo on 21-02-2017.
 */
public class Esfuerzo_Real {
    //Atributos
    private Date fecha_inicio;
    private Date fecha_fin;
    private String usuario;
    private String cliente;
    private Double hora;
    private String proyecto;

    //Constructor
    public Esfuerzo_Real() {
    }

    //Constructor Builder
    private Esfuerzo_Real(Builder builder) {
        setFecha_inicio(builder.fecha_inicio);
        setFecha_fin(builder.fecha_fin);
        setUsuario(builder.usuario);
        setCliente(builder.cliente);
        setHora(builder.hora);
        setProyecto(builder.proyecto);
    }

    //Getters and setters
    public Date getFecha_inicio() {
        return fecha_inicio;
    }
    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }
    public Date getFecha_fin() {
        return fecha_fin;
    }
    public void setFecha_fin(Date fecha_fin) {
        this.fecha_fin = fecha_fin;
    }
    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public String getCliente() {
        return cliente;
    }
    public void setCliente(String cliente) {
        this.cliente = cliente;
    }
    public Double getHora() {
        return hora;
    }
    public void setHora(Double hora) {
        this.hora = hora;
    }
    public String getProyecto() {
        return proyecto;
    }
    public void setProyecto(String proyecto) {
        this.proyecto = proyecto;
    }

    //Builder
    public static final class Builder {
        private Date fecha_inicio;
        private Date fecha_fin;
        private String usuario;
        private String cliente;
        private Double hora;
        private String proyecto;

        public Builder() {
        }

        public Builder setFecha_Inicio(Date fecha_inicio) {
            this.fecha_inicio = fecha_inicio;
            return this;
        }

        public Builder setFecha_Fin(Date fecha_fin) {
            this.fecha_fin = fecha_fin;
            return this;
        }

        public Builder setUsuario(String usuario) {
            this.usuario = usuario;
            return this;
        }

        public Builder setCliente(String cliente) {
            this.cliente = cliente;
            return this;
        }

        public Builder setHora(Double hora) {
            this.hora = hora;
            return this;
        }

        public Builder setProyecto(String proyecto) {
            this.proyecto = proyecto;
            return this;
        }

        public Esfuerzo_Real build() {
            return new Esfuerzo_Real(this);
        }
    }

    @Override
    public String toString() {
        return "Esfuerzo_Real{" +
                "fecha_inicio=" + fecha_inicio +
                ", fecha_fin=" + fecha_fin +
                ", usuario='" + usuario + '\'' +
                ", cliente='" + cliente + '\'' +
                ", hora=" + hora +
                ", proyecto='" + proyecto + '\'' +
                '}';
    }


}
