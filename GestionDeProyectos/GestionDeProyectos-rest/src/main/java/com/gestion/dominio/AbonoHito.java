package com.gestion.dominio;

/**
 * Created by cvera on 30-06-17.
 */
public class AbonoHito {

    private String fkIdProyecto;
    private String hitoComercial;
    private Double abono;

    public String getFkIdProyecto() {
        return fkIdProyecto;
    }

    public void setFkIdProyecto(String fkIdProyecto) {
        this.fkIdProyecto = fkIdProyecto;
    }


    public String getHitoComercial() {
        return hitoComercial;
    }

    public void setHitoComercial(String hitoComercial) {
        this.hitoComercial = hitoComercial;
    }

    public Double getAbono() {
        return abono;
    }

    public void setAbono(Double abono) {
        this.abono = abono;
    }

    @Override
    public String toString() {
        return "AbonoHito{" +
                "fkIdProyecto='" + fkIdProyecto + '\'' +
                ", hitoComercial='" + hitoComercial + '\'' +
                ", abono=" + abono +
                '}';
    }

}
