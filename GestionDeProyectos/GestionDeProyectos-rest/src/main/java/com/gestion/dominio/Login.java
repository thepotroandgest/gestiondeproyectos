package com.gestion.dominio;

/**
 * Created by Gustavo on 21-02-2017.
 */
public class Login {
    //Atributos
    private String rut;
    private String password;

    //Constructor
    public Login() {
    }

    //Constructor Builder
    private Login(Builder builder) {
        setRut(builder.rut);
        setPassword(builder.password);
    }

    //Getters and Setters
    public String getRut() {
        return rut;
    }
    public void setRut(String rut) {
        this.rut = rut;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    //Builder
    public static final class Builder {
        private String rut;
        private String password;

        public Builder() {
        }

        public Builder setRut(String rut) {
            this.rut = rut;
            return this;
        }

        public Builder setPassword(String password) {
            this.password = password;
            return this;
        }

        public Login build() {
            return new Login(this);
        }
    }
}
