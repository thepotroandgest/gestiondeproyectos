package com.gestion.dominio;

/**
 * Created by Gustavo on 21-02-2017.
 */
public class HitoComercial {
    //Atributos
    private Long secHito;
    private String hitoComercial;
    private Double porcentaje;
    private Double valorUf;
    private Double abono;
    private Double saldo;
    private String fkIdProyecto;



    public Long getSecHito() {
        return secHito;
    }

    public void setSecHito(Long secHito) {
        this.secHito = secHito;
    }

    public Double getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(Double porcentaje) {
        this.porcentaje = porcentaje;
    }

    public String getHitoComercial() {
        return hitoComercial;
    }

    public void setHitoComercial(String hitoComercial) {
        this.hitoComercial = hitoComercial;
    }

    public Double getValorUf() {
        return valorUf;
    }

    public void setValorUf(Double valorUf) {
        this.valorUf = valorUf;
    }

    public String getFkIdProyecto() {
        return fkIdProyecto;
    }

    public void setFkIdProyecto(String fkIdProyecto) {
        this.fkIdProyecto = fkIdProyecto;
    }

    public Double getAbono() {
        return abono;
    }

    public void setAbono(Double abono) {
        this.abono = abono;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return "HitoComercial{" +
                "secHito=" + secHito +
                ", hitoComercial='" + hitoComercial + '\'' +
                ", porcentaje=" + porcentaje +
                ", valorUf=" + valorUf +
                ", abono=" + abono +
                ", saldo=" + saldo +
                ", fkIdProyecto='" + fkIdProyecto + '\'' +
                '}';
    }
}

