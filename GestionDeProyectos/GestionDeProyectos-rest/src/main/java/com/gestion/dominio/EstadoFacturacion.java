package com.gestion.dominio;

/**
 * Created by cvera on 13-07-17.
 */
public class EstadoFacturacion {

    private Long secHito;
    private Integer numeroFactura;
    private String fkIdProyecto;
    private String estadoProyecto;
    private String nombreHito;
    private Double porcentaje;
    private Double valorUf;
    private Double abono;
    private Double saldo;
    private String estadoHito;
    private String fechaEmision;
    private String estadoFactura;
    private String fkIdContraparte;


    public Long getSecHito() {
        return secHito;
    }

    public void setSecHito(Long secHito) {
        this.secHito = secHito;
    }

    public Integer getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(Integer numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public String getFkIdProyecto() {
        return fkIdProyecto;
    }

    public void setFkIdProyecto(String fkIdProyecto) {
        this.fkIdProyecto = fkIdProyecto;
    }

    public String getEstadoProyecto() {
        return estadoProyecto;
    }

    public void setEstadoProyecto(String estadoProyecto) {
        this.estadoProyecto = estadoProyecto;
    }

    public String getNombreHito() {
        return nombreHito;
    }

    public void setNombreHito(String nombreHito) {
        this.nombreHito = nombreHito;
    }

    public Double getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(Double porcentaje) {
        this.porcentaje = porcentaje;
    }

    public Double getValorUf() {
        return valorUf;
    }

    public void setValorUf(Double valorUf) {
        this.valorUf = valorUf;
    }

    public Double getAbono() {
        return abono;
    }

    public void setAbono(Double abono) {
        this.abono = abono;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public String getEstadoHito() {
        return estadoHito;
    }

    public void setEstadoHito(String estadoHito) {
        this.estadoHito = estadoHito;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getEstadoFactura() {
        return estadoFactura;
    }

    public void setEstadoFactura(String estadoFactura) {
        this.estadoFactura = estadoFactura;
    }

    public String getFkIdContraparte() {
        return fkIdContraparte;
    }

    public void setFkIdContraparte(String fkIdContraparte) {
        this.fkIdContraparte = fkIdContraparte;
    }




    @Override
    public String toString() {
        return "EstadoFacturacion{" +
                "secHito=" + secHito +
                ", numeroFactura=" + numeroFactura +
                ", fkIdProyecto='" + fkIdProyecto + '\'' +
                ", estadoProyecto='" + estadoProyecto + '\'' +
                ", nombreHito='" + nombreHito + '\'' +
                ", porcentaje=" + porcentaje +
                ", valorUf=" + valorUf +
                ", abono=" + abono +
                ", saldo=" + saldo +
                ", estadoHito='" + estadoHito + '\'' +
                ", fechaEmision='" + fechaEmision + '\'' +
                ", estadoFactura='" + estadoFactura + '\'' +
                ", fkIdContraparte='" + fkIdContraparte + '\'' +
                '}';
    }





}
