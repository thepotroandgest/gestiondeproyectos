package com.gestion.dominio;

import java.util.Date;

/**
 * Created by Gustavo on 06-03-2017.
 */
public class Calendario {
    //Atributos
    private String title;
    private Date start;
    private Date end;

    //Constructor
    public Calendario() {
    }

    //Constructor Builder
    private Calendario(Builder builder){
        setTitle(builder.title);
        setStart(builder.start);
        setEnd(builder.end);
    }
    //Getters and Setters
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public Date getStart() {
        return start;
    }
    public void setStart(Date start) {
        this.start = start;
    }
    public Date getEnd() {
        return end;
    }
    public void setEnd(Date end) {
        this.end = end;
    }

    //Builder
    public static final class Builder{
        private String title;
        private Date start;
        private Date end;

        public Builder(){

        }
        public Builder setTitle(String title){
            this.title = title;
            return this;
        }
        public Builder setStart(Date start){
            this.start = start;
            return this;
        }
        public Builder setEnd(Date end){
            this.end = end;
            return this;
        }
        public Calendario build(){
            return new Calendario(this);
        }
    }
}
