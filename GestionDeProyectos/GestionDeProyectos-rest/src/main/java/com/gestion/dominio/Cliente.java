package com.gestion.dominio;

import java.util.LinkedList;

/**
 * Created by Gustavo on 27-01-2017.
 */
public class Cliente {
    //Atributos
    private String rutCliente ;
    private String nombre ;
    private String fono ;
    private String correo ;
    private String direccion ;

    //Constructor
    public Cliente() {
    }

    public String getRutCliente() {
        return rutCliente;
    }

    public void setRutCliente(String rutCliente) {
        this.rutCliente = rutCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFono() {
        return fono;
    }

    public void setFono(String fono) {
        this.fono = fono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "rutCliente='" + rutCliente + '\'' +
                ", nombre='" + nombre + '\'' +
                ", fono='" + fono + '\'' +
                ", correo='" + correo + '\'' +
                ", direccion='" + direccion + '\'' +
                '}';
    }
}
