package com.gestion.exceptions;

/**
 * Created by Jimmy Sarmiento on 16-07-2015.
 */
public class GestionDatabaseException extends SystemException {
    public GestionDatabaseException(String message, String codigo, Throwable cause) {
        super(message, codigo, cause);
    }
}