package com.gestion.exceptions;

/**
 * Created by Jimmy Sarmiento on 16-07-2015.
 */
public interface CodificableException {
    String getCodigo();
}
