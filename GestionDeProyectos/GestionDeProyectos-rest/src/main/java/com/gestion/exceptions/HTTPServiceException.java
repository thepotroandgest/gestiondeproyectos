package com.gestion.exceptions;

import org.springframework.http.HttpStatus;

/**
 * User: GRN - Seiza SpA.
 * Date: AGO-2016
 * Project: template api rest
 */
public class HTTPServiceException extends javax.xml.ws.ProtocolException {

  protected HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
  public HTTPServiceException() {
    super();
  }

  public HTTPServiceException(String message) {
    super(message);
  }

  public int getStatusCode() {
    return getHttpStatus().value();
  }

  public HttpStatus getHttpStatus() {
    return httpStatus;
  }

  public void setHttpStatus(HttpStatus httpStatus) {
    this.httpStatus = httpStatus;
  }
}
