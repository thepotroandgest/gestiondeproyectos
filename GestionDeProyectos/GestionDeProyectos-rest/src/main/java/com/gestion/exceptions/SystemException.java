package com.gestion.exceptions;

/**
 * Created by Jimmy Sarmiento on 16-07-2015.
 */
/**
 * Excepciones para reportar errores de sistema, como conexiones, disponibilidad de servicios.
 */
public class SystemException extends RuntimeException implements CodificableException {

    private final String codigo;

    public SystemException(String message, String codigo, Throwable cause) {
        super(message, cause);
        this.codigo = codigo;
    }

    @Override
    public String getCodigo() {
        return codigo;
    }

}
